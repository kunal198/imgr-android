package com.imgr.service;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScreenReceiver extends BroadcastReceiver {
 
    private boolean screenOff;
 
   // @SuppressLint("NewApi")
	@Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            screenOff = true;
            
            Log.e("offfffff","offfffff");
            
            ExitActivity.exitApplicationAnRemoveFromRecent(context);
            if(android.os.Build.VERSION.SDK_INT >= 21)
            {
            	//((Activity)context).finishAndRemoveTask();
            }
            else
            {
                //((Activity)context).finish();
            }
            
            //ExitActivity.exitApplication(context);
            /*ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
            if(am != null) {
                List<ActivityManager.AppTask> tasks = am.getAppTasks();
                if (tasks != null) {
                    tasks.get(0).setExcludeFromRecents(true);
                }
            }*/
            
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            screenOff = false;
            
            Log.e("onnnnnn","onnnnnn");
            
        }
        Intent i = new Intent(context, UpdateService.class);
        i.putExtra("screen_state", screenOff);
        context.startService(i);
    }
 
}