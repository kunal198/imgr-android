package com.imgr.chat.message;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.OfflineMessageManager;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Contacts.People;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.AppLifeCycle;
import com.imgr.chat.BaseConvert;
import com.imgr.chat.LocalNotificationSend;
import com.imgr.chat.R;
import com.imgr.chat.UILApplication;
import com.imgr.chat.adapter.ChatAdapter;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.BlockAndMeaageActivity;
import com.imgr.chat.contact.EditScreenImgr;
import com.imgr.chat.cropping.CropOption;
import com.imgr.chat.cropping.CropOptionAdapter;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.IMGR_Model;
import com.imgr.chat.setting.SeeSponsoredandPersonal;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;
import com.joooonho.SelectableRoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ChatScreen extends AppBaseActivity {
	String mString_name, mStringOnlyOneSelectPromoId = "0";
	TextView mTextViewTitle, mTextView_text_adv_changed;
	SharedPreferences sharedPreferences, mSharedPreferences;
	Editor editor;
	String mStringPhone, mStringusername, mStringPassword, mStringHeader,
			mStringFooter, mStringPersonalSetting;
	String s;
	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;
	String personal_api_time, personal_api_pushkey, personal_api_idtag,
			personal_jidto, personal_jidfrom;
	JSONObject personaljsonObj;
	JSONArray mArrayPersonal = null;
	static final int MIN_DISTANCE = 150;
	ConnectionConfiguration config;
	String testpromo_id;
	XMPPConnection connection;
	EditText input;
	Button send;
	StickyListHeadersListView lv;
	// ListView lv;
	TextView tv, sender, mTextViewBack, mTextViewChanged;
	String getfriend, getfrdsname;
	String JID, mString_Override;
	String username;
	String text;
	String delivery;
	String number;
	Message message;

	String origin;

	ArrayAdapter<String> adapter;
	ArrayList<String[]> messages = new ArrayList<String[]>();
	ArrayList<String> mChatStores = null;
	private Handler mHandler = new Handler();
	ChatAdapter chat_list_adapter;
	static String NamedeviceToken;
	static HashMap<String, String> mHashMap;
	ConnectionDetector mConnectionDetector;
	// int count_test = 0;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	String mStringBase64 = null;
	String profileBase64;
	String notification_on_or_off;
	int check_count = 0;
	String fromJID = null, toJID = null;
	String RECENTFROMJID = null, RECENTTOJID = null;
	private static final int CROP_FROM_CAMERA = 3;
	private Uri mImageCaptureUri;
	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	// private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	private static final int PICK_FROM_CAMERA = 1;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "ImgrChat";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;
	ImageView mImageViewProfile, mImageViewAdv_on, mImageViewAdv_off,
			mImageViewProTop, image_adv_show_OFF, mImageView_adv_delete_icon,
			mImageView_adv_image;
	SelectableRoundedImageView mImageViewPro;
	boolean adv_on_off;
	boolean sponsored_adv_on_off = true;
	boolean send_sponsored_adv_on_off = false;
	int[] imgIds = { R.drawable.first_promo, R.drawable.second_promo,
			R.drawable.third_promo, R.drawable.four_promo,
			R.drawable.five_promo, R.drawable.six_promo, R.drawable.seven_promo };

	String promo_image, promo_name, promoid;

	ArrayList<String> promo_images;
	ArrayList<String> promo_names;
	ArrayList<String> promoids;

	String receievedPromoId = "0";

	String is_sponsered_promo_on_off, is_personal_promo_on_off,
			is_autorotate_on_off, is_promo_on_off,
			user_is_sponsered_promo_on_off, user_is_personal_promo_on_off,
			user_is_autorotate_on_off, user_is_promo_on_off;

	boolean togglePromoTemp = true;

	boolean isTempPromo = true;
	boolean isOffPromo = true;

	boolean allowPromos = true;

	int lastPromoIndex = 0;

	ArrayList<String> mArrayList_ToJid;
	ArrayList<String> mArrayList_FromJid;
	ArrayList<String> mArrayList_Time;
	ArrayList<String> mArrayList_Message;
	ArrayList<String> mArrayList_uniqueid;
	ArrayList<String> mArrayList_receive;
	ArrayList<String> mArrayList_promo_not;

	ArrayList<String> mArrayList_imageurl;
	ArrayList<String> mArrayList_promo_uniqueid;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListpromoname;

	ArrayList<String> mArrayDATE;

	ArrayList<String> newmArrayList_imageurl;
	ArrayList<String> newmArrayList_promo_uniqueid;
	ArrayList<String> newmArrayList_promo_uniqueid_personal;
	ArrayList<String> newmArrayList_promo_uniqueid_sponsored;
	ArrayList<String> newmArrayListisdelete;
	ArrayList<String> newmArrayListisactive;
	ArrayList<String> newmArrayListpromoname;
	Roster roster;
	String messageSend;
	String inputTypetext, mStringPhoneid, mStringNochangename,
			mStringPromoOnOFF, mStringAutorotate;

	String message_id;

	ImageButton mImageButtonPlus;
	ImageLoader imageLoader;
	DisplayImageOptions options;
	// promo show or
	boolean onlyOnePromo_select = false;
	boolean auto_rotate_Promo_OFF = false;

	VCard card;
	FrameLayout mFrameLayout_adv_view_off, mFrameLayout_adv_view;

	SwipeDismissTouchListener swipeListener;

	ImageButton mImageButton;
	TextView mTextViewAdd;
	String mStringSponsored_BarChanged;

	// new Frame layout Image view Text view Image button
	FrameLayout mFrameLayout_adv_view_OFF_OFF;
	ImageView image_adv_on_OFF, image_OFF_adv_show_OFF, image_adv_show_OFF_OFF,
			image_adv_off_OFF;
	TextView text_changed_OFF;
	ImageButton img_add_new_OFF;
	ArrayList<String> mArrayListisenabled;
	ArrayList<String> mArrayListtype;

	// for push notification parameter
	String to_notification, from_notification, messgae_notification,
			fromjid_notification, tojid_notification, time_notification,
			timestamp_notification, date_notification,
			localdatabase_notification, messgaepacketid_notification,
			memberid_notification, contacyuniqueid_notification,
			receiveornot_notification, bubblecolor_notification,
			fontsize_notification, username_notification,
			profileimage_notification, recentfromjid_notification,
			recenttojid_notification, unique_id_notification_user,
			final_show_message, message_payload;

	String setPromo, setAutorotate, setSponsoredads, setpersonalads,
			setuniqueid;

	AlertDialog dialog;

	private Timer myTimer;
	boolean isCallForChating = true;

	// PacketListener packetListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chatscreen);

		Application application = (Application) UILApplication.context;

		application.registerActivityLifecycleCallbacks(new AppLifeCycle());

		// try {
		swipeListener = new SwipeDismissTouchListener(this);
		activity = this;
		mConnectionDetector = new ConnectionDetector(this);
		config = new ConnectionConfiguration("64.235.48.26", 5222);
		connection = new XMPPConnection(config);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setReconnectionAllowed(true);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		config.setSecurityMode(SecurityMode.enabled);
		configure(ProviderManager.getInstance());
		config.setDebuggerEnabled(true);
		connection.DEBUG_ENABLED = true;
		config.setReconnectionAllowed(true);
		mFrameLayout_adv_view = (FrameLayout) findViewById(R.id.adv_view);
		mFrameLayout_adv_view_off = (FrameLayout) findViewById(R.id.adv_view_off);

		try {
			// handleOfflineMessages(connection, getApplicationContext());
			// handleOfflineMessages(connection);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			Log.e("offline messages failed", "offline messages faileddddddddd");

		}

		connection.addConnectionListener(new ConnectionListener() {

			@Override
			public void reconnectionSuccessful() {

				LogMessage.showDialog(ChatScreen.this, null,
						"Successfully reconnected to the XMPP server", null,
						"Ok");

				Log.i("Successfully reconnected to the XMPP server.",
						"Successfully reconnected to the XMPP server.");

			}

			@Override
			public void reconnectionFailed(Exception arg0) {

				LogMessage.showDialog(ChatScreen.this, null,
						"Failed to reconnect to the XMPP server.", null, "Ok");

				Log.i("Failed to reconnect to the XMPP server.",
						"Failed to reconnect to the XMPP server.");
			}

			@Override
			public void reconnectingIn(int seconds) {

				LogMessage.showDialog(ChatScreen.this, null, "Reconnecting in "
						+ seconds + " seconds.", null, "Ok");

				Log.i("Reconnecting in " + seconds + " seconds.",
						"Reconnecting in " + seconds + " seconds.");
			}

			@Override
			public void connectionClosedOnError(Exception arg0) {

				LogMessage.showDialog(ChatScreen.this, null,
						"Connection to XMPP server was lost", null, "Ok");

				Log.i("Connection to XMPP server was lost.",
						"Connection to XMPP server was lost.");
			}

			@Override
			public void connectionClosed() {

				LogMessage.showDialog(ChatScreen.this, null,
						"XMPP connection was closed.", null, "Ok");

				Log.i("XMPP connection was closed.",
						"XMPP connection was closed.");

			}
		});

		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mImageViewAdv_on = (ImageView) findViewById(R.id.image_adv_on);
		mImageViewAdv_off = (ImageView) findViewById(R.id.image_adv_off);
		mImageViewProfile = (ImageView) findViewById(R.id.send_image);
		mImageViewPro = (SelectableRoundedImageView) findViewById(R.id.image_pro);
		mImageViewProTop = (ImageView) findViewById(R.id.image_adv_show);
		image_adv_show_OFF = (ImageView) findViewById(R.id.image_adv_show_OFF);
		mImageButtonPlus = (ImageButton) findViewById(R.id.img_add_new);
		mTextViewTitle = (TextView) findViewById(R.id.header_title);
		mTextViewChanged = (TextView) findViewById(R.id.text_changed);
		mTextView_text_adv_changed = (TextView) findViewById(R.id.text_adv_changed);
		mImageView_adv_delete_icon = (ImageView) findViewById(R.id.adv_delete_icon);
		mImageView_adv_image = (ImageView) findViewById(R.id.adv_image);
		// new change code=====
		mFrameLayout_adv_view_OFF_OFF = (FrameLayout) findViewById(R.id.adv_view_OFF_OFF);
		image_adv_on_OFF = (ImageView) findViewById(R.id.image_adv_on_OFF);
		image_OFF_adv_show_OFF = (ImageView) findViewById(R.id.image_OFF_adv_show_OFF);
		image_adv_show_OFF_OFF = (ImageView) findViewById(R.id.image_adv_show_OFF_OFF);
		image_adv_off_OFF = (ImageView) findViewById(R.id.image_adv_off_OFF);
		text_changed_OFF = (TextView) findViewById(R.id.text_changed_OFF);
		img_add_new_OFF = (ImageButton) findViewById(R.id.img_add_new_OFF);
		mImageButton = (ImageButton) findViewById(R.id.img_speak);
		mTextViewAdd = (TextView) findViewById(R.id.add_plus);
		// backButton = (FrameLayout) findViewById(R.id.btn_back_loginpage);
		// closed

		mArrayList_imageurl = new ArrayList<String>();
		mArrayDATE = new ArrayList<String>();
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		// Editor editor = sharedPreferences.edit();
		// editor.putInt(Constant.DIMPAL, 0);
		// editor.commit();

		Intent intent = getIntent();
		getfriend = intent.getStringExtra("Chat_name_select");

	
		number = intent.getStringExtra("Chat_no_select");
		if (number.length() >= 10) {
			number = number.substring(number.length() - 10);
		} else {
			// number=number;
		}

		Log.e("GetFriend",""+getfriend+"--"+number);
		
		if (getfriend.equalsIgnoreCase("No Name")
				|| getfriend.equalsIgnoreCase("No")
				|| getfriend.equalsIgnoreCase("")) {
			getfriend = number;
		}

		is_sponsered_promo_on_off = "true";
		is_personal_promo_on_off = "true";
		is_autorotate_on_off = "true";
		is_promo_on_off = "true";

		user_is_sponsered_promo_on_off = "true";
		user_is_personal_promo_on_off = "true";
		user_is_autorotate_on_off = "true";
		user_is_promo_on_off = "true";

		mSharedPreferences = getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		sharedPrefernces();

		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");

		mStringPromoOnOFF = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "");

		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);

		String device_id = sharedPreferences.getString("DeviceId", "");

		fromJID = mStringPassword + "_" + device_id;

		toJID = "";

		Cursor mJidCursor = mDatasourceHandler.FETCH_(number);

		Log.e("mJidCursor", "" + mJidCursor.getCount());
		Log.e("number", "" + number);

		if (mJidCursor.getCount() != 0) {
			if (mJidCursor.moveToFirst()) {
				toJID = mJidCursor.getString(4);
				Log.e("Cursor KEys",""+mJidCursor.getString(mJidCursor.getColumnIndex("_jidcontacts")));
				
				Log.e("toJID11", "" + toJID);

				/*
				 * Log.e("ChatHistoryDetailsssssssssssssssssss", "" +
				 * mJidCursor.getString(0).trim() + ", " +
				 * mJidCursor.getString(1).trim() + ", " +
				 * mJidCursor.getString(2).trim() + ", " +
				 * mJidCursor.getString(3).trim() + ", " +
				 * mJidCursor.getString(4).trim() + ", " +
				 * mJidCursor.getString(5).trim() + ", " +
				 * mJidCursor.getString(6).trim() + ", " +
				 * mJidCursor.getString(7).trim() + ", " +
				 * mJidCursor.getString(8).trim() + ", " +
				 * mJidCursor.getString(9).trim());
				 */
			}
		}

		if (toJID == null) {

			LogMessage
					.showDialog(
							ChatScreen.this,
							"Invalid Contact",
							"This is not a valid contact. Please refresh the contact list and then try again.",
							null, "OK");

			// LogMessage.dismiss();
			// finish();
		} else {
			if (toJID.equals("")) {
				Cursor mRecentJidCursor = mDatasourceHandler
						.FETCH_RECENT_FOR_UNKNOWN(number);

				if (mRecentJidCursor.getCount() != 0) {
					if (mRecentJidCursor.moveToFirst()) {
						if (!mRecentJidCursor.getString(2).trim().split("_")[0]
								.equalsIgnoreCase(mStringPassword)) {
							toJID = mRecentJidCursor.getString(2);
						} else {
							toJID = mRecentJidCursor.getString(3);
						}
					}
				}
			}

			if (toJID == null || toJID.equalsIgnoreCase("")) {

				Log.e("toJID22", "" + toJID);
				LogMessage
						.showDialog(
								ChatScreen.this,
								"Invalid Contact",
								"This is not a valid contact. Please refresh the contact list and then try again.",
								null, "OK");

				// LogMessage.dismiss();
				// finish();
			} else {
				setPromo = "true";

				setSponsoredads = "true";

				setpersonalads = "true";

				mStringAutorotate = sharedPreferences.getString(
						Constant.SETTING_PROMO_AUTO_ROTATE, "true");
				mStringSponsored_BarChanged = sharedPreferences.getString(
						Constant.SPONSORED_ADS, "");
				notification_on_or_off = sharedPreferences.getString(
						Constant.SETTING_NOTIFICATION_ON_OFF, "true");

				// Log.e("promosettingssss", mStringPromoOnOFF + ", " +
				// mStringAutorotate
				// + ", ");

				mTextViewChanged.setOnTouchListener(swipeListener);
				mImageViewProTop.setOnTouchListener(swipeListener);
				text_changed_OFF.setOnTouchListener(swipeListener);
				image_OFF_adv_show_OFF.setOnTouchListener(swipeListener);

				mString_name = getIntent().getStringExtra("Chat_name_select");

				mStringNochangename = getIntent().getStringExtra(
						"Chat_name_select");
				Log.e("chat screen nameeeeeeeeeee", "" + mString_name);
				mTextViewTitle.setText(mString_name);
				mTextViewBack = (TextView) findViewById(R.id.btn_back_loginpage);

				origin = getIntent().getStringExtra("origin");

				mTextViewBack.setText(origin);

				promo_images = new ArrayList<String>();
				promo_names = new ArrayList<String>();
				promoids = new ArrayList<String>();

				promo_images.clear();
				promo_names.clear();
				promoids.clear();

				mChatStores = new ArrayList<String>();
				mArrayList_promo_uniqueid = new ArrayList<String>();
				mArrayList_ToJid = new ArrayList<String>();
				mArrayList_FromJid = new ArrayList<String>();
				mArrayList_Time = new ArrayList<String>();
				mArrayList_Message = new ArrayList<String>();
				mArrayList_uniqueid = new ArrayList<String>();
				mArrayList_promo_not = new ArrayList<String>();
				mArrayList_receive = new ArrayList<String>();
				mArrayListisdelete = new ArrayList<String>();
				mArrayListisactive = new ArrayList<String>();
				mArrayListpromoname = new ArrayList<String>();
				newmArrayListisdelete = new ArrayList<String>();
				newmArrayListisactive = new ArrayList<String>();
				newmArrayList_promo_uniqueid = new ArrayList<String>();
				newmArrayList_promo_uniqueid_personal = new ArrayList<String>();
				newmArrayList_promo_uniqueid_sponsored = new ArrayList<String>();
				mArrayListtype = new ArrayList<String>();
				newmArrayList_imageurl = new ArrayList<String>();
				newmArrayListpromoname = new ArrayList<String>();
				mArrayListisenabled = new ArrayList<String>();
				if (mArrayList_uniqueid.size() != 0
						|| mArrayList_FromJid.size() != 0
						|| mArrayList_Time.size() != 0
						|| mArrayList_Message.size() != 0
						|| mArrayList_ToJid.size() != 0) {
					mArrayList_uniqueid.clear();
					mArrayList_ToJid.clear();
					mArrayList_FromJid.clear();
					mArrayList_Time.clear();
					mArrayList_Message.clear();
					mArrayList_promo_not.clear();
					mArrayList_receive.clear();
					mArrayListisdelete.clear();
					mArrayList_promo_uniqueid.clear();
					mArrayListisactive.clear();
					newmArrayList_promo_uniqueid_personal.clear();
					newmArrayList_promo_uniqueid_sponsored.clear();
					newmArrayListisdelete.clear();
					newmArrayListisactive.clear();
					newmArrayList_promo_uniqueid.clear();
					newmArrayList_imageurl.clear();
					mArrayListpromoname.clear();
					mArrayListtype.clear();

					newmArrayListpromoname.clear();
				}

				mHashMap = new HashMap<String, String>();

				// promo fetch from local database
				mArrayListisenabled.clear();

				imageLoader = ImageLoader.getInstance();
				options = new DisplayImageOptions.Builder()
						.showStubImage(R.drawable.ic_launcher)
						.showImageForEmptyUri(R.drawable.ic_launcher)
						.cacheInMemory().cacheOnDisc().build();

				// imageLoader.displayImage(listFlag.get(position),
				// view.imgViewFlag);

				// /mImageViewProTop.setBackgroundResource(promo_image);

				mImageViewPro.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						Cursor mCursorClick = mDatasourceHandler.FETCH_(number);

						if (mCursorClick.getCount() != 0) {

							Log.e("", "CLICK HERE");
							Intent mIntent_info = new Intent(ChatScreen.this,
									BlockAndMeaageActivity.class);
							mIntent_info.putExtra("name_select",
									mStringNochangename);
							mIntent_info.putExtra("phone_id", mStringPhoneid);
							mIntent_info.putExtra("phoneno_select", number);
							// Log.e("number------------------", ""+number);
							startActivity(mIntent_info);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_right);
						} else {

							// TODO Auto-generated method stub

							AlertDialog.Builder dialog = new AlertDialog.Builder(
									ChatScreen.this);
							dialog.setMessage(
									"No contact found in the address book to display details."
											+ "Do you want to add this in your address book?")
									.setPositiveButton(
											"Ok",
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {

													// Toast.makeText(UILApplication.context,
													// "Transfer canceled!",
													// Toast.LENGTH_LONG).show();

													Intent mIntent = new Intent(
															ChatScreen.this,
															EditScreenImgr.class);
													mIntent.putExtra("NAME", "");
													mIntent.putExtra("LAST", "");
													mIntent.putExtra(
															"Edit_phone_id",
															mStringPhoneid);
													mIntent.putExtra(
															"PHONE_NO", number);
													mIntent.putExtra("EMAIL",
															"");

													mIntent.putExtra("jid",
															toJID);

													startActivity(mIntent);
													overridePendingTransition(
															R.anim.slide_in_left,
															R.anim.slide_out_right);
												}
											})
									.setNegativeButton(
											"Cancel",
											new DialogInterface.OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.cancel();
												}
											}).show();
						}

						// }

					}
				});

				mImageViewAdv_on.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Log.e("adv_on_off: on ", "" + adv_on_off);

						adv_on_off = false;
						mImageViewProTop.setVisibility(View.GONE);
						mImageViewAdv_on.setVisibility(View.GONE);
						mImageViewAdv_off.setVisibility(View.VISIBLE);
						image_adv_show_OFF.setVisibility(View.VISIBLE);
						image_adv_show_OFF.setImageResource(R.drawable.ic_off);

						mTextViewChanged.setText("Off");
						mTextViewChanged.setTextColor(Color
								.parseColor("#B1B1B1"));

						togglePromoTemp = false;

						// sharedPrefernces(adv_on_off);

					}
				});
				mImageViewAdv_off.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// Log.e("adv_on_off: off ", "" + adv_on_off);

						resetTempOffPromoSettings();

						// sharedPrefernces(adv_on_off);

					}
				});
				mImageButtonPlus.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						Intent mIntent_info = new Intent(ChatScreen.this,
								SeeSponsoredandPersonal.class);
						mIntent_info.putExtra("phone_id", mStringPhoneid);
						startActivity(mIntent_info);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);

					}
				});
				mTextViewAdd.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent_info = new Intent(ChatScreen.this,
								SeeSponsoredandPersonal.class);
						mIntent_info.putExtra("phone_id", mStringPhoneid);
						startActivity(mIntent_info);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
					}
				});
				mImageButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent_info = new Intent(ChatScreen.this,
								SeeSponsoredandPersonal.class);
						mIntent_info.putExtra("phone_id", mStringPhoneid);
						startActivity(mIntent_info);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
					}
				});

				mTextViewBack.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						s = "TT";
						editor = mSharedPreferences.edit();
						editor.putInt(Constant.COUNT_NOTIFICATION, 0);
						editor.commit();

						overridePendingTransition(R.anim.slide_out_left,
								R.anim.slide_in_right);
						finish();
					}
				});

				// new code implement click event
				image_adv_on_OFF.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						sponsored_adv_on_off = false;
						// adv_on_off = false;
						send_sponsored_adv_on_off = false;
						image_OFF_adv_show_OFF.setVisibility(View.GONE);
						image_adv_on_OFF.setVisibility(View.GONE);
						image_adv_off_OFF.setVisibility(View.VISIBLE);
						image_adv_show_OFF_OFF.setVisibility(View.VISIBLE);

						image_adv_show_OFF_OFF
								.setImageResource(R.drawable.ic_off);

						text_changed_OFF.setText("Off");
						text_changed_OFF.setTextColor(Color
								.parseColor("#B1B1B1"));

					}
				});
				image_adv_off_OFF.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						sponsored_adv_on_off = true;
						// adv_on_off = true;
						send_sponsored_adv_on_off = false;
						image_adv_off_OFF.setVisibility(View.GONE);
						image_adv_show_OFF_OFF.setVisibility(View.GONE);

						image_OFF_adv_show_OFF.setVisibility(View.VISIBLE);
						// image_OFF_adv_show_OFF.setBackgroundResource(R.drawable.ic_off);
						image_adv_on_OFF.setVisibility(View.VISIBLE);
						text_changed_OFF.setVisibility(View.VISIBLE);
						// image_adv_show_OFF_OFF.set
						image_adv_show_OFF_OFF
								.setImageResource(R.color.transparent);
						text_changed_OFF.setText("");
						if (mStringSponsored_BarChanged.equals("true")
								&& setSponsoredads.equals("true")
								&& mStringPersonalSetting.equals("true")
								&& setpersonalads.equals("true")
								&& auto_rotate_Promo_OFF) {

							// image_adv_off_OFF.setBackgroundResource(R.drawable.ic_off);
							// image_adv_on_OFF.setBackgroundResource(R.drawable.ic);
							imageLoader.displayImage(promo_image,
									image_OFF_adv_show_OFF, options,
									new SimpleImageLoadingListener() {
										@Override
										public void onLoadingComplete(
												Bitmap loadedImage) {
											Animation anim = AnimationUtils
													.loadAnimation(activity,
															R.anim.fade_in);
											// imgViewFlag.setAnimation(anim);
											// anim.start();
										}
									});
							text_changed_OFF.setText(promo_name);
							text_changed_OFF.setTextColor(Color
									.parseColor("#000000"));

						} else {
							if (auto_rotate_Promo_OFF
									&& mStringPersonalSetting.equals("true")
									&& setpersonalads.equals("true")) {

								imageLoader.displayImage(promo_image,
										image_OFF_adv_show_OFF, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
								text_changed_OFF.setText(promo_name);
								text_changed_OFF.setTextColor(Color
										.parseColor("#000000"));

							} else {
								if (mStringPersonalSetting.equals("true")
										&& setpersonalads.equals("true")) {
									if (promoid != null) {
										if (promoid.equals("0")) {

										} else {

											imageLoader
													.displayImage(
															promo_image,
															image_OFF_adv_show_OFF,
															options,
															new SimpleImageLoadingListener() {
																@Override
																public void onLoadingComplete(
																		Bitmap loadedImage) {
																	Animation anim = AnimationUtils
																			.loadAnimation(
																					activity,
																					R.anim.fade_in);
																	// imgViewFlag.setAnimation(anim);
																	// anim.start();
																}
															});
											text_changed_OFF
													.setText(promo_name);
											text_changed_OFF.setTextColor(Color
													.parseColor("#000000"));
										}
									}
								} else {
									if (mStringSponsored_BarChanged
											.equals("true")
											&& setSponsoredads.equals("true")) {

										imageLoader
												.displayImage(
														promo_image,
														image_OFF_adv_show_OFF,
														options,
														new SimpleImageLoadingListener() {
															@Override
															public void onLoadingComplete(
																	Bitmap loadedImage) {
																Animation anim = AnimationUtils
																		.loadAnimation(
																				activity,
																				R.anim.fade_in);
																// imgViewFlag.setAnimation(anim);
																// anim.start();
															}
														});
										text_changed_OFF.setText(promo_name);
										text_changed_OFF.setTextColor(Color
												.parseColor("#000000"));
									}
								}

							}
						}

						// /

					}
				});
				img_add_new_OFF.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						Intent mIntent_info = new Intent(ChatScreen.this,
								SeeSponsoredandPersonal.class);
						mIntent_info.putExtra("phone_id", mStringPhoneid);
						startActivity(mIntent_info);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);
					}
				});

				if (mHashMap != null) {
					mHashMap.clear();
				}

				input = (EditText) findViewById(R.id.input);
				send = (Button) findViewById(R.id.send_clicked);
				lv = (StickyListHeadersListView) findViewById(R.id.listview1);
				lv.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

				Connection_Pool connection_Pool = Connection_Pool.getInstance();
				connection = connection_Pool.getConnection(activity);
				setConnection(connection);
				card = new VCard();

				send.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						if (isInternetAvailable(UILApplication.context)) {
							JID = NamedeviceToken;

							String incomingNumber = JID.split("_")[0];

							String isBlocked = "false";

							Cursor mBlockCursor = mDatasourceHandler
									.FETCH_(incomingNumber);

							if (mBlockCursor.getCount() != 0) {

								if (mBlockCursor.moveToFirst()) {
									isBlocked = mBlockCursor.getString(9);
								}
							}

//							handleOfflineMessages(connection);

							message_id = toJID + "_" + timestamp();

							Log.e("blockedddddddddddddd", isBlocked);

							if (!isBlocked.equalsIgnoreCase("true")) {

								if (connection != null) {
									if (connection.isConnected()) {

										sendPendingMessageIfAny();

									} else {

										// new login_Existing_User().execute();

										LogMessage
												.showDialog(
														ChatScreen.this,
														"Lost connection to IMGR servers",
														"Please wait while Imgr is retrieving connection.",
														null, "Ok");

									}

								} else {
									Log.i("XMPPChatDemoActivity",
											"no massage send");
									Log.e("XMPPChatDemoActivity",
											"Failed to log in as "
													+ connection.getUser());
								}

							} else {
								LogMessage
										.showDialog(
												ChatScreen.this,
												"Contact Blocked",
												"To send a message to blocked user, first unblock user.",
												null, "Ok");
							}

						} else {
							LogMessage.showDialog(ChatScreen.this,
									"Waiting for Newtwork",
									"Please check your internet connection.",
									null, "OK");

						}

					}
				});

				// Log.e("number: ", "" + number);
				mTextViewTitle.setText(getfriend.split("\\@")[0]);
				getfrdsname = mTextViewTitle.getText().toString();
				username_notification = mTextViewTitle.getText().toString();

				// xmpp work

				connectivity_Manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

				Connection.DEBUG_ENABLED = true;

				mStringPhone = sharedPreferences
						.getString(Constant.PHONENO, "");

				if (isInternetAvailable(UILApplication.context)) {
					new ChatHistory().execute();

				} else {
					fetchChatHistory();

					// startCheckingConnection();

					LogMessage.showDialog(ChatScreen.this, null,
							"No Internet Connection", null, "Ok");
				}

				mImageViewProfile.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (isInternetAvailable(UILApplication.context)) {
							selectImage();
						} else {
							LogMessage.showDialog(ChatScreen.this, null,
									"No Internet Connection", null, "Ok");
						}
					}
				});

				mImageView_adv_delete_icon
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								mImageView_adv_delete_icon
										.setVisibility(View.GONE);
								mTextView_text_adv_changed
										.setVisibility(View.GONE);
								mImageView_adv_image.setVisibility(View.GONE);
								Constant.SELECTED_PROMO_ID = "";
								isOffPromo = false;
								// mStringOnlyOneSelectPromoId = "0";

							}
						});
				mString_Override = sharedPreferences.getString(
						Constant.BUBBLE_SET_OVERRIDE_FIXED, "");
				// } catch (Exception ex) {
				// Log.e("", "CRASH ON CREATE");
				// /}

				// notification value

				from_notification = mStringPhone;
				to_notification = number;
			}

		}

		myTimer = new Timer();
		myTimer.schedule(new TimerTask() {
			@Override
			public void run() {

				Connection_Pool connection_Pool = Connection_Pool.getInstance();
				connection = connection_Pool.getConnection(activity);
				Log.e("tag", "connectionconnection :" + connection);

				if (connection.isConnected()) {
					if (isCallForChating == true) {
						setConnection(connection);
					}

				}
			}

		}, 0, 30000);
	}

	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
		finish();
	}

	// public static void handleOfflineMessages(XMPPConnection connection,
	// Context ctx)throws Exception {
//	public static void handleOfflineMessages(XMPPConnection connection) {
//
//		try {
//
//			ServiceDiscoveryManager manager = ServiceDiscoveryManager
//					.getInstanceFor(connection);
//			DiscoverInfo info = manager.discoverInfo(null,
//					"http://jabber.org/protocol/offline");
//			Form extendedInfo = Form.getFormFrom(info);
//			if (extendedInfo != null) {
//				String value = extendedInfo.getField("number_of_messages")
//						.getValues().next();
//
//				Log.e("number of messagessssssssssss inside", value);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//
//			Log.e("number of messagessssssssssss exception", e.getMessage());
//
//		}
//		// return 0;
//
//		Log.e("number of messagessssssssssss", "0");
//
//		/*
//		 * ServiceDiscoveryManager sdm = ServiceDiscoveryManager
//		 * .getInstanceFor(connection);
//		 * 
//		 * OfflineMessageManager offlineManager = new OfflineMessageManager(
//		 * connection); try { Iterator<org.jivesoftware.smack.packet.Message> it
//		 * = offlineManager .getMessages(); Log.e("offline11111111111111", "" +
//		 * offlineManager.supportsFlexibleRetrieval());
//		 * Log.e("offline22222222222222", "Number of offline messages:: " +
//		 * offlineManager.getMessageCount()); Map<String, ArrayList<Message>>
//		 * offlineMsgs = new HashMap<String, ArrayList<Message>>(); while
//		 * (it.hasNext()) { org.jivesoftware.smack.packet.Message message = it
//		 * .next(); Log.e("offline2222222222222",
//		 * "receive offline messages, the Received from [" + message.getFrom() +
//		 * "] the message:" + message.getBody()); String fromUser =
//		 * message.getFrom().split("/")[0];
//		 * 
//		 * if (offlineMsgs.containsKey(fromUser)) {
//		 * offlineMsgs.get(fromUser).add(message); } else { ArrayList<Message>
//		 * temp = new ArrayList<Message>(); temp.add(message);
//		 * offlineMsgs.put(fromUser, temp); } } // Deal with a collection of
//		 * offline messages ... Set<String> keys = offlineMsgs.keySet();
//		 * Iterator<String> offIt = keys.iterator(); while (offIt.hasNext()) {
//		 * String key = offIt.next(); ArrayList<Message> ms =
//		 * offlineMsgs.get(key);
//		 * 
//		 * } offlineManager.deleteMessages(); } catch (Exception e) {
//		 * e.printStackTrace(); }
//		 */
//		/*
//		 * OfflineMessageManager offlineMessageManager = new
//		 * OfflineMessageManager(connection);
//		 * 
//		 * if (!offlineMessageManager.supportsFlexibleRetrieval()) {
//		 * Log.e("offffflineeeeeeeeeeeeeeeee","Offline messages not supported");
//		 * return; }
//		 * 
//		 * if (offlineMessageManager.getMessageCount() == 0) {
//		 * Log.e("offffflineeeeeeeeeeeeeeeee"
//		 * ,"No offline messages found on server"); } else { List<Message> msgs
//		 * = (List<Message>) offlineMessageManager.getMessages(); for (Message
//		 * msg : msgs) { String fullJid = msg.getFrom(); String bareJid =
//		 * StringUtils.parseBareAddress(fullJid); String messageBody =
//		 * msg.getBody(); if (messageBody != null) {
//		 * Log.e("offffflineeeeeeeeeeeeeeeee","Retrieved offline message from "
//		 * +messageBody); } } offlineMessageManager.deleteMessages(); }
//		 */
//	}

	
	/* (non-Javadoc)
	 * @see android.app.Activity#onStart()
	 */
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
//	
//		Timer myTimer = new Timer();
//		myTimer.schedule(new TimerTask() {
//			@Override
//			public void run() {
//
//				try{
//					fetchChatHistory();
//					chat_list_adapter.notifyDataSetChanged();
//				}catch(Exception e){
//					e.printStackTrace();
//					Log.e("Chat Screen", "inside onStart"+e);
//				}
//			}
//
//		}, 0, 20000);
//		}catch(Exception e){
//			e.printStackTrace();
//			Log.e("insideOnstart ","inside onStart - "+e);
//		}
		
	}
	
	public void startCheckingConnection() {
		Handler handler = new Handler();

		final Runnable r = new Runnable() {
			public void run() {
				// tv.append("Hello World");

				String currentPhoneNo = mSharedPreferences.getString(
						"currentPhoneNo", "");

				if (!currentPhoneNo.equals("")) {
					retrieveConnection();
				}

				Handler handler = new Handler();

				handler.postDelayed(this, 500);
			}
		};

		//handler.postDelayed(r, 10);
	}

	public void retrieveConnection() {
		if (isInternetAvailable(UILApplication.context)) {
			// Log.e("threadddddddddddddddddd", "startCheckingConnection");

			if (connection == null || !connection.isConnected()) {

				// Log.e("threadddddddddddddddddd1111111111111111111",
				// "startCheckingConnection");

				new login_Existing_User().execute();
			}

		}
	}

	public void sendPendingMessageIfAny() {
		String text = String.valueOf(input.getText().toString());

		if (!text.equals("")
				|| (mStringBase64 != null && !mStringBase64.equals(""))) {
			if (mStringBase64 != null && !mStringBase64.equals("")) {
				final_show_message = "image";
				inputTypetext = "";

				JSONObject json = new JSONObject();

				try {
					json.put("server_URL", "Not Uploaded");
					json.put("pushkey", "image");
					json.put("preview_Icon", mStringBase64);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String messageData = json.toString();		
				
				message_payload = messageData;
				Log.e("Message Payload",""+message_payload);

				Message msg = new Message(JID, Message.Type.chat);
				msg.setBody(messageData);

				DeliveryReceiptManager.addDeliveryReceiptRequest(msg);

				connection.sendPacket(msg);

				String time = currenttime();
				String date_sent = date();

				String[] msg_Data = new String[7];
				msg_Data[0] = text;
				msg_Data[1] = "pic";
				msg_Data[2] = "";
				msg_Data[3] = mStringBase64;
				msg_Data[4] = time;
				msg_Data[6] = date_sent;

				if ((allowPromos && togglePromoTemp) || isTempPromo
						|| isOffPromo) {
					msg_Data[5] = "1";
				} else {
					msg_Data[5] = "0";
				}

				messages.add(msg_Data);

				if (chat_list_adapter.getCount() >= 5) {
					lv.setStackFromBottom(true);
				} else {
					lv.setStackFromBottom(false);
				}
				mArrayDATE.add(date());
				chat_list_adapter.notifyDataSetChanged();
				lv.setAdapter(chat_list_adapter);

				Log.e("jidsssssssssssssss", "" + toJID + ", " + fromJID);

				String messageDataToBeSaved = "{\"body\":[" + messageData
						+ "]}";

				mDatasourceHandler.insertChathistory(msg.getPacketID(),
						"username", toJID, fromJID, messageDataToBeSaved, time,
						"0", "0", date_sent, getApplicationContext(), number,
						"yes");
				String timestamp = timestamp();
				/*
				 * boolean flag = mDatasourceHandler .UpdateRecentReceive(toJID,
				 * fromJID, messageDataToBeSaved, time, date_sent, timestamp,
				 * "0", "yes", mStringPassword);
				 * 
				 * if(!flag) {
				 */
				mDatasourceHandler.insertrecentMessage(messageDataToBeSaved,
						fromJID, toJID, "", "", date_sent, time, timestamp,
						"0", "yes", mStringPassword);
				// }

				Log.v("text send", connection.getUser().toString());

				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {
					Presence entryPresence = roster
							.getPresence(entry.getUser());
					Presence.Type type = entryPresence.getType();
				}

				mStringBase64 = null;

//				new ExecuteChatNotification().execute();
			}

			if (!text.equals("")) {
				final_show_message = text;
				inputTypetext = input.getText().toString();

				String messageData = "";

				if ((allowPromos && togglePromoTemp) || isTempPromo
						|| isOffPromo) {
					messageData = "{\"preview_Icon\":\"null\",\"_promo_id_tag\":\""
							+ promoid
							+ "\",\"pushkey\":\""
							+ inputTypetext
							+ "\",\"message_id\":\""
							+ message_id
							+ "\""
							+ ",\"_message_body_tag\":\""
							+ inputTypetext
							+ "\"}";
				} else {
					messageData = "{\"preview_Icon\":\"null\",\"_promo_id_tag\":\"0\",\"pushkey\":\""
							+ inputTypetext
							+ "\",\"message_id\":\""
							+ message_id
							+ "\""
							+ ",\"_message_body_tag\":\""
							+ inputTypetext + "\"}";
				}

				message_payload = messageData;

				Message msg = new Message(JID, Message.Type.chat);
				msg.setBody(messageData);

				input.setText("");

				// Log.e("message_sentttttttttttttt",messageData);

				Log.e("message_sentttttttttttttt", messageData);

				DeliveryReceiptManager.addDeliveryReceiptRequest(msg);

				connection.sendPacket(msg);

				String time = currenttime();
				String date_sent = date();

				String[] msg_Data = new String[9];
				msg_Data[0] = text;
				msg_Data[1] = "my";
				msg_Data[2] = "0";
				msg_Data[3] = msg.getPacketID();
				msg_Data[4] = "" + promo_image;
				msg_Data[6] = time;

				msg_Data[5] = "0";

				if ((allowPromos && togglePromoTemp) || isTempPromo
						|| isOffPromo) {
					msg_Data[5] = "1";
					msg_Data[7] = promoid;
				}

				msg_Data[8] = date_sent;

				messages.add(msg_Data);

				if (chat_list_adapter.getCount() >= 5) {
					lv.setStackFromBottom(true);
				} else {
					lv.setStackFromBottom(false);
				}
				mArrayDATE.add(date());
				chat_list_adapter.notifyDataSetChanged();
				lv.setAdapter(chat_list_adapter);

				// messages.add(connection.getUser() + ":  "
				// + text);
				// messages.add(connection.getUser() + ":");
				// messages.add(text);

				Log.e("jidsssssssssssssss", "" + toJID + ", " + fromJID);
				String messageDataToBeSaved = "";

				if ((allowPromos && togglePromoTemp) || isTempPromo
						|| isOffPromo) {
					messageDataToBeSaved = "{\"body\":[{\"preview_Icon\":\"null\",\"_promo_id_tag\":\""
							+ promoid
							+ "\",\"pushkey\":\""
							+ inputTypetext
							+ "\","
							+ "\"_message_body_tag\":\""
							+ inputTypetext + "\"}]}";
				} else {
					messageDataToBeSaved = "{\"body\":[{\"preview_Icon\":\"null\",\"_promo_id_tag\":\"0\",\"pushkey\":\""
							+ inputTypetext
							+ "\","
							+ "\"_message_body_tag\":\""
							+ inputTypetext
							+ "\"}]}";
				}

				mDatasourceHandler.insertChathistory(msg.getPacketID(),
						"username", toJID, fromJID, messageDataToBeSaved, time,
						"0", "0", date_sent, getApplicationContext(), number,
						"yes");
				String timestamp = timestamp();
				/*
				 * boolean flag = mDatasourceHandler .UpdateRecentReceive(toJID,
				 * fromJID, messageDataToBeSaved, time, date_sent, timestamp,
				 * "0", "yes", mStringPassword);
				 */

				/*
				 * if(!flag) {
				 */
				mDatasourceHandler.insertrecentMessage(messageDataToBeSaved,
						fromJID, toJID, "", "", date_sent, time, timestamp,
						"0", "yes", mStringPassword);
				// }

				// Log.v("text send", connection.getUser().toString());
				// Toast.makeText(FirstPage.this,
				// "txt send", Toast.LENGTH_SHORT).show();
				// setListAdapter();
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();
				for (RosterEntry entry : entries) {
					Presence entryPresence = roster
							.getPresence(entry.getUser());
					Presence.Type type = entryPresence.getType();
				}

//				new ExecuteChatNotification().execute();

			} else {

			}

			new ExecuteChatNotification().execute();
			
			
			if (togglePromoTemp) {
				if (is_autorotate_on_off.equalsIgnoreCase("true")) {
					if (user_is_autorotate_on_off.equalsIgnoreCase("true")) {
						if (allowPromos) {
							if (lastPromoIndex == (promoids.size() - 1)) {
								lastPromoIndex = 0;
							} else {
								lastPromoIndex++;
							}
							mDatasourceHandler.updateLastPromoId(promoids
									.get(lastPromoIndex));
							promodata(promoids.get(lastPromoIndex));
							renderPromobar();
						}
					}
				}
			}

			isTempPromo = false;
			isOffPromo = false;

			mImageView_adv_delete_icon.setVisibility(View.GONE);
			mTextView_text_adv_changed.setVisibility(View.GONE);
			mImageView_adv_image.setVisibility(View.GONE);

			resetTempOffPromoSettings();

			mImageViewProfile.setImageResource(R.drawable.ic_camerachat);

			// mImageViewProfile.setBackgroundDrawable(R.drawable.ic_camerachat);

		}
	}

	public boolean isInternetAvailable(final Context context) {
		/*
		 * try { InetAddress ipAddr = InetAddress.getByName("google.com"); //You
		 * can replace it with your name
		 * 
		 * if (ipAddr.equals("")) { return false; } else { return true; }
		 * 
		 * } catch (Exception e) { return false; }
		 */

		/*
		 * ConnectivityManager connectivityManager = (ConnectivityManager)
		 * getSystemService(Context.CONNECTIVITY_SERVICE); NetworkInfo
		 * activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		 * return activeNetworkInfo != null && activeNetworkInfo.isConnected();
		 */

		final ConnectivityManager connectivityManager = ((ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE));
		return connectivityManager.getActiveNetworkInfo() != null
				&& connectivityManager.getActiveNetworkInfo().isConnected();

		/*
		 * ConnectivityManager cm = (ConnectivityManager)
		 * getSystemService(Context.CONNECTIVITY_SERVICE); return
		 * cm.getActiveNetworkInfo() != null &&
		 * cm.getActiveNetworkInfo().isConnectedOrConnecting();
		 */

	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public String historypromo_data(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = mCursor.getString(4).trim();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "";
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public String countcheck(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	public String count_check_update(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA_Update(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	private void addEntry(String userName) {
		try {

			Presence subscribe = new Presence(Presence.Type.available);
			subscribe.setTo(userName);
			connection.sendPacket(subscribe);
			// Log.e("subscribe", "" + subscribe);
			// Log.e("userName", "" + userName);
		} catch (Exception e) {
			// Log.e("tag", "unable to add contact: ", e);
		}

	}

	private void retrieveState_mode(Mode mode, boolean available, String string) {
		// TODO Auto-generated method stub
		int userState = 0;
		/** 0 for offline, 1 for online, 2 for away,3 for busy */
		if (mode == Mode.dnd) {
			// /Log.e("Busy", "Busy" + mode);
			userState = 3;
		} else if (mode == Mode.away || mode == Mode.xa) {
			// Log.e("away", "away");
			userState = 2;
		} else if (available) {
			// Log.e("online", "online" + mode);
			Toast.makeText(getApplicationContext(),
					getfriend.split("\\@")[0] + "is    Online", 5000).show();
			userState = 1;
		} else {
			Log.e("offline-----------------------------", "offline" + mode);

			// new ExecuteChatNotification().execute();

		}
	}

	private String getDate(long timeStampStr) {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {
			// Log.e("timeStampStr: ", "" + timeStampStr);

			Date date = new Date(timeStampStr * 1000); // *1000 is to convert

			SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

			sdf.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
															// reference for
															// formating (see
															// comment at the
															// bottom

			String format = sdf.format(date);
			// Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	// shuffle images on click
	public void shuffleArray(int[] ar) {
		// TODO Auto-generated method stub

		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;

		}

	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public class ChatHistory extends AsyncTask<String, Void, String> {
		public ChatHistory() {
			UI.showProgressDialog(activity);
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("ME mStringPhone: ", "" + mStringPhone);
			Log.e("used selected number: ", "" + number);
			String result = JsonParserConnector.ChatHistory("imgr",
					mStringPhone, number);
			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();

			// startCheckingConnection();

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();

			// startCheckingConnection();

			try {

				// startCheckingConnection();

				Log.e("result:------------onPostExecute--------- ", "" + result);
				Chat_Store(result);
			} catch (Exception e) {
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ChatScreen.this);
		}

	}

	public void Chat_Store(String strJson) {

		try {
			// /mArrayDATE = new ArrayList<String>();
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = null;
			try {
				jsonOBject = new JSONObject(strJson);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (jsonOBject.getBoolean("success")) {
					NamedeviceToken = jsonOBject.getString("devicetoken")
							+ Constant.XMPP_DNS_Name;

					// Log.e("NamedeviceToken: ", "" + NamedeviceToken);
					s = NamedeviceToken.replace(Constant.XMPP_DNS_Name, "");

					addEntry(NamedeviceToken);
					Roster roster = connection.getRoster();
					Presence availability = roster.getPresence(NamedeviceToken);

					Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.manual);

					// String jid=userName+Constants.ADVANTLPC__SMACK;
					try {
						roster.createEntry(NamedeviceToken, NamedeviceToken,
								null);
					} catch (XMPPException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					String mStringbubblecolor = null;
					if (mString_Override != null) {
						if (mString_Override.equals("true")) {
							mStringbubblecolor = "1";
							shared_Prefernces("blue");
						} else {
							if (jsonOBject.has("color")) {
								mStringbubblecolor = jsonOBject
										.getString("color");
							}
							if (mStringbubblecolor != null) {
								if (mStringbubblecolor.equals("1")) {
									shared_Prefernces("blue");
								} else if (mStringbubblecolor.equals("2")) {
									shared_Prefernces("green");
								} else if (mStringbubblecolor.equals("3")) {
									shared_Prefernces("purple");
								} else if (mStringbubblecolor.equals("0")) {
									shared_Prefernces("blue");
								}

							} else {
								shared_Prefernces("blue");
							}
						}

					} else {
						if (jsonOBject.has("color")) {
							mStringbubblecolor = jsonOBject.getString("color");
						}

						if (mStringbubblecolor != null) {
							if (mStringbubblecolor.equals("1")) {
								shared_Prefernces("blue");
							} else if (mStringbubblecolor.equals("2")) {
								shared_Prefernces("green");
							} else if (mStringbubblecolor.equals("3")) {
								shared_Prefernces("purple");
							} else if (mStringbubblecolor.equals("0")) {
								shared_Prefernces("blue");
							}

						} else {
							shared_Prefernces("blue");
						}
					}

					fetchChatHistory();

				} else {
					NamedeviceToken = jsonOBject.getString("devicetoken")
							+ Constant.XMPP_DNS_Name;
					fetchChatHistory();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) { // TODO: handle exception

			fetchChatHistory();
			/*
			 * LogMessage.showDialog(ChatScreen.this, "Waiting for Newtwork",
			 * "Please check your internet connection.", null, "OK");
			 */

			// Log.e("CRASh: ", "" + e.getMessage());
		}

	}

	public void fetchChatHistory() {
		try {
			// mDatasourceHandler.UpdateRecentReceiveCount(RECENTTOJID,
			// RECENTFROMJID);
			mDatasourceHandler.UpdateRecentReceiveCount(toJID, fromJID);
		} catch (Exception e) {
			// Log.e("UPDATE TIME:", "" + e.getMessage());
		}
		// database fetch working
		try {

			Log.e("here it goessssssssssssssss", "here it goessssssssssssssss");

			Log.e("----tojid--------------", "" + toJID + "," + fromJID);

			mCursor = mDatasourceHandler.FetchChatHistory(toJID, fromJID);

			if (mCursor.getCount() != 0) {
				mArrayList_uniqueid.clear();
				mArrayList_ToJid.clear();
				mArrayList_FromJid.clear();
				mArrayList_Time.clear();
				mArrayList_Message.clear();
				mArrayList_receive.clear();
				mArrayList_promo_not.clear();
				mArrayDATE.clear();
				do {
					mArrayList_uniqueid.add(mCursor.getString(0).trim());

					mArrayList_ToJid.add(mCursor.getString(2).trim());

					mArrayList_FromJid.add(mCursor.getString(3).trim());
					mArrayList_Message.add(mCursor.getString(4).trim());

					Log.e("msg----------------------", mCursor.getString(4)
							.trim()
							+ "____"
							+ mCursor.getString(2).trim()
							+ "____" + mCursor.getString(3).trim());

					mArrayList_Time.add(mCursor.getString(5).trim());

					mArrayList_receive.add(mCursor.getString(6).trim());
					mArrayDATE.add(mCursor.getString(9).trim());

					// mArrayList_promo_not.add(mCursor.getString(7).trim());
					// /Log.e("mCursor.getString(7).trim():", "" +
					// mCursor.getString(7).trim());

				} while (mCursor.moveToNext());

				mCursor.close();
			}

			chat_list_adapter = new ChatAdapter(ChatScreen.this, messages);

			if (mArrayList_uniqueid.size() == 0) {

			} else {
				Log.e("mesage_sizeeeeeeeee", "" + mArrayList_uniqueid.size());

				messages.clear();

				for (int i = 0; i < mArrayList_uniqueid.size(); i++) {

					/*
					 * if (NamedeviceToken.equals(mArrayList_FromJid .get(i) +
					 * Constant.XMPP_DNS_Name) ||
					 * NamedeviceToken.equals(mArrayList_ToJid .get(i) +
					 * Constant.XMPP_DNS_Name))
					 */

					if (toJID.equalsIgnoreCase(mArrayList_FromJid.get(i))
							|| toJID.equals(mArrayList_ToJid.get(i))) {
						String time = mArrayList_Time.get(i);
						String mStringReceive = mArrayList_receive.get(i);
						String date_sent = mArrayDATE.get(i);
						// String mStringpromo =
						// mArrayList_promo_not.get(i);
						// Log.e("mStringpromo: ", "" + mStringpromo);
						/*
						 * if (NamedeviceToken.equals(mArrayList_FromJid .get(i)
						 * + Constant.XMPP_DNS_Name))
						 */
						if (toJID.equalsIgnoreCase(mArrayList_FromJid.get(i))) {

							String mStringBody = mArrayList_Message.get(i);

							Log.e("image_messageeeeeeeeee", mStringBody);

							JSONObject jsonObj = new JSONObject(
									mArrayList_Message.get(i));

							JSONArray mJsonArray = null;

							String textmessage = "";

							JSONArray bodyArray = jsonObj.getJSONArray("body");

							JSONObject bodyJsonObj = new JSONObject(
									bodyArray.getString(0));

							if (bodyJsonObj.has("_message_body_tag")) {

								String[] msg_Data = new String[9];
								String promo_ID;
								if (bodyJsonObj.has("_promo_id_tag")) {
									promo_ID = bodyJsonObj
											.getString("_promo_id_tag");

								} else {
									promo_ID = "0";
								}

								String test;
								if (promo_ID.equals("0")) {
									test = "0";
								} else {
									test = "";
								}
								String path = historypromo_data(promo_ID);

								msg_Data[3] = bodyJsonObj
										.getString("_message_body_tag");
								msg_Data[1] = "friend";
								msg_Data[2] = "";
								msg_Data[0] = "";
								msg_Data[4] = time;
								msg_Data[5] = test;
								msg_Data[6] = path;
								msg_Data[7] = promo_ID;
								msg_Data[8] = date_sent;

								messages.add(msg_Data);

								String msgstring = "";
								for (int msg = 0; msg < 9; msg++) {
									msgstring += ", " + msg_Data[msg];
								}

								if (chat_list_adapter.getCount() >= 5) {
									lv.setStackFromBottom(true);
								} else {
									lv.setStackFromBottom(false);
								}
								/*
								 * chat_list_adapter .notifyDataSetChanged();
								 * lv.setAdapter(chat_list_adapter);
								 */
							} else {
								String[] msg_Data = new String[6];
								msg_Data[0] = "";
								msg_Data[1] = "friend_pic";
								msg_Data[2] = "";
								msg_Data[3] = bodyJsonObj
										.getString("preview_Icon");
								msg_Data[4] = time;
								msg_Data[5] = date_sent;
								messages.add(msg_Data);

								if (chat_list_adapter.getCount() >= 5) {
									lv.setStackFromBottom(true);
								} else {
									lv.setStackFromBottom(false);
								}
								/*
								 * chat_list_adapter .notifyDataSetChanged();
								 * lv.setAdapter(chat_list_adapter);
								 */
							}
						} else {
							// Log.e("", "more condition not matched");

							String mStringBody = mArrayList_Message.get(i);

							JSONObject jsonObj = new JSONObject(
									mArrayList_Message.get(i));

							JSONArray mJsonArray = null;

							Log.e("testmsg-----------------------: ", ""
									+ mArrayList_Message.get(i));

							String textmessage = "";

							JSONArray bodyArray = jsonObj.getJSONArray("body");

							JSONObject bodyJsonObj = new JSONObject(
									bodyArray.getString(0));

							if (bodyJsonObj.has("_message_body_tag")) {

								String promo_ID;
								if (bodyJsonObj.has("_promo_id_tag")) {
									promo_ID = bodyJsonObj
											.getString("_promo_id_tag");

								} else {
									promo_ID = "0";
								}
								String test;
								if (promo_ID.equals("0")) {
									test = "0";
								} else {
									test = "";
								}
								String path = historypromo_data(promo_ID);

								String[] msg_Data = new String[9];
								msg_Data[6] = time;
								msg_Data[5] = test;
								msg_Data[3] = "";
								msg_Data[1] = "my";
								msg_Data[2] = mStringReceive;
								msg_Data[0] = bodyJsonObj.getString("pushkey");
								msg_Data[4] = path;
								msg_Data[7] = promo_ID;
								msg_Data[8] = date_sent;

								messages.add(msg_Data);

								String msgstring = "";
								for (int msg = 0; msg < 9; msg++) {
									msgstring += ", " + msg_Data[msg];
								}

								// Log.e("", "HERE");
								if (chat_list_adapter.getCount() >= 5) {
									lv.setStackFromBottom(true);
								} else {
									lv.setStackFromBottom(false);
								}

								/*
								 * chat_list_adapter .notifyDataSetChanged();
								 * lv.setAdapter(chat_list_adapter);
								 */
								// Log.e("", "LAST HERE");
							} else {
								String[] msg_Data = new String[7];
								msg_Data[4] = time;
								msg_Data[0] = "";
								msg_Data[1] = "pic";
								msg_Data[2] = "";
								msg_Data[3] = bodyJsonObj
										.getString("preview_Icon");
								msg_Data[6] = date_sent;
								messages.add(msg_Data);

								if (chat_list_adapter.getCount() >= 5) {
									lv.setStackFromBottom(true);
								} else {
									lv.setStackFromBottom(false);
								}
								/*
								 * chat_list_adapter .notifyDataSetChanged();
								 * lv.setAdapter(chat_list_adapter);
								 */
							}
						}

					} else {

					}
				}
			}

			chat_list_adapter.notifyDataSetChanged();
			lv.setAdapter(chat_list_adapter);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		UILApplication.activityResumed();

		// NotificationManager notificationManager = (NotificationManager)
		// this.getSystemService(Context.NOTIFICATION_SERVICE);
		// notificationManager.cancelAll();
		editor = mSharedPreferences.edit();
		editor.putInt(Constant.COUNT_NOTIFICATION, 0);
		editor.putString("currentPhoneNo", number);

		editor.putString("currentScreen", "chat");

		editor.putString("Notification_Click_Message", "false");

		editor.commit();

		int value;
		fetchChatHistory();

//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		/*
		 * sharedPreferences = getActivity().getSharedPreferences(
		 * Constant.IMGRCHAT, Context.MODE_PRIVATE);
		 */

		// mCursor = mDatasourceHandler.Notifications_Fetch(to_message);

		arrangePromos();

		mDatasourceHandler.RecentRecentReceiveCount(toJID, fromJID);

		Cursor mCursor = mDatasourceHandler.Notifications_Fetch(number);

		if (mCursor.getCount() != 0) {

			do {

				int notificationId = Integer.parseInt(mCursor.getString(0)
						.trim());

				NotificationManager notificationManager = (NotificationManager) this
						.getSystemService(Context.NOTIFICATION_SERVICE);
				notificationManager.cancel(notificationId);
			} while (mCursor.moveToNext());

			int rows = mDatasourceHandler.Notifications_Delete(number);
			Log.e("no of rows deleted", "" + rows);
		}
		mCursor.close();

		value = mDatasourceHandler.Notifications_Count_Fetch();

		// mCursor.close();

		Log.e("DEMO", "Changing : " + value);

		/*
		 * PackageManager pm = this.getPackageManager();
		 * 
		 * String lastEnabled = "";
		 * 
		 * for (int i = 0; i <= 11; i++) { if (i <= 0) { lastEnabled =
		 * "com.imgr.chat.SplashActivity"; } else if (i > 10) { lastEnabled =
		 * "com.imgr.chat.a10p"; } else { lastEnabled = "com.imgr.chat.a" + i; }
		 * 
		 * Log.e("DEMO", "Removingggggggggggggggg : " + lastEnabled);
		 * 
		 * ComponentName componentName = new ComponentName("com.imgr.chat",
		 * lastEnabled); pm.setComponentEnabledSetting(componentName,
		 * PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
		 * PackageManager.DONT_KILL_APP); Log.e("DEMO",
		 * "Removingfffffffffffffff : " + lastEnabled); }
		 * 
		 * Log.e("DEMO", "Removing : " + lastEnabled);
		 * 
		 * if (value <= 0) { lastEnabled = "com.imgr.chat.SplashActivity"; }
		 * else if (value == 1) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 2) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 3) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 4) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 5) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 6) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 7) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 8) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 9) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 10) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value > 10) { lastEnabled = "com.imgr.chat.a10p"; } else {
		 * lastEnabled = "com.imgr.chat.SplashActivity"; }
		 * 
		 * ComponentName componentName = new ComponentName("com.imgr.chat",
		 * lastEnabled); pm.setComponentEnabledSetting(componentName,
		 * PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
		 * PackageManager.DONT_KILL_APP);
		 */

		// Log.e("DEMO", "Adding : " + lastEnabled);

		try {

			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection = connection_Pool.getConnection(activity);
			PingManager.getInstanceFor(connection);

			DeliveryReceiptManager delievery_Manager = DeliveryReceiptManager
					.getInstanceFor(connection);
			delievery_Manager.setAutoReceiptsEnabled(true);
			delievery_Manager.enableAutoReceipts();

			delievery_Manager
					.addReceiptReceivedListener(new ReceiptReceivedListener() {
						@Override
						public void onReceiptReceived(String fromJid,
								String toJid, String receiptId) {
							// Log.e("Chat_Screen message sent confirmation:",
							// fromJid
							// + ", " + toJid + ", " + receiptId);
							loop: for (String[] msg_Data : messages) {
								if (msg_Data[3].equals(receiptId)) {
									msg_Data[2] = "1";
									// Log.e("Pinging", "on Resumeflag:  "
									// + receiptId);
									boolean flag = mDatasourceHandler
											.UpdateReceive(receiptId);
									Log.e("Pingingggg", "on Resumeflag:  "
											+ flag);
									// update query implement--------------
									// Log.v("Delivery", "" + msg_Data[2]);
									mHandler.post(new Runnable() {
										public void run() {

											if (chat_list_adapter.getCount() >= 5) {
												lv.setStackFromBottom(true);
											} else {
												lv.setStackFromBottom(false);
											}
											chat_list_adapter
													.notifyDataSetChanged();

										}
									});
									break loop;
								}

							}

						}

					});
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			Log.e("exceptionn", "exceptionn");
			e1.printStackTrace();
		}
		try {

			String profileBase64 = "Null";
			String Imgrcontactname = "";
			String Imgrcontactnumber = "";
			String Imgrcontactlastname = "";
			String ImgrMemberId = "";
			String base64_string = "";
			String Imgrcontactunquieid = "";
			String isContactBlocked = "";

			try {

				mCursor = mDatasourceHandler.FetchContact(mStringPhoneid);

			} catch (Exception e) {

				e.printStackTrace();
			}

			if (mCursor.getCount() != 0) {

				do {
					profileBase64 = mCursor.getString(7).trim();
					Imgrcontactname = mCursor.getString(1).trim();
					Imgrcontactnumber = mCursor.getString(2).trim();
					Imgrcontactlastname = mCursor.getString(3);
					ImgrMemberId = mCursor.getString(5);
					base64_string = mCursor.getString(7).trim();
					// Log.e("", "HERE");
					Imgrcontactunquieid = mCursor.getString(8).trim();
					isContactBlocked = mCursor.getString(9).trim();

				} while (mCursor.moveToNext());

				mCursor.close();

				Log.e("profileBase64", "" + profileBase64);
			}

			IMGR_Model wp = new IMGR_Model(Imgrcontactname,
					Imgrcontactlastname, Imgrcontactnumber, base64_string,
					Imgrcontactunquieid, Imgrcontactname + " "
							+ Imgrcontactlastname, ImgrMemberId);

			/*
			 * mImageViewPro.setScaleType(ScaleType.CENTER_CROP);
			 * mImageViewPro.setOval(true); mImageViewPro
			 * .setBackgroundResource(R.drawable.ic_contact_place);
			 */

			if (profileBase64.equals("Null")) {
				mImageViewPro.setScaleType(ScaleType.CENTER_CROP);
				mImageViewPro.setOval(true);
				mImageViewPro
						.setBackgroundResource(R.drawable.ic_contact_place);
			} else {
				/*
				 * Bitmap mBitmap = ConvertToImage(profileBase64);
				 * mImageViewPro.setScaleType(ScaleType.CENTER_CROP);
				 * mImageViewPro.setOval(true);
				 */

				Bitmap mBitmap = ConvertToImage(wp.getBase64());
				mImageViewPro.setScaleType(ScaleType.CENTER_CROP);
				mImageViewPro.setOval(true);

				mImageViewPro.setImageBitmap(mBitmap);
				// mImageViewPro.setImageBitmap(mBitmap);

			}
			if (RECENTTOJID != null) {
				if (RECENTTOJID.equals("")) {

				} else {
					try {
						mDatasourceHandler.UpdateRecentReceiveCount(
								RECENTTOJID, RECENTFROMJID);
					} catch (Exception e) {
						// Log.e("COUNT SIZE:", "" + e.getMessage());
					}
				}
			} else {
				// Log.e("COUNT SIZE:", "NULL");
			}
		} catch (Exception e) {

		}

		Log.e("-------RECENTFROMJID---------", "" + RECENTTOJID + "---"
				+ RECENTFROMJID);

	}

	public void arrangePromos() {

		promoid = "0";

		is_promo_on_off = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "");

		is_autorotate_on_off = sharedPreferences.getString(
				Constant.SETTING_PROMO_AUTO_ROTATE, "");

		is_sponsered_promo_on_off = sharedPreferences.getString(
				Constant.SPONSORED_ADS, "");

		is_personal_promo_on_off = sharedPreferences.getString(
				Constant.PERSONAL_ADS, "");

		Log.e("is_promo_on_offffffff: ", "" + is_promo_on_off);
		Log.e("is_autorotate_on_offfff: ", "" + is_autorotate_on_off);
		Log.e("is_sponsered_promo_on_offffff: ", "" + is_sponsered_promo_on_off);
		Log.e("is_personal_promo_on_offfff: ", "" + is_personal_promo_on_off);

		mStringPhoneid = getIntent().getStringExtra("phone_id");
		Log.e("mStringPhoneid: ", "" + mStringPhoneid);
		unique_id_notification_user = mStringPhoneid;

		mDatasourceHandler = new DatasourceHandler(this);

		// mDatasourceHandler.resetLastPromoId();

		String lastPromoId = "0";

		Cursor mCursor = mDatasourceHandler.User_Set_Fetch(mStringPhoneid);

		if (mCursor.getCount() != 0) {

			user_is_promo_on_off = mCursor.getString(0);

			user_is_autorotate_on_off = mCursor.getString(1);

			user_is_sponsered_promo_on_off = mCursor.getString(2);

			user_is_personal_promo_on_off = mCursor.getString(3);

			lastPromoId = mCursor.getString(5);

			// lastPromoIndex
			mCursor.moveToNext();
		} else {
			user_is_promo_on_off = "false";

			user_is_autorotate_on_off = "false";

			user_is_sponsered_promo_on_off = "false";

			user_is_personal_promo_on_off = "false";
		}

		if (promoids != null) {
			promoids.clear();
			promo_images.clear();
			promo_names.clear();
		}

		Log.e("is_promo_on_off", is_promo_on_off);
		Log.e("is_autorotate_on_off", is_autorotate_on_off);
		Log.e("is_sponsered_promo_on_off", is_sponsered_promo_on_off);
		Log.e("is_personal_promo_on_off", is_personal_promo_on_off);
		Log.e("user_is_promo_on_off", user_is_promo_on_off);
		Log.e("user_is_autorotate_on_off", user_is_autorotate_on_off);
		Log.e("user_is_sponsered_promo_on_off", user_is_sponsered_promo_on_off);
		Log.e("user_is_personal_promo_on_off", user_is_personal_promo_on_off);

		if (is_promo_on_off.equalsIgnoreCase("true")) {
			if (user_is_promo_on_off.equalsIgnoreCase("true")) {
				boolean isPromoAllowed = false;

				if (is_sponsered_promo_on_off.equalsIgnoreCase("true")
						&& is_personal_promo_on_off.equalsIgnoreCase("true")) {

					if (user_is_sponsered_promo_on_off.equalsIgnoreCase("true")
							&& user_is_personal_promo_on_off
									.equalsIgnoreCase("true")) {
						isPromoAllowed = true;
						allowPromos = true;
						mCursor = mDatasourceHandler.fetchAllActivePromos();
					}

					if (user_is_sponsered_promo_on_off.equalsIgnoreCase("true")
							&& user_is_personal_promo_on_off
									.equalsIgnoreCase("false")) {
						isPromoAllowed = true;
						allowPromos = true;
						mCursor = mDatasourceHandler
								.fetchAllActiveSponseredPromos();
					}

					if (user_is_sponsered_promo_on_off
							.equalsIgnoreCase("false")
							&& user_is_personal_promo_on_off
									.equalsIgnoreCase("true")) {
						isPromoAllowed = true;
						allowPromos = true;
						mCursor = mDatasourceHandler
								.fetchAllActivePersonalPromos();
					}
				}

				if (is_sponsered_promo_on_off.equalsIgnoreCase("true")
						&& is_personal_promo_on_off.equalsIgnoreCase("false")) {
					if (user_is_sponsered_promo_on_off.equalsIgnoreCase("true")) {
						isPromoAllowed = true;
						allowPromos = true;
						mCursor = mDatasourceHandler
								.fetchAllActiveSponseredPromos();
					}
				}

				if (is_sponsered_promo_on_off.equalsIgnoreCase("false")
						&& is_personal_promo_on_off.equalsIgnoreCase("true")) {
					if (user_is_personal_promo_on_off.equalsIgnoreCase("true")) {
						isPromoAllowed = true;
						allowPromos = true;
						mCursor = mDatasourceHandler
								.fetchAllActivePersonalPromos();
					}
				}

				if (isPromoAllowed) {
					int i = 0;
					if (mCursor.getCount() > 0) {
						do {
							if (promoids != null) {
								promoids.add(mCursor.getString(0).trim());
								promo_images.add(mCursor.getString(4).trim());
								promo_names.add(mCursor.getString(5).trim());
							}
							if (lastPromoId.equalsIgnoreCase(mCursor.getString(
									0).trim())) {

								lastPromoIndex = i;
							}

							i++;
						} while (mCursor.moveToNext());
					}
					if (promoids != null) {
						if (promoids.size() > 0) {
							allowPromos = true;
							if (lastPromoIndex >= promoids.size()) {
								lastPromoIndex = 0;
							}
						}
						promodata(promoids.get(lastPromoIndex));
					} else {
						allowPromos = false;
						promoid = "0";

						promo_name = "";
						promo_image = "";

						// promo_name = "No Promo";
						// promo_image = "https://" + Constant.SERVERNAME +
						// "/release/uploads/promos/prosper-la.png";
					}
					// Constant.SELECTED_PROMO_ID

					if (!Constant.SELECTED_PROMO_ID.equalsIgnoreCase("")) {
						isTempPromo = true;
						isOffPromo = false;
						promodata(Constant.SELECTED_PROMO_ID);
						Constant.SELECTED_PROMO_ID = "";
					} else {
						isTempPromo = false;
						isOffPromo = false;
					}
					renderPromobar();

				} else {

					// Log.e("promoiddddddddddddddddddddddd", promoid + "_____"
					// + promoids.size());

					allowPromos = false;
					mFrameLayout_adv_view.setVisibility(View.GONE);
					mFrameLayout_adv_view_OFF_OFF.setVisibility(View.GONE);
					mFrameLayout_adv_view_off.setVisibility(View.VISIBLE);

					if (!Constant.SELECTED_PROMO_ID.equalsIgnoreCase("")) {
						isTempPromo = false;
						isOffPromo = true;
						renderOffPromoBar();
					} else {
						isTempPromo = false;
						isOffPromo = false;
					}
				}
			} else {

				allowPromos = false;
				mFrameLayout_adv_view.setVisibility(View.GONE);
				mFrameLayout_adv_view_OFF_OFF.setVisibility(View.GONE);
				mFrameLayout_adv_view_off.setVisibility(View.VISIBLE);

				if (!Constant.SELECTED_PROMO_ID.equalsIgnoreCase("")) {
					isTempPromo = false;
					isOffPromo = true;
					renderOffPromoBar();
				} else {
					isTempPromo = false;
					isOffPromo = false;
				}
			}

		} else {
			allowPromos = false;
			mFrameLayout_adv_view.setVisibility(View.GONE);
			mFrameLayout_adv_view_OFF_OFF.setVisibility(View.GONE);
			mFrameLayout_adv_view_off.setVisibility(View.VISIBLE);

			if (!Constant.SELECTED_PROMO_ID.equalsIgnoreCase("")) {
				isTempPromo = false;
				isOffPromo = true;
				renderOffPromoBar();
			} else {
				isTempPromo = false;
				isOffPromo = false;
			}
		}

	}

	public void promodata(String id) {
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);
			// Log.e("mString_phone_no", id + "");
			// Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoid = mCursor.getString(0).trim();
			promo_image = mCursor.getString(4).trim();
			promo_name = mCursor.getString(5).trim();

		} else {
			// Log.e("", "Nothing data show");
		}
	}

	public void renderPromobar() {
		Log.e("renderPromobar", "renderPromobar: ");

		mFrameLayout_adv_view_off.setVisibility(View.GONE);
		mFrameLayout_adv_view.setVisibility(View.VISIBLE);

		// mImageViewProTop.setVisibility(View.VISIBLE);
		// mTextViewChanged.setVisibility(View.VISIBLE);
		// mImageViewAdv_on.setVisibility(View.VISIBLE);
		//
		mTextViewChanged.setText(promo_name);

		if (!promo_image.equalsIgnoreCase("")) {
			imageLoader.displayImage(promo_image, mImageViewProTop, options,
					new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									activity, R.anim.fade_in);
							// imgViewFlag.setAnimation(anim);
							// anim.start();
						}
					});
		}
	}

	public void renderOffPromoBar() {
		// promodata
		Log.e("renderOffPromoBar", "renderOffPromoBar: ");

		promodata(Constant.SELECTED_PROMO_ID);

		Constant.SELECTED_PROMO_ID = "";
		mTextView_text_adv_changed.setVisibility(View.VISIBLE);
		mImageView_adv_delete_icon.setVisibility(View.VISIBLE);
		mImageView_adv_image.setVisibility(View.VISIBLE);

		mTextView_text_adv_changed.setText(promo_name);
		if (!promo_image.equalsIgnoreCase("")) {
			imageLoader.displayImage(promo_image, mImageView_adv_image,
					options, new SimpleImageLoadingListener() {
						@Override
						public void onLoadingComplete(Bitmap loadedImage) {
							Animation anim = AnimationUtils.loadAnimation(
									activity, R.anim.fade_in);
							// imgViewFlag.setAnimation(anim);
							// anim.start();
						}
					});
		}
	}

	public void resetTempOffPromoSettings() {
		togglePromoTemp = true;
		Log.e("resetTempOffPromoSettings", "resetTempOffPromoSettings: ");
		// adv_on_off = true;
		mImageViewAdv_off.setVisibility(View.GONE);
		image_adv_show_OFF.setVisibility(View.GONE);
		mImageViewAdv_on.setVisibility(View.VISIBLE);
		mImageViewProTop.setVisibility(View.VISIBLE);
		mTextViewChanged.setVisibility(View.VISIBLE);
		mTextViewChanged.setTextColor(Color.parseColor("#000000"));
		if (promoids.size() == 0) {
			// mTextViewChanged.setText("No Promo");
			// promo_image = "https://" + Constant.SERVERNAME +
			// "/release/uploads/promos/prosper-la.png";

			mTextViewChanged.setText("");
			promo_image = "";

			if (!promo_image.equalsIgnoreCase("")) {
				imageLoader.displayImage(promo_image, mImageViewProTop,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils.loadAnimation(
										activity, R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});
			}

		} else {

			mTextViewChanged.setText(promo_name);

			if (!promo_image.equalsIgnoreCase("")) {
				imageLoader.displayImage(promo_image, mImageViewProTop,
						options, new SimpleImageLoadingListener() {
							@Override
							public void onLoadingComplete(Bitmap loadedImage) {
								Animation anim = AnimationUtils.loadAnimation(
										activity, R.anim.fade_in);
								// imgViewFlag.setAnimation(anim);
								// anim.start();
							}
						});
			}
		}
	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	public void setConnection(final XMPPConnection connection) {

		this.connection = connection;
		try {

			if (connection != null) {

				// Add a packet listener to get messages sent to us

				String currentPhoneNo = mSharedPreferences.getString(
						"currentPhoneNo", "");

				if (!currentPhoneNo.equals("")) {
					
				}

				
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {

					public void processPacket(Packet packet) {

						SharedPreferences newSharedPreferences = getSharedPreferences(
								Constant.IMGRNOTIFICATION_RECEIVE,
								Context.MODE_PRIVATE);

						String currentPhoneNo = newSharedPreferences.getString(
								"currentPhoneNo", "");

						String currentScreen = newSharedPreferences.getString(
								"currentScreen", "");

						if (UILApplication.isInBackground) {
							if (currentScreen.equalsIgnoreCase("chat")) {
								saveReceivedMessage(packet);
							}
						} else {
							if (!currentPhoneNo.equalsIgnoreCase("")) {
								saveReceivedMessage(packet);
							}
						}

					}
				}, filter);
			}

		} catch (Exception e) {

		}
	}

	public void saveReceivedMessage(Packet packet) {
		message = (Message) packet;

		String message_send_local_notify = null;
		String promoID = null;
		String mStringnewtojid = null;
		String mStringnewfromjid = null;
		JSONObject objMainList = null;
		String check = null;
		String check_update = null;
		String time = currenttime();
		String date_sent = date();

		String message_id = "";

		// Log.e("message.getPacketID(): ", "" +
		// message.getPacketID());

		if (message.getBody() != null) {

			Log.e("message.getBody()1111: ", "" + message.getBody());

			String fromName = StringUtils.parseBareAddress(message.getFrom()
					.split("\\@")[0]);
			mStringnewtojid = fromJID;
			mStringnewfromjid = fromName;

			Log.e("fromjidddddddddd", "" + fromName);

			String incomingNumber = "";

			/*
			 * if (mStringnewtojid.split("_")[0]
			 * .equalsIgnoreCase(mStringPassword)) { incomingNumber =
			 * mStringnewfromjid.split("_")[0]; } else { incomingNumber =
			 * mStringnewtojid.split("_")[0]; }
			 */

			incomingNumber = mStringnewfromjid.split("_")[0];

			String isBlocked = "false";

			Cursor mBlockCursor = mDatasourceHandler.FETCH_(incomingNumber);

			if (mBlockCursor.getCount() != 0) {

				if (mBlockCursor.moveToFirst()) {
					isBlocked = mBlockCursor.getString(9);
				}
			}

			Log.e("blockedddddddddddddd", isBlocked);

			if (!isBlocked.equalsIgnoreCase("true")) {

				try {

					chat_list_adapter = new ChatAdapter(ChatScreen.this,
							messages);

					JSONObject bodyJsonObj = new JSONObject(message.getBody());

					if (bodyJsonObj.has("message_id")) {
						message_id = bodyJsonObj.getString("message_id");
					} else {
						message_id = message.getPacketID();
					}

					String messageDataToBeSaved = "";

					String promo_ID = "0";

					if (bodyJsonObj.has("_message_body_tag")) {

						// String promo_ID;
						if (bodyJsonObj.has("_promo_id_tag")) {
							promo_ID = bodyJsonObj.getString("_promo_id_tag");

						} else {
							promo_ID = "0";
						}

						String message = bodyJsonObj
								.getString("_message_body_tag");

						messageDataToBeSaved = "{\"body\":[{\"preview_Icon\":\"null\",\"_promo_id_tag\":\""
								+ promo_ID
								+ "\",\"pushkey\":\""
								+ message
								+ "\","
								+ "\"_message_body_tag\":\""
								+ message
								+ "\"}]}";
					} else {
						JSONObject json = new JSONObject();
						String mStringBase64 = bodyJsonObj
								.getString("preview_Icon");

						try {
							json.put("server_URL", "Not Uploaded");
							json.put("pushkey", "image");
							json.put("preview_Icon", mStringBase64);

							String messageData = json.toString();
							messageDataToBeSaved = "{\"body\":[" + messageData
									+ "]}";

						} catch (JSONException e) {
							// TODO Auto-generated catch
							// block
							e.printStackTrace();
						}
					}

					boolean isInserted = mDatasourceHandler.insertChathistory(
							message_id, "username", fromJID, mStringnewfromjid,
							messageDataToBeSaved, time, "0", "1", date_sent,
							getApplicationContext(), number, "no");
					String timestamp = timestamp();

					int countincrease = 0;

					String currentPhoneNo = mSharedPreferences.getString(
							"currentPhoneNo", "");

					if (isInserted) {
						if (fromName.split("_")[0]
								.equalsIgnoreCase(currentPhoneNo)) {
							if (bodyJsonObj.has("_message_body_tag")) {

								String[] msg_Data = new String[9];
								// String promo_ID;
								if (bodyJsonObj.has("_promo_id_tag")) {
									promo_ID = bodyJsonObj
											.getString("_promo_id_tag");

								} else {
									promo_ID = "0";
								}

								String test;
								if (promo_ID.equals("0")) {
									test = "0";
								} else {
									test = "";
								}
								String path = historypromo_data(promo_ID);

								msg_Data[3] = bodyJsonObj
										.getString("_message_body_tag");
								msg_Data[1] = "friend";
								msg_Data[2] = "";
								msg_Data[0] = "";
								msg_Data[4] = time;
								msg_Data[5] = test;
								msg_Data[6] = path;
								msg_Data[7] = promo_ID;
								msg_Data[8] = date_sent;

								messages.add(msg_Data);

								String msgstring = "";
								for (int msg = 0; msg < 9; msg++) {
									msgstring += ", " + msg_Data[msg];
								}

							} else {
								String[] msg_Data = new String[6];
								msg_Data[0] = "";
								msg_Data[1] = "friend_pic";
								msg_Data[2] = "";
								msg_Data[3] = bodyJsonObj
										.getString("preview_Icon");
								msg_Data[4] = time;
								msg_Data[5] = date_sent;
								messages.add(msg_Data);

							}

							Log.e("frommmmmmmmmmmmmm", fromName + "____"
									+ toJID + "____" + fromJID);

							this.runOnUiThread(new Runnable() {
								@Override
								public void run() {

									if (chat_list_adapter.getCount() >= 5) {
										lv.setStackFromBottom(true);
									} else {
										lv.setStackFromBottom(false);
									}
									mArrayDATE.add(date());
									chat_list_adapter.notifyDataSetChanged();

									// chat_list_adapter.refresh(messages);
									lv.setAdapter(chat_list_adapter);
								}
							});

						} else {
							countincrease = mDatasourceHandler
									.UpdateRecentReceiveCount(
											mStringnewfromjid, fromJID,
											messageDataToBeSaved, time,
											date_sent, timestamp);

							countincrease = countincrease + 1;

						}

						Log.e("new message count", "new message count "
								+ countincrease);

						String mString = "" + countincrease;

						mDatasourceHandler.insertrecentMessage(
								messageDataToBeSaved, mStringnewfromjid,
								fromJID, "", "", date_sent, time, timestamp,
								mString, "no", mStringPassword);

					}

					if (!promo_ID.equalsIgnoreCase("0")) {
						Log.e("received promo idddddddddddddddd", "" + promo_ID);
						receievedPromoId = promo_ID;
						// new
						// Execute_New_Promo().execute();
						new ExecuteChatNotification1().execute();
					}

					/*
					 * chat_list_adapter .notifyDataSetChanged();
					 * lv.setAdapter(chat_list_adapter);
					 */

					// fetchChatHistory();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	}

	public String promo_data_(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringHeader = mCursor.getString(3).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringHeader: ", "   " +
				// mStringHeader);
			} else {
				mStringHeader = "NO";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringHeader;
	}

	public String promo_data_link(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringFooter = mCursor.getString(2).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringHeader: ", "   " +
				// mStringHeader);
			} else {
				mStringFooter = "NO";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringFooter;
	}

	protected void selectImage() {

		final CharSequence[] options = { "Choose from Gallery", "Take Photo",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(ChatScreen.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		Log.e("size", "" + size);

		if (size == 0) {
			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 400);
			intent.putExtra("outputY", 400);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			if (size == 1) {
				Intent i = new Intent(intent);

				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);

					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(
						getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);

							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							getContentResolver().delete(mImageCaptureUri, null,
									null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	protected void chooseFromGallery() {
		Log.e("PICK_FROM_CAMERA", "PICK_FROM_CAMERA");

		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		startActivityForResult(i, GALLERY_IMAGE_REQUEST_CODE);

		// fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		// Intent intent = new Intent(Intent.ACTION_PICK,
		// android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		// intent.setType("image/*");
		// intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		// intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		// startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case PICK_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {

				// Bundle extras = data.getExtras();

				Bitmap photo = (Bitmap) data.getExtras().get("data");
				mImageViewProfile.setImageBitmap(photo);
				mStringBase64 = convert_image(photo);

				// Intent intent = new Intent("com.android.camera.action.CROP");
				// intent.setType("image/*");
				// intent.setData(mImageCaptureUri);
				//
				// Log.e("intent", ""+intent);

				// doCrop();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				mImageCaptureUri = data.getData();

				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(mImageCaptureUri,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);

				cursor.close();

				try {
					// hide video preview

					Bitmap bitmap = decodeFile(picturePath);

					mImageViewProfile.setImageBitmap(bitmap);
					mStringBase64 = convert_image(bitmap);
					// Log.e("mString_sendserver:" + "", mString_sendserver);

				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				// Log.e("mImageCaptureUri", ""+mImageCaptureUri);

				// mImageViewProfile.setImageURI(mImageCaptureUri);
				// mStringBase64 = convert_image(mImageCaptureUri);
				// doCrop();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(ChatScreen.this,
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(ChatScreen.this, "Sorry! Process Failed",
						Toast.LENGTH_SHORT).show();
			}
			break;

		case CROP_FROM_CAMERA:
			if (resultCode == Activity.RESULT_OK) {
				Bundle extras = data.getExtras();

				if (extras != null) {
					Bitmap photo = extras.getParcelable("data");

					mImageViewProfile.setImageBitmap(photo);
					mStringBase64 = convert_image(photo);
				}
				File f = new File(mImageCaptureUri.getPath());

				if (f.exists())
					f.delete();
			}
			break;
		}

	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFilePath(File mFile) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {

				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());

		mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "CHAT_IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to

		byte[] byte_arr = stream.toByteArray();

		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	protected void captureImage() {

		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(cameraIntent, PICK_FROM_CAMERA);

		// Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		//
		// mImageCaptureUri = Uri.fromFile(new File(Environment
		// .getExternalStorageDirectory(), "tmp_"
		// + String.valueOf(System.currentTimeMillis()) + ".jpg"));
		//
		// intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
		// mImageCaptureUri);
		//
		// try {
		// intent.putExtra("return-data", true);
		//
		// startActivityForResult(intent, PICK_FROM_CAMERA);
		// } catch (ActivityNotFoundException e) {
		// e.printStackTrace();
		// }

	}

	public Bitmap decodeFile(String path) {// you can provide file path here

		int orientation;
		try {
			if (path == null) {
				return null;
			}
			// decode image size
			BitmapFactory.Options option1 = new BitmapFactory.Options();
			option1.inJustDecodeBounds = false;

			BitmapFactory.decodeFile(path, option1);

			// decode with inSampleSize
			option1.inSampleSize = calculateInSampleSize(option1, 100, 100);
			option1.inJustDecodeBounds = false;

			Bitmap bm = BitmapFactory.decodeFile(path, option1);
			Bitmap bitmap = bm;

			ExifInterface exif = new ExifInterface(path);

			orientation = exif
					.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

			// exif.setAttribute(ExifInterface.ORIENTATION_ROTATE_90, 90);

			Matrix m = new Matrix();

			if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
				m.postRotate(180);
				// m.postScale((float) bm.getWidth(), (float) bm.getHeight());
				// if(m.preRotate(90)){

				bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
						bm.getHeight(), m, true);
				return bitmap;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
				m.postRotate(90);

				bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
						bm.getHeight(), m, true);
				return bitmap;
			} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
				m.postRotate(270);

				bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
						bm.getHeight(), m, true);
				return bitmap;
			}
			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {

				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 6;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		// Log.e("Insample size", inSampleSize + "");
		return inSampleSize;
	}

	/*** end of onPostExecute ***/

	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {

			mStringusername = sharedPreferences.getString(
					Constant.USERNAME_XMPP, "");
			mStringPassword = sharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");
			// Log.e("mStringusername: ", "" + mStringusername);
			// Log.e("mStringPassword: ", "" + mStringPassword);
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected String doInBackground(String... params) {
			// Log.e("doInBackground", "doInBackground");

			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);
			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();

				// connection.login(mStringusername, mStringPassword);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
				Roster roster = connection.getRoster();
				Collection<RosterEntry> entries = roster.getEntries();

				ServiceDiscoveryManager sdm = ServiceDiscoveryManager
						.getInstanceFor(connection);

				OfflineMessageManager offlineManager = new OfflineMessageManager(
						connection);
				try {
					Iterator<org.jivesoftware.smack.packet.Message> it = offlineManager
							.getMessages();
					Log.e("offline11111111111111",
							"" + offlineManager.supportsFlexibleRetrieval());
					Log.e("offline22222222222222",
							"Number of offline messages:: "
									+ offlineManager.getMessageCount());
					Map<String, ArrayList<Message>> offlineMsgs = new HashMap<String, ArrayList<Message>>();
					while (it.hasNext()) {
						org.jivesoftware.smack.packet.Message message = it
								.next();
						Log.e("offline2222222222222",
								"receive offline messages, the Received from ["
										+ message.getFrom() + "] the message:"
										+ message.getBody());
						String fromUser = message.getFrom().split("/")[0];

						if (offlineMsgs.containsKey(fromUser)) {
							offlineMsgs.get(fromUser).add(message);
						} else {
							ArrayList<Message> temp = new ArrayList<Message>();
							temp.add(message);
							offlineMsgs.put(fromUser, temp);
						}
					}
					// Deal with a collection of offline messages ...
					Set<String> keys = offlineMsgs.keySet();
					Iterator<String> offIt = keys.iterator();
					while (offIt.hasNext()) {
						String key = offIt.next();
						ArrayList<Message> ms = offlineMsgs.get(key);
						/*
						 * TelFrame tel = new TelFrame(key); ChatFrameThread cft
						 * = new ChatFrameThread(key, null); cft.setTel(tel);
						 * cft.start(); for (int i = 0; i < ms.size(); i++) {
						 * tel.messageReceiveHandler(ms.get(i)); }
						 */
					}
					offlineManager.deleteMessages();
				} catch (Exception e) {
					e.printStackTrace();
				}

				for (RosterEntry entry : entries) {
					// Log.e("XMPPChatDemoActivity",
					// "--------------------------------------");
					// Log.e("XMPPChatDemoActivity", "RosterEntry " + entry);
					// Log.e("XMPPChatDemoActivity", "User: " +
					// entry.getUser());
					// Log.e("XMPPChatDemoActivity", "Name: " +
					// entry.getName());
					// Log.e("XMPPChatDemoActivity",
					// "Status: " + entry.getStatus());
					// Log.e("XMPPChatDemoActivity", "Type: " +
					// entry.getType());
					Presence entryPresence = roster
							.getPresence(entry.getUser());

					// Log.e("XMPPChatDemoActivity", "Presence Status: "
					// + entryPresence.getStatus());
					// Log.e("XMPPChatDemoActivity", "Presence Type: "
					// + entryPresence.getMode());

					Presence.Type type = entryPresence.getType();
					if (type == Presence.Type.available) {
					}
					// Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
					// Log.e("XMPPChatDemoActivity", "Presence : " +
					// entryPresence);
				}
			} catch (XMPPException e) {
				// Log.e("Cannot connect to XMPP server with default admin username and password.",
				// "0");
				// Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// UI.hideProgressDialog();
			// Log.e("result", "" + result);
			if (result != null) {

				// sendPendingMessageIfAny();

				/*
				 * OfflineMessageManager offlineMessageManager = new
				 * OfflineMessageManager(connection);//This is the method get
				 * the offline message try {
				 * Log.e("message_countttttttttttttttttttt",
				 * ""+offlineMessageManager.getMessageCount()); } catch
				 * (XMPPException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); }
				 */

				// Log.e("ConnectionId", "" + connection.getConnectionID());
				// Log.e("Servicename", "" + connection.getServiceName());
				// Log.e("User", "" + connection.getUser());

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getApplicationContext(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

	}

	class ExecuteChatNotification extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {

			String url = JsonParserConnector.ALL_TIME_CALL_NOTIFICATION(
					fromJID, toJID, final_show_message, message_payload,
					message_id);

			Log.e("Notification___", "" + url);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			Log.e("Check result",""+result);

		}

	}

	class ExecuteChatNotification1 extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {

			String url;
			Log.e("Execute Check111","check");
			String id = receievedPromoId;
			Log.e("received promo idddddddddddddddd", "" + id);
			url = JsonParserConnector.getPERSONALSponsored(id);
			Log.e("received promo iddddddddddddd111111", "" + url);
			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			Log.e("Execute Check","check");
			Personal_List(result);
		}

	}

	public void configure(ProviderManager pm) {
		// Private Data Storage
		pm.addIQProvider("query", "jabber:iq:private",
				new PrivateDataManager.PrivateDataIQProvider());
		// Time
		try {
			pm.addIQProvider("query", "jabber:iq:time",
					Class.forName("org.jivesoftware.smackx.packet.Time"));
		} catch (ClassNotFoundException e) {

		}

		// Roster Exchange
		pm.addExtensionProvider("x", "jabber:x:roster",
				new RosterExchangeProvider());

		// Message Events
		pm.addExtensionProvider("x", "jabber:x:event",
				new MessageEventProvider());

		// Chat State
		pm.addExtensionProvider("active",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("composing",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("paused",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("inactive",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("gone",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		// XHTML
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
				new XHTMLExtensionProvider());

		// Group Chat Invitations
		pm.addExtensionProvider("x", "jabber:x:conference",
				new GroupChatInvitation.Provider());

		// Service Discovery # Items
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());

		// Service Discovery # Info
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Data Forms
		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

		// MUC User
		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
				new MUCUserProvider());

		// MUC Admin
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
				new MUCAdminProvider());

		// MUC Owner
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
				new MUCOwnerProvider());

		// SharedGroupsInfo
		pm.addIQProvider("sharedgroup",
				"http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Delayed Delivery
		pm.addExtensionProvider("x", "jabber:x:delay",
				new DelayInformationProvider());

		// Version
		try {
			pm.addIQProvider("query", "jabber:iq:version",
					Class.forName("org.jivesoftware.smackx.packet.Version"));
		} catch (ClassNotFoundException e) {
			// Not sure what's happening here.
		}

		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

		// Offline Message Requests
		pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
				new OfflineMessageRequest.Provider());

		// Offline Message Indicator
		pm.addExtensionProvider("offline",
				"http://jabber.org/protocol/offline",
				new OfflineMessageInfo.Provider());

		// Last Activity
		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

		// User Search
		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

		// JEP-33: Extended Stanza Addressing
		pm.addExtensionProvider("addresses",
				"http://jabber.org/protocol/address",
				new MultipleAddressesProvider());

		// FileTransfer
		pm.addIQProvider("si", "http://jabber.org/protocol/si",
				new StreamInitiationProvider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());

		// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
		// IBBProviders.Open());

		// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
		// IBBProviders.Close());

		// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
		// new
		// IBBProviders.Data());

		// Privacy
		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());

		pm.addIQProvider("command", "http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider());
		pm.addExtensionProvider("malformed-action",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.MalformedActionError());
		pm.addExtensionProvider("bad-locale",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadLocaleError());
		pm.addExtensionProvider("bad-payload",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadPayloadError());
		pm.addExtensionProvider("bad-sessionid",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadSessionIDError());
		pm.addExtensionProvider("session-expired",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.SessionExpiredError());

		pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
				DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
		pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
				new DeliveryReceiptRequest().getNamespace(),
				new DeliveryReceiptRequest.Provider());
		/*
		 * pm.addExtensionProvider(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE,
		 * new ReadReceipt.Provider());
		 */
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		// Log.e("Pinging", "on Pause");
		Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection(activity);
		PingManager.getInstanceFor(connection);
		UILApplication.activityPaused();

		SharedPreferences newSharedPreferences = getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		editor = newSharedPreferences.edit();
		editor.putString("currentPhoneNo", "");
		editor.commit();

		if (dialog != null) {
			LogMessage.dismiss();
		}
		// finish();

	}

	public void makeConnection() {

		/*
		 * config = new ConnectionConfiguration("64.235.48.26", 5222);
		 * connection = new XMPPConnection(config);
		 * config.setSASLAuthenticationEnabled(true);
		 * config.setCompressionEnabled(true);
		 * config.setReconnectionAllowed(true);
		 * SASLAuthentication.registerSASLMechanism("PLAIN",
		 * SASLPlainMechanism.class);
		 * config.setSecurityMode(SecurityMode.enabled);
		 * configure(ProviderManager.getInstance());
		 * config.setDebuggerEnabled(true); connection.DEBUG_ENABLED = true;
		 * config.setReconnectionAllowed(true);
		 * 
		 * Connection_Pool connection_Pool = Connection_Pool.getInstance();
		 * connection = connection_Pool.getConnection();
		 * setConnection(connection); card = new VCard();
		 * Connection.DEBUG_ENABLED = true;
		 */

		/*
		 * mStringusername = sharedPreferences.getString(
		 * Constant.USERNAME_XMPP, ""); mStringPassword =
		 * sharedPreferences.getString( Constant.PASSWORD_XMPP, "");
		 * 
		 * String host = "64.235.48.26"; String port = "5222"; String service =
		 * "64.235.48.26"; String username = mStringusername; String password =
		 * mStringPassword;
		 * 
		 * // Create a connection ConnectionConfiguration connConfig = new
		 * ConnectionConfiguration(host, Integer.parseInt(port), service);
		 * //XMPPConnection connection = new XMPPConnection(connConfig);
		 * connConfig.setSASLAuthenticationEnabled(false); connection = new
		 * XMPPConnection(connConfig);
		 * 
		 * Connection_Pool connection_Pool = Connection_Pool.getInstance();
		 * connection_Pool.setConnection(connection);
		 * 
		 * try { if (!connection.isConnected()) connection.connect();
		 * Log.i("XMPPClient", "[SettingsDialog] Connected to " +
		 * connection.getHost()); } catch (XMPPException ex) {
		 * Log.e("XMPPClient", "[SettingsDialog] Failed to connect to " +
		 * connection.getHost()); Log.e("XMPPClient", ex.toString());
		 * this.setConnection(null); } try { connection.login(username,
		 * password); Log.i("XMPPClient", "Logged in as " +
		 * connection.getUser());
		 * 
		 * // Set the status to available Presence presence = new
		 * Presence(Presence.Type.available); connection.sendPacket(presence);
		 * this.setConnection(connection); } catch (XMPPException ex) {
		 * Log.e("XMPPClient", "[SettingsDialog] Failed to log in as " +
		 * username); Log.e("XMPPClient", ex.toString());
		 * this.setConnection(null); }
		 */

		new login_Existing_User().execute();

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		SharedPreferences newSharedPreferences = getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		editor = newSharedPreferences.edit();
		editor.putString("currentPhoneNo", "");
		editor.commit();

		if (dialog != null) {
			LogMessage.dismiss();
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (dialog != null) {
			LogMessage.dismiss();
		}

		isCallForChating = false;
		//myTimer.cancel();

	}

	public void sharedPrefernces(boolean flag) {
		editor = sharedPreferences.edit();
		editor.putBoolean(Constant.on_off_ads, flag);

		editor.commit();

	}

	public void shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.BUBBLE_SET_FREIND, mString);

		editor.commit();

	}

	public void Sponsored_shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.SPONSORED_on_off_ads, mString);

		editor.commit();

	}

	public class SwipeDismissTouchListener implements View.OnTouchListener {

		static final String logTag = "ActivitySwipeDetector";
		private Activity activity;
		static final int MIN_DISTANCE = 100;
		private float downX, upX;

		public SwipeDismissTouchListener(Activity mActivity) {
			activity = mActivity;
		}

		public void onRightToLeftSwipe() {
			if (allowPromos) {
				if (lastPromoIndex == 0) {
					lastPromoIndex = promoids.size() - 1;
				} else {
					lastPromoIndex--;
				}
				mDatasourceHandler.updateLastPromoId(promoids
						.get(lastPromoIndex));
				promodata(promoids.get(lastPromoIndex));
				renderPromobar();
			}
		}

		public void onLeftToRightSwipe() {
			if (allowPromos) {
				if (lastPromoIndex == (promoids.size() - 1)) {
					lastPromoIndex = 0;
				} else {
					lastPromoIndex++;
				}
				mDatasourceHandler.updateLastPromoId(promoids
						.get(lastPromoIndex));
				promodata(promoids.get(lastPromoIndex));
				renderPromobar();
			}
		}

		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN: {
				downX = event.getX();
				return true;
			}
			case MotionEvent.ACTION_UP: {
				upX = event.getX();

				float deltaX = downX - upX;

				if (Math.abs(deltaX) > MIN_DISTANCE) {
					if (deltaX < 0) {
						this.onLeftToRightSwipe();
						return true;
					}
					if (deltaX > 0) {
						this.onRightToLeftSwipe();
						return true;
					}
				} else {

				}

				return false; // no swipe horizontally and no swipe vertically
			}// case MotionEvent.ACTION_UP:
			}
			return false;
		}
	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class ExecuteNewPromo extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			// Log.e("id:", "doingbackground");
			// Log.e("id:", "" + testpromo_id);
			url = JsonParserConnector.getPERSONALSponsored(testpromo_id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Log.e("result: ", "" + result);
			// UI.hideProgressDialog();
			// PersonalList(result);
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	class ExecuteNewPromoUpdate extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			// Log.e("id:", "doingbackground");
			// Log.e("id:", "" + testpromo_id);
			url = JsonParserConnector.getPERSONALSponsored(testpromo_id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Log.e("result: ", "" + result);
			// UI.hideProgressDialog();
			// PersonalListUpdate(result);
		}

	}

	/**
	 * Execute NewPromo Asynctask
	 */
	// class Execute_New_Promo extends AsyncTask<String, Integer, String> {
	//
	// @Override
	// protected String doInBackground(String... params) {
	// // TODO Auto-generated method stub
	// String url;
	//
	// String id = receievedPromoId;
	// Log.e("received promo idddddddddddddddd", ""+id);
	// url = JsonParserConnector.getPERSONALSponsored(id);
	// Log.e("received promo iddddddddddddd111111", ""+url);
	// return url;
	//
	// // String url = JsonParserConnector.ALL_TIME_CALL_NOTIFICATION(
	// // fromJID, toJID, final_show_message, message_payload,
	// // message_id);
	//
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// UI.showProgressDialog(ChatScreen.this);
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	// // TODO Auto-generated method stub
	// super.onPostExecute(result);
	// // Log.e("result: ", "" + result);
	// UI.hideProgressDialog();
	// Personal_List(result);
	// }
	//
	// }

	/**
	 * Execute NewPromo Asynctask
	 */
	class Execute_New_Promo_Update extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url;
			String id = params[0];
			url = JsonParserConnector.getPERSONALSponsored(id);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Log.e("result: ", "" + result);
			UI.hideProgressDialog();
			//
			// Personal_List_update(result);
		}

	}

	private void PersonalListUpdate(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				JSONArray mArray = jsonOBject.getJSONArray("data");
				for (int i = 0; i < mArray.length(); i++) {
					JSONObject sys = mArray.getJSONObject(i);
					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					// mStringsmodified_date = sys.getString("modified_date");
					mString_link_text = sys.getString("promo_link_text");
					mString_link_promo = sys.getString("promo_link");
					mString_header_promo_message = sys
							.getString("promo_message_header");

					boolean flag = mDatasourceHandler.update_Sponsored(
							mStringspromoid, "NO_Define", mStringspromo_name,
							"https://imgrapp.com/release/"
									+ mStringspromo_image, mString_link_promo,
							mString_link_text, mString_header_promo_message,
							"0", "", "1", "1");

					// L/og.e("flag", "" + flag);

					JSONObject json = new JSONObject();
					mArrayPersonal = new JSONArray();
					JSONObject objMainList = new JSONObject();
					String date_sent = date();
					String path = historypromo_data(mStringspromoid);
					String[] msg_Data = new String[9];
					msg_Data[0] = "";
					msg_Data[4] = personal_api_time;
					msg_Data[1] = "friend";
					msg_Data[2] = "0";
					msg_Data[3] = personal_api_pushkey;
					msg_Data[5] = personal_api_idtag;
					msg_Data[6] = path;
					msg_Data[7] = mStringspromoid;
					msg_Data[8] = date_sent;
					messages.add(msg_Data);

					try {

						json.put("pushkey",
								personaljsonObj.getString("pushkey"));
						json.put("_message_body_tag",
								personaljsonObj.getString("_message_body_tag"));
						json.put("_promo_id_tag",
								personaljsonObj.getString("_promo_id_tag"));
						json.put("preview_Icon", "null");

					} catch (Exception e) {

					}

					mArrayPersonal.put(json);
					// Log.e("mArrayPersonal: ", "" + mArrayPersonal);
					try {
						objMainList.put("body", mArrayPersonal);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//

					String date = date();
					String timestamp = timestamp();

					int history_count = mDatasourceHandler
							.HISTORYUpdateRecentReceiveCount(message
									.getPacketID());
					if (history_count == 0) {
						int countincrease = 0;
						countincrease = mDatasourceHandler
								.UpdateRecentReceiveCount(personal_jidto,
										personal_jidfrom,
										objMainList.toString(),
										personal_api_time, date, timestamp);
						String mString = "" + (countincrease + 1);

						boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
								personal_jidto, personal_jidfrom,
								objMainList.toString(), personal_api_time,
								date, timestamp, "0", "no", mStringPassword);
						// Log.e("RECEIVE 1 flag1:", "" + flag1);
						if (!flag1) {
							/*
							 * mDatasourceHandler.insertrecentMessage(
							 * objMainList.toString(), personal_jidfrom,
							 * personal_jidto, "", getfrdsname, date,
							 * personal_api_time, timestamp, "0", "no",
							 * mStringPassword);
							 */
						}

					}

					mDatasourceHandler.insertChathistory(message.getPacketID(),
							"username", personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time,
							mStringspromoid, "0", date,
							getApplicationContext(), number, "no");

					personal_jidfrom = null;
					personal_jidto = null;
					// Add the incoming message to the list view
					mHandler.post(new Runnable() {
						public void run() {

							if (chat_list_adapter.getCount() >= 5) {
								lv.setStackFromBottom(true);
							} else {
								lv.setStackFromBottom(false);
							}
							mArrayDATE.add(date());
							chat_list_adapter.notifyDataSetChanged();
							lv.setAdapter(chat_list_adapter);
						}
					});
				}

			} else {

			}

		} catch (JSONException e) {

			// Log.e("", e.getMessage());
		}

	}

	private void PersonalList(String response) {
		try {
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {
				JSONArray mArray = jsonOBject.getJSONArray("data");
				for (int i = 0; i < mArray.length(); i++) {
					JSONObject sys = mArray.getJSONObject(i);
					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					// mStringsmodified_date = sys.getString("modified_date");
					mString_link_text = sys.getString("promo_link_text");
					mString_link_promo = sys.getString("promo_link");
					mString_header_promo_message = sys
							.getString("promo_message_header");

					boolean flag = mDatasourceHandler.insertSponsored(
							mStringspromoid, "NO_Define", mStringspromo_name,
							"https://imgrapp.com/release/"
									+ mStringspromo_image, mString_link_promo,
							mString_link_text, mString_header_promo_message,
							"0", "", "1", "1");

					// Log.e("flag", "" + flag);

					JSONObject json = new JSONObject();
					mArrayPersonal = new JSONArray();
					JSONObject objMainList = new JSONObject();
					String date_sent = date();
					String path = historypromo_data(mStringspromoid);
					String[] msg_Data = new String[9];
					msg_Data[0] = "";
					msg_Data[4] = personal_api_time;
					msg_Data[1] = "friend";
					msg_Data[2] = "0";
					msg_Data[3] = personal_api_pushkey;
					msg_Data[5] = personal_api_idtag;
					msg_Data[6] = path;
					msg_Data[7] = mStringspromoid;
					msg_Data[8] = date_sent;
					messages.add(msg_Data);

					try {

						json.put("pushkey",
								personaljsonObj.getString("pushkey"));
						json.put("_message_body_tag",
								personaljsonObj.getString("_message_body_tag"));
						json.put("_promo_id_tag",
								personaljsonObj.getString("_promo_id_tag"));
						json.put("preview_Icon", "null");

					} catch (Exception e) {

					}

					mArrayPersonal.put(json);
					// Log.e("mArrayPersonal: ", "" + mArrayPersonal);
					try {
						objMainList.put("body", mArrayPersonal);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String date = date();
					String timestamp = timestamp();

					int history_count = mDatasourceHandler
							.HISTORYUpdateRecentReceiveCount(message
									.getPacketID());
					if (history_count == 0) {
						int countincrease = 0;
						countincrease = mDatasourceHandler
								.UpdateRecentReceiveCount(personal_jidto,
										personal_jidfrom,
										objMainList.toString(),
										personal_api_time, date, timestamp);
						String mString = "" + (countincrease + 1);

						boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
								personal_jidto, personal_jidfrom,
								objMainList.toString(), personal_api_time,
								date, timestamp, "0", "no", mStringPassword);
						// Log.e("RECEIVE 1 flag1:", "" + flag1);
						if (!flag1) {
							/*
							 * mDatasourceHandler.insertrecentMessage(
							 * objMainList.toString(), personal_jidfrom,
							 * personal_jidto, "", getfrdsname, date,
							 * personal_api_time, timestamp, "0", "no",
							 * mStringPassword);
							 */
						}

					}

					mDatasourceHandler.insertChathistory(message.getPacketID(),
							"username", personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time,
							mStringspromoid, "0", date,
							getApplicationContext(), number, "no");

					personal_jidfrom = null;
					personal_jidto = null;
					// Add the incoming message to the list view
					mHandler.post(new Runnable() {
						public void run() {
							Log.e("hereeeeeeeeeeeeeeeeeee33333",
									"hereeeeeeeeeeeeeeeeeee3333");
							// Log.e("Set connection COUNT:", ""
							// + chat_list_adapter.getCount());
							if (chat_list_adapter.getCount() >= 5) {
								lv.setStackFromBottom(true);
							} else {
								lv.setStackFromBottom(false);
							}
							mArrayDATE.add(date());
							chat_list_adapter.notifyDataSetChanged();
							lv.setAdapter(chat_list_adapter);
						}
					});
				}

			} else {

			}

		} catch (JSONException e) {

			// Log.e("", e.getMessage());
		}

	}

	private void Personal_List(String response) {
		try {
			String message_send_local_notify = null;
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				Log.e("new promoooooooo", response);

				JSONArray bodyJsonObj = jsonOBject.getJSONArray("data");

				JSONObject sys = new JSONObject(bodyJsonObj.getString(0));

				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				// mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				Log.e("promo_data", "promo_id: " + mStringspromoid
						+ ", promo_type: NO_Define, promo_name: "
						+ mStringspromo_name + ", promo_image: "
						+ "https://imgrapp.com/release/" + mStringspromo_image
						+ ", promo_link: " + mString_link_promo
						+ ", promo_link_text: " + mString_link_promo
						+ ", promo_message_header: "
						+ mString_header_promo_message
						+ ", is_deleted: 0, modified_date: "
						+ mStringsmodified_date
						+ ", isActive: 1, is_enabled: 1");

				boolean flag = mDatasourceHandler.insertSponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				if (flag) {
					fetchChatHistory();
				}

			} else {

			}

		} catch (JSONException e) {

			// Log.e("", e.getMessage());
		}

	}

	private void Personal_List_update(String response) {
		try {
			String message_send_local_notify = null;
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				JSONObject sys = jsonOBject.getJSONObject("data");
				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				boolean flag = mDatasourceHandler.update_Sponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

				// Log.e("flag", "" + flag);

				JSONObject json = new JSONObject();
				mArrayPersonal = new JSONArray();
				JSONObject objMainList = new JSONObject();

				try {
					message_send_local_notify = personaljsonObj
							.getString("_message_body_tag");
					json.put("pushkey", personaljsonObj.getString("pushkey"));
					json.put("_message_body_tag",
							personaljsonObj.getString("_message_body_tag"));
					json.put("_promo_id_tag",
							personaljsonObj.getString("_promo_id_tag"));
					json.put("preview_Icon", "null");

				} catch (Exception e) {

				}

				mArrayPersonal.put(json);

				try {
					objMainList.put("body", mArrayPersonal);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String date = date();

				String timestamp = timestamp();

				int history_count = mDatasourceHandler
						.HISTORYUpdateRecentReceiveCount(message.getPacketID());
				if (history_count == 0) {
					int countincrease = 0;
					countincrease = mDatasourceHandler
							.UpdateRecentReceiveCount(personal_jidto,
									personal_jidfrom, objMainList.toString(),
									personal_api_time, date, timestamp);
					String mString = "" + (countincrease + 1);

					boolean flag1 = mDatasourceHandler.UpdateRecentReceive(
							personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time, date,
							timestamp, mString, "no", mStringPassword);
					// Log.e("RECEIVE 2 flag:", "" + flag);
					if (!flag1) {
						/*
						 * mDatasourceHandler.insertrecentMessage(
						 * objMainList.toString(), personal_jidfrom,
						 * personal_jidto, "", "", date, personal_api_time,
						 * timestamp, "1", "no", mStringPassword);
						 */
					}

				}
				boolean unique_test = mDatasourceHandler
						.Unique_message_Id(message.getPacketID());
				if (unique_test) {
					mDatasourceHandler.insertChathistory(message.getPacketID(),
							"username", personal_jidto, personal_jidfrom,
							objMainList.toString(), personal_api_time,
							mStringspromoid, "1", date,
							getApplicationContext(), number, "no");
					if (UILApplication.activityVisible) {

					} else {
						String usernam = null;
						Cursor mCursor = mDatasourceHandler
								.FETCH_(personal_jidfrom.split("_")[0]);
						// Log.e("mCursor: ", "" + mCursor.getCount());

						if (mCursor.getCount() != 0) {

							if (mCursor.moveToFirst()) {
								usernam = mCursor.getString(1).trim();
							}

							mCursor.close();

						} else {
							usernam = personal_jidfrom.split("_")[0];

							mCursor.close();
						}
						// Log.e("SEEEE: ",
						// "Application is not visible TESTING");
						new LocalNotificationSend(connection.isConnected(),
								getApplicationContext(),
								message_send_local_notify, usernam);
					}
				}

				// promoID = null;
				personal_jidto = null;
				personal_jidfrom = null;

			} else {

			}

		} catch (JSONException e) {

			// Log.e("", e.getMessage());
		}

	}

	public void sharedPrefernces() {
		editor = mSharedPreferences.edit();
		editor.putString("Notification_Click_Message", "false");

		editor.commit();

	}
	
	private void addContact(String name, String phone) {
        ContentValues values = new ContentValues();
        values.put(People.NUMBER, phone);
        values.put(People.TYPE, Phone.TYPE_CUSTOM);
        values.put(People.LABEL, name);
        values.put(People.NAME, name);
        Uri dataUri = getContentResolver().insert(People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(People.Phones.TYPE, People.TYPE_MOBILE);
        values.put(People.NUMBER, phone);
        updateUri = getContentResolver().insert(updateUri, values);
      }

}
