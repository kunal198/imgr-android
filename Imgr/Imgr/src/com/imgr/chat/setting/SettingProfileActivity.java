package com.imgr.chat.setting;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.R;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.edit.EditProfileActivity;
import com.joooonho.SelectableRoundedImageView;

public class SettingProfileActivity extends AppBaseActivity implements
		OnClickListener {
	TextView mImageView_back, mTextViewname, mTextViewphoneno, mTextViewemail;
	Button mButtonEdit;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	SelectableRoundedImageView mImageViewProfile;

	String mStringFirstname, mStringLastname, mStringBase64, mStringEmail,
			mStringPhone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_profile);
		registerBaseActivityReceiver();
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mImageView_back = (TextView) findViewById(R.id.btn_back_loginpage);
		mImageViewProfile = (SelectableRoundedImageView) findViewById(R.id.image_profile);
		mButtonEdit = (Button) findViewById(R.id.btn_edit);
		mTextViewname = (TextView) findViewById(R.id.textview_name);
		mTextViewphoneno = (TextView) findViewById(R.id.textview_phone);
		mTextViewemail = (TextView) findViewById(R.id.textview_email);
		mImageView_back.setOnClickListener(this);
		mButtonEdit.setOnClickListener(this);

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			mCursor = mDatasourceHandler.ProfileInfo();
			Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {

			do {
				mStringFirstname = mCursor.getString(0).trim();
				mStringLastname = mCursor.getString(1).trim();
				mStringPhone = mCursor.getString(2).trim();
				mStringEmail = mCursor.getString(3).trim();
				mStringBase64 = mCursor.getString(4).trim();

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		mTextViewname.setText(mStringFirstname + " " + mStringLastname);
		mTextViewemail.setText(mStringEmail);
		mTextViewphoneno.setText(mStringPhone);
		Bitmap mBitmap;
		if (mStringBase64.equals("")) {
			mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
			mImageViewProfile.setOval(true);
			mImageViewProfile
					.setBackgroundResource(R.drawable.ic_contact_place);
		} else if (mStringBase64 != null) {
			mBitmap = ConvertToImage(mStringBase64);
			mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
			mImageViewProfile.setOval(true);
			mImageViewProfile.setImageBitmap(mBitmap);
		} else {
			mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
			mImageViewProfile.setOval(true);
			mImageViewProfile
					.setBackgroundResource(R.drawable.ic_contact_place);
		}

	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.btn_back_loginpage:
			finish();
			overridePendingTransition(R.anim.slide_out_left,
					R.anim.slide_in_right);
			break;

		case R.id.btn_edit:
			Intent mIntent = new Intent(SettingProfileActivity.this,
					EditProfileActivity.class);
			startActivity(mIntent);
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		default:
			break;
		}
	}

}
