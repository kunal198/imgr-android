package com.imgr.chat.setting;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.AppBaseFragment;
import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.SponsoredPersonalModel;

/**
 * author: amit agnihotri
 */
public class SettingPromoActivity extends AppBaseActivity {
	TextView mImageView_back;
	Button button_promo;
	LinearLayout mLinearLayout_two, mLinearLayout_fourth;

	Switch switch_sponsored, switch_promons_personal,
			switch_auto_rotate_promons;
	SharedPreferences sharedPreferences;
	Editor editor;
	String mStringSponsored, mStringPersonal, mStringAutorotate;
	AlertDialog pinDialog;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	ArrayList<String> mArrayListpromoname;
	ArrayList<String> mArrayListpromoid;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListisenabled;
	ArrayList<SponsoredPersonalModel> mSponsoredModels;
	ArrayList<String> mArrayListpersonal_or;
	int personal_size = 0;

	ArrayList<String> mArrayListImageUrl;
	ArrayList<String> mArrayListheader;
	ArrayList<String> mArrayListlink_footer;
	ArrayList<String> mArrayListlink_text_footer;

	ArrayList<String> mArrayListPullid_promo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_promo);
		registerBaseActivityReceiver();
		mImageView_back = (TextView) findViewById(R.id.btn_back_loginpage);
		mLinearLayout_two = (LinearLayout) findViewById(R.id.linear_two);
		mLinearLayout_fourth = (LinearLayout) findViewById(R.id.linear_fourth);
		button_promo = (Button) findViewById(R.id.button_promo_create);
		switch_sponsored = (Switch) findViewById(R.id.switch_sponsored);
		switch_promons_personal = (Switch) findViewById(R.id.switch_promons_personal);
		switch_auto_rotate_promons = (Switch) findViewById(R.id.switch_auto_rotate_promons);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringSponsored = sharedPreferences.getString(Constant.SPONSORED_ADS,
				"");
		mStringPersonal = sharedPreferences
				.getString(Constant.PERSONAL_ADS, "");
		mStringAutorotate = sharedPreferences.getString(
				Constant.SETTING_PROMO_AUTO_ROTATE, "");
		if (mStringSponsored.equals("true")) {
			switch_sponsored.setChecked(true);
			mLinearLayout_two.setVisibility(View.VISIBLE);
		} else {
			switch_sponsored.setChecked(false);
			mLinearLayout_two.setVisibility(View.GONE);
		}
		if (mStringPersonal.equals("true")) {
			switch_promons_personal.setChecked(true);
			mLinearLayout_fourth.setVisibility(View.VISIBLE);
		} else {
			switch_promons_personal.setChecked(false);
			mLinearLayout_fourth.setVisibility(View.GONE);
		}

		if (mStringAutorotate != null) {
			if (mStringAutorotate.equals("true")) {
				switch_auto_rotate_promons.setChecked(true);
			} else {
				switch_auto_rotate_promons.setChecked(false);
			}
		} else {
			switch_auto_rotate_promons.setChecked(false);
			shared_Prefernces_Auto_Rotate("false");
		}
		switch_auto_rotate_promons
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							shared_Prefernces_Auto_Rotate("true");

						} else {
							shared_Prefernces_Auto_Rotate("false");
						}

					}
				});

		switch_sponsored
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mLinearLayout_two.setVisibility(View.VISIBLE);
							shared_Prefernces_Sponsorned("true");
						} else {
							mLinearLayout_two.setVisibility(View.GONE);
							shared_Prefernces_Sponsorned("false");
						}

					}
				});
		switch_promons_personal
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							mLinearLayout_fourth.setVisibility(View.VISIBLE);
							shared_Prefernces_Personal("true");
						} else {
							mLinearLayout_fourth.setVisibility(View.GONE);
							shared_Prefernces_Personal("false");
						}

					}
				});
		mImageView_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		mLinearLayout_two.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SettingPromoActivity.this,
						ViewSponsored.class);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		mLinearLayout_fourth.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent = new Intent(SettingPromoActivity.this,
						ViewPersonalPromo.class);
				startActivity(mIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});

		button_promo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (personal_size == 12) {
					New_again_popup();
				} else {
					Intent i = new Intent(SettingPromoActivity.this,
							NewPromoActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_right);
				}

				// finish();
			}
		});

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		value_pick();
	}

	protected void New_again_popup() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.pop_up_promo_not_created, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);

		pinDialog = new AlertDialog.Builder(SettingPromoActivity.this).setView(
				v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						// sharedPrefernces();
						pinDialog.cancel();
						// finish();
						// overridePendingTransition(R.anim.slide_out_left,
						// R.anim.slide_in_right);

					}
				});

			}
		});

		pinDialog.show();

	}

	public void value_pick() {

		// 18 sept start
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mSponsoredModels = new ArrayList<SponsoredPersonalModel>();
		mSponsoredModels.clear();
		mArrayListpromoname = new ArrayList<String>();
		mArrayListpromoid = new ArrayList<String>();
		mArrayListisdelete = new ArrayList<String>();
		mArrayListisactive = new ArrayList<String>();
		mArrayListisenabled = new ArrayList<String>();
		mArrayListpersonal_or = new ArrayList<String>();
		mArrayListheader = new ArrayList<String>();
		mArrayListlink_footer = new ArrayList<String>();
		mArrayListlink_text_footer = new ArrayList<String>();
		mArrayListImageUrl = new ArrayList<String>();
		mArrayListPullid_promo = new ArrayList<String>();
		// closed

		mCursor = mDatasourceHandler.SponsoredData();
		Log.e("SponsoredData count: ", mCursor.getCount() + "");

		if (mCursor.getCount() != 0) {

			mArrayListpromoname.clear();
			mArrayListpromoid.clear();
			mArrayListisdelete.clear();
			mArrayListisactive.clear();
			mArrayListisenabled.clear();
			mArrayListpersonal_or.clear();
			mArrayListheader.clear();
			mArrayListlink_footer.clear();
			mArrayListlink_text_footer.clear();
			mArrayListImageUrl.clear();
			mArrayListPullid_promo.clear();

			do {

				mArrayListpromoid.add(mCursor.getString(0).trim());
				mArrayListisactive.add(mCursor.getString(1).trim());
				mArrayListisdelete.add(mCursor.getString(2).trim());
				mArrayListisenabled.add(mCursor.getString(3).trim());
				mArrayListpromoname.add(mCursor.getString(4).trim());
				mArrayListpersonal_or.add(mCursor.getString(5).trim());

			} while (mCursor.moveToNext());

			mCursor.close();

			for (int i = 0; i < mArrayListpromoid.size(); i++) {

				if (mArrayListpersonal_or.get(i).equals("personal")) {
					if (mArrayListisactive.get(i).equals("1")
							&& mArrayListisdelete.get(i).equals("0")) {

						SponsoredPersonalModel mSponsoredModel = new SponsoredPersonalModel(
								mArrayListpromoid.get(i),
								mArrayListisenabled.get(i),
								mArrayListpromoname.get(i),
								mArrayListisactive.get(i),
								mArrayListisdelete.get(i));

						mSponsoredModels.add(mSponsoredModel);
					}
				}

			}
			Log.e("Size: ", " " + mSponsoredModels.size());
			personal_size = mSponsoredModels.size();
		}
	}

	public void shared_Prefernces_Sponsorned(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.SPONSORED_ADS, mString);

		editor.commit();

	}

	public void shared_Prefernces_Auto_Rotate(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.SETTING_PROMO_AUTO_ROTATE, mString);
		editor.commit();
		String is_autorotate_on_off = sharedPreferences.getString(
				Constant.SETTING_PROMO_AUTO_ROTATE, "");
		Log.e("shared valueeeee", "" + is_autorotate_on_off);

	}

	public void shared_Prefernces_Personal(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.PERSONAL_ADS, mString);

		editor.commit();

	}

}
