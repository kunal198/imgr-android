package com.imgr.chat.connector;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.imgr.chat.JsonParser.JSONARRAY;
import com.imgr.chat.JsonParser.JSONParserPost;
import com.imgr.chat.JsonParser.SocketConnection;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.model.AllContactModel;
import com.imgr.chat.model.SingleModelAdd;
import com.imgr.chat.util.ImgrContact;

public class JsonParserConnector {
	static String mString_success;
	private static final String TAG = "JSONParser handler";
	static JSONObject jsonobject;

	public static String PostRegisterPhone(String apiKey, String appVersion,
			String countryCode, String deviceType, String phonenumber,
			String os, String deviceid) {
		try {
			String Url = Constant.Register_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			if (deviceid != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceid));
			}
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs
					.add(new BasicNameValuePair("appVersion", appVersion));
			nameValuePairs.add(new BasicNameValuePair("countryCode",
					countryCode));

			nameValuePairs
					.add(new BasicNameValuePair("deviceType", deviceType));
			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));
			nameValuePairs.add(new BasicNameValuePair("os", os));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return mString_success;
	}

	public static String PostPersonalInfo(String apiKey, String appVersion,
			String countryCode, String deviceType, String phonenumber,
			String os, String deviceid, String Email, String mStringfirstname,
			String mStringlastname, String mStringBase, String mStringExt) {
		try {
			String Url = Constant.USER_INFO_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			if (deviceid != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceid));
			}
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs
					.add(new BasicNameValuePair("appVersion", appVersion));
			nameValuePairs.add(new BasicNameValuePair("countryCode",
					countryCode));

			nameValuePairs.add(new BasicNameValuePair("firstname",
					mStringfirstname));
			nameValuePairs.add(new BasicNameValuePair("nickname",
					mStringlastname));
			nameValuePairs.add(new BasicNameValuePair("email", Email));
			nameValuePairs
					.add(new BasicNameValuePair("image_path", mStringBase));
			nameValuePairs.add(new BasicNameValuePair("image_extension",
					mStringExt));

			nameValuePairs
					.add(new BasicNameValuePair("deviceType", deviceType));
			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));
			nameValuePairs.add(new BasicNameValuePair("os", os));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return mString_success;
	}

	public static String PostVerfication(String activationcode, String apiKey,
			String deviceId, String phonenumber) {
		try {
			String Url = Constant.verifyRegistration_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("activationcode",
					activationcode));
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			if (deviceId != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceId));
			}

			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			// Log.e("jsonobject: ", "" + jsonobject);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return mString_success;
	}

	public static String PostImgrContact(String apiKey,
			ArrayList<String> contact_phonenumber) {
		JSONObject jsonobject = null;

		try {
			int totalElements = contact_phonenumber.size();

			String Url = Constant.IMGERContact;

			ArrayList<String> newphone = new ArrayList<String>();
			newphone.clear();

			for (int i = 0; i < totalElements; i++) {

				newphone.add(contact_phonenumber.get(i).replace("[(", "")
						.replace(")", "").replace(" ", "").replace(")]", "")
						.replace("(", "").replace("-", "").replace("]", "")
						.replace("[", ""));

			}

			int total_Elements = newphone.size();

			contact_phonenumber.clear();
			for (int i = 0; i < total_Elements; i++) {
				if (newphone.get(i).length() >= 10) {

					contact_phonenumber.add(newphone.get(i).substring(
							newphone.get(i).length() - 10));
				} else {

					contact_phonenumber.add(newphone.get(i));
				}

			}

			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs
					.add(new BasicNameValuePair("phonenumbers",
							contact_phonenumber.toString().replace("[(", "")
									.replace(")", "").replace(" ", "")
									.replace(")]", "").replace("(", "")
									.replace("-", "").replace("]", "")
									.replace("[", "")));

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}

		return jsonobject.toString();
	}

	public static String SINGLE_PostImgrContact(String apiKey,
			String phonenumber) {
		JSONObject jsonobject = null;

		try {

			String Url = Constant.IMGERContact;

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phonenumbers",
					phonenumber));

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}
		// Log.e("see jsonobject: ", "" + jsonobject);
		return jsonobject.toString();
	}

	public static String OWNID(String response) {

		String MEMBERID = null;
		try {

			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);

					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					if (mStringsImgrUser.equals("0")) {
						MEMBERID = "";
					} else {
						MEMBERID = (jsonOBjectContact.getString("memberId"));
					}

				}

			} else {

			}

		} catch (JSONException e) {

			Log.e(TAG, e.getMessage());
		}

		return MEMBERID;

	}

	public static String PostEditSponsored(String apiKey,
			ArrayList<String> phonenumber, String username, String deviceid,
			ArrayList<String> promo_isenabled) {
		JSONObject json_object = null;

		try {

			String Url = Constant.PromoStatus_EDIT;

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("username", username
					+ "_" + deviceid));
			nameValuePairs.add(new BasicNameValuePair("is_enable",
					promo_isenabled.toString().replace("[(", "")
							.replace(")", "").replace(" ", "")
							.replace(")]", "").replace("(", "")
							.replace("-", "")));

			nameValuePairs.add(new BasicNameValuePair("promo_id", phonenumber
					.toString().replace("[(", "").replace(")", "")
					.replace(" ", "").replace(")]", "").replace("(", "")
					.replace("-", "")));

			json_object = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}
		// Log.e("see jsonobject: ", "" + jsonobject);
		return json_object.toString();
	}

	public static String AllContact(String apiKey,
			ArrayList<String> phonenumber, ArrayList<String> contactname) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.IMGERContact;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phonenumbers",
					phonenumber.toString().replace("[(", "").replace(")", "")
							.replace(" ", "").replace(")]", "")
							.replace("(", "").replace("-", "").replace("]", "")
							.replace("[", "")));
			// Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);
			// Log.e("jsonobject: ", "" + jsonobject);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static List<AllContactModel> ModelAllContactNormalUser(
			String strJson, ArrayList<String> phonenumber,
			ArrayList<String> contactname, ArrayList<String> contactlastname,
			ArrayList<String> mArrayListContactId) {

		List<AllContactModel> mContactModels = null;
		try {

			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mContactModels = new ArrayList<AllContactModel>();

				JSONArray jArray = jsonOBject.getJSONArray("data");
				// Log.e("jArray.length(): ", "" + jArray);

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					final AllContactModel mContact = new AllContactModel();
					mContact.setPhonenumber(jsonOBjectContact
							.getString("phonenumber").replace("[(", "")
							.replace(")", "").replace(" ", "")
							.replace(")]", "").replace("(", "")
							.replace("-", "").replace("]", "").replace("[", ""));

					mContact.setPhoneid(mArrayListContactId.get(i));
					mContact.setMemberId(jsonOBjectContact
							.getString("memberId"));

					mContact.setFirstName(contactname.get(i));
					mContact.setLastName(contactlastname.get(i));
					mContact.setJid(jsonOBjectContact.getString("jid"));
					mContact.setIsImgrUser(jsonOBjectContact
							.getString("isImgrUser"));
					mContact.setBase64(jsonOBjectContact.getString("image"));

					mContactModels.add(mContact);

				}

			} else {
				mContactModels = new ArrayList<AllContactModel>();
			}

		} catch (JSONException e) {
			mContactModels = new ArrayList<AllContactModel>();
			Log.e(TAG, e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mContactModels.size());

		return mContactModels;

	}

	public static List<AllContactModel> ModelIMGRContact(String strJson,
			ArrayList<String> phonenumber, ArrayList<String> contactname,
			ArrayList<String> mArrayListContactid, ArrayList<String> contactlast) {

		List<AllContactModel> mContactModels = null;

		// mContactModels.clear();
		try {

			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mContactModels = new ArrayList<AllContactModel>();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					AllContactModel mContact = new AllContactModel();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setPhonenumber(phonenumber.get(i)
								.replace("[(", "").replace(")", "")
								.replace(" ", "").replace(")]", "")
								.replace("(", "").replace("-", "")
								.replace("]", "").replace("[", ""));
						// mContact.setPhonenumber(jsonOBjectContact
						// .getString("phonenumber").replace("[(", "")
						// .replace(")", "").replace(" ", "")
						// .replace(")]", "").replace("(", "")
						// .replace("-", "").replace("]", "")
						// .replace("[", ""));
						mContact.setFirstName(contactname.get(i));
						mContact.setLastName(contactlast.get(i));
						mContact.setPhoneid(mArrayListContactid.get(i));
						Log.e("memberId: ",
								"" + jsonOBjectContact.getString("memberId"));

						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mContact.setBase64(jsonOBjectContact.getString("image"));

						mContactModels.add(mContact);
					}

				}

			} else {
				mContactModels = new ArrayList<AllContactModel>();
			}

		} catch (JSONException e) {
			mContactModels = new ArrayList<AllContactModel>();
			Log.e(TAG, e.getMessage());
		}
		Log.e(TAG, "mImgrContacts.SIZE: " + mContactModels.size());

		return mContactModels;

	}

	public static List<AllContactModel> FirstModelIMGRContact(String strJson,
			ArrayList<String> phonenumber, ArrayList<String> contactname,
			ArrayList<String> contactlast, ArrayList<String> mArrayListContactid) {

		List<AllContactModel> mContactModels = null;
		try {

			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mContactModels = new ArrayList<AllContactModel>();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					AllContactModel mContact = new AllContactModel();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");
					// Log.e("IMGR TEST mStringsImgrUser: ", "" +
					// mStringsImgrUser);

					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setFirstName(contactname.get(i));
						mContact.setLastName(contactlast.get(i));
						mContact.setPhonenumber(jsonOBjectContact
								.getString("phonenumber").replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", ""));
						mContact.setPhoneid(mArrayListContactid.get(i));
						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mContact.setBase64(jsonOBjectContact.getString("image"));

						mContactModels.add(mContact);
					}

				}

			} else {
				mContactModels = new ArrayList<AllContactModel>();
			}

		} catch (JSONException e) {
			mContactModels = new ArrayList<AllContactModel>();
			Log.e(TAG, e.getMessage());
		}

		return mContactModels;

	}

	public static List<ImgrContact> PostImgrContact(String strJson) {

		List<ImgrContact> mImgrContacts = null;
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mImgrContacts = new ArrayList<ImgrContact>();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					ImgrContact mContact = new ImgrContact();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					String mStringsph = jsonOBjectContact
							.getString("phonenumber").replace("[(", "")
							.replace(")", "").replace(" ", "")
							.replace(")]", "").replace("(", "")
							.replace("-", "").replace("]", "").replace("[", "");
					// Log.e("mStringsph: ", "" + mStringsph);
					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setPhonenumber(jsonOBjectContact
								.getString("phonenumber"));
						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mImgrContacts.add(mContact);
					}

				}

			} else {
				mImgrContacts = new ArrayList<ImgrContact>();
			}

		} catch (JSONException e) {
			mImgrContacts = new ArrayList<ImgrContact>();
			Log.e(TAG, e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mImgrContacts.size());
		// Log.e(TAG, "mImgrContacts: " + mImgrContacts);

		return mImgrContacts;

	}

	public static String AddContactPhone(String apiKey, String phonenumber) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.IMGERContact;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phonenumbers",
					phonenumber.toString().replace("[(", "").replace(")", "")
							.replace(" ", "").replace(")]", "")
							.replace("(", "").replace("-", "").replace("]", "")
							.replace("[", "")));
			// Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static List<SingleModelAdd> SingleContactupdate(String strJson) {

		List<SingleModelAdd> mSingleModelAdds = null;
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(strJson);
			if (jsonOBject.getBoolean("success")) {
				mSingleModelAdds = new ArrayList<SingleModelAdd>();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					SingleModelAdd mContact = new SingleModelAdd();
					String mStringsImgrUser = jsonOBjectContact
							.getString("isImgrUser");

					if (mStringsImgrUser.equals("0")) {

					} else {
						mContact.setPhonenumber(jsonOBjectContact
								.getString("phonenumber").replace("[(", "")
								.replace(")", "").replace(" ", "")
								.replace(")]", "").replace("(", "")
								.replace("-", "").replace("]", "")
								.replace("[", ""));
						mContact.setMemberId(jsonOBjectContact
								.getString("memberId"));
						mContact.setJid(jsonOBjectContact.getString("jid"));
						mContact.setIsImgrUser(jsonOBjectContact
								.getString("isImgrUser"));
						mContact.setBase64(jsonOBjectContact.getString("image"));
						mSingleModelAdds.add(mContact);
					}

				}

			} else {
				mSingleModelAdds = new ArrayList<SingleModelAdd>();
			}

		} catch (JSONException e) {
			mSingleModelAdds = new ArrayList<SingleModelAdd>();
			Log.e(TAG, e.getMessage());
		}
		// Log.e(TAG, "mImgrContacts.SIZE: " + mImgrContacts.size());
		// Log.e(TAG, "mImgrContacts: " + mImgrContacts);

		return mSingleModelAdds;

	}

	public static String ChatHistory(String apiKey, String fromnumber,
			String tophone) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.CHAT_HISTORY;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("from", fromnumber));
			nameValuePairs.add(new BasicNameValuePair("to", tophone));
			// mStringPhone

			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String PushNotification(String deviceId, String phonenumber) {
		try {
			String Url = Constant.verifyRegistration_URL;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			if (deviceId != null) {
				nameValuePairs
						.add(new BasicNameValuePair("deviceId", deviceId));
			}

			nameValuePairs.add(new BasicNameValuePair("phonenumber",
					phonenumber));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);
			// Log.e("jsonobject: ", "" + jsonobject);

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String getDevice(String apiKey, String username,
			String deviceToken, String deviceId) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.DeviceToken + "deviceToken=" + deviceToken + "&"
				+ "apiKey=imgr" + "&" + "username=" + username + "&"
				+ "deviceId=" + deviceId;

		String result = jsonarray.getJSONFromUrl(Url);

		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			mString_success = jsonobject.getString("success");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (mString_success.equals("true")) {

		} else {
			try {
				mString_success = jsonobject.getString("error");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return mString_success;
	}

	public static String getSponsored(String ispersonal, String username,
			String deviceId) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.SponsoredAds + "apiKey=imgr&isPersonal="
				+ ispersonal + "&username=" + username + "_" + deviceId;

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String getPERSONALSponsored(String id) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.PERSONAL_PROMO_CHAT_FETCH + id;
		Log.e("result-->>", "" + Url);
		String result = jsonarray.getJSONFromUrl(Url);

		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String get_SponsoredAds(String isenabled, String username,
			String deviceId, String promoid) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.GET_PromoStatus_EDIT + "apiKey=imgr&is_enable="
				+ isenabled + "&promo_id=" + promoid + "&username=" + username
				+ "_" + deviceId;

		String result = jsonarray.getJSONFromUrl(Url);

		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String ALL_TIME_CALL_NOTIFICATION(String sender_id,
			String receiver_id, String message, String messagePayload,
			String message_id) {
		try {

			// String message_id = receiver_id+"_"+timestamp();

			String Url = Constant.CALL_ALL_TIME_CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();
			String messageUTF8 = URLEncoder.encode(message, "utf-8");
			messageUTF8 = messageUTF8.replaceAll("\\+", "%20");
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("sender_id", sender_id));
			nameValuePairs.add(new BasicNameValuePair("receiver_id",
					receiver_id));

			nameValuePairs.add(new BasicNameValuePair("type", "2"));
			nameValuePairs.add(new BasicNameValuePair("message", messageUTF8));

			nameValuePairs
					.add(new BasicNameValuePair("message_id", message_id));
			nameValuePairs.add(new BasicNameValuePair("messagePayload",
					messagePayload));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);

			Log.e("parametersssssssssssssssssssssssss", "" + sender_id + "____"
					+ receiver_id + "____" + messageUTF8 + "____"
					+ messagePayload + "____" + message_id);
			
//			Log.e("Api Key "<""+)

			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {
				Log.e("successssssssss", "sadfersterytrtyrty");
			} else {
				mString_success = jsonobject.getString("error");
				Log.e("errorrrrrrrrrrr", "sadfersterytrtyrty");
			}

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		// try {
		// Log.e("sender_id-->>", "" + sender_id);
		// Log.e("receiver_id-->>", "" + receiver_id);
		// JSONARRAY jsonarray = new JSONARRAY();
		//
		// String messageUTF8 = URLEncoder.encode(message, "utf-8");
		// messageUTF8 = messageUTF8.replaceAll("\\+", "%20");
		//
		// Log.e("mesageeeeeeeeeeeeeeeeee",messageUTF8);
		// String message_id = receiver_id+"_"+timestamp();
		//
		// String Url = Constant.CALL_ALL_TIME_CHAT_NOTIFICATION
		// + "apiKey=imgr&sender_id=" + sender_id + "&sender_id="
		// + receiver_id + "&type=2&version=1.2&message=" + messageUTF8;
		// Log.e("Url-->>", "" + Url);
		// String result = jsonarray.getJSONFromUrl(Url);
		//
		//
		// Log.e("result-->>", "" + result);
		// }
		// catch (Exception e)
		// {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// JSONObject jsonobject = null;
		// try {
		// jsonobject = new JSONObject(result);
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return mString_success;
	}

	public static String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public static String post_ChatNotificaton(String to, String message,
			String from, String localdatabase, String username, String fromJID,
			String toJID) {
		try {
			String Url = Constant.CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("to", to));

			nameValuePairs.add(new BasicNameValuePair("message", message));
			nameValuePairs.add(new BasicNameValuePair("localdatabase",
					localdatabase));

			nameValuePairs.add(new BasicNameValuePair("username", username));

			nameValuePairs.add(new BasicNameValuePair("from", from));
			nameValuePairs.add(new BasicNameValuePair("fromJID", fromJID));
			nameValuePairs.add(new BasicNameValuePair("toJID", toJID));

			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);

			mString_success = "";
			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String new_post_ChatNotificaton(String to, String from,
			String message, String fromjid, String tojid, String time,
			String timestamp, String date, String localdatabase,
			String messagepacketid, String memberid, String Contactuniqueid,
			String receiveornot, String bubblecolor, String fontsize,
			String username, String profileimage, String recentfromjid,
			String recenttojid) {
		try {

			String Url = Constant.CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("to", to));
			nameValuePairs.add(new BasicNameValuePair("from", from));
			nameValuePairs.add(new BasicNameValuePair("message", message));
			nameValuePairs.add(new BasicNameValuePair("fromJID", fromjid));
			nameValuePairs.add(new BasicNameValuePair("toJID", tojid));

			nameValuePairs.add(new BasicNameValuePair("time", time));
			nameValuePairs.add(new BasicNameValuePair("timestamp", timestamp));
			nameValuePairs.add(new BasicNameValuePair("date", date));
			nameValuePairs.add(new BasicNameValuePair("localdatabase",
					localdatabase));
			nameValuePairs.add(new BasicNameValuePair("messsgepacketId",
					messagepacketid));
			nameValuePairs.add(new BasicNameValuePair("memberid", memberid));
			nameValuePairs.add(new BasicNameValuePair("contactuniqueid",
					Contactuniqueid));
			nameValuePairs.add(new BasicNameValuePair("receiveornot",
					receiveornot));
			nameValuePairs.add(new BasicNameValuePair("bubblecolor",
					bubblecolor));
			nameValuePairs.add(new BasicNameValuePair("fontsize", fontsize));
			nameValuePairs.add(new BasicNameValuePair("username", username));
			nameValuePairs.add(new BasicNameValuePair("profilepicture",
					profileimage));
			nameValuePairs.add(new BasicNameValuePair("recenttojid",
					recenttojid));
			nameValuePairs.add(new BasicNameValuePair("recentfromjid",
					recentfromjid));
			Log.e("nameValuePairs:- ", "" + nameValuePairs);
			JSONObject jsonobject = mJsonParserPost.getJSONFromUrl(Url,
					nameValuePairs);

			mString_success = "";
			mString_success = jsonobject.getString("success");
			if (mString_success.equals("true")) {

			} else {
				mString_success = jsonobject.getString("error");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return mString_success;
	}

	public static String post_receive_ChatNotificaton(String to) {
		JSONObject jsonobject = null;
		try {
			String Url = Constant.AFTER_CHAT_NOTIFICATION;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("to", to));

			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String PersonalPromoInfo(String promo_name,
			String promo_image, String promo_link, String promo_link_text,
			String promo_message_header, String finaltype, String username) {
		try {

			String Url = Constant.CREATE_PERSONAL_PROMO;
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs
					.add(new BasicNameValuePair("promo_name", promo_name));

			nameValuePairs.add(new BasicNameValuePair("promo_image_url",
					promo_image));

			nameValuePairs
					.add(new BasicNameValuePair("promo_link", promo_link));
			nameValuePairs.add(new BasicNameValuePair("promo_link_text",
					promo_link_text));
			nameValuePairs.add(new BasicNameValuePair("promo_message_header",
					promo_message_header));
			nameValuePairs.add(new BasicNameValuePair("username", username));

			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String UpdatePersonalPromoInfo(String promo_name,
			String promo_image, String promo_link, String promo_link_text,
			String promo_message_header, String finaltype, String username,
			String promoid) {
		try {

			String Url = Constant.UPDATE_PERSONAL_PROMO;
			Log.e("Url: ", "" + Url);
			JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));
			nameValuePairs.add(new BasicNameValuePair("promo_id", promoid));
			nameValuePairs
					.add(new BasicNameValuePair("promo_name", promo_name));

			nameValuePairs.add(new BasicNameValuePair("promo_image_url",
					promo_image));

			nameValuePairs
					.add(new BasicNameValuePair("promo_link", promo_link));
			nameValuePairs.add(new BasicNameValuePair("promo_link_text",
					promo_link_text));
			nameValuePairs.add(new BasicNameValuePair("promo_message_header",
					promo_message_header));
			nameValuePairs.add(new BasicNameValuePair("username", username));

			Log.e("nameValuePairs: ", "" + nameValuePairs);
			jsonobject = mJsonParserPost.getJSONFromUrl(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash");

		}

		return jsonobject.toString();
	}

	public static String DeletePersonalPromo(String promoid, String username,
			String deviceId) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.DELETE_PERSONAL_PROMO + "apiKey=imgr&promo_id="
				+ promoid + "&username=" + username + "_" + deviceId;

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String getGalleryImage(String username) {

		JSONARRAY jsonarray = new JSONARRAY();

		String Url = Constant.PROMO_IMAGES_GALLERY + username;

		String result = jsonarray.getJSONFromUrl(Url);

		// Log.e("result-->>", "" + result);
		JSONObject jsonobject = null;
		try {
			jsonobject = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonobject.toString();
	}

	public static String PostMessageList(String apiKey, String phonenumber) {
		JSONObject jsonobject = null;

		try {

			String Url = Constant.RECENT_MESSAGE_LIST;

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs.add(new BasicNameValuePair("phoneno", phonenumber
					.toString().replace("[(", "").replace(")", "")
					.replace(" ", "").replace(")]", "").replace("(", "")
					.replace("-", "").replace("]", "").replace("[", "")));

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}
		// Log.e("see jsonobject: ", "" + jsonobject);
		return jsonobject.toString();
	}

	public static String PostBubbleColor(String colorcode, String jid) {
		JSONObject jsonobject = null;

		try {

			String Url = Constant.BUBBLE_COLOR;

			// Log.e("phonenumber:", "" + phonenumber);
			SocketConnection mSocketConnection = new SocketConnection();
			// JSONParserPost mJsonParserPost = new JSONParserPost();

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

			nameValuePairs.add(new BasicNameValuePair("apiKey", "imgr"));

			nameValuePairs
					.add(new BasicNameValuePair("bubble_color", colorcode));
			nameValuePairs.add(new BasicNameValuePair("jid", jid));

			jsonobject = mSocketConnection.postJSONObject(Url, nameValuePairs);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e("", "crash: " + ex.getMessage());
			Log.e("String", "crash: " + ex.toString());
			Log.e("Cause", "crash: " + ex.getCause());

		}

		return jsonobject.toString();
	}

}
