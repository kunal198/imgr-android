package com.imgr.chat.xmpp;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.ChatState;
import org.jivesoftware.smackx.ChatStateListener;

import android.util.Log;

public class chatstate implements ChatStateListener {

	public chatstate() {
		Log.v("", "Chat State Changed Listner Constructor");

	}

	@Override
	public void processMessage(Chat arg0, Message arg1) {
		Log.v("", "process");

	}

	@Override
	public void stateChanged(Chat chat, ChatState state) {

		if (state.toString().equals(ChatState.composing.toString())) {
			Log.v("", "typing...");
		} else if (state.toString().equals(ChatState.paused.toString())) {
			Log.v("", "Pause...");
		} else {
			Log.v("", "nothing...");
		}

	}

}
