package com.imgr.chat.xmpp;

import java.io.IOException;
import java.util.Collection;

import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.search.UserSearch;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.util.Log;

import com.service.AppSettings;

public class Connection_Pool {
	private XMPPConnection connection = null;
	ConnectionConfiguration config;
	private static Connection_Pool instance = null;

	// public synchronized static Connection_Pool getInstance() {
	// if (instance == null) {
	// instance = new Connection_Pool();
	// Log.i("CONNECTION INSTANCE:", "CREATED HERE");
	// }
	// return instance;
	// }

	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}

	// public XMPPConnection getConnection() {
	// return this.connection;
	// }

	public void diconnect_Connection() {

		if (connection.isConnected()) {
			connection.disconnect();
		}
	}

	public XMPPConnection getConnection(Context context) {

		if ((this.connection == null) || (!this.connection.isConnected())) {
			// if (this.connection == null) {
			try {
				connection = connect();
				try {
					connection.login(AppSettings.getUser(context),
							AppSettings.getPassword(context));
				} catch (Exception e) {
					e.printStackTrace();
				}
				context.sendBroadcast(new Intent("Imgrapp.loggedin"));

			} catch (XMPPException localXMPPException) {

			} catch (SaslException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return connection;
	}

	// public void login(String user, String pass, String username) throws
	// XMPPException, SaslException, IOException {
	//
	// StrictMode.ThreadPolicy policy = new
	// StrictMode.ThreadPolicy.Builder().permitAll().build();
	// StrictMode.setThreadPolicy(policy);
	// // if (connection != null) {
	// // connection.disconnect();
	// // }
	//
	//
	//
	// config = new ConnectionConfiguration("64.235.48.26", 5222);
	// config.setSASLAuthenticationEnabled(true);
	// config.setCompressionEnabled(true);
	// config.setSecurityMode(SecurityMode.enabled);
	// SASLAuthentication.registerSASLMechanism("PLAIN",
	// SASLPlainMechanism.class);
	// connection.DEBUG_ENABLED = true;
	// config.setSASLAuthenticationEnabled(true);
	// connection = new XMPPConnection(config);
	// // Log.e("tag", "connection 123 :" + connection);
	//
	// // ConnectionConfiguration config = new ConnectionConfiguration(
	// // "64.235.48.26", 5222);
	// // // config.DEBUG_ENABLED = true;
	// // XMPPConnection.DEBUG_ENABLED = true;
	// // config.setSecurityMode(SecurityMode.enabled);
	// // SASLAuthentication.supportSASLMechanism("MD5", 0);
	// // System.setProperty("smack.debugEnabled", "true");
	// //
	// // config.setSASLAuthenticationEnabled(true);
	// // config.setCompressionEnabled(false);
	// // // config.set
	// // config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
	// // config.setReconnectionAllowed(true);
	// // config.setSendPresence(true);
	// // config.setRosterLoadedAtLogin(true);
	// //
	// // if (Build.VERSION.SDK_INT >= 14) {
	// // config.setKeystoreType("AndroidCAStore");
	// // // config.setTruststorePassword(null);
	// // config.setKeystorePath(null);
	// // } else {
	// // config.setKeystoreType("BKS");
	// // String str = System.getProperty("javax.net.ssl.trustStore");
	// // if (str == null) {
	// // str = System.getProperty("java.home") + File.separator + "etc"
	// // + File.separator + "security" + File.separator
	// // + "cacerts.bks";
	// // }
	// // config.setKeystorePath(str);
	// // }
	// // if (connection == null) {
	// // this.connection = new XMPPConnection(config);
	// // }
	// // this.connection.connect();
	//
	// // Log.e("tag", "connection ::" + connection.isAuthenticated());
	//
	// // this.connection.connect();
	//
	// if (!connection.isConnected()) {
	// this.connection.connect();
	// }

	// Log.e("connected", "server connect");
	//
	// if (!connection.isAuthenticated()) {
	// return;
	// }

	public void login(String user, String pass, String username)
			throws XMPPException, SaslException, IOException {

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		XMPPConnection connect = connect();
		if (connect.isAuthenticated()) {
			return;
		}

		// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
		Log.e("tag" + user, "" + pass);
		// connection.login(user, pass);
		try 
		{

			connection.login(user, pass);

		} catch (IllegalStateException e) {

			Log.e("MessagingService",
					"Already Logged in as " + connection.getUser());
		}

		Presence presence = new Presence(Presence.Type.available);
		try {
			connection.sendPacket(presence);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Roster roster = connection.getRoster();
		Collection<RosterEntry> entries = roster.getEntries();
		for (RosterEntry entry : entries) {
			Log.e("XMPPChatDemoActivity",
					"--------------------------------------");
			Log.e("XMPPChatDemoActivity", "RosterEntry " + entry);
			Log.e("XMPPChatDemoActivity", "User: " + entry.getUser());
			Log.e("XMPPChatDemoActivity", "Name: " + entry.getName());
			Log.e("XMPPChatDemoActivity", "Status: " + entry.getStatus());
			Log.e("XMPPChatDemoActivity", "Type: " + entry.getType());
			Presence entryPresence = roster.getPresence(entry.getUser());

			Log.e("XMPPChatDemoActivity",
					"Presence Status: " + entryPresence.getStatus());
			Log.e("XMPPChatDemoActivity",
					"Presence Type: " + entryPresence.getMode());

			Presence.Type type = entryPresence.getType();
			if (type == Presence.Type.available)
				Log.e("XMPPChatDemoActivity", "Presence AVIALABLE");
			Log.e("XMPPChatDemoActivity", "Presence : " + entryPresence);

		}
	}

	public void configure(ProviderManager pm) {
		// Private Data Storage
		pm.addIQProvider("query", "jabber:iq:private",
				new PrivateDataManager.PrivateDataIQProvider());
		// Time
		try {
			pm.addIQProvider("query", "jabber:iq:time",
					Class.forName("org.jivesoftware.smackx.packet.Time"));
		} catch (ClassNotFoundException e) {
			Log.w("TestClient",
					"Can't load class for org.jivesoftware.smackx.packet.Time");
		}

		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

		// Roster Exchange
		pm.addExtensionProvider("x", "jabber:x:roster",
				new RosterExchangeProvider());

		// Message Events
		pm.addExtensionProvider("x", "jabber:x:event",
				new MessageEventProvider());

		// Chat State
		pm.addExtensionProvider("active",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("composing",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("paused",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("inactive",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		pm.addExtensionProvider("gone",
				"http://jabber.org/protocol/chatstates",
				new ChatStateExtension.Provider());

		// XHTML
		pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
				new XHTMLExtensionProvider());

		// Group Chat Invitations
		pm.addExtensionProvider("x", "jabber:x:conference",
				new GroupChatInvitation.Provider());

		// Service Discovery # Items
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());

		// Service Discovery # Info
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Data Forms
		pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());

		// MUC User
		pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
				new MUCUserProvider());

		// MUC Admin
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
				new MUCAdminProvider());

		// MUC Owner
		pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
				new MUCOwnerProvider());

		// SharedGroupsInfo
		pm.addIQProvider("sharedgroup",
				"http://www.jivesoftware.org/protocol/sharedgroup",
				new SharedGroupsInfo.Provider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
				new DiscoverItemsProvider());
		pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
				new DiscoverInfoProvider());

		// Delayed Delivery
		pm.addExtensionProvider("x", "jabber:x:delay",
				new DelayInformationProvider());

		// Version
		try {
			pm.addIQProvider("query", "jabber:iq:version",
					Class.forName("org.jivesoftware.smackx.packet.Version"));
		} catch (ClassNotFoundException e) {
			// Not sure what's happening here.
		}

		// VCard
		pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

		// Offline Message Requests
		pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
				new OfflineMessageRequest.Provider());

		// Offline Message Indicator
		pm.addExtensionProvider("offline",
				"http://jabber.org/protocol/offline",
				new OfflineMessageInfo.Provider());

		// Last Activity
		pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());

		// User Search
		pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());

		// JEP-33: Extended Stanza Addressing
		pm.addExtensionProvider("addresses",
				"http://jabber.org/protocol/address",
				new MultipleAddressesProvider());

		// FileTransfer
		pm.addIQProvider("si", "http://jabber.org/protocol/si",
				new StreamInitiationProvider());

		pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
				new BytestreamsProvider());

		// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
		// IBBProviders.Open());

		// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
		// IBBProviders.Close());

		// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
		// new
		// IBBProviders.Data());

		// Privacy
		pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());

		pm.addIQProvider("command", "http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider());
		pm.addExtensionProvider("malformed-action",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.MalformedActionError());
		pm.addExtensionProvider("bad-locale",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadLocaleError());
		pm.addExtensionProvider("bad-payload",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadPayloadError());
		pm.addExtensionProvider("bad-sessionid",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.BadSessionIDError());
		pm.addExtensionProvider("session-expired",
				"http://jabber.org/protocol/commands",
				new AdHocCommandDataProvider.SessionExpiredError());

		pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
				DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
		pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
				new DeliveryReceiptRequest().getNamespace(),
				new DeliveryReceiptRequest.Provider());
		/*
		 * pm.addExtensionProvider(ReadReceipt.ELEMENT, ReadReceipt.NAMESPACE,
		 * new ReadReceipt.Provider());
		 */
	}

	public static Connection_Pool getInstance() {
		if (instance == null) {
			instance = new Connection_Pool();
		}
		return instance;
	}

	public void close() {
		if (this.connection != null) {
			try {
				this.connection.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		instance = null;
	}

	private XMPPConnection connect() throws XMPPException, IOException {
		if ((this.connection != null) && (this.connection.isConnected())) {
			return this.connection;
		}
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		// ConnectionConfiguration config = new
		// ConnectionConfiguration("64.235.48.26", 5222);
		// connection.DEBUG_ENABLED = true;
		// SASLAuthentication.supportSASLMechanism("MD5", 0);
		// System.setProperty("smack.debugEnabled", "true");
		// // XMPPConnection.DEBUG_ENABLED = true;
		//
		// config.setSASLAuthenticationEnabled(true);
		// config.setCompressionEnabled(false);
		// // config.set
		// config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
		// config.setReconnectionAllowed(true);
		// config.setSendPresence(true);
		// config.setRosterLoadedAtLogin(true);
		//
		// if (Build.VERSION.SDK_INT >= 14) {
		// config.setKeystoreType("AndroidCAStore");
		// // config.setTruststorePassword(null);
		// config.setKeystorePath(null);
		// } else {
		// config.setKeystoreType("BKS");
		// String str = System.getProperty("javax.net.ssl.trustStore");
		// if (str == null) {
		// str = System.getProperty("java.home") + File.separator + "etc" +
		// File.separator + "security"
		// + File.separator + "cacerts.bks";
		// }
		// config.setKeystorePath(str);
		// }
		// if (connection == null) {
		// this.connection = new XMPPConnection(config);
		// }
		//
		// this.connection.connect();
		// Roster roster = connection.getRoster();
		// roster.addRosterListener(new RosterListener() {
		//
		// @Override
		// public void presenceChanged(Presence arg0) {
		// Log.d("deb", "ug");
		// }
		//
		// @Override
		// public void entriesUpdated(Collection<String> arg0) {
		// Log.d("deb", "ug");
		// }
		//
		// @Override
		// public void entriesDeleted(Collection<String> arg0) {
		// Log.d("deb", "ug");
		// }
		//
		// @Override
		// public void entriesAdded(Collection<String> arg0) {
		// Log.d("deb", "ug");
		// }
		// });

		config = new ConnectionConfiguration("64.235.48.26", 5222);
		config.setSASLAuthenticationEnabled(true);
		config.setCompressionEnabled(true);
		config.setSecurityMode(SecurityMode.enabled);
		SASLAuthentication.registerSASLMechanism("PLAIN",
				SASLPlainMechanism.class);
		connection.DEBUG_ENABLED = true;
		config.setSASLAuthenticationEnabled(true);
		connection = new XMPPConnection(config);
		this.connection.connect();
		return this.connection;
	}

}
