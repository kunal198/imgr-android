package com.imgr.chat.xmpp;

import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.provider.EmbeddedExtensionProvider;

public class ReadReceipt implements PacketExtension {

	public static final String NAMESPACE = "urn:xmpp:read";
	public static final String ELEMENT = "read";

	private String id; // / original ID of the delivered message

	public ReadReceipt(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getElementName() {
		return ELEMENT;
	}

	public String getNamespace() {
		return NAMESPACE;
	}

	public String toXML() {
		return "<read xmlns='" + NAMESPACE + "' id='" + id + "'/>";
	}

	public static class Provider extends EmbeddedExtensionProvider {

		@Override
		protected PacketExtension createReturnExtension(String arg0,
				String arg1, Map<String, String> attributeMap,
				List<? extends PacketExtension> arg3) {
			// TODO Auto-generated method stub
			return new ReadReceipt(attributeMap.get("id"));
		}
	}
}