package com.imgr.chat.model;

public class Product {
	public String phoneno;
	public String contactname;
	public String fullname;
	public String last;

	public boolean box;

	public Product(String _phone, String name, String last, String fullname,
			boolean _box) {
		phoneno = _phone;
		contactname = name;
		this.last = last;
		this.fullname = fullname;
		box = _box;
	}

	public String toLowerCase() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return this.contactname;
	}

	public String getNumber() {
		return this.phoneno;
	}

	public String getFullname() {
		return this.fullname;
	}

	public String getlast() {
		return this.last;
	}
}
