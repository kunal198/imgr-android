package com.imgr.chat.model;

public class CustomGalleryModel implements Comparable<CustomGalleryModel> {

	private String name;
	private String url;

	public CustomGalleryModel(String name, String url) {

		this.name = name;
		this.url = url;

	}

	public String getname() {
		return this.name;
	}

	public String geturl() {
		return this.url;
	}

	@Override
	public int compareTo(CustomGalleryModel another) {
		// TODO Auto-generated method stub
		return name.compareTo(another.name);
	}

}
