package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.imgr.chat.R;
import com.imgr.chat.adapter.EditSponsoredAdapter.Edit_Sponsored_Ads;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.ModelPersonalSponsoredSelect;
import com.imgr.chat.model.ModelSponsoredSelect;
import com.imgr.chat.setting.OpendetailPersonalPromo;
import com.imgr.chat.util.UI;

public class EditSponsoredPersonalAdapter extends BaseAdapter implements
		SectionIndexer {

	private ArrayList<String> contactnumber1;
	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;
	private static String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private ItemFilter mFilter = new ItemFilter();
	ArrayList<ModelPersonalSponsoredSelect> objects;
	private List<ModelPersonalSponsoredSelect> mPhoneModels = null;

	String Final_value, is_enable;
	String mStringPassword, deviceId;
	SharedPreferences sharedPreferences;
	boolean value_changed;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;

	public EditSponsoredPersonalAdapter(Activity fragmentActivity,
			ArrayList<String> contactname, ArrayList<String> contactnumber,
			List<ModelPersonalSponsoredSelect> products) {
		// TODO Auto-generated constructor stub
		this.mPhoneModels = products;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		// objects = products;
		context = fragmentActivity;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.objects = new ArrayList<ModelPersonalSponsoredSelect>();
		this.objects.addAll(mPhoneModels);
		sharedPreferences = context.getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		deviceId = Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceId:", "" + deviceId);
		mDatabaseHelper = new DatabaseHelper(context);
		mDatasourceHandler = new DatasourceHandler(context);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	public ModelPersonalSponsoredSelect getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	private void setSection(LinearLayout header, String label) {
		select = label.substring(0, 1).toUpperCase();
		TextView text = new TextView(context);
		// header.setBackgroundColor();
		text.setTextColor(Color.parseColor("#007AFF"));
		text.setText(select);
		text.setTextSize(20);
		text.setPadding(20, 0, 0, 0);
		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.checkbox_listview, null);

		LinearLayout header = (LinearLayout) rowView.findViewById(R.id.section);
		String name = mPhoneModels.get(position).getPromoName();
		// String number = contactnumber1.get(position);
		char firstChar = name.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, name);
		} else {
			String preLabel = filteredData.get(position - 1);
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {
				setSection(header, name);
			} else {
				header.setVisibility(View.GONE);
			}
		}
		ModelPersonalSponsoredSelect p = getProduct(position);
		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);

		CheckBox cbBuy = (CheckBox) rowView.findViewById(R.id.checkbox);
		cbBuy.setOnCheckedChangeListener(myCheckChangList);
		cbBuy.setTag(position);
		cbBuy.setChecked(mPhoneModels.get(position).box);
		textView.setText(mPhoneModels.get(position).getPromoName());
		cbBuy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				value_changed = mPhoneModels.get(position).box;
				Log.e("value_changed: ", "" + value_changed);
				if (value_changed) {
					Final_value = mPhoneModels.get(position).getPromoId();
					is_enable = "1";
					new Edit_Sponsored_Ads().execute();
				} else {
					Final_value = mPhoneModels.get(position).getPromoId();
					is_enable = "0";
					new Edit_Sponsored_Ads().execute();
				}

			}
		});
		final ImageView message = (ImageView) rowView
				.findViewById(R.id.message);
		message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent mIntent_info = new Intent(context,
						OpendetailPersonalPromo.class);

				mIntent_info.putExtra("PROMO_ID", mPhoneModels.get(position)
						.getPromoId());

				context.startActivity(mIntent_info);
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);

			}
		});

		return rowView;
	}

	private ModelPersonalSponsoredSelect getProduct(int position) {
		// TODO Auto-generated method stub
		return ((ModelPersonalSponsoredSelect) getItem(position));
	}

	public ArrayList<ModelPersonalSponsoredSelect> getBox() {
		ArrayList<ModelPersonalSponsoredSelect> box = new ArrayList<ModelPersonalSponsoredSelect>();
		for (ModelPersonalSponsoredSelect p : objects) {
			if (p.box)
				box.add(p);
		}
		return box;
	}

	OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			getProduct((Integer) buttonView.getTag()).box = isChecked;
		}
	};

	@Override
	public int getPositionForSection(int section) {

		for (int i = 0; i < this.getCount(); i++) {
			String item = getItem(i).getPromoName();
			if (item.charAt(0) == sections.charAt(section))
				return i;
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int arg0) {

		return 0;
	}

	@Override
	public Object[] getSections() {

		String[] sectionsArr = new String[sections.length()];
		for (int i = 0; i < sections.length(); i++)
			sectionsArr[i] = "" + sections.charAt(i);
		return sectionsArr;
	}

	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final List<ModelPersonalSponsoredSelect> list = mPhoneModels;

			int count = list.size();
			final ArrayList<String> nlist = new ArrayList<String>(count);

			String filterableString;

			for (int i = 0; i < count; i++) {
				filterableString = list.get(i).getPromoName();
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(filterableString);
				}
			}

			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredData = (ArrayList<String>) results.values;
			Log.e("", "" + filteredData);
			notifyDataSetChanged();
		}

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mPhoneModels.clear();
		if (charText.length() == 0) {
			mPhoneModels.addAll(objects);
		} else {
			for (ModelPersonalSponsoredSelect wp : objects) {
				if (wp.getPromoName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mPhoneModels.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}

	/**
	 * Execute Sponsored Asynctask
	 */
	class Edit_Sponsored_Ads extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.e("is_enable: ", "" + is_enable);
			Log.e("Final_value: ", "" + Final_value);
			String url = JsonParserConnector.get_SponsoredAds(is_enable,
					mStringPassword, deviceId, Final_value);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(context);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Log.e("result----", "" + result);

			UI.hideProgressDialog();
			boolean test;
			if (value_changed) {
				test = mDatasourceHandler.UpdateAds(Final_value, "1");
			} else {
				test = mDatasourceHandler.UpdateAds(Final_value, "0");
			}
			Log.e("test: ", "" + test);
			// SponsornedPullList(result);

		}

	}

}
