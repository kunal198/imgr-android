package com.imgr.chat.adapter;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.UILApplication;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

public class ChatAdapter extends BaseAdapter implements
		StickyListHeadersAdapter, SectionIndexer {
	LayoutInflater inflator;
	ArrayList<String[]> chat_List;
	Context activity;
	TextView tvHome;
	SharedPreferences sharedPreferences;
	Editor editor;
	String mStringfont, mStringBubble, mStringBubble_receive, mString_override;
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	String mStringHeader, mStringLink, mStringImageUrl, MemberId, OwnId;
	ImageLoader imageLoader;
	DisplayImageOptions options;

	// ArrayList<String>mArrayListDATE;
	private int[] mSectionIndices;
	private String[] mSectionLetters;
	String[] test;
	boolean split = false;

	int splitAfter = 70;

	public ChatAdapter(Context context, ArrayList<String[]> messages) {

		this.chat_List = messages;

		this.activity = context;
		inflator = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		sharedPreferences = context.getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mDatabaseHelper = new DatabaseHelper(context);
		mDatasourceHandler = new DatasourceHandler(context);
		// FONT SECTION WORKING ON THIS
		mStringfont = sharedPreferences.getString(Constant.FONT_SET, "");

		// BUBBLE SECTION WORKING ON THIS
		mString_override = sharedPreferences.getString(
				Constant.BUBBLE_SET_OVERRIDE_FIXED, "");

		mStringBubble_receive = sharedPreferences.getString(
				Constant.BUBBLE_SET_FREIND, "");
		MemberId = sharedPreferences.getString(Constant.FRIEND_ID_PROMO, "");
		OwnId = sharedPreferences.getString(Constant.YOUR_ID_PROMO, "");
		// Log.e("mStringBubble_receive: ", "" + mStringBubble_receive);
		Log.e("MemberIdddddddddddddddddddddddd: ", "" + MemberId);

		if (mStringBubble_receive != null) {
			if (mStringBubble_receive.equals("blue")) {
				mStringBubble = "blue";
			} else if (mStringBubble_receive.equals("green")) {
				mStringBubble = "green";
			} else if (mStringBubble_receive.equals("purple")) {
				mStringBubble = "purple";

			} else {
				mStringBubble = "blue";
			}

		} else {
			mStringBubble = "blue";
		}
		// Log.e("mStringBubble: ", "" + mStringBubble);

	}

	public void refresh(ArrayList<String[]> messages) {
		this.chat_List = messages;
		notifyDataSetChanged();
	}

	private int[] getSectionIndices() {
		int[] sections = null;
		ArrayList<Integer> sectionIndices = new ArrayList<Integer>();

		// char lastFirstChar = mArrayListDATE[0].charAt(0);
		// Log.e("lastFirstChar: ", "" + lastFirstChar);
		sectionIndices.add(0);

		final String temp[] = test;

		if (temp[1].equals("my")) {

			String mString = temp[8];
			// Log.e("mArrayListDATE: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[8].equals(mString)) {
					mString = temp[i];
					// Log.e("My SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}

		}
		if (temp[1].equals("pic")) {
			String mString = temp[6];
			// Log.e("mArrayListDATE: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[6].equals(mString)) {
					mString = temp[i];
					// Log.e("PIC SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}
		}

		if (temp[1].equals("friend")) {
			String mString = temp[8];
			// Log.e(" friend mArrayListDATE: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[8].equals(mString)) {
					mString = temp[i];
					// Log.e(" friend SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}

		}

		if (temp[1].equals("friend_pic")) {
			String mString = temp[5];
			// Log.e(" friend_pic SEE CALLING mString: ", "" + mString);
			for (int i = 1; i < temp.length; i++) {
				if (!temp[5].equals(mString)) {
					mString = temp[i];
					// Log.e("friend_pic SEE CALLING mString: ", "" + mString);
					// lastFirstChar = mArrayListDATE[i].charAt(0);
					sectionIndices.add(i);
				}
			}
			sections = new int[sectionIndices.size()];
			for (int i = 0; i < sectionIndices.size(); i++) {
				sections[i] = sectionIndices.get(i);
			}
		}

		// Log.e("sections: ", "" + sections);
		return sections;
	}

	private String[] getSectionLetters() {
		String[] letters = new String[mSectionIndices.length];
		// Log.e("letters: ", "" + letters);
		for (int i = 0; i < mSectionIndices.length; i++) {
			letters[i] = test[mSectionIndices[i]];
		}
		// Log.e("last letters: ", "" + letters);
		return letters;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return chat_List.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View my_View = convertView;
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_launcher)
				.showImageForEmptyUri(R.drawable.ic_launcher).cacheInMemory()
				.cacheOnDisc().build();
		if (position == 0) {
			// Log.e("", "NOT CALLING");
		} else {
			// Log.e("", "CALLING");
			// Arrays.fill(test, null);
			// test.
			test = chat_List.get(position);
			mSectionIndices = getSectionIndices();
			mSectionLetters = getSectionLetters();
		}

		if (mStringfont.equals("normal")) {
			final String temp[] = chat_List.get(position);
			if (temp[1].equals("my")) {
				if (temp[5].equals("0")) {
					split = false;
					// Log.e(" ", "0");
					my_View = (View) inflator.inflate(R.layout.simple_white,
							parent, false);
				} else {
					// Log.e(" ", "ELSE");
					String PROMO_ID = temp[7];
					// Log.e("PROMO_ID: ", "" + PROMO_ID);
					String header = MYpromo_data_header(PROMO_ID);
					String footer = MYpromo_data_footer_text(PROMO_ID);
					String footerlink = MYpromo_data_link(PROMO_ID);
					// Log.e("header: ", "" + header);
					// Log.e("footer: ", "" + footer);
					if (header != null) {
						if (footer != null) {

							String type = promo_type(PROMO_ID);
							if (type.matches("personal")) {
								// both here header and footer
								if (header.equals("")) {
									split = true;
									my_View = (View) inflator.inflate(
											R.layout.customchat_list_own,
											parent, false);
									Log.e("no footerrrrrrrrrrrrrrrrrrrr",
											"no footerrrrrrrrrrrrrrrrrrrr");
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);

									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
								} else {
									split = true;
									my_View = (View) inflator.inflate(
											R.layout.header_footer_white,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);
									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
									final TextView header_link = (TextView) my_View
											.findViewById(R.id.header);
									header_link.setTextSize(14.0f);
									header_link.setText(header);
								}

							} else {
								split = true;
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

						}

						else {
							// no footer
							if (footer.equals("")) {
								split = true;
								// Log.e("no footerrrrrrrrrrrrrrrrrrrr",
								// "no footerrrrrrrrrrrrrrrrrrrr");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							} else {
								// Log.e(" ", "no");
								split = true;
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

							}

						}

					}

					else {
						if (footer != null) {
							if (footer.equals("")) {
								Log.e("nthing simple imageeeeeeeeeeeeee ",
										"nothing simple image");
								// nothing simple image
								split = true;
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

							else {
								split = true;
								// both header and footer
								// /Log.e(" ", "both header and footer");
								my_View = (View) inflator.inflate(
										R.layout.header_footer_white, parent,
										false);

								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								if (temp[0].toString().length() > splitAfter) {

								} else {
									// mImageViewPromo_id.
								}
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

								final TextView header_link = (TextView) my_View
										.findViewById(R.id.header);
								header_link.setTextSize(13.0f);
								header_link.setText(header);

							}
						}

						else {
							split = false;
							// Log.e(" ", "nothing happened");
							// nothing happened
							my_View = (View) inflator.inflate(
									R.layout.simple_white, parent, false);
						}

					}

				}
				// Log.e("temp[0]: ", "" + temp[0]);
				// Log.e("temp[1]: ", "" + temp[1]);
				// Log.e("temp[2]: ", "" + temp[2]);
				// Log.e("temp[3]: ", "" + temp[3]);
				// Log.e("temp[4]: ", "" + temp[4]);
				// Log.e("temp[5]: ", "" + temp[5]);
				// Log.e("temp[6]: ", "" + temp[6]);
				// Log.e("temp[7]: ", "" + temp[7]);
				// Log.e("temp[8]: ", "" + temp[8]);
				TextView tvName = (TextView) my_View
						.findViewById(R.id.usermessage);
				TextView tv_Name = (TextView) my_View
						.findViewById(R.id.user_message);
				tvName.setTextSize(15.0f);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				tvHome.setTextSize(9.0f);

				ImageView mImagetick = (ImageView) my_View
						.findViewById(R.id.sent_pending_tick);

				tvHome.setText(temp[6]);
				if (temp[2].equals("1")) {
					mImagetick.setBackgroundResource(R.drawable.conform_sent);
				} else {
					mImagetick.setBackgroundResource(R.drawable.pending_sent);
				}
				if (split) {

					/*
					 * Rect bounds = new Rect(); Paint paint = new Paint();
					 * paint.setTextSize(tvName.getTextSize());
					 * paint.getTextBounds(temp[0].toString(), 0,
					 * temp[0].toString().length(), bounds);
					 * 
					 * int width = (int) Math.ceil( bounds.width());
					 * 
					 * float numberOfLines = width/tvName.getWidth();
					 * 
					 * Log.e("number of linesssssssssssssssssssssssssssssssssss",
					 * ""+numberOfLines);
					 */

					// getNumberOfLinesInMessage(temp[0].toString(), tvName);

					String mString = temp[0].toString();

					float numberOfLines = getNumberOfLinesInMessage(mString,
							tvName);

					String firstTextViewText = "";

					int lastSpace = -1;

					int breakPoint = 0;

					for (int i = 0; i < mString.length(); i++) {
						if (mString.substring(i, i + 1).equalsIgnoreCase(" ")) {

							lastSpace = i;
						}

						firstTextViewText += mString.substring(i, i + 1);

						if (getNumberOfLinesInMessage(firstTextViewText, tvName) > 2.0) {
							breakPoint = i;
							break;
						}

					}

					if (numberOfLines > 2.0) {
						int splitIndex = 0;
						if (lastSpace != -1) {
							splitIndex = lastSpace;
						} else {
							splitIndex = breakPoint;
						}

						String s = mString.substring(0, splitIndex);

						tv_Name.setVisibility(View.VISIBLE);
						tvName.setText(s);
						mString = mString.substring(splitIndex,
								mString.length());
						// Log.e("HERE HERE HERE mString: ", "" + mString);
						tv_Name.setText(mString);
						// tvName.setText(s+mString);

					} else {
						tvName.setText(mString);
					}

					/*
					 * if (temp[0].toString().length() > splitAfter) { String
					 * mString = temp[0].toString(); String s =
					 * mString.substring(0, splitAfter);
					 * 
					 * if (mString.length() > splitAfter) {
					 * tv_Name.setVisibility(View.VISIBLE); tvName.setText(s);
					 * mString = mString.substring(splitAfter,
					 * mString.length()); // Log.e("HERE HERE HERE mString: ",
					 * "" + mString); tv_Name.setText(mString); } else {
					 * tvName.setText(temp[0]); //
					 * contact_phonenumber.add(newphone.get(i)); } } else {
					 * tvName.setText(temp[0]); }
					 */
				} else {
					tvName.setText(temp[0]);
				}

			}
			if (temp[1].equals("pic")) {

				my_View = (View) inflator.inflate(R.layout.forimage, parent,
						false);

				ImageView tvName = (ImageView) my_View.findViewById(R.id.pic);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				String image = temp[3];
				// Log.e("PIC image: ", "" + image);
				Bitmap myBitmap = ConvertToImage(image);

				tvName.setImageBitmap(myBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(9.0f);

			}

			// Log.v("temp[2]", "" + temp[2]);
			// Log.v("temp[1]", "" + temp[1]);
			if (temp[1].equals("friend")) {

				// Log.e("", "ENTER HERE");
				// Log.e("FREDS temp[7]", "" + temp[7]);
				// Log.e("FREDS temp[6]", "" + temp[6]);
				// Log.e("FREDS temp[5]", "" + temp[5]);
				// // Log.e("FREDS temp[4]", "" + temp[4]);
				// Log.e("FREDS temp[3]", "" + temp[3]);
				// Log.e("FREDS temp[2]", "" + temp[2]);
				// Log.e("FREDS temp[1]", "" + temp[1]);
				// Log.e("FREDS temp[0]", "" + temp[0]);
				String picPath = promo_image(temp[7]);
				// Log.e("FREDS picPath", "" + picPath);

				if (temp[7].equals("0")) {
					split = false;
					// Log.e("", "last try");
					// Log.e("", "last try: " + temp[7]);
					my_View = (View) inflator.inflate(R.layout.all_simple,
							parent, false);
					RelativeLayout mRelativeLayout = (RelativeLayout) my_View
							.findViewById(R.id.frame);
					if (mStringBubble.equals("blue")) {

						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_blue);
						// mRelativeLayout
						// .setBackgroundResource(R.drawable.index);

					} else if (mStringBubble.equals("green")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_green);
					} else if (mStringBubble.equals("purple")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_purple);

					}

				} else {
					// Log.e("test:   ", "HIIIIIIIIIIIIIIII");
					String mString_header = promo_data_header(temp[7]);
					String footerlink = MYpromo_data_link(temp[7]);
					// Log.e("????????mString_header:   ", "" + mString_header);

					if (mString_header.equals("")) {
						// NO Header SHOW in the screen
						Log.e("", "No header SINGLE");
						split = true;
						my_View = (View) inflator.inflate(
								R.layout.customforfriend_noheader, parent,
								false);
						FrameLayout mRelativeLayout = (FrameLayout) my_View
								.findViewById(R.id.frame);

						if (mStringBubble.equals("blue")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterblue);
						} else if (mStringBubble.equals("green")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfootergreen);

						} else if (mStringBubble.equals("purple")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterpurple);
						}

						if (footerlink.equals("")) {
							Log.e("no header footer", "no header footer");
							split = true;
							my_View = (View) inflator.inflate(
									R.layout.no_header_footer, parent, false);
							FrameLayout mRelativeLayout1 = (FrameLayout) my_View
									.findViewById(R.id.frame);
							Log.e("mStringBubble", "" + mStringBubble);

							if (mStringBubble.equals("blue")) {
								mRelativeLayout1
										.setBackgroundResource(R.drawable.simple_bubble_blue);
							} else if (mStringBubble.equals("green")) {
								mRelativeLayout1
										.setBackgroundResource(R.drawable.simple_bubble_green);
							} else if (mStringBubble.equals("purple")) {
								mRelativeLayout1
										.setBackgroundResource(R.drawable.simple_bubble_purple);
							}

							// TextView mTextViewheader = (TextView) my_View
							// .findViewById(R.id.header);
							// mTextViewheader.setText(mString_header);
							// mTextViewheader.setTextSize(17.0f);
							ImageView mImageViewIcon = (ImageView) my_View
									.findViewById(R.id.icon);
							// Log.e("Inside picPath", "" + picPath);
							imageLoader.displayImage(picPath, mImageViewIcon,
									options, new SimpleImageLoadingListener() {
										@Override
										public void onLoadingComplete(
												Bitmap loadedImage) {
											Animation anim = AnimationUtils
													.loadAnimation(activity,
															R.anim.fade_in);
											// imgViewFlag.setAnimation(anim);
											// anim.start();
										}
									});
						}
					}

					else {

						if (footerlink.equals("")) {
							// Log.e("no footerrrrrrrrrrrrrrrrrrrr",
							// "no footerrrrrrrrrrrrrrrrrrrr");
							split = true;
							my_View = (View) inflator.inflate(
									R.layout.customforfriend_nofooter, parent,
									false);
							FrameLayout mRelativeLayout = (FrameLayout) my_View
									.findViewById(R.id.frame);

							if (mStringBubble.equals("blue")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.onlyheader_frd_blue);
							} else if (mStringBubble.equals("green")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.onlyheader_frd_green);

							} else if (mStringBubble.equals("purple")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.onlyheader_frd_purple);
							}

							TextView mTextViewheader = (TextView) my_View
									.findViewById(R.id.header);
							
							mTextViewheader.setText(mString_header);
							mTextViewheader.setTextSize(17.0f);
							ImageView mImageViewIcon = (ImageView) my_View
									.findViewById(R.id.icon);
							// Log.e("Inside picPath", "" + picPath);
							imageLoader.displayImage(picPath, mImageViewIcon,
									options, new SimpleImageLoadingListener() {
										@Override
										public void onLoadingComplete(
												Bitmap loadedImage) {
											Animation anim = AnimationUtils
													.loadAnimation(activity,
															R.anim.fade_in);
											// imgViewFlag.setAnimation(anim);
											// anim.start();
										}
									});
						} else {
							// Log.e("", "both");
							// Both Header and Footer show in screen
							split = true;
							my_View = (View) inflator.inflate(
									R.layout.custom_forfriend, parent, false);
							FrameLayout mRelativeLayout = (FrameLayout) my_View
									.findViewById(R.id.frame);

							if (mStringBubble.equals("blue")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.header_footer_blue);
							} else if (mStringBubble.equals("green")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.header_footer_green);

							} else if (mStringBubble.equals("purple")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.header_footer_purple);
							}

							TextView mTextViewheader = (TextView) my_View
									.findViewById(R.id.header);
							mTextViewheader.setText(mString_header);
							mTextViewheader.setTextSize(14.0f);
						}

					}

					ImageView mImageViewIcon = (ImageView) my_View
							.findViewById(R.id.icon);
					// Log.e("Inside picPath", "" + picPath);
					imageLoader.displayImage(picPath, mImageViewIcon, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
					String mString_footer = promo_data_footer_text(temp[7]);
					// Log.e("mString_footer: ", "" + mString_footer);

					final TextView mTextViewFooter_link = (TextView) my_View
							.findViewById(R.id.footer_link);
					TextView mTextViewFooter = (TextView) my_View
							.findViewById(R.id.footer);
					if (mString_footer != null) {
						if (mString_footer.equals("")) {

						} else {
							Log.e("mString_footer", mString_footer);
							if (!mString_footer.equalsIgnoreCase("NO")) {
								mTextViewFooter.setText(mString_footer);
								mTextViewFooter.setTextSize(16.0f);
								mTextViewFooter_link.setText(footerlink);
								mTextViewFooter
										.setOnClickListener(new OnClickListener() {

											@Override
											public void onClick(View v) {
												// TODO Auto-generated method
												// stub
												String mString_footer_link = mTextViewFooter_link
														.getText().toString();
												// Log.e("mString_footer_link: ",
												// ""
												// + mString_footer_link);
												String test = mString_footer_link
														.substring(mString_footer_link
																.length() - 1);
												// Log.e("test: ", "" + test);
												String main_link;
												Log.e("testtttttttttttttttttttttttttt",
														""
																+ mString_footer_link);
												// main_link =
												// mString_footer_link +
												// MemberId;

												// if (test.equals("=")) {
												// main_link =
												// mString_footer_link
												// + MemberId;
												// Log.e("MemberIdddddddddddddddddddddddd111: ",
												// "" + MemberId);
												// }

												if (test.equals("=")
														|| test.equals("/")) {
													main_link = mString_footer_link
															+ MemberId;
													Log.e("MemberIdddddddddddddddddddddddd111: ",
															"" + MemberId);
												}

												else {
													main_link = mString_footer_link;
													// + "/"+ MemberId;
													// main_link =
													// mString_footer_link +
													// MemberId;

													Log.e("MemberIdddddddddddddddddddddddd000: ",
															"" + MemberId);
												}
												Log.e("testtttttttttttttttttttttttttt",
														"" + main_link);
												// String link =
												// mString_footer_link
												// + MemberId;
												// Log.e("MemberId: ", "" +
												// MemberId);

												if (!main_link
														.startsWith("https://")
														&& !main_link
																.startsWith("http://")) {
													main_link = "http://"
															+ main_link;
												}
												// Log.e("FINAl link: ", ""
												// + main_link);
												Intent i = new Intent(
														Intent.ACTION_VIEW);
												i.setData(Uri.parse(main_link));
												activity.startActivity(i);

											}
										});
							}

						}

					}

				}

				final TextView rec = (TextView) my_View
						.findViewById(R.id.frommessage);
				final TextView tv_Name = (TextView) my_View
						.findViewById(R.id.from_message);

				tvHome = (TextView) my_View.findViewById(R.id.seen);
				rec.setText(temp[3]);
				rec.setTextSize(15.0f);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(9.0f);

				if (split) {

					/*
					 * Rect bounds = new Rect(); Paint paint = new Paint();
					 * paint.setTextSize(rec.getTextSize());
					 * paint.getTextBounds(temp[3].toString(), 0,
					 * temp[3].toString().length(), bounds);
					 * 
					 * int width = (int) Math.ceil( bounds.width());
					 * 
					 * float numberOfLines = width/rec.getWidth();
					 * 
					 * Log.e("number of linesssssssssssssssssssssssssssssssssss",
					 * ""+numberOfLines);
					 */

					String mString = temp[3].toString();

					// float numberOfLines = getNumberOfLinesInMessage(mString,
					// rec);

					float numberOfLines = getNumberOfLinesInMessageForSmall(
							mString, rec);

					String firstTextViewText = "";

					int lastSpace = -1;

					int breakPoint = 0;

					for (int i = 0; i < mString.length(); i++) {
						if (mString.substring(i, i + 1).equalsIgnoreCase(" ")) {
							lastSpace = i;

						}

						firstTextViewText += mString.substring(i, i + 1);

						// if (getNumberOfLinesInMessage(firstTextViewText, rec)
						// > 2.0) {
						// breakPoint = i;
						// break;
						// }

						if (getNumberOfLinesInMessageForSmall(
								firstTextViewText, rec) > 2.0) {
							breakPoint = i;
							break;
						}

					}

					if (numberOfLines > 2.0) {
						int splitIndex = 0;
						if (lastSpace != -1) {
							splitIndex = lastSpace;
						} else {
							splitIndex = breakPoint;
						}

						String s = mString.substring(0, splitIndex);

						tv_Name.setVisibility(View.VISIBLE);
						rec.setText(s);
						mString = mString.substring(splitIndex,
								mString.length());
						mString.replaceAll("                  ", "");
						// Log.e("HERE HERE HERE mString: ", "" + mString);
						tv_Name.setText(mString);
						tv_Name.setTextSize(15.0f);
					} else {
						rec.setText(mString);

					}

					/*
					 * if (temp[3].toString().length() > splitAfter) { String
					 * mString = temp[3].toString(); String s =
					 * mString.substring(0, splitAfter);
					 * 
					 * if (mString.length() > splitAfter) {
					 * tv_Name.setVisibility(View.VISIBLE); rec.setText(s);
					 * mString = mString.substring(splitAfter,
					 * mString.length()); // Log.e("HERE HERE HERE mString: ",
					 * "" + mString); tv_Name.setText(mString);
					 * tv_Name.setTextSize(15.0f); } else {
					 * rec.setText(temp[3]); //
					 * contact_phonenumber.add(newphone.get(i)); } } else {
					 * rec.setText(temp[3]); }
					 */
				} else {
					rec.setText(temp[3]);

				}

				// Log.e("rec.getLineCount(): ", ""+rec.getLineCount());

			}

			if (temp[1].equals("friend_pic")) {
				my_View = (View) inflator.inflate(R.layout.freindimage, parent,
						false);

				ImageView tvName = (ImageView) my_View
						.findViewById(R.id.pic_freind);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				Bitmap mBitmap = ConvertToImage(temp[3]);
				tvName.setImageBitmap(mBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(9.0f);

				tvHome.setId(position);

				tvName.setTag("messageimage");

			}
		}

		// LARGER FONT SIZE SET ALL TEXT SIZE
		else {
			final String temp[] = chat_List.get(position);
			if (temp[1].equals("my")) {
				if (temp[5].equals("0")) {
					split = false;

					my_View = (View) inflator.inflate(R.layout.simple_white,
							parent, false);
				} else {
					// Log.e(" ", "ELSE");
					String PROMO_ID = temp[7];
					// Log.e("PROMO_ID: ", "" + PROMO_ID);
					String header = MYpromo_data_header(PROMO_ID);
					String footer = MYpromo_data_footer_text(PROMO_ID);
					String footerlink = MYpromo_data_link(PROMO_ID);
					// Log.e("header: ", "" + header);
					// Log.e("footer: ", "" + footer);
					if (header != null) {
						if (footer != null) {

							String type = promo_type(PROMO_ID);
							if (type.matches("personal")) {
								// both here header and footer
								if (header.equals("")) {
									split = true;
									my_View = (View) inflator.inflate(
											R.layout.customchat_list_own,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);

									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
								} else {
									split = true;
									my_View = (View) inflator.inflate(
											R.layout.header_footer_white,
											parent, false);
									ImageView mImageViewPromo_id = (ImageView) my_View
											.findViewById(R.id.promo_id);
									imageLoader.displayImage(temp[4],
											mImageViewPromo_id, options,
											new SimpleImageLoadingListener() {
												@Override
												public void onLoadingComplete(
														Bitmap loadedImage) {
													Animation anim = AnimationUtils
															.loadAnimation(
																	activity,
																	R.anim.fade_in);
													// imgViewFlag.setAnimation(anim);
													// anim.start();
												}
											});
									final TextView header_link = (TextView) my_View
											.findViewById(R.id.header);
									header_link.setTextSize(14.0f);
									header_link.setText(header);
								}

							} else {
								split = true;
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

						}

						else {
							// no footer
							if (footer.equals("")) {
								split = true;
								// / Log.e(" ", "no footer");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);

								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							} else {
								split = true;
								// Log.e(" ", "no");
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

							}

						}

					}

					else {
						if (footer != null) {
							if (footer.equals("")) {
								split = true;
								// Log.e(" ", "nothing simple image");
								// nothing simple image
								my_View = (View) inflator.inflate(
										R.layout.customchat_list_own, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});
							}

							else {
								// both header and footer
								// /Log.e(" ", "both header and footer");
								split = true;
								my_View = (View) inflator.inflate(
										R.layout.header_footer_white, parent,
										false);
								ImageView mImageViewPromo_id = (ImageView) my_View
										.findViewById(R.id.promo_id);
								imageLoader.displayImage(temp[4],
										mImageViewPromo_id, options,
										new SimpleImageLoadingListener() {
											@Override
											public void onLoadingComplete(
													Bitmap loadedImage) {
												Animation anim = AnimationUtils
														.loadAnimation(
																activity,
																R.anim.fade_in);
												// imgViewFlag.setAnimation(anim);
												// anim.start();
											}
										});

								final TextView header_link = (TextView) my_View
										.findViewById(R.id.header);
								header_link.setTextSize(17.0f);
								header_link.setText(header);

							}
						}

						else {
							split = false;
							// Log.e(" ", "nothing happened");
							// nothing happened
							my_View = (View) inflator.inflate(
									R.layout.simple_white, parent, false);
						}

					}

				}

				TextView tvName = (TextView) my_View
						.findViewById(R.id.usermessage);
				TextView tv_Name = (TextView) my_View
						.findViewById(R.id.user_message);
				tvName.setTextSize(17.0f);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				tvHome.setTextSize(11.0f);

				ImageView mImagetick = (ImageView) my_View
						.findViewById(R.id.sent_pending_tick);
				tvName.setText(temp[0]);

				tvHome.setText(temp[6]);
				if (temp[2].equals("1")) {
					mImagetick.setBackgroundResource(R.drawable.conform_sent);
				} else {
					mImagetick.setBackgroundResource(R.drawable.pending_sent);
				}
				if (split) {

					/*
					 * Rect bounds = new Rect(); Paint paint = new Paint();
					 * paint.setTextSize(tvName.getTextSize());
					 * paint.getTextBounds(temp[0].toString(), 0,
					 * temp[0].toString().length(), bounds);
					 * 
					 * int width = (int) Math.ceil( bounds.width());
					 * 
					 * float numberOfLines = width/tvName.getWidth();
					 * 
					 * Log.e("number of linesssssssssssssssssssssssssssssssssss",
					 * ""+numberOfLines);
					 */
					String mString = temp[0].toString();

					float numberOfLines = getNumberOfLinesInMessage(mString,
							tvName);

					String firstTextViewText = "";

					int lastSpace = -1;

					int breakPoint = 0;

					for (int i = 0; i < mString.length(); i++) {
						if (mString.substring(i, i + 1).equalsIgnoreCase(" ")) {
							lastSpace = i;
						}

						firstTextViewText += mString.substring(i, i + 1);

						if (getNumberOfLinesInMessage(firstTextViewText, tvName) > 2.0) {
							breakPoint = i;
							break;
						}

					}

					if (numberOfLines > 2.0) {
						int splitIndex = 0;
						if (lastSpace != -1) {
							splitIndex = lastSpace;
						} else {
							splitIndex = breakPoint;
						}

						String s = mString.substring(0, splitIndex);

						tv_Name.setVisibility(View.VISIBLE);
						tvName.setText(s);
						mString = mString.substring(splitIndex,
								mString.length());
						// Log.e("HERE HERE HERE mString: ", "" + mString);
						tv_Name.setText(mString);
						tv_Name.setTextSize(17.0f);
					} else {
						tvName.setText(mString);
					}

					/*
					 * if (temp[0].toString().length() > splitAfter) { String
					 * mString = temp[0].toString(); String s =
					 * mString.substring(0, splitAfter);
					 * 
					 * if (mString.length() > splitAfter) {
					 * tv_Name.setVisibility(View.VISIBLE); tvName.setText(s);
					 * mString = mString.substring(splitAfter,
					 * mString.length()); // Log.e("HERE HERE HERE mString: ",
					 * "" + mString); tv_Name.setText(mString); } else {
					 * tvName.setText(temp[0]); //
					 * contact_phonenumber.add(newphone.get(i)); } } else {
					 * tvName.setText(temp[0]); }
					 */
				} else {
					tvName.setText(temp[0]);
				}

			}
			if (temp[1].equals("pic")) {

				my_View = (View) inflator.inflate(R.layout.forimage, parent,
						false);

				ImageView tvName = (ImageView) my_View.findViewById(R.id.pic);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				String image = temp[3];
				// Log.e("PIC image: ", "" + image);
				Bitmap myBitmap = ConvertToImage(image);

				tvName.setImageBitmap(myBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(11.0f);

			}

			// Log.v("temp[2]", "" + temp[2]);
			if (temp[1].equals("friend")) {
				String picPath = promo_image(temp[7]);
				// String promo_count=temp[7];
				// Log.e("temp[7]", "" + temp[7]);

				if (temp[7].equals("0")) {
					split = false;
					my_View = (View) inflator.inflate(R.layout.all_simple,
							parent, false);
					RelativeLayout mRelativeLayout = (RelativeLayout) my_View
							.findViewById(R.id.frame);
					if (mStringBubble.equals("blue")) {

						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_blue);
					} else if (mStringBubble.equals("green")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_green);
					} else if (mStringBubble.equals("purple")) {
						mRelativeLayout
								.setBackgroundResource(R.drawable.simple_bubble_purple);

					}

				} else {

					String mString_header = promo_data_header(temp[7]);
					String footerlink = MYpromo_data_link(temp[7]);
					// Log.e("????????mString_header:   ", "" + mString_header);

					if (mString_header.equals("")) {
						// NO Header SHOW in the screen
						// Log.e("", "SINGLE");
						split = true;
						my_View = (View) inflator.inflate(
								R.layout.customforfriend_noheader, parent,
								false);
						FrameLayout mRelativeLayout = (FrameLayout) my_View
								.findViewById(R.id.frame);

						if (mStringBubble.equals("blue")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterblue);
						} else if (mStringBubble.equals("green")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfootergreen);

						} else if (mStringBubble.equals("purple")) {
							mRelativeLayout
									.setBackgroundResource(R.drawable.onlyfooterpurple);
						}

						if (footerlink.equals("")) {
							Log.e("large no header no footer",
									"large no header no footer");

							// split = true;
							// my_View = (View) inflator.inflate(
							// R.layout.no_header_footer, parent, false);
							FrameLayout mRelativeLayout1 = (FrameLayout) my_View
									.findViewById(R.id.frame);
							Log.e("mStringBubble", "" + mStringBubble);

							if (mStringBubble.equals("blue")) {
								mRelativeLayout1
										.setBackgroundResource(R.drawable.simple_bubble_blue);
							} else if (mStringBubble.equals("green")) {
								mRelativeLayout1
										.setBackgroundResource(R.drawable.simple_bubble_green);
							} else if (mStringBubble.equals("purple")) {
								mRelativeLayout1
										.setBackgroundResource(R.drawable.simple_bubble_purple);
							}
							//
							// // TextView mTextViewheader = (TextView) my_View
							// // .findViewById(R.id.header);
							// // mTextViewheader.setText(mString_header);
							// // mTextViewheader.setTextSize(17.0f);
							// ImageView mImageViewIcon = (ImageView) my_View
							// .findViewById(R.id.icon);
							// // Log.e("Inside picPath", "" + picPath);
							// imageLoader.displayImage(picPath, mImageViewIcon,
							// options, new SimpleImageLoadingListener() {
							// @Override
							// public void onLoadingComplete(
							// Bitmap loadedImage) {
							// Animation anim = AnimationUtils
							// .loadAnimation(activity,
							// R.anim.fade_in);
							// // imgViewFlag.setAnimation(anim);
							// // anim.start();
							// }
							// });
						}
					}

					else {
						if (footerlink.equals("")) {

							split = true;
							my_View = (View) inflator.inflate(
									R.layout.customforfriend_nofooter, parent,
									false);
							FrameLayout mRelativeLayout = (FrameLayout) my_View
									.findViewById(R.id.frame);

							if (mStringBubble.equals("blue")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.onlyheader_frd_blue);
							} else if (mStringBubble.equals("green")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.onlyheader_frd_green);

							} else if (mStringBubble.equals("purple")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.onlyheader_frd_purple);
							}

							TextView mTextViewheader = (TextView) my_View
									.findViewById(R.id.header);
							mTextViewheader.setText(mString_header);
							mTextViewheader.setTextSize(17.0f);
						} else {
							// Log.e("", "both");
							// Both Header and Footer show in screen
							split = true;
							my_View = (View) inflator.inflate(
									R.layout.custom_forfriend, parent, false);
							FrameLayout mRelativeLayout = (FrameLayout) my_View
									.findViewById(R.id.frame);

							if (mStringBubble.equals("blue")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.header_footer_blue);
							} else if (mStringBubble.equals("green")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.header_footer_green);

							} else if (mStringBubble.equals("purple")) {
								mRelativeLayout
										.setBackgroundResource(R.drawable.header_footer_purple);
							}

							TextView mTextViewheader = (TextView) my_View
									.findViewById(R.id.header);
							mTextViewheader.setText(mString_header);
							mTextViewheader.setTextSize(17.0f);
						}

					}

					ImageView mImageViewIcon = (ImageView) my_View
							.findViewById(R.id.icon);
					// Log.e("temp[6]", "" + temp[6]);
					imageLoader.displayImage(picPath, mImageViewIcon, options,
							new SimpleImageLoadingListener() {
								@Override
								public void onLoadingComplete(Bitmap loadedImage) {
									Animation anim = AnimationUtils
											.loadAnimation(activity,
													R.anim.fade_in);
									// imgViewFlag.setAnimation(anim);
									// anim.start();
								}
							});
					String mString_footer = promo_data_footer_text(temp[7]);
					Log.e("mString_footerrrrrrr: ", "" + mString_footer);

					final TextView mTextViewFooter_link = (TextView) my_View
							.findViewById(R.id.footer_link);
					TextView mTextViewFooter = (TextView) my_View
							.findViewById(R.id.footer);
					mTextViewFooter.setTextSize(15.0f);

					if (mString_footer == null || mString_footer.equals(""))
					{
						mString_footer = "";
					}
					
					//mString_footer = "sd";
					
					Log.e("mString_footer", mString_footer);
					
//					try{
//					mTextViewFooter.setText(mString_footer);
////					mTextViewFooter.setTextSize(16.0f);
//
//					mTextViewFooter_link.setText(footerlink);
//					}catch(Exception e){e.printStackTrace();}
//					
//					if(!mString_footer.equals("") || mString_footer != null){
//					
//					mTextViewFooter.setOnClickListener(new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//							// TODO Auto-generated method stub
//							String mString_footer_link = mTextViewFooter_link
//									.getText().toString();
//
//							String test = mString_footer_link
//									.substring(mString_footer_link.length() - 1);
//							String main_link;
//
//							// main_link = mString_footer_link + MemberId;
//
//							// if (test.equals("=")) {
//							// main_link = mString_footer_link + MemberId;
//							// Log.e("MemberIdddddddddddddddddddddddd222: ",
//							// "" + MemberId);
//							// }
//
//							if (test.equals("=")) {
//								main_link = mString_footer_link + MemberId;
//								Log.e("MemberIdddddddddddddddddddddddd222: ",
//										"" + MemberId);
//							} else {
//								main_link = mString_footer_link;
//								// + "/"+ MemberId;
//							}
//
//							// String link = mString_footer_link + MemberId;
//							// Log.e("MemberId: ", "" + MemberId);
//
//							if (!main_link.startsWith("https://")
//									&& !main_link.startsWith("http://")) {
//								main_link = "http://" + main_link;
//							}
//							// Log.e("FINAl link: ", "" + main_link);
//							Intent i = new Intent(Intent.ACTION_VIEW);
//							i.setData(Uri.parse(main_link));
//							activity.startActivity(i);
//
//						}
//					});
//
//				}
				}

				TextView rec = (TextView) my_View
						.findViewById(R.id.frommessage);
				TextView tv_Name = (TextView) my_View
						.findViewById(R.id.from_message);

				tvHome = (TextView) my_View.findViewById(R.id.seen);
				// rec.setText(temp[3]);
				rec.setTextSize(17.0f);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(11.0f);

				if (split) {

					/*
					 * Rect bounds = new Rect(); Paint paint = new Paint();
					 * paint.setTextSize(rec.getTextSize());
					 * paint.getTextBounds(temp[3].toString(), 0,
					 * temp[3].toString().length(), bounds);
					 * 
					 * int width = (int) Math.ceil( bounds.width());
					 * 
					 * float numberOfLines = width/rec.getWidth();
					 * 
					 * Log.e("number of linesssssssssssssssssssssssssssssssssss",
					 * ""+numberOfLines);
					 */

					String mString = temp[3].toString();

					float numberOfLines = getNumberOfLinesInMessage(mString,
							rec);

					String firstTextViewText = "";

					int lastSpace = -1;

					int breakPoint = 0;

					for (int i = 0; i < mString.length(); i++) {
						if (mString.substring(i, i + 1).equalsIgnoreCase(" ")) {
							lastSpace = i;
						}

						firstTextViewText += mString.substring(i, i + 1);

						if (getNumberOfLinesInMessage(firstTextViewText, rec) > 2.0) {
							breakPoint = i;
							break;
						}

					}

					if (numberOfLines > 2.0) {
						int splitIndex = 0;
						if (lastSpace != -1) {
							splitIndex = lastSpace;
						} else {
							splitIndex = breakPoint;
						}

						String s = mString.substring(0, splitIndex);

						tv_Name.setVisibility(View.VISIBLE);
						rec.setText(s);
						mString = mString.substring(splitIndex,
								mString.length());
						// Log.e("HERE HERE HERE mString: ", "" + mString);
						tv_Name.setText(mString);
						tv_Name.setTextSize(17.0f);
					} else {
						rec.setText(mString);

					}

					/*
					 * if (temp[3].toString().length() > splitAfter) { String
					 * mString = temp[3].toString(); String s =
					 * mString.substring(0, splitAfter);
					 * 
					 * if (mString.length() > splitAfter) {
					 * tv_Name.setVisibility(View.VISIBLE); rec.setText(s);
					 * mString = mString.substring(splitAfter,
					 * mString.length()); // Log.e("HERE HERE HERE mString: ",
					 * "" + mString); tv_Name.setText(mString);
					 * tv_Name.setTextSize(17.0f); } else {
					 * rec.setText(temp[3]); //
					 * contact_phonenumber.add(newphone.get(i)); } } else {
					 * rec.setText(temp[3]); }
					 */
				} else {
					rec.setText(temp[3]);

				}

			}

			if (temp[1].equals("friend_pic")) {
				my_View = (View) inflator.inflate(R.layout.freindimage, parent,
						false);

				ImageView tvName = (ImageView) my_View
						.findViewById(R.id.pic_freind);
				tvHome = (TextView) my_View.findViewById(R.id.seen);
				Bitmap mBitmap = ConvertToImage(temp[3]);
				tvName.setImageBitmap(mBitmap);
				tvHome.setText(temp[4]);
				tvHome.setTextSize(11.0f);
				tvName.setTag("messageimage");
			}
		}

		my_View.setId(position);

		my_View.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				// TODO Auto-generated method stub
				if (chat_List.get(v.getId())[1].equalsIgnoreCase("friend_pic")) {

					// yes no dialog
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							activity);
					alertDialog
							.setTitle("Alert")
							.setMessage("Do you want to save image to Gallery")
							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub
											addImageToGallery(
													chat_List.get(v.getId())[3],
													UILApplication.context);
											Toast.makeText(activity,
													"Image Saved to Gallery",
													Toast.LENGTH_LONG).show();
										}
									})
							.setNegativeButton("NO",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub
											dialog.dismiss();

										}
									});

					AlertDialog dialog = alertDialog.create();
					dialog.show();

				}

			}
		});

		my_View.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub

				if (!(chat_List.get(v.getId())[1].equalsIgnoreCase("pic") || chat_List
						.get(v.getId())[1].equalsIgnoreCase("friend_pic"))) {

					// list dialog

					Toast.makeText(activity, "Text Copied to Clipboard",
							Toast.LENGTH_LONG).show();

					String text = "";

					if (chat_List.get(v.getId())[1].equalsIgnoreCase("my")) {
						text = chat_List.get(v.getId())[0];
					} else {
						text = chat_List.get(v.getId())[3];
					}

					ClipboardManager clipboard = (ClipboardManager) UILApplication.context
							.getSystemService(UILApplication.context.CLIPBOARD_SERVICE);
					ClipData clip = ClipData.newPlainText("ImgrChat", text);
					clipboard.setPrimaryClip(clip);
				}

				return false;
			}
		});

		return my_View;
	}

	public static float getNumberOfLinesInMessage(String message,
			TextView tvName) {
		Rect bounds = new Rect();
		Paint paint = new Paint();
		paint.setTextSize(tvName.getTextSize());
		paint.getTextBounds(message.toString(), 0, message.toString().length(),
				bounds);

		float width = (float) Math.ceil(bounds.width());

		// float numberOfLines = width/tvName.getMaxWidth();
		float numberOfLines = width / 700;
		// Log.e("max widthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",""+tvName.getMaxWidth());
		// Log.e("number of linesssssssssssssssssssssssssssssssssss", ""+
		// numberOfLines);
		// Log.e("mesageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "" +
		// message);

		return numberOfLines;

	}

	public static float getNumberOfLinesInMessageForSmall(String message,
			TextView tvName) {
		Rect bounds = new Rect();
		Paint paint = new Paint();
		paint.setTextSize(tvName.getTextSize());
		paint.getTextBounds(message.toString(), 0, message.toString().length(),
				bounds);

		float width = (float) Math.ceil(bounds.width());

		// float numberOfLines = width/tvName.getMaxWidth();
		float numberOfLines = width / 1100;
		// Log.e("max widthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",""+tvName.getMaxWidth());
		// Log.e("number of linesssssssssssssssssssssssssssssssssss", ""+
		// numberOfLines);
		// Log.e("mesageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", "" +
		// message);

		return numberOfLines;

	}

	public static void addImageToGallery(final String base64Data,
			Context context) {

		byte[] decodedString = Base64.decode(base64Data, Base64.DEFAULT);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0,
				decodedString.length);

		// Log.e("preprepresaveeeeeeeeeeeeeee", Environment.DIRECTORY_PICTURES);

		File myDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES
						+ "/ImgrChat");

		myDir.mkdirs();
		Random generator = new Random();
		// int n = 10000;
		long n = System.currentTimeMillis();
		String fname = "Image-" + n + ".jpg";
		File file = new File(myDir, fname);
		if (file.exists())
			file.delete();
		try {

			FileOutputStream out = new FileOutputStream(file);

			Log.e("presaveeeeeeeeeeeeeee", "presaveeeeeeeeeeeeeee");

			decodedByte.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();

			scanFile(context, file.getAbsolutePath(), "image/*");

			Log.e("postsaveeeeeeeeeeeeeee", "postsaveeeeeeeeeeeeeee");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void scanFile(Context context, String path, String mimeType) {
		Client client = new Client(path, mimeType);
		MediaScannerConnection connection = new MediaScannerConnection(context,
				client);
		client.connection = connection;
		connection.connect();
	}

	private static final class Client implements MediaScannerConnectionClient {
		private final String path;
		private final String mimeType;
		MediaScannerConnection connection;

		public Client(String path, String mimeType) {
			this.path = path;
			this.mimeType = mimeType;
		}

		@Override
		public void onMediaScannerConnected() {
			connection.scanFile(path, mimeType);
		}

		@Override
		public void onScanCompleted(String path, Uri uri) {
			connection.disconnect();
		}
	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	public String promo_data_link(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringLink = mCursor.getString(2).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + mStringLink);
			} else {
				// mStringLink = "NO";
				mStringLink = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringLink;
	}

	public String promo_image(String id) {
		String imagepath = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				imagepath = mCursor.getString(4).trim();
				// Log.e(" SSSSSSSSSSSSSSS imagepath: ", "   " + imagepath);
			} else {
				imagepath = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return imagepath.toString().trim();
	}

	public String promo_data_header(String id) {
		String header = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				header = mCursor.getString(1).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + header);
			} else {
				// header = "NO";
				header = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return header;
	}

	public String promo_data_footer_text(String id) {
		String fotter_link = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);
			// Log.e(" id id: ", "   " + id);

			if (mCursor.getCount() != 0) {

				fotter_link = mCursor.getString(3).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + fotter_link);
			} else {
				fotter_link = "NO";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return fotter_link;
	}

	// my
	public String MYpromo_data_link(String id) {

		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				mStringLink = mCursor.getString(2).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + mStringLink);
			} else {
				mStringLink = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return mStringLink;
	}

	public String MYpromo_data_header(String id) {
		String header = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);

			if (mCursor.getCount() != 0) {

				header = mCursor.getString(1).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + header);
			} else {
				header = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return header;
	}

	public String MYpromo_data_footer_text(String id) {
		String fotter_link = null;
		try {

			mCursor = mDatasourceHandler.FETCHPROMODATA(id);
			// Log.e(" id id: ", "   " + id);

			if (mCursor.getCount() != 0) {

				fotter_link = mCursor.getString(3).trim();
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + fotter_link);
			} else {
				fotter_link = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return fotter_link;
	}

	public String promo_type(String id) {
		String type_personal_or_not = null;
		try {

			mCursor = mDatasourceHandler.promo_type(id);

			if (mCursor.getCount() != 0) {

				type_personal_or_not = mCursor.getString(9).trim();
				if (type_personal_or_not.matches("NO_Define")) {
					type_personal_or_not = "personal";
				} else if (type_personal_or_not.matches("personal")) {
					type_personal_or_not = "personal";
				} else if (type_personal_or_not.matches("no_personal")) {
					type_personal_or_not = "no_personal";
				}
				// Log.e(" SSSSSSSSSSSSSSS mStringLink: ", "   " + mStringLink);
			} else {
				type_personal_or_not = "";
				// Log.e("", "Nothing data show");
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return type_personal_or_not;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final String temp[] = chat_List.get(position);
		HeaderViewHolder holder;
		// Log.e("", "HOW many Time calling");
		if (convertView == null) {
			holder = new HeaderViewHolder();
			convertView = inflator.inflate(R.layout.chat_header_view, parent,
					false);
			holder.text = (TextView) convertView.findViewById(R.id.text1);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}

		String matched_date = date();
		String headerChar = null;
		if (temp[1].equals("my")) {

			if (temp[8].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[8].split("/")[0];
			}
		}
		if (temp[1].equals("pic")) {
			if (temp[6].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[6].split("/")[0];
			}
		}

		if (temp[1].equals("friend")) {
			if (temp[8].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[8].split("/")[0];
			}
		}

		if (temp[1].equals("friend_pic")) {
			if (temp[5].equals(matched_date)) {
				headerChar = "Today";
			} else {
				headerChar = temp[5].split("/")[0];
			}
		}
		// Log.e("", "LASt HOW many Time calling");
		// set header text as first char in name

		String headerText = null;
		// if (headerChar % 2 == 0) {
		// headerText = headerChar + "\n" + headerChar + "\n" + headerChar;
		// } else {
		// headerText = headerChar + "\n" + headerChar;
		// }
		headerText = "" + headerChar;
		holder.text.setText(headerText);
		// Log.e("", "Final HOW many Time calling");
		return convertView;
	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public String date_new_style() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	/**
	 * Remember that these have to be static, postion=1 should always return the
	 * same Id that is.
	 */
	@Override
	public long getHeaderId(int position) {
		final String temp[] = chat_List.get(position);
		String outputDateStr = null;
		// String outputDateStr = "";

		if (temp[1].equals("my")) {

			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[8]);
				outputDateStr = outputFormat.format(date);
				// Log.e("my enhfbb", "" + outputDateStr);
				// Log.e("my temp[8]", "" + temp[8]);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				outputDateStr = outputFormat.format(new Date());
			}
		}
		if (temp[1].equals("pic")) {
			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[6]);
				outputDateStr = outputFormat.format(date);
				// Log.e("enhfbb", "" + outputDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				outputDateStr = outputFormat.format(new Date());
			}
		}

		if (temp[1].equals("friend")) {
			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[8]);

				outputDateStr = outputFormat.format(date);
				// Log.e("enhfbb", "" + outputDateStr);
				// Log.e("frds enhfbb", "" + outputDateStr);
				// Log.e("frds temp[8]", "" + temp[8]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				outputDateStr = outputFormat.format(new Date());
			}

		}

		if (temp[1].equals("friend_pic")) {
			DateFormat inputFormat = new SimpleDateFormat("MMM dd/yyyy");
			DateFormat outputFormat = new SimpleDateFormat("ddMMyyyy");

			Date date;
			try {

				date = inputFormat.parse(temp[5]);
				outputDateStr = outputFormat.format(date);

				// Log.e("enhfbb", "" + outputDateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				outputDateStr = outputFormat.format(new Date());
			}
		}

		// return the first character of the country as ID because this is what
		// headers are based upon

		Log.e("outputDateStrrrrrrrrrrrrrrrrrrr", "" + outputDateStr);

		return Long.parseLong(outputDateStr);
	}

	@Override
	public int getPositionForSection(int section) {
		if (section >= mSectionIndices.length) {
			section = mSectionIndices.length - 1;
		} else if (section < 0) {
			section = 0;
		}
		return mSectionIndices[section];
	}

	@Override
	public int getSectionForPosition(int position) {
		for (int i = 0; i < mSectionIndices.length; i++) {
			if (position < mSectionIndices[i]) {
				return i - 1;
			}
		}
		return mSectionIndices.length - 1;
	}

	@Override
	public Object[] getSections() {
		return mSectionLetters;
	}

	public void clear() {

		// mArrayListDATE.get(0);
		// mArrayListDATE = new String[0];
		mSectionIndices = new int[0];
		mSectionLetters = new String[0];
		notifyDataSetChanged();
	}

	// public void restore() {
	// mCountries = mContext.getResources().getStringArray(R.array.countries);
	// mSectionIndices = getSectionIndices();
	// mSectionLetters = getSectionLetters();
	// notifyDataSetChanged();
	// }
	class HeaderViewHolder {
		TextView text;
	}
}
