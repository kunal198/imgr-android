package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;

import com.imgr.chat.contact.BlockAndMeaageActivity;
import com.imgr.chat.home.AllContact;
import com.imgr.chat.model.SponsoredModel;
import com.imgr.chat.model.SponsoredPersonalModel;
import com.imgr.chat.setting.OpendetailPersonalPromo;

public class SponsoredPersonalAdapter extends BaseAdapter implements
		SectionIndexer {

	@SuppressWarnings("unused")
	private ArrayList<String> contactnumber1;
	@SuppressWarnings("unused")
	private ArrayList<String> ISIMGR;
	LinearLayout header;

	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;

	private List<SponsoredPersonalModel> mPhoneModels = null;
	private ArrayList<SponsoredPersonalModel> arraylist;

	private static String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	// private static String sections = "abcdefghijklmnoprrstuvwxyz";
	private ItemFilter mFilter = new ItemFilter();

	public SponsoredPersonalAdapter(Activity fragmentActivity,
			ArrayList<String> contactname, ArrayList<String> contactnumber,
			ArrayList<String> isIMGR, List<SponsoredPersonalModel> mPhoneModels) {
		// TODO Auto-generated constructor stub
		this.mPhoneModels = mPhoneModels;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		ISIMGR = isIMGR;
		context = fragmentActivity;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.arraylist = new ArrayList<SponsoredPersonalModel>();
		this.arraylist.addAll(mPhoneModels);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	// public String getItem(int position) {
	// return filteredData.get(position);
	// }

	public SponsoredPersonalModel getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	@SuppressLint("DefaultLocale")
	private void setSection(LinearLayout header, String label) {

		select = label.substring(0, 1).toUpperCase();

		TextView text = new TextView(context);

		text.setTextColor(Color.parseColor("#007AFF"));
		text.setText(select);
		text.setTextSize(20);
		text.setPadding(20, 0, 0, 0);

		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);

	}

	@SuppressLint("DefaultLocale")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.custom_personal_sponsored, null);

		header = (LinearLayout) rowView.findViewById(R.id.section);
		String name = mPhoneModels.get(position).getpromoname();
		// String number = contactnumber1.get(position);
		char firstChar = name.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, name);
		} else {
			String preLabel = mPhoneModels.get(position - 1).getpromoname();
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {

				setSection(header, name);
			} else {

				header.setVisibility(View.GONE);
			}
		}
		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);
		final ImageView message = (ImageView) rowView
				.findViewById(R.id.message);

		if (mPhoneModels.get(position).getEnabled().equals("0")) {
			textView.setText(mPhoneModels.get(position).getpromoname());
			textView.setTextColor(Color.RED);
		} else {
			textView.setText(mPhoneModels.get(position).getpromoname());
			textView.setTextColor(Color.BLACK);
		}
		message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent mIntent_info = new Intent(context,
						OpendetailPersonalPromo.class);

				mIntent_info.putExtra("PROMO_ID", mPhoneModels.get(position)
						.getPromoId());

				context.startActivity(mIntent_info);
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);

			}
		});

		return rowView;
	}

	@Override
	public int getPositionForSection(int section) {

		for (int i = 0; i < this.getCount(); i++) {

			String item = this.getItem(i).getpromoname();

			if (item.charAt(0) == sections.charAt(section))
				return i;
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int arg0) {

		return 0;
	}

	@Override
	public Object[] getSections() {

		String[] sectionsArr = new String[sections.length()];

		Log.e("sectionsArr: ", "" + sectionsArr);
		for (int i = 0; i < sections.length(); i++) {
			sectionsArr[i] = "" + sections.charAt(i);
			Log.e("sectionsArr: ", "" + sections.charAt(i));
		}

		return sectionsArr;
	}

	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final List<String> list = originalData;

			int count = list.size();
			final ArrayList<String> nlist = new ArrayList<String>(count);

			String filterableString;

			for (int i = 0; i < count; i++) {
				filterableString = list.get(i);
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(filterableString);
				}
			}

			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredData = (ArrayList<String>) results.values;

			notifyDataSetChanged();
		}

	}

	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mPhoneModels.clear();
		if (charText.length() == 0) {
			mPhoneModels.addAll(arraylist);
		} else {
			for (SponsoredPersonalModel wp : arraylist) {
				if (wp.getpromoname().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mPhoneModels.add(wp);
				}
			}
		}
		char firstChar = charText.toUpperCase().charAt(0);

		setSection(header, "" + firstChar);
		notifyDataSetChanged();
	}

}
