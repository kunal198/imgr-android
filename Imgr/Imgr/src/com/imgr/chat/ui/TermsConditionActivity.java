package com.imgr.chat.ui;

/**
 * author: amit agnihotri
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.imgr.chat.R;
import com.imgr.chat.register.RegisterActivity;

public class TermsConditionActivity extends Activity {

	WebView webView;
	Button btnAccept;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms_condition);
		webView = (WebView) findViewById(R.id.webview);
		btnAccept = (Button) findViewById(R.id.btn_accept);

		webView.loadUrl("file:///android_asset/IMGR-TOS-4-14-14.html");
		btnAccept.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent_register = new Intent(
						TermsConditionActivity.this, RegisterActivity.class);
				// intent_home = new Intent(SplashActivity.this,
				// LoginActivity.class);
				startActivity(intent_register);
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
			}
		});

	}
}
