package com.imgr.chat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;

import com.imgr.chat.constant.Constant;
import com.imgr.chat.home.MenuActivity;

public class LocalNotificationSend {

	public LocalNotificationSend(boolean connection, Context context,
			String message, String title) {
		if (connection) {
			/*
			 * SharedPreferences mSharedPreferences; mSharedPreferences =
			 * context.getSharedPreferences( Constant.IMGRNOTIFICATION_RECEIVE,
			 * Context.MODE_PRIVATE); int co =
			 * mSharedPreferences.getInt(Constant.COUNT_NOTIFICATION, 0); co++;
			 * Editor editor = mSharedPreferences.edit();
			 * editor.putInt(Constant.COUNT_NOTIFICATION, co); editor.commit();
			 * 
			 * Intent resultIntent = new Intent(context, MenuActivity.class);
			 * resultIntent.putExtra("fragment", "Message"); // Construct a back
			 * stack for cross-task navigation: TaskStackBuilder stackBuilder =
			 * TaskStackBuilder.create(context);
			 * stackBuilder.addParentStack(MenuActivity.class);
			 * stackBuilder.addNextIntent(resultIntent);
			 * 
			 * // Create the PendingIntent with the back stack: PendingIntent
			 * resultPendingIntent = stackBuilder.getPendingIntent( 0, (int)
			 * PendingIntent.FLAG_ONE_SHOT); String title_ =
			 * context.getString(R.string.app_name); // Build the notification:
			 * NotificationCompat.Builder builder = new
			 * NotificationCompat.Builder( context).setAutoCancel(true) //
			 * Dismiss from the notif. area // when clicked
			 * .setContentIntent(resultPendingIntent) // Start 2nd activity //
			 * when the intent // is clicked. .setContentTitle(title_) // Set
			 * its title // Display the count in the Content Info
			 * .setSmallIcon(R.drawable.ic_launcher) // Display this icon
			 * .setContentText(String.format(title + " : " + message, 0)); //
			 * The // message // to // display.
			 * 
			 * // Finally, publish the notification: NotificationManager
			 * notificationManager = (NotificationManager) context
			 * .getSystemService(Context.NOTIFICATION_SERVICE);
			 * //notificationManager.notify(0, builder.build());
			 * 
			 * // notificationManager.cancelAll(); // NotificationManager
			 * notificationManager = (NotificationManager) // context //
			 * .getSystemService(Context.NOTIFICATION_SERVICE); Notification
			 * notification = new Notification( R.drawable.ic_launcher, message,
			 * 0); // // Intent notificationIntent = new Intent(context, //
			 * SplashActivity.class); // //
			 * notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP // |
			 * Intent.FLAG_ACTIVITY_SINGLE_TOP // |
			 * Intent.FLAG_ACTIVITY_NEW_TASK // |
			 * PendingIntent.FLAG_CANCEL_CURRENT); // // PendingIntent intent =
			 * PendingIntent.getActivity(context, 0, // notificationIntent, 0);
			 * // String title = context.getString(R.string.app_name); //
			 * notification.setLatestEventInfo(context, title, message, intent);
			 * // notification.flags |= Notification.FLAG_AUTO_CANCEL; //
			 * notificationManager.notify(0, notification);
			 * 
			 * //GCMIntentService gcmis = new GCMIntentService();
			 */
			/*
			 * int value; mSharedPreferences = context.getSharedPreferences(
			 * Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE); value =
			 * mSharedPreferences.getInt(Constant.COUNT_NOTIFICATION, 0);
			 * 
			 * Log.e("DEMO", "Changing : " + value); PackageManager pm =
			 * context.getPackageManager();
			 * 
			 * String lastEnabled = getLastEnabled(context); // Getting last //
			 * enabled // from shared // preference
			 * 
			 * if (TextUtils.isEmpty(lastEnabled)) { lastEnabled =
			 * "com.imgr.chat.SplashActivity"; }
			 * 
			 * ComponentName componentName = new ComponentName("com.imgr.chat",
			 * lastEnabled); pm.setComponentEnabledSetting(componentName,
			 * PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
			 * PackageManager.GET_ACTIVITIES);
			 * 
			 * Log.i("DEMO", "Removing : " + lastEnabled);
			 * 
			 * if (value <= 0) { lastEnabled = "com.imgr.chat.SplashActivity"; }
			 * else if (value == 1) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 2) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 3) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 4) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 5) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 6) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 7) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 8) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 9) { lastEnabled = "com.imgr.chat.a" + value; }
			 * else if (value == 10) { lastEnabled = "com.imgr.chat.a" + value;
			 * } else if (value > 10) { lastEnabled = "com.imgr.chat.a10p"; }
			 * else { lastEnabled = "com.imgr.chat.SplashActivity"; }
			 * 
			 * componentName = new ComponentName("com.imgr.chat", lastEnabled);
			 * pm.setComponentEnabledSetting(componentName,
			 * PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
			 * PackageManager.GET_ACTIVITIES); Log.e("DEMO", "Adding : " +
			 * lastEnabled); setLastEnabled(lastEnabled, context);
			 */// Saving last enabled to
				// shared
			// preference
		}

	}

	private String getLastEnabled(Context mContext) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return pref.getString("LastEnabled", "");
	}

	private void setLastEnabled(String value, Context mContext) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("LastEnabled", value);
		editor.commit();
	}

}
