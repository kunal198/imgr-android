package com.imgr.chat.database;

/**
 * author: amit agnihotri
 */
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	/**
	 * All Static variables Database Version
	 */

	public static final int DATABASE_VERSION = 3;

	// Database Name
	public static final String DATABASE_NAME = "imgr.db";

	// Contacts table name
	public static final String TABLE_ALL_CONTACTS = "_allcontacts";
	// Profile table name
	public static final String TABLE_PROFILE = "_allprofile";

	// Chat table name
	public static final String TABLE_CHATTING = "_allchathistory";

	// Sponsored table name
	public static final String TABLE_ALL_SPONSORED = "_allsponsored";

	// Sponsored table name
	public static final String TABLE_ALL_PERSONAL = "_allpersonal";

	// recent message table name
	public static final String TABLE_RECENT_CHAT_HISTORY = "_allrecentchathistory";

	public static final String TABLE_PERSONAL_SPONSORED_SET_USER = "_personalsponsoredsetuser";

	public static final String TABLE_NOTIFICATIONS = "_TableNotifications";

	// all PERSONAL_SPONSORED_SET_USER Table Columns names
	public static String KEY_USER_SET_PROMO = "_user_set_promos";
	public static String KEY_USER_SET_AUTOROATE = "_user_set_autorotate";
	public static final String KEY_USER_SET_SPONSORED_ADS = "_user_set_sponsoredads";
	public static final String KEY_USER_SET_PERSONAL_ADS = "_user_set_personalads";
	public static final String KEY_LAST_PROMO_ID = "_last_promo_id";
	public static final String KEY_USER_SET_UNQIUE_ID = "_user_set_uniqueId";

	// all Recent Chat Columns names
	public static String KEY_RECENT_MESSAGE = "_recent_message_";
	public static String KEY_RECENT_FIRSTNAME = "_recent_firstname";
	public static final String KEY_RECENT_JID_TO = "_recent_jid_to";
	public static final String KEY_RECENT_JID_FROM = "_recent_jid_from";
	public static final String KEY_RECENT_LASTNAME = "_recent_lastname";
	public static final String KEY_RECENT_DATE = "_recent_date";
	public static final String KEY_RECENT_TIME = "_recent_time";
	public static final String KEY_RECENT_TIMESTAMP = "_recent_timestamp";
	public static final String KEY_RECENT_COUNT = "_recent_count";

	public static final String RECENT_PERSONAL_PROMO_ON_OFF = "_personalpromoonoff";
	public static final String RECENT_SPONSERED_PROMO_ON_OFF = "_sponseredpromoonoff";
	public static final String RECENT_PROMO_ON_OFF = "_promoonoff";
	public static final String RECENT_AUTO_ROTATE_PROMO_ON_OFF = "_autorotatepromoonoff";
	public static final String RECENT_LAST_PROMO_ID = "_lastpromoid";

	// all Personal Table Columns names
	public static String KEY_personal_promo_id = "_personal_promo_id";
	public static String KEY_personal_promo_final_type = "_personal_final_type";
	public static final String KEY_personal_promo_type = "_personal_promo_type";
	public static final String KEY_personal_promo_name = "_personal_promo_name";
	public static final String KEY_personal_promo_image = "_personal_promo_image";
	public static final String KEY_personal_promo_link = "_personal_promo_link";
	public static final String KEY_personal_promo_link_text = "_personal_promo_link_text";
	public static final String KEY_personal_promo_message_header = "_personal_promo_message_header";
	public static final String KEY_personal_is_deleted = "_personal_is_deleted";
	public static final String KEY_personal_modified_date = "_personal_modified_date";
	public static final String KEY_personal_isActive = "_personal_isActive";
	public static final String KEY_personal_is_enabled = "_personal_is_enabled";

	// all Sponsored Table Columns names
	public static String KEY_promo_id = "_promo_id";
	public static final String KEY_promo_type = "_promo_type";
	public static final String KEY_promo_name = "_promo_name";
	public static final String KEY_promo_image = "_promo_image";
	public static final String KEY_promo_link = "_promo_link";
	public static final String KEY_promo_link_text = "_promo_link_text";
	public static final String KEY_promo_message_header = "_promo_message_header";
	public static final String KEY_is_deleted = "_is_deleted";
	public static final String KEY_modified_date = "_modified_date";
	public static final String KEY_isActive = "_isActive";
	public static final String KEY_is_enabled = "_is_enabled";

	// all chat Table Columns names
	public static String TKEY_UNQUIE_CHAT = "_iduniquechat";
	public static final String KEY_CHAT_USERNAME = "_usernamechat";
	public static final String KEY_TOJID_CHAT = "_tojidchat";
	public static final String KEY_FROMJID_CHAT = "_fromjidchat";
	public static final String KEY_MESSAGE_CHAT = "_messagechat";
	public static final String KEY_TIME_CHAT = "_timechat";
	public static final String KEY_DATE_CHAT = "_datechat";
	public static final String KEY_RECEIVE_TICK_CHAT = "_receive_chat_tick";
	public static final String KEY_BUBBLE_OR_NOT_CHAT = "_bubbleornot";
	public static final String KEY_AUTO_INC_CHAT = "_autoincchat";

	// all profile Table Columns names
	public static String TKEY_UNQUIE_PROFILE = "_iduniqueprofile";
	public static final String KEY_ID_PROFILE = "_id";
	public static final String KEY_FIRST_NAME_PROFILE = "_firstnameprofile";
	public static final String KEY_LAST_NAME_PROFILE = "_lastnameprofile";
	public static final String KEY_NAME_PHONE = "_phonenoprofile";
	public static final String KEY_NAME_PHONE_CODE = "_phonenocodeprofile";
	public static final String KEY_EMAIL_PHONE = "_emailprofile";
	public static final String KEY_IMAGE_BASE = "_base64";

	// all contacts Table Columns names
	public static String TKEY_UNQUIE_CONTACTS = "_iduniquecontacts";
	public static final String KEY_isImgrUser = "_isImgrUser";
	public static final String KEY_PHONENO_CONTACTS = "_phonenocontact";
	public static final String KEY_FIRST_NAME_CONTACTS = "_firstnamecontact";
	public static final String KEY_LASTNAME_CONTACTS = "_lastnamecontact";
	public static final String KEY_EMAIL_CONTACTS = "_emailcontacts";
	public static final String KEY_JID_CONTACTS = "_jidcontacts";
	public static final String KEY_MEMBERID_CONTACTS = "_memberidcontact";
	public static final String KEY_IMAGE_BASE_CONTACT = "_base64contact";

	public static final String KEY_BLOCKED_CONTACT = "_blockcontact";

	public static final String PERSONAL_PROMO_ON_OFF = "_personalpromoonoff";
	public static final String SPONSERED_PROMO_ON_OFF = "_sponseredpromoonoff";
	public static final String PROMO_ON_OFF = "_promoonoff";
	public static final String AUTO_ROTATE_PROMO_ON_OFF = "_autorotatepromoonoff";
	public static final String LAST_PROMO_ID = "_lastpromoid";

	public static final String KEY_PHONE_CONTACTS_ID = "_phoneidcontact";

	// TABLE_NOTIFICATIONS table Columns names
	public static final String KEY_NOTIFICATION_ID = "_notificationId";
	public static final String KEY_PHONENO_NOTIFICATION = "_phonenonotification";
	public static final String KEY_AUTO_INC_NOTIFICATION = "_autoincnotification";

	public static String QUERY_SPONSORED = "create table "
			+ TABLE_ALL_SPONSORED + " ( " + KEY_promo_type + " TEXT, "
			+ KEY_promo_name + " TEXT, " + KEY_promo_image + " TEXT , "
			+ KEY_promo_link + " TEXT," + KEY_promo_link_text + " TEXT,"
			+ KEY_promo_message_header + " TEXT," + KEY_is_deleted + " TEXT,"
			+ KEY_modified_date + " TEXT," + KEY_isActive + " TEXT,"
			+ KEY_is_enabled + " TEXT," + KEY_promo_id + " TEXT PRIMARY KEY)";

	public static String QUERY_PERSONAL = "create table " + TABLE_ALL_PERSONAL
			+ " ( " + KEY_personal_promo_type + " TEXT, "
			+ KEY_personal_promo_name + " TEXT, " + KEY_personal_promo_image
			+ " TEXT , " + KEY_personal_promo_link + " TEXT,"
			+ KEY_personal_promo_link_text + " TEXT,"
			+ KEY_personal_promo_message_header + " TEXT,"
			+ KEY_personal_is_deleted + " TEXT," + KEY_personal_modified_date
			+ " TEXT," + KEY_personal_isActive + " TEXT,"
			+ KEY_personal_promo_final_type + " TEXT,"
			+ KEY_personal_is_enabled + " TEXT," + KEY_personal_promo_id
			+ " TEXT PRIMARY KEY)";

	public static String QUERY_CONTACTS = "create table " + TABLE_ALL_CONTACTS
			+ " ( " + KEY_isImgrUser + " TEXT, " + KEY_FIRST_NAME_CONTACTS
			+ " TEXT , " + KEY_PHONENO_CONTACTS + " TEXT,"
			+ KEY_LASTNAME_CONTACTS + " TEXT," + KEY_JID_CONTACTS + " TEXT,"
			+ KEY_IMAGE_BASE_CONTACT + " TEXT," + KEY_EMAIL_CONTACTS + " TEXT,"
			+ KEY_MEMBERID_CONTACTS + " TEXT," + " TEXT," + KEY_BLOCKED_CONTACT
			+ " TEXT," + PERSONAL_PROMO_ON_OFF + " INTEGER, "
			+ SPONSERED_PROMO_ON_OFF + " INTEGER, " + PROMO_ON_OFF
			+ " INTEGER, " + AUTO_ROTATE_PROMO_ON_OFF + " INTEGER, "
			+ LAST_PROMO_ID + " INTEGER, " + KEY_PHONE_CONTACTS_ID
			+ " TEXT PRIMARY KEY)";

	public static String QUERY_PROFILE = "create table " + TABLE_PROFILE
			+ " ( " + KEY_FIRST_NAME_PROFILE + " TEXT, "
			+ KEY_LAST_NAME_PROFILE + " TEXT , " + KEY_NAME_PHONE
			+ " TEXT not null, " + KEY_IMAGE_BASE + " TEXT , "
			+ KEY_EMAIL_PHONE + " TEXT , " + " TEXT , " + KEY_NAME_PHONE_CODE
			+ " TEXT , " + TKEY_UNQUIE_PROFILE
			+ " INTEGER PRIMARY KEY AUTOINCREMENT)";

	public static String QUERY_CHAT_HISTORY = "create table " + TABLE_CHATTING
			+ " ( " + KEY_CHAT_USERNAME + " TEXT, " + KEY_TOJID_CHAT
			+ " TEXT , " + KEY_FROMJID_CHAT + " TEXT not null, "
			+ KEY_MESSAGE_CHAT + " TEXT , " + KEY_TIME_CHAT + " TEXT , "
			+ KEY_DATE_CHAT + " TEXT , " + KEY_RECEIVE_TICK_CHAT + " TEXT , "
			+ KEY_BUBBLE_OR_NOT_CHAT + " TEXT , " + TKEY_UNQUIE_CHAT
			+ " TEXT not null UNIQUE ," + KEY_AUTO_INC_CHAT
			+ " INTEGER PRIMARY KEY AUTOINCREMENT)";

	//
	public static String QUERY_TABLE_NOTIFICATIONS = "create table "
			+ TABLE_NOTIFICATIONS + " ( " + KEY_NOTIFICATION_ID + " TEXT, "
			+ KEY_PHONENO_NOTIFICATION + " TEXT , " + KEY_AUTO_INC_NOTIFICATION
			+ " INTEGER PRIMARY KEY AUTOINCREMENT)";

	//

	public static String QUERY_RECENT_MESSAGE = "create table "
			+ TABLE_RECENT_CHAT_HISTORY + " ( " + KEY_RECENT_MESSAGE + " TEXT,"
			+ KEY_RECENT_JID_FROM + " TEXT," + KEY_RECENT_LASTNAME + " TEXT,"
			+ KEY_RECENT_DATE + " TEXT," + KEY_RECENT_TIME + " TEXT,"
			+ KEY_RECENT_TIMESTAMP + " TEXT," + KEY_RECENT_FIRSTNAME + " TEXT,"
			+ RECENT_PERSONAL_PROMO_ON_OFF + " INTEGER, "
			+ RECENT_SPONSERED_PROMO_ON_OFF + " INTEGER, "
			+ RECENT_PROMO_ON_OFF + " INTEGER, "
			+ RECENT_AUTO_ROTATE_PROMO_ON_OFF + " INTEGER, "
			+ RECENT_LAST_PROMO_ID + " INTEGER, " + KEY_RECENT_COUNT + " TEXT,"
			+ KEY_RECENT_JID_TO + " TEXT)";

	//
	public static String USER_SET_PERSONAL = "create table "
			+ TABLE_PERSONAL_SPONSORED_SET_USER + " ( " + KEY_USER_SET_PROMO
			+ " TEXT, " + KEY_USER_SET_AUTOROATE + " TEXT , "
			+ KEY_LAST_PROMO_ID + " TEXT, " + KEY_USER_SET_SPONSORED_ADS
			+ " TEXT, " + KEY_USER_SET_PERSONAL_ADS + " TEXT, "
			+ KEY_USER_SET_UNQIUE_ID + " TEXT PRIMARY KEY)";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		
		try {
			Log.e("", "CALLING");
			// db.execSQL(QUERY_PROFILE);
			db.execSQL(QUERY_CONTACTS);
			db.execSQL(QUERY_PROFILE);
			db.execSQL(QUERY_CHAT_HISTORY);
			db.execSQL(QUERY_SPONSORED);
			db.execSQL(QUERY_RECENT_MESSAGE);
			db.execSQL(USER_SET_PERSONAL);
			db.execSQL(QUERY_TABLE_NOTIFICATIONS);

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Tableeeeeeeeeeeeeee Exception", e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALL_CONTACTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHATTING);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALL_SPONSORED);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECENT_CHAT_HISTORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONAL_SPONSORED_SET_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATIONS);
	}

	public ArrayList<HashMap<String, String>> NewAllContacts() throws SQLException {
		 ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();
		    
		// ArrayList<String> a = new ArrayList<String>();
		String mString_query = DatabaseHelper.KEY_isImgrUser
				+ "  COLLATE NOCASE";
//		SQLiteDatabase database = this.getWritableDatabase();
//		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
//				allcolumn, null, null, null, null, mString_query, null);
//		Log.e("mcursor", "" + mCursor);
//		if (mCursor != null) {
//			mCursor.moveToFirst();
//		}
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM _allcontacts", null);
		if (c.moveToFirst()) {
	        do {
	            HashMap<String, String> map = new HashMap<String, String>();
	            for(int i=0; i<c.getColumnCount();i++)
	            {
	                map.put(c.getColumnName(i), c.getString(i));
	            }

	            maplist.add(map);
	        } while (c.moveToNext());
	    }

//		db.close();
	    // return contact list
	    return maplist;
	}
	
	
	public boolean checkSponsorData (){
		SQLiteDatabase db = this.getReadableDatabase();
		boolean empty = true;
				Cursor cur = db.rawQuery("SELECT COUNT(*) FROM _allsponsored", null);
				if (cur != null && cur.moveToFirst()) {
				    empty = (cur.getInt (0) == 0);
				    Log.e("InDatabaase",""+empty+"----"+cur.getInt (0));
				}
				cur.close();

				return empty;
	}
	
	public void deleteSopnserData(){

	      SQLiteDatabase db = this.getWritableDatabase();
		
		int b = db.delete(TABLE_ALL_SPONSORED, null, null);
		Log.e("InDelete","Deleted---"+b);
//		return false;
	}
}
