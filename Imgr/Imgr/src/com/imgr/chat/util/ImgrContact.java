package com.imgr.chat.util;

public class ImgrContact {

	private String IsImgrUser;
	private String phonenumber;
	private String memberId;
	private String firstName;
	private String lastName;
	private String email;
	private String jid;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ImgrContact() {
		super();

	}

	public String getIsImgrUser() {
		return IsImgrUser;
	}

	public void setIsImgrUser(String isImgrUser) {
		IsImgrUser = isImgrUser;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}

}
