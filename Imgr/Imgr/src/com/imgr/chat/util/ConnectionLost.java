package com.imgr.chat.util;

import com.imgr.chat.constant.Constant;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class ConnectionLost {
	public static void showDialog(final Activity context, String title,
			String Message, String text, String text_) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog
				.setTitle(title)
				.setMessage(Message)
				.setCancelable(false)
				.setPositiveButton(text, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				})
				.setNegativeButton(text_,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Constant.CONNECTION_LOST = 1;
								dialog.dismiss();
								context.finish();

							}
						});

		AlertDialog dialog = alertDialog.create();
		dialog.show();

	}

}
