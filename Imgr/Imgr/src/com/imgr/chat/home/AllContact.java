package com.imgr.chat.home;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.adapter.ImgrAdapter;
import com.imgr.chat.adapter.MyAdapter;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.InviteFreindActivity;
import com.imgr.chat.contact.InviteImgrPeople;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.model.AllContactModel;
import com.imgr.chat.model.ContactPhoneModel;
import com.imgr.chat.model.ExampleContactItem;
import com.imgr.chat.model.IMGR_Model;
import com.imgr.chat.segmented.SegmentedRadioGroup;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.ngohung.widget.ContactItemInterface;

public class AllContact extends Fragment {
	// ListView mListView;
	static FastListView mListView;
	// SegmentedRadioGroup segmentText;
	ConnectionDetector mConnectionDetector;
	public static Context mContext;
	public static ProgressDialog pd;
	
	   public static final String MyPREFERENCES = "oneTimePrefs" ;
	   public static SharedPreferences sharedpreferences;
	
	//////////
//	ArrayList<String> Imgrcontactname = new ArrayList<String>();

	public static ArrayList<String> ImgrMemberId = new ArrayList<String>();
	ArrayList<String> NewImgrMemberId = new ArrayList<String>();

//	ArrayList<String> Imgrcontactunquieid = new ArrayList<String>();
//	ArrayList<String> Imgrcontactlastname = new ArrayList<String>();
//	ArrayList<String> Imgrcontactnumber = new ArrayList<String>();

	ArrayList<String> ImgrcontactnameNEW = new ArrayList<String>();

	ArrayList<String> ImgrcontactunquieidNEW = new ArrayList<String>();
	ArrayList<String> ImgrcontactlastnameNEW = new ArrayList<String>();
	ArrayList<String> ImgrcontactnumberNEW = new ArrayList<String>();

//	ArrayList<String> ImgrcontactunquieidNEW_DELETE = new ArrayList<String>();
	
	public static ProgressDialog pDialog;
	
	
	///////////

	boolean inSearchMode = false;
	private EditText searchBox;

	static List<ContactItemInterface> contactList;
	List<ContactItemInterface> filterList;
	// ListView mListView;
	View V;
	Button mButton_invite;
	// database work
	DatabaseHelper mDatabaseHelper;
	static DatasourceHandler mDatasourceHandler;
	static Cursor mCursor;
	static Cursor mCursor2;
	static SwipeRefreshLayout mSwipeRefreshLayout_AllContact;
	static ArrayList<String> contactname = new ArrayList<String>();
	static ArrayList<String> contactlastname = new ArrayList<String>();
	static ArrayList<String> contactnumber = new ArrayList<String>();
	static ArrayList<String> isIMGR_array = new ArrayList<String>();
	ArrayList<String> base64_array = new ArrayList<String>();
	static ArrayList<String> Imgrcontactunquieid = new ArrayList<String>();
	ArrayList<String> Imgrcontactname = new ArrayList<String>();
	ArrayList<String> Imgrcontactlastname = new ArrayList<String>();
	ArrayList<String> Imgrcontactnumber = new ArrayList<String>();
	static ArrayList<String> ImgrcontactunquieidNEW_DELETE = new ArrayList<String>();
	static MyAdapter adapter;
	// ImgrAdapter mImgrAdapter;
	List<AllContactModel> mImgrContacts = null;
	List<AllContactModel> mAllContactModels = null;
	RelativeLayout mRelativeLayout_IMGR, mRelativeLayout_ALL;
	// RadioButton mRadioButton_one, mRadioButton_two;
	boolean flag = true;
	SharedPreferences sharedPreferences;
	FragmentTransaction t;
	FragmentManager my_fragment_manager;
	static ArrayList<String> mArrayListBlockedContact;

	static ArrayList<ContactPhoneModel> arraylist = new ArrayList<ContactPhoneModel>();

	ArrayList<IMGR_Model> arraylistImgr = new ArrayList<IMGR_Model>();
	TextView mRadioButton_one, mRadioButton_two;
	static String mStringPassword;

	public static int click_invite_all = 0;
	
	public static boolean isFirstTimefetch = false;

	Editor editor;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		V = inflater.inflate(R.layout.all_contact, container, false);
		mConnectionDetector = new ConnectionDetector(getActivity());
		mButton_invite = (Button) V.findViewById(R.id.invite_people);
		mDatabaseHelper = new DatabaseHelper(getActivity());
		mDatasourceHandler = new DatasourceHandler(getActivity());
		contactList = new ArrayList<ContactItemInterface>();
		filterList = new ArrayList<ContactItemInterface>();
		sharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		mContext = this.getActivity();
		
		base64_array = new ArrayList<String>();
		isIMGR_array = new ArrayList<String>();
		Imgrcontactunquieid = new ArrayList<String>();
		mRadioButton_one = (TextView) V.findViewById(R.id.button_one);
		mRadioButton_two = (TextView) V.findViewById(R.id.button_two);
		Imgrcontactname = new ArrayList<String>();
		Imgrcontactlastname = new ArrayList<String>();
		Imgrcontactnumber = new ArrayList<String>();
		mArrayListBlockedContact = new ArrayList<String>();
		mRadioButton_one.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Contactimgr mContactimgr = new Contactimgr();
				changeFragment(mContactimgr);

			}
		});

		mSwipeRefreshLayout_AllContact = (SwipeRefreshLayout) V
				.findViewById(R.id.swipe_container_all);
		mListView = (FastListView) V.findViewById(R.id.listView);
		// mListView.setFastScrollEnabled(true);

		mSwipeRefreshLayout_AllContact.setColorSchemeResources(
				android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		mSwipeRefreshLayout_AllContact
				.setOnRefreshListener(new OnRefreshListener() {

					@Override
					public void onRefresh() {
						// TODO Auto-generated method stub
						if (mConnectionDetector.isConnectingToInternet()) {
							// NxtAndEverytimefetchContacts();

							PulltorefreshLocalDataBaseFetchContacts();
						}

						else {
							mSwipeRefreshLayout_AllContact.setRefreshing(false);
							LogMessage.showDialog(getActivity(), null,
									"No Internet Connection", null, "Ok");
						}
					}
				});

		searchBox = (EditText) V.findViewById(R.id.input_search_query);
		searchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					adapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@SuppressLint("DefaultLocale")
			@Override
			public void afterTextChanged(Editable s) {
				// searchString = searchBox.getText().toString().trim()
				// .toUpperCase();
				// searchBox.setText("");

			}
		});

		if (MenuActivity.first_time.equals("1")) {
			mRadioButton_two.setTypeface(null, Typeface.BOLD);
			mRadioButton_one.setTypeface(null, Typeface.NORMAL);

			FirstfetchContacts();
			MenuActivity.first_time = "2";
		} else {
			mRadioButton_two.setTypeface(null, Typeface.BOLD);
			mRadioButton_one.setTypeface(null, Typeface.NORMAL);

			LocalDataBaseFetchContacts();
		}

		return V;
	}

	public void changeFragment(Fragment targetFragment) {

		my_fragment_manager = getActivity().getSupportFragmentManager();
		t = my_fragment_manager.beginTransaction();
		t.replace(R.id.menu_frame_page, targetFragment, "fragment");
		// t.addToBackStack(null);
		t.commit();

	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		SharedPreferences newSharedPreferences = getActivity()
				.getSharedPreferences(Constant.IMGRNOTIFICATION_RECEIVE,
						Context.MODE_PRIVATE);
		editor = newSharedPreferences.edit();
		editor.putString("Notification_Click_Message", "false");

		editor.commit();
		
		try{
		
		SharedPreferences prefs = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE); 
		boolean bool =  prefs.getBoolean("isFirstTimeLoad", false);
		if(!bool){
//		int SPLASH_TIME_OUT = 3000;
//		
//		final ProgressDialog dialog = ProgressDialog.show(mContext, "Title", 
//				"Message", true);
//				final Handler handler = new Handler() {
//				   public void handleMessage(Message msg) {
//				      dialog.dismiss();
//				      }
//				   };
//				Thread t = new Thread() {  
//				   public void run() {
//				// write process code here.
			
//			if(!isFirstTimefetch){
				pDialog = new ProgressDialog(mContext);
				pDialog.setMessage("Loading Contacts ...");
				pDialog.show();
//			}
					   LocalDataBaseFetchContacts();
//		            	
//		            	Toast.makeText(mContext, "Please Wait for a While", Toast.LENGTH_SHORT).show();
		     	       PulltorefreshLocalDataBaseFetchContacts();
		     	       Contactimgr.imgrfetchContacts();
		     	       
		     	       
//		     	      if(!isFirstTimefetch){
//		     				if(pDialog.isShowing()){
//		     					pDialog.dismiss();
//		     					}
//		     				isFirstTimefetch = true;
////		     	          
////		     		                
//		     		                AllContact.sharedpreferences = mContext
//		     		        				.getSharedPreferences(MyPREFERENCES,
//		     		        						Context.MODE_PRIVATE);
//		     		        		editor = sharedpreferences.edit();
//		     		        		editor.putBoolean("isFirstTimeLoad", isFirstTimefetch);
//
//		     		        		editor.commit();
//		     				
//		     				
//		     				Toast.makeText(mContext, "Fetching Done ...", Toast.LENGTH_SHORT).show();
//		     				
//		     			}
//				      handler.sendEmptyMessage(0);
//				      }
//				   };
//				t.start();
			
//		new Handler().postDelayed(new Runnable() {
//			 
//            /*
//             * Showing splash screen with a timer. This will be useful when you
//             * want to show case your app logo / company
//             */
// 
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                // Start your app main activity
////                Intent i = new Intent(SplashScreen.this, MainActivity.class);
////                startActivity(i);
//// 
////                // close this activity
////                finish();
//            	
//            	
//            	
//            	LocalDataBaseFetchContacts();
////            	
////            	Toast.makeText(mContext, "Please Wait for a While", Toast.LENGTH_SHORT).show();
//     	       PulltorefreshLocalDataBaseFetchContacts();
//     	       Contactimgr.imgrfetchContacts();
//     	       
//     	       
//            }
//        }, 4000);
//
//
//			 new Handler().postDelayed(new Runnable() {
//				 
//		            /*
//		             * Showing splash screen with a timer. This will be useful when you
//		             * want to show case your app logo / company
//		             */
//		 
//		            @Override
//		            public void run() {
//		                // This method will be executed once the timer is over
//		                // Start your app main activity
////		                Intent i = new Intent(SplashScreen.this, MainActivity.class);
////		                startActivity(i);
//		// 
////		                // close this activity
////		                finish();
//		            	isFirstTimefetch = true;
////                
////		                
//		                sharedpreferences = getActivity()
//		        				.getSharedPreferences(MyPREFERENCES,
//		        						Context.MODE_PRIVATE);
//		        		editor = sharedpreferences.edit();
//		        		editor.putBoolean("isFirstTimeLoad", isFirstTimefetch);
//
//		        		editor.commit();
//		            	Toast.makeText(mContext, "Processing Done", Toast.LENGTH_SHORT).show();
//		            }
//		        }, 2000);
			 
			
			
		}
		}catch(Exception e){}
		
//		Toast.makeText(mContext, "inside resume", Toast.LENGTH_SHORT).show();

		if (InviteImgrPeople.update == 1) {
//			Toast.makeText(mContext, "inside 1", Toast.LENGTH_SHORT).show();
			InviteImgrPeople.update = 0;
			LocalDataBaseFetchContacts();
//			PulltorefreshLocalDataBaseFetchContacts();
//			Contactimgr.imgrfetchContacts();
		}
		if (click_invite_all == 1) {
//			Toast.makeText(mContext, "inside 2", Toast.LENGTH_SHORT).show();
			Log.e("", "CALLING");
			click_invite_all = 0;
			LocalDataBaseFetchContacts();
//			PulltorefreshLocalDataBaseFetchContacts();
//			Contactimgr.imgrfetchContacts();
		}
		if (Constant.HANDLE_BACK_CONATCT_ADD == 1) {
//			Toast.makeText(mContext, "inside 3", Toast.LENGTH_SHORT).show();
			Constant.HANDLE_BACK_CONATCT_ADD = 0;
			LocalDataBaseFetchContacts();
//			PulltorefreshLocalDataBaseFetchContacts();
//			Contactimgr.imgrfetchContacts();
		} else {
			Constant.HANDLE_BACK_CONATCT_ADD = 0;
		}
		
	}

	
	public void run() {
		LocalDataBaseFetchContacts();
	       PulltorefreshLocalDataBaseFetchContacts();
	       Contactimgr.imgrfetchContacts();
        handler.sendEmptyMessage(0);
}

private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
                pd.dismiss();
//                tv.setText(pi_string);
//
        }
        
        
};
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

//		SharedPreferences newSharedPreferences = getActivity()
//				.getSharedPreferences(Constant.IMGRNOTIFICATION_RECEIVE,
//						Context.MODE_PRIVATE);
//		editor = newSharedPreferences.edit();
//		editor.putString("Notification_Click_Message", "false");
//
//		editor.commit();
//		
//		Toast.makeText(mContext, "inside resume", Toast.LENGTH_SHORT).show();
//
//		if (InviteImgrPeople.update == 1) {
//			InviteImgrPeople.update = 0;
//			LocalDataBaseFetchContacts();
//			PulltorefreshLocalDataBaseFetchContacts();
//		}
//		if (click_invite_all == 1) {
//			Log.e("", "CALLING");
//			click_invite_all = 0;
//			LocalDataBaseFetchContacts();
//			PulltorefreshLocalDataBaseFetchContacts();
//
//		}
//		if (Constant.HANDLE_BACK_CONATCT_ADD == 1) {
//			Constant.HANDLE_BACK_CONATCT_ADD = 0;
//			LocalDataBaseFetchContacts();
//			PulltorefreshLocalDataBaseFetchContacts();
//		} else {
//			Constant.HANDLE_BACK_CONATCT_ADD = 0;
//		}
//		new AsyncTask<Void, Void, Void>(){
//
//			protected void onPreExecute() {
//				Contactimgr mContactimgr = new Contactimgr();
//				changeFragment(mContactimgr);
//			};
//			
//			@Override
//			protected Void doInBackground(Void... params) {
//				// TODO Auto-generated method stub
//				
//				return null;
//			}
//			
//			protected void onPostExecute(Void result) {
//				AllContact mAllContact = new AllContact();
//				changeFragment(mAllContact);
//			};
//			
//		}.execute();
//		Contactimgr.isRefresh = true;
//		
//		if(Contactimgr.isRefresh){
//			Contactimgr mContactimgr = new Contactimgr();
//			changeFragment(mContactimgr);
//		}
	

	}

	public void FirstfetchContacts() {
		contactname.clear();
		contactnumber.clear();
		contactList.clear();
		isIMGR_array.clear();
		contactlastname.clear();
		base64_array.clear();
		Imgrcontactunquieid.clear();
		mArrayListBlockedContact.clear();
		// String phoneNumber = null;
		// String email = null;
		//
		// Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		// String _ID = ContactsContract.Contacts._ID;
		// String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		// // String Given_NAME = ContactsContract.Contacts.;
		// String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
		//
		// Uri PhoneCONTENT_URI =
		// ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		// String Phone_CONTACT_ID =
		// ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		// String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
		//
		// Uri EmailCONTENT_URI =
		// ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		// String EmailCONTACT_ID =
		// ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		// String DATA = ContactsContract.CommonDataKinds.Email.DATA;
		//
		// StringBuffer output = new StringBuffer();
		//
		// ContentResolver contentResolver = getActivity().getContentResolver();
		//
		// Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
		// null);
		//
		// // Loop for every contact in the phone
		// if (cursor.getCount() > 0) {
		//
		// while (cursor.moveToNext()) {
		//
		// String contact_id = cursor
		// .getString(cursor.getColumnIndex(_ID));
		// String name = cursor.getString(cursor
		// .getColumnIndex(DISPLAY_NAME));
		//
		// int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
		// .getColumnIndex(HAS_PHONE_NUMBER)));
		//
		// if (hasPhoneNumber > 0) {
		//
		// output.append("\n First Name:" + name);
		//
		// // Query and loop for every phone number of the contact
		// Cursor phoneCursor = contentResolver.query(
		// PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
		// new String[] { contact_id }, null);
		//
		// while (phoneCursor.moveToNext()) {
		// phoneNumber = phoneCursor.getString(phoneCursor
		// .getColumnIndex(NUMBER));
		// output.append("\n Phone number:" + phoneNumber);
		// // Log.e("name: ", ""+name);
		// if (name.equals("")) {
		//
		// } else {
		//
		// try {
		// if (mStringPassword.equals(phoneNumber
		// .replace("[(", "").replace(")", "")
		// .replace(" ", "").replace(")]", "")
		// .replace("(", "").replace("-", "")
		// .replace("]", "").replace("[", "")
		// .replace("*", "").replace(" ", "")
		// .replace("+1", "").replace("+91", ""))) {
		//
		// } else {
		// contactList.add(new ExampleContactItem(
		// name, name));
		// // contactname.add(name);
		// String[] splited = name.split(" ");
		//
		// // Log.e("splited: ", "" + splited.length);
		//
		// if (splited.length == 2) {
		// contactname.add(splited[0]);
		// contactlastname.add(splited[1]);
		// }
		//
		// else if (splited.length == 1) {
		// contactname.add(splited[0]);
		// contactlastname.add("");
		// } else {
		// // Log.e("nothing: ", "Nothing");
		// contactname.add(name);
		// contactlastname.add("");
		// }
		//
		// Imgrcontactunquieid.add(contact_id);
		// contactnumber.add(phoneNumber
		// .replace("[(", "").replace(")", "")
		// .replace(" ", "").replace(")]", "")
		// .replace("(", "").replace("-", "")
		// .replace("]", "").replace("[", "")
		// .replace("*", ""));
		// }
		//
		// } catch (Exception e) {
		//
		// }
		// }
		//
		// }
		//
		// phoneCursor.close();
		//
		// // Query and loop for every email of the contact
		// Cursor emailCursor = contentResolver.query(
		// EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
		// new String[] { contact_id }, null);
		//
		// while (emailCursor.moveToNext()) {
		//
		// email = emailCursor.getString(emailCursor
		// .getColumnIndex(DATA));
		//
		// output.append("\nEmail:" + email);
		//
		// }
		//
		// emailCursor.close();
		// }
		//
		// output.append("\n");
		// // cursor.close();
		// }

		// Log.e("START contactnumber: ", "" + contactnumber.size());
		// for (int i = 0; i < contactnumber.size(); i++) {
		//
		// mDatasourceHandler.FIRST_AllContactsUnquie(contactnumber.get(i)
		// .replace("[(", "").replace(")", "")
		// .replace(" ", "").replace(")]", "")
		// .replace("(", "").replace("-", "").replace("]", "")
		// .replace("[", ""), contactname.get(i),
		// contactlastname.get(i), null, null, "0", "",
		// "Null", Imgrcontactunquieid.get(i), "false");
		//
		// }

		try {
			mCursor = mDatasourceHandler.AllContacts();
			Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			contactname.clear();
			contactnumber.clear();
			contactlastname.clear();
			isIMGR_array.clear();
			Imgrcontactunquieid.clear();
			do {
				isIMGR_array.add(mCursor.getString(0).trim());
				contactname.add(mCursor.getString(1).trim());
				contactnumber.add(mCursor.getString(2).trim());
				contactlastname.add(mCursor.getString(3).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		arraylist.clear();
		Log.e("local Imgrcontactunquieid: ", "" + Imgrcontactunquieid.size());
		Log.e("local contactnumber: ", "" + contactnumber.size());
		if (contactnumber.size() == 0) {
			Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			new ALLCONTACT().execute();
		}

		// }

		// }
	}

	public static void LocalDataBaseFetchContacts() {
		contactname.clear();
		contactnumber.clear();
		contactList.clear();
		contactlastname.clear();
		Imgrcontactunquieid.clear();
		
//		Toast.makeText(mContext, "inside local fetcch", Toast.LENGTH_SHORT).show();
		// String phoneNumber = null;
		// String email = null;
		//
		// Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		// String _ID = ContactsContract.Contacts._ID;
		// String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		// String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
		//
		// Uri PhoneCONTENT_URI =
		// ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		// String Phone_CONTACT_ID =
		// ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		// String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
		//
		// Uri EmailCONTENT_URI =
		// ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		// String EmailCONTACT_ID =
		// ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		// String DATA = ContactsContract.CommonDataKinds.Email.DATA;
		//
		// StringBuffer output = new StringBuffer();
		//
		// ContentResolver contentResolver = getActivity().getContentResolver();
		//
		// Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
		// null);
		//
		// // Loop for every contact in the phone
		// if (cursor.getCount() > 0) {
		//
		// while (cursor.moveToNext()) {
		//
		// String contact_id = cursor
		// .getString(cursor.getColumnIndex(_ID));
		// String name = cursor.getString(cursor
		// .getColumnIndex(DISPLAY_NAME));
		//
		// int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
		// .getColumnIndex(HAS_PHONE_NUMBER)));
		//
		// if (hasPhoneNumber > 0) {
		//
		// output.append("\n First Name:" + name);
		//
		// // Query and loop for every phone number of the contact
		// Cursor phoneCursor = contentResolver.query(
		// PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
		// new String[] { contact_id }, null);
		//
		// while (phoneCursor.moveToNext()) {
		// phoneNumber = phoneCursor.getString(phoneCursor
		// .getColumnIndex(NUMBER));
		// output.append("\n Phone number:" + phoneNumber);
		// // Log.e("name: ", ""+name);
		// if (name.equals("")) {
		//
		// } else {
		//
		// if (mStringPassword.equals(phoneNumber
		// .replace("[(", "").replace(")", "")
		// .replace(" ", "").replace(")]", "")
		// .replace("(", "").replace("-", "")
		// .replace("]", "").replace("[", "")
		// .replace("*", "").replace(" ", "")
		// .replace("+1", "").replace("+91", ""))) {
		//
		// } else {
		// contactList.add(new ExampleContactItem(name,
		// name));
		// // contactname.add(name);
		// try {
		// String[] splited = name.split(" ");
		//
		// // Log.e("splited: ", ""+splited.length);
		//
		// if (splited.length == 2) {
		// contactname.add(splited[0]);
		// contactlastname.add(splited[1]);
		// }
		//
		// else if (splited.length == 1) {
		// contactname.add(splited[0]);
		// contactlastname.add("");
		// } else {
		// // Log.e("nothing: ", "Nothing");
		// contactname.add(name);
		// contactlastname.add("");
		// }
		//
		// contactnumber.add(phoneNumber
		// .replace("[(", "").replace(")", "")
		// .replace(" ", "").replace(")]", "")
		// .replace("(", "").replace("-", "")
		// .replace("]", "").replace("[", "")
		// .replace("*", ""));
		// Imgrcontactunquieid.add(contact_id);
		// } catch (Exception e) {
		//
		// }
		// }
		//
		// }
		//
		// }
		//
		// phoneCursor.close();
		//
		// // Query and loop for every email of the contact
		// Cursor emailCursor = contentResolver.query(
		// EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
		// new String[] { contact_id }, null);
		//
		// while (emailCursor.moveToNext()) {
		//
		// email = emailCursor.getString(emailCursor
		// .getColumnIndex(DATA));
		//
		// output.append("\nEmail:" + email);
		//
		// }
		//
		// emailCursor.close();
		// }
		//
		// output.append("\n");
		// // cursor.close();
		// }

		// if (contactnumber.size() == 0) {
		// Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
		// .show();
		// } else {

		
		
		
		for (int i = 0; i < contactnumber.size(); i++) {
			Log.e(": ", ""
					+ Imgrcontactunquieid.get(i)
					+ "------"
					+ contactnumber.get(i).replace("[(", "").replace(")", "")
							.replace(" ", "").replace(")]", "")
							.replace("(", "").replace("-", "").replace("]", "")
							.replace("[", ""));
			mDatasourceHandler.NewAllContactsNoPullUnquie(contactnumber.get(i)
					.replace("[(", "").replace(")", "").replace(" ", "")
					.replace(")]", "").replace("(", "").replace("-", "")
					.replace("]", "").replace("[", ""), contactname.get(i),
					contactlastname.get(i), null, null, "0", "", null,
					Imgrcontactunquieid.get(i));

			// mDatasourceHandler.AllContactsLocalUnquie(contactnumber.get(i)
			// .replace("[(", "").replace(")", "").replace(" ", "")
			// .replace(")]", "").replace("(", "").replace("-", "")
			// .replace("]", "").replace("[", ""), contactname.get(i),
			// contactlastname.get(i), Imgrcontactunquieid.get(i));

		}

		try {
			mCursor = mDatasourceHandler.AllContacts();
			Log.e("value in cursor at starting", "" + mCursor.getCount());
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			contactname.clear();
			contactnumber.clear();
			contactlastname.clear();
			isIMGR_array.clear();
			Imgrcontactunquieid.clear();
			mArrayListBlockedContact.clear();
			do {
				isIMGR_array.add(mCursor.getString(0).trim());
				contactname.add(mCursor.getString(1).trim());
				contactnumber.add(mCursor.getString(2).trim());
				Log.e("Cursor : ", "" + mCursor.getString(2));
				contactlastname.add(mCursor.getString(3).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		arraylist.clear();

		for (int i = 0; i < contactnumber.size(); i++) {
			if (mArrayListBlockedContact.get(i).equals("true")) {

			} else {
				Log.e("Model : ", "" + Imgrcontactunquieid.get(i) + "------"
						+ contactnumber.get(i));
				ContactPhoneModel wp = new ContactPhoneModel(
						contactname.get(i), contactnumber.get(i),
						isIMGR_array.get(i), contactlastname.get(i),
						Imgrcontactunquieid.get(i), contactname.get(i) + " "
								+ contactlastname.get(i));
				arraylist.add(wp);
			}

			// Binds all strings into an array

		}

		Log.e("arraylist.size1():", "" + arraylist.size());
		if (arraylist.size() == 0) {
			Toast.makeText(mContext, "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			adapter = new MyAdapter((FragmentActivity) mContext, contactname, contactnumber,
					isIMGR_array, arraylist);

			mListView.setAdapter(adapter);
//			refreshMethod();
			
		}

		
		// Collections.sort(contactname);

		// }

		// }
		// cursor.close();
	}

	public static void PulltorefreshLocalDataBaseFetchContacts() {
		contactname.clear();
		contactnumber.clear();
		contactList.clear();
		contactlastname.clear();
		Imgrcontactunquieid.clear();
		mArrayListBlockedContact.clear();
		String phoneNumber = null;
		String email = null;
//		Toast.makeText(mContext, "inside pul", Toast.LENGTH_SHORT).show();
		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;

		StringBuffer output = new StringBuffer();

		ContentResolver contentResolver = mContext.getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
				null);

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {

				String contact_id = cursor
						.getString(cursor.getColumnIndex(_ID));
				String name = cursor.getString(cursor
						.getColumnIndex(DISPLAY_NAME));

				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
						.getColumnIndex(HAS_PHONE_NUMBER)));

				if (hasPhoneNumber > 0) {

					output.append("\n First Name:" + name);

					// Query and loop for every phone number of the contact
					Cursor phoneCursor = contentResolver.query(
							PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor
								.getColumnIndex(NUMBER));
						output.append("\n Phone number:" + phoneNumber);
						// Log.e("name: ", ""+name);
						if (name.equals("")) {

						} else {

							if (mStringPassword.equals(phoneNumber
									.replace("[(", "").replace(")", "")
									.replace(" ", "").replace(")]", "")
									.replace("(", "").replace("-", "")
									.replace("]", "").replace("[", "")
									.replace("*", "").replace(" ", "")
									.replace("+1", "").replace("+91", ""))) {

							} else {
								contactList.add(new ExampleContactItem(name,
										name));
								// contactname.add(name);
								try {
									String[] splited = name.split(" ");

									// Log.e("splited: ", ""+splited.length);

									if (splited.length == 2) {
										contactname.add(splited[0]);
										contactlastname.add(splited[1]);
									}

									else if (splited.length == 1) {
										contactname.add(splited[0]);
										contactlastname.add("");
									} else {
										// Log.e("nothing: ", "Nothing");
										contactname.add(name);
										contactlastname.add("");
									}

									contactnumber.add(phoneNumber
											.replace("[(", "").replace(")", "")
											.replace(" ", "").replace(")]", "")
											.replace("(", "").replace("-", "")
											.replace("]", "").replace("[", "")
											.replace("*", ""));
									Imgrcontactunquieid.add(contact_id);
								} catch (Exception e) {

								}
							}

						}

					}

					phoneCursor.close();

					// Query and loop for every email of the contact
					Cursor emailCursor = contentResolver.query(
							EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (emailCursor.moveToNext()) {

						email = emailCursor.getString(emailCursor
								.getColumnIndex(DATA));

						output.append("\nEmail:" + email);

					}

					emailCursor.close();
				}

				output.append("\n");
				// cursor.close();
			}

			if (contactnumber.size() == 0) {
				Toast.makeText(mContext, "No Contacts", Toast.LENGTH_SHORT)
						.show();
			} else {

				for (int i = 0; i < contactnumber.size(); i++) {
					Log.e(": ",
							""
									+ contactname.get(i)
									+ "----"
									+ Imgrcontactunquieid.get(i)
									+ "------"
									+ contactnumber.get(i).replace("[(", "")
											.replace(")", "").replace(" ", "")
											.replace(")]", "").replace("(", "")
											.replace("-", "").replace("]", "")
											.replace("[", ""));
					mDatasourceHandler.NewAllContactsPull_AllContactsUnquie(
							contactnumber.get(i).replace("[(", "")
									.replace(")", "").replace(" ", "")
									.replace(")]", "").replace("(", "")
									.replace("-", "").replace("]", "")
									.replace("[", ""), contactname.get(i),
							contactlastname.get(i), null, null, "0", "",
							"Null", Imgrcontactunquieid.get(i), "false");

					// mDatasourceHandler.NewAllContactsPull(contactnumber.get(i)
					// .replace("[(", "").replace(")", "")
					// .replace(" ", "").replace(")]", "")
					// .replace("(", "").replace("-", "").replace("]", "")
					// .replace("[", ""), contactname.get(i),
					// contactlastname.get(i), Imgrcontactunquieid.get(i));

				}

				try {
					mCursor = mDatasourceHandler.AllContacts();
					Log.e("value in cursor at starting", mCursor.getCount()
							+ "");
				} catch (Exception e) {

					e.printStackTrace();
				}

				if (mCursor.getCount() != 0) {
					ImgrcontactunquieidNEW_DELETE.clear();
					do {

						ImgrcontactunquieidNEW_DELETE.add(mCursor.getString(8)
								.trim());

						// Log.e("ImgrcontactunquieidNEW_DELETE: ", ""
						// + ImgrcontactunquieidNEW_DELETE);

					} while (mCursor.moveToNext());

					mCursor.close();
				}

				// Log.e("ImgrcontactunquieidNEW_DELETE", ""+
				// ImgrcontactunquieidNEW_DELETE);

				ImgrcontactunquieidNEW_DELETE.removeAll(Imgrcontactunquieid);

				if (ImgrcontactunquieidNEW_DELETE.size() != 0) {

					for (int i = 0; i < ImgrcontactunquieidNEW_DELETE.size(); i++) {
						mDatasourceHandler
								.deleteRow(ImgrcontactunquieidNEW_DELETE.get(i));
					}
				} else {
					ImgrcontactunquieidNEW_DELETE.clear();
				}

				Log.e("NOW value in cursor at starting", mCursor.getCount()
						+ "");
				try {
					mCursor2 = mDatasourceHandler.AllContacts();
					Log.e("value in mCursor2 at starting", mCursor2.getCount()
							+ "");
				} catch (Exception e) {

					e.printStackTrace();
				}
				if (mCursor2.getCount() != 0) {
					contactname.clear();
					contactnumber.clear();
					contactlastname.clear();
					isIMGR_array.clear();
					Imgrcontactunquieid.clear();
					mArrayListBlockedContact.clear();
					do {
						isIMGR_array.add(mCursor2.getString(0).trim());
						contactname.add(mCursor2.getString(1).trim());
						contactnumber.add(mCursor2.getString(2).trim());
						Log.e("Cursor : ", "" + mCursor2.getString(2));
						contactlastname.add(mCursor2.getString(3).trim());
						Imgrcontactunquieid.add(mCursor2.getString(8).trim());
						mArrayListBlockedContact.add(mCursor2.getString(9)
								.trim());

					} while (mCursor2.moveToNext());

					mCursor2.close();
				}
				arraylist.clear();

				for (int i = 0; i < contactnumber.size(); i++) {
					if (mArrayListBlockedContact.get(i).equals("true")) {

					} else {
						Log.e("Model : ", "" + Imgrcontactunquieid.get(i)
								+ "------" + contactnumber.get(i));
						ContactPhoneModel wp = new ContactPhoneModel(
								contactname.get(i), contactnumber.get(i),
								isIMGR_array.get(i), contactlastname.get(i),
								Imgrcontactunquieid.get(i), contactname.get(i)
										+ " " + contactlastname.get(i));
						arraylist.add(wp);
					}

					// Binds all strings into an array

				}

				Log.e("arraylist.size2():", "" + arraylist.size());
				mSwipeRefreshLayout_AllContact.setRefreshing(false);

				adapter = new MyAdapter((FragmentActivity) mContext, contactname,
						contactnumber, isIMGR_array, arraylist);

				mListView.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
			cursor.close();
		}
	}

	public void NxtAndEverytimefetchContacts() {
		contactname.clear();
		contactnumber.clear();
		contactList.clear();
		contactlastname.clear();
		Imgrcontactunquieid.clear();

		String phoneNumber = null;
		String email = null;

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;

		StringBuffer output = new StringBuffer();

		ContentResolver contentResolver = getActivity().getContentResolver();

		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
				null);

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {

				String contact_id = cursor
						.getString(cursor.getColumnIndex(_ID));
				String name = cursor.getString(cursor
						.getColumnIndex(DISPLAY_NAME));

				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
						.getColumnIndex(HAS_PHONE_NUMBER)));

				if (hasPhoneNumber > 0) {

					output.append("\n First Name:" + name);

					// Query and loop for every phone number of the contact
					Cursor phoneCursor = contentResolver.query(
							PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor
								.getColumnIndex(NUMBER));
						output.append("\n Phone number:" + phoneNumber);
						// Log.e("name: ", ""+name);
						if (name.equals("")) {

						} else {

							if (mStringPassword.equals(phoneNumber
									.replace("[(", "").replace(")", "")
									.replace(" ", "").replace(")]", "")
									.replace("(", "").replace("-", "")
									.replace("]", "").replace("[", "")
									.replace("*", "").replace(" ", "")
									.replace("+1", "").replace("+91", ""))) {

							} else {
								contactList.add(new ExampleContactItem(name,
										name));
								try {
									String[] splited = name.split(" ");

									// Log.e("splited: ", ""+splited.length);

									if (splited.length == 2) {
										contactname.add(splited[0]);
										contactlastname.add(splited[1]);
									}

									else if (splited.length == 1) {
										contactname.add(splited[0]);
										contactlastname.add("");
									} else {
										// Log.e("nothing: ", "Nothing");
										contactname.add(name);
										contactlastname.add("");
									}
								} catch (Exception e) {

								}
								// contactname.add(name);
								contactnumber.add(phoneNumber);
								Imgrcontactunquieid.add(contact_id);
							}

						}

					}

					phoneCursor.close();

					// Query and loop for every email of the contact
					Cursor emailCursor = contentResolver.query(
							EmailCONTENT_URI, null, EmailCONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (emailCursor.moveToNext()) {

						email = emailCursor.getString(emailCursor
								.getColumnIndex(DATA));

						output.append("\nEmail:" + email);

					}

					emailCursor.close();
				}

				output.append("\n");
				// cursor.close();
			}
			// for (int i = 0; i < contactnumber.size(); i++) {
			// mDatasourceHandler.AllContactsNoPullUnquie(contactnumber.get(i)
			// .replace("[(", "").replace(")", "").replace(" ", "")
			// .replace(")]", "").replace("(", "").replace("-", "")
			// .replace("]", "").replace("[", ""), contactname.get(i),
			// contactlastname.get(i), null, null, "0", "", "Null",
			// Imgrcontactunquieid.get(i));
			// }
			//
			// try {
			// mCursor = mDatasourceHandler.AllContacts();
			// // Log.e("value in cursor at starting", mCursor.getCount() +
			// // "");
			// } catch (Exception e) {
			//
			// e.printStackTrace();
			// }
			//
			// if (mCursor.getCount() != 0) {
			// contactname.clear();
			// contactnumber.clear();
			// contactlastname.clear();
			// Imgrcontactunquieid.clear();
			// do {
			//
			// contactname.add(mCursor.getString(1).trim());
			// contactnumber.add(mCursor.getString(2).trim());
			// contactlastname.add(mCursor.getString(3).trim());
			// Imgrcontactunquieid.add(mCursor.getString(8).trim());
			//
			// } while (mCursor.moveToNext());
			//
			// mCursor.close();
			// }
			for (int i = 0; i < contactnumber.size(); i++) {
				Log.e("",
						"" + contactname.get(i) + "---"
								+ contactlastname.get(i));
			}

			new EverytimeALLCONTACT().execute();

		}
	}

	public class ALLCONTACT extends AsyncTask<String, Void, String> {
		public ALLCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {

			String mString = JsonParserConnector.PostImgrContact("imgr",
					contactnumber);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// Log.e("result: ", "" + result);
			UI.hideProgressDialog();
			mAllContactModels = JsonParserConnector.ModelAllContactNormalUser(
					result, contactnumber, contactname, contactlastname,
					Imgrcontactunquieid);
			allcontactlist(mAllContactModels);

			// Log.e("mAllContactModels: ", "" + mAllContactModels.size());

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(getActivity());
		}

	}

	public class EverytimeALLCONTACT extends AsyncTask<String, Void, String> {
		public EverytimeALLCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {
			String mString = JsonParserConnector.PostImgrContact("imgr",
					contactnumber);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			mSwipeRefreshLayout_AllContact.setRefreshing(false);
			// Log.e("result: ", "" + result);
			mAllContactModels = JsonParserConnector.ModelAllContactNormalUser(
					result, contactnumber, contactname, contactlastname,
					Imgrcontactunquieid);
			Updateallcontactlist(mAllContactModels);

			// UI.hideProgressDialog();

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// UI.showProgressDialog(getActivity());
		}

	}

	private void allcontactlist(List<AllContactModel> mAllContactModels) {

		for (int i = 0; i < mAllContactModels.size(); i++) {
			final AllContactModel item = mAllContactModels.get(i);
			Log.e("lastname: ", "" + item.getLastName());

			mDatasourceHandler.AllContactsNoPullUnquie(item.getPhonenumber(),
					item.getFirstName(), item.getLastName(), null, null,
					item.getIsImgrUser(), "", item.getBase64(),
					item.getPhoneid());

		}

		try {
			mCursor = mDatasourceHandler.AllContacts();
			// Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			contactname.clear();
			contactnumber.clear();
			Imgrcontactunquieid.clear();
			contactlastname.clear();
			mArrayListBlockedContact.clear();
			do {
				isIMGR_array.add(mCursor.getString(0).trim());
				contactname.add(mCursor.getString(1).trim());
				contactnumber.add(mCursor.getString(2).trim());
				contactlastname.add(mCursor.getString(3).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		// Log.e("contactnumber: ", "" + contactnumber);
		// Log.e("contactname: ", "" + contactname);
		// Log.e("contactlastname: ", "" + contactlastname);
		if (contactnumber.size() == 0) {
			Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			for (int i = 0; i < contactnumber.size(); i++) {
				if (mArrayListBlockedContact.get(i).equals("true")) {

				} else {
					ContactPhoneModel wp = new ContactPhoneModel(
							contactname.get(i), contactnumber.get(i),
							isIMGR_array.get(i), contactlastname.get(i),
							Imgrcontactunquieid.get(i), contactname.get(i)
									+ " " + contactlastname.get(i));
					// Binds all strings into an array
					arraylist.add(wp);
				}

			}

			adapter = new MyAdapter(getActivity(), contactname, contactnumber,
					isIMGR_array, arraylist);

			mListView.setAdapter(adapter);
		}

	}

	private void Updateallcontactlist(List<AllContactModel> mAllContactModels) {

		for (int i = 0; i < mAllContactModels.size(); i++) {
			final AllContactModel item = mAllContactModels.get(i);
			if (item.getPhonenumber().replace("#", "").replace("*", "")
					.replace("-", "").replace("(", "").replace(")", "")
					.replace(" ", "").replace("[(", "").replace(")", "")
					.replace(" ", "").replace(")]", "").replace("(", "")
					.replace("-", "").replace("]", "").replace("[", "")
					.replace(" ", "").replace("(", "").replace(")", "")
					.replace("-", "").equals("")) {

			} else {

				mDatasourceHandler.UpdateImgrContact(item.getPhonenumber()
						.replace("#", "").replace("*", "").replace("-", "")
						.replace("(", "").replace(")", "").replace(" ", "")
						.replace("[(", "").replace(")", "").replace(" ", "")
						.replace(")]", "").replace("(", "").replace("-", "")
						.replace("]", "").replace("[", "").replace(" ", "")
						.replace("(", "").replace(")", "").replace("-", ""),
						item.getJid(), item.getMemberId(),
						item.getIsImgrUser(), item.getBase64(),
						item.getPhoneid(), item.getFirstName(),
						item.getLastName());

			}

		}

		try {
			mCursor = mDatasourceHandler.AllContacts();

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			contactname.clear();
			contactnumber.clear();
			Imgrcontactunquieid.clear();
			mArrayListBlockedContact.clear();
			contactlastname.clear();
			do {

				contactname.add(mCursor.getString(1).trim());
				contactnumber.add(mCursor.getString(2).trim());
				contactlastname.add(mCursor.getString(3).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		// Log.e("contactnumber: ", "" + contactnumber);
		if (contactnumber.size() == 0) {
			Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			arraylist.clear();
			for (int i = 0; i < contactnumber.size(); i++) {
				if (mArrayListBlockedContact.get(i).equals("true")) {

				} else {
					ContactPhoneModel wp = new ContactPhoneModel(
							contactname.get(i), contactnumber.get(i),
							isIMGR_array.get(i), contactlastname.get(i),
							Imgrcontactunquieid.get(i), contactname.get(i)
									+ " " + contactlastname.get(i));
					// Binds all strings into an array
					arraylist.add(wp);
				}

			}

			mListView.setAdapter(adapter);
			adapter.notifyDataSetChanged();
		}

	}
	
	
	
	//////////////////////////
	
	public class IMGRCONTACT extends AsyncTask<String, Void, String> {
		public IMGRCONTACT() {
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("pull contactnumber: ", "" + contactnumber);
			String mString = JsonParserConnector.PostImgrContact("imgr",
					contactnumber);
			return mString;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("POST: ", "" + ImgrcontactnumberNEW);
			mImgrContacts = JsonParserConnector.ModelIMGRContact(result,
					ImgrcontactnumberNEW, contactname, Imgrcontactunquieid,
					contactlastname);

			Imgrcontact(mImgrContacts);

//			mSwipeLayout_Imgr.setRefreshing(false);

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

	}
	
	
	
	public void Imgrcontact(List<AllContactModel> mAllContactModels) {

		ArrayList<String> NEWPhone = new ArrayList<String>();
		ArrayList<String> NEWName = new ArrayList<String>();
		ArrayList<String> NEWuniqueID = new ArrayList<String>();
		ArrayList<String> NEWlastname = new ArrayList<String>();
		NEWPhone.clear();
		NEWName.clear();
		NEWuniqueID.clear();
		NEWlastname.clear();
		for (int i = 0; i < mAllContactModels.size(); i++) {
			final AllContactModel item = mAllContactModels.get(i);

			Log.e("Fragment getPhoneid: ", item.getPhoneid()
					+ "---first---:"
					+ item.getFirstName()
					+ "---last----:"
					+ item.getMemberId()
					+ "----------"
					+ item.getPhonenumber().replace("#", "").replace("*", "")
							.replace("-", "").replace("(", "").replace(")", "")
							.replace(" ", "") + "----------" + item.getJid());

			mDatasourceHandler.UpdateImgrContact(
					item.getPhonenumber().replace("#", "").replace("*", "")
							.replace("-", "").replace("(", "").replace(")", "")
							.replace(" ", ""), item.getJid(),
					item.getMemberId(), item.getIsImgrUser(), item.getBase64(),
					item.getPhoneid(), item.getFirstName(), item.getLastName());
			NEWPhone.add(item.getPhonenumber().replace("#", "")
					.replace("*", "").replace("-", "").replace("(", "")
					.replace(")", "").replace(" ", ""));
			NEWName.add(item.getFirstName());
			NEWlastname.add(item.getLastName());
			NEWuniqueID.add(item.getPhoneid());
		}

		ArrayList<String> unique = new ArrayList<String>();
		unique.clear();

		//
		unique.addAll(Imgrcontactunquieid);
		Log.e("Before new", "" + NEWuniqueID.size());
		Log.e("Before imgr", "" + Imgrcontactunquieid.size());

		unique.removeAll(NEWuniqueID);
		Log.e("Remove unqie:", "" + unique.size());

		for (int i = 0; i < unique.size(); i++) {
			mDatasourceHandler.UpdateNoImgrContact("0", unique.get(i));
		}

		try {
			mCursor = mDatasourceHandler.ImgrContacts();
			Log.e("Fragment Database: ", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			Imgrcontactname.clear();
			Imgrcontactnumber.clear();
			Imgrcontactlastname.clear();
			Imgrcontactunquieid.clear();
			mArrayListBlockedContact.clear();
			ImgrMemberId.clear();
			do {

				Imgrcontactname.add(mCursor.getString(1).trim());
				Imgrcontactnumber.add(mCursor.getString(2).trim());
				Imgrcontactlastname.add(mCursor.getString(3));
				ImgrMemberId.add(mCursor.getString(5));
				base64_array.add(mCursor.getString(7).trim());
				Imgrcontactunquieid.add(mCursor.getString(8).trim());
				mArrayListBlockedContact.add(mCursor.getString(9).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}

		if (Imgrcontactname.size() == 0) {
			Toast.makeText(getActivity(), "No Contacts", Toast.LENGTH_SHORT)
					.show();
		} else {
			Log.e("ImgrMemberId: ", "" + ImgrMemberId);
			arraylistImgr.clear();
			ImgrcontactnameNEW.clear();
			// /ImgrcontactnumberNEW.clear();
			ImgrcontactlastnameNEW.clear();
			ImgrcontactunquieidNEW.clear();
			for (int i = 0; i < Imgrcontactnumber.size(); i++) {
				if (mArrayListBlockedContact.get(i).equals("true")) {

				} else {
					IMGR_Model wp = new IMGR_Model(Imgrcontactname.get(i),
							Imgrcontactlastname.get(i),
							Imgrcontactnumber.get(i), base64_array.get(i),
							Imgrcontactunquieid.get(i), Imgrcontactname.get(i)
									+ " " + Imgrcontactlastname.get(i),
							ImgrMemberId.get(i));
					ImgrcontactnameNEW.add(Imgrcontactname.get(i));
					ImgrcontactlastnameNEW.add(Imgrcontactnumber.get(i));

					arraylistImgr.add(wp);
					mDatasourceHandler.SingleUser_set("true", "true", "true",
							"true", Imgrcontactunquieid.get(i));
				}

			}
			Log.e("Imgrcontactunquieid: ", "" + Imgrcontactunquieid);
			Log.e("arraylistImgr: ", "" + arraylistImgr.size());
			
//			if(!isRefresh){
//			mImgrAdapter = new ImgrAdapter(getActivity(), ImgrcontactnameNEW,
//					ImgrcontactlastnameNEW, arraylistImgr);
//
//			mFastListView.setAdapter(mImgrAdapter);
//			}else{
//				isRefresh = false;
//				
//				AllContact mAllContact = new AllContact();
//				changeFragment(mAllContact);
//			}
			// mImgrAdapter.notifyDataSetChanged();
		}

	}
	
	public static void refreshMethod(){
		 
		new AsyncTask<Void, Void, Void>(){

			protected void onPreExecute() {
				pd = new ProgressDialog(mContext);
			       pd.setMessage("loading");
			       pd.show();
			};
			
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				PulltorefreshLocalDataBaseFetchContacts();
				Contactimgr.imgrfetchContacts();
				return null;
			}
			
			protected void onPostExecute(Void result) {
				if (pd != null)
			      {
			         pd.dismiss();
			         Toast.makeText(mContext, "Done ....", Toast.LENGTH_SHORT).show();
			      }
			};
			
		}.execute();
	}
}
