package com.imgr.chat.home;

/**
 * author: amit agnihotri
 */
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.imgr.chat.AppBaseFragment;
import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.AddContact;
import com.imgr.chat.message.MessageSelectImgrFrds;
import com.imgr.service.ExitActivity;
import com.imgr.service.ScreenReceiver;
import com.service.FakeService;
import com.service.ImgrAppService;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

public class MenuActivity extends AppBaseFragment implements
		View.OnClickListener {
	boolean viewopen = false;
	private ResideMenu resideMenu;
	@SuppressWarnings("unused")
	private MenuActivity mContext;
	private ResideMenuItem itemHome;
	private ResideMenuItem itemMessage;
	ImageButton mImageButton, mImageButton_add, mButton_messgae_imgr;
	private ResideMenuItem itemSettings;
	TextView mTextViewTitle, img_textedit;
	ContactFragment contactFragment;
	MessageFragment messageFragment;
	SettingsFragment settingsFragment;

	Editor editor;

	int selected_tab = 1;
	FragmentTransaction t;
	FragmentManager my_fragment_manager;
	String mString_fragment;
	public static String first_time, mString_IMGR;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
		
//        ExitActivity.exitApplicationAnRemoveFromRecent(this);
//        if(android.os.Build.VERSION.SDK_INT >= 21)
//        {
//        	//finishAndRemoveTask();
//        }
//        else
//        {
//           finish();
//        }
//        
        
		
		setContentView(R.layout.activity_menu);
		StrictMode.ThreadPolicy policy1 = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy1);
		registerBaseActivityReceiver();
		mString_fragment = getIntent().getStringExtra("fragment");
		// System.out.println("-mString_fragment-"+mString_fragment);
		mImageButton = (ImageButton) findViewById(R.id.img_left_nav);
		mImageButton_add = (ImageButton) findViewById(R.id.img_add_new);
		mButton_messgae_imgr = (ImageButton) findViewById(R.id.img_message);
		mTextViewTitle = (TextView) findViewById(R.id.txt_title);
		img_textedit = (TextView) findViewById(R.id.img_textedit);
		mContext = this;

		contactFragment = new ContactFragment();
		messageFragment = new MessageFragment(getApplicationContext());
		settingsFragment = new SettingsFragment();
		setUpMenu();
		// edited by Saveen 24Dec
		int SDK_INT = android.os.Build.VERSION.SDK_INT;
		if (SDK_INT > 8) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			if (!ImgrAppService.isMyServiceRunning(this)) {
				startService(new Intent(this, FakeService.class));
			}

		}

		// System.out.println("==device_id="+Secure.getString(MenuActivity.this.getContentResolver(),Secure.ANDROID_ID));
		if (savedInstanceState == null)
			if (mString_fragment.equals("Contacts")) {
				first_time = "1";
				mString_IMGR = "1";
				mButton_messgae_imgr.setVisibility(View.GONE);
				img_textedit.setVisibility(View.GONE);
				mImageButton_add.setVisibility(View.VISIBLE);

				changeFragment(contactFragment);
			} else {
				mTextViewTitle.setText("Message");
				first_time = "2";
				mString_IMGR = "2";
				mImageButton_add.setVisibility(View.GONE);
				img_textedit.setVisibility(View.VISIBLE);
				mButton_messgae_imgr.setVisibility(View.VISIBLE);
				changeFragment(messageFragment);
			}

		mImageButton_add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_add = new Intent(MenuActivity.this,
						AddContact.class);
				startActivity(mIntent_add);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);

			}
		});
		img_textedit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_add = new Intent(MenuActivity.this,
						Select_Delete_Java.class);
				startActivity(mIntent_add);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);
			}
		});
		mButton_messgae_imgr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent mIntent_message = new Intent(MenuActivity.this,
						MessageSelectImgrFrds.class);
				startActivity(mIntent_message);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_right);

			}
		});
	}

    public static void exitApplicationAnRemoveFromRecent(Context mContext)
    {
        Intent intent = new Intent(mContext, ExitActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK  | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_NO_ANIMATION);

        mContext.startActivity(intent);
    }
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();

		Log.e("Destoryyyyyyyyyyyyy", "destroyeeeeddddddd");

		SharedPreferences newSharedPreferences = getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		editor = newSharedPreferences.edit();
		editor.putString("isTerminated", "yes");
		editor.commit();

		Log.e("Destoryyyyyyyyyyyyy",
				newSharedPreferences.getString("isTerminated", "no"));
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		if (contactFragment.isVisible()) {
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			// finish();
		} else if (messageFragment.isVisible()) {
			// finish();
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
		} else if (settingsFragment.isVisible()) {
			// finish();
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
		}

	}

	private void setUpMenu() {

		// attach to current activity;
		resideMenu = new ResideMenu(this);
		resideMenu.setBackground(R.drawable.ic_slider_background);
		resideMenu.attachToActivity(this);
		resideMenu.setMenuListener(menuListener);
		// valid scale factor is between 0.0f and 1.0f. leftmenu'width is
		// 150dip.
		resideMenu.setScaleValue(0.6f);

		// create menu items;
		itemHome = new ResideMenuItem(this, R.drawable.ic_contacts, "Contacts",
				1);
		itemMessage = new ResideMenuItem(this, R.drawable.ic_messages,
				"Messages", 2);
		itemSettings = new ResideMenuItem(this, R.drawable.ic_settings,
				"Settings", 3);
		// itemHome.Bold_Size(this, Typeface.BOLD);
		itemHome.setOnClickListener(this);
		itemMessage.setOnClickListener(this);
		itemSettings.setOnClickListener(this);

		resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
		resideMenu.addMenuItem(itemMessage, ResideMenu.DIRECTION_LEFT);
		resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);

		// You can disable a direction by setting ->
		resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
		mImageButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
			}
		});

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return resideMenu.dispatchTouchEvent(ev);
	}

	@Override
	public void onClick(View view) {

		if (view == itemHome) {

			mTextViewTitle.setText("Contacts");
			itemHome.Bold_Size(MenuActivity.this, Typeface.BOLD, 1);
			mButton_messgae_imgr.setVisibility(View.GONE);
			img_textedit.setVisibility(View.GONE);
			mImageButton_add.setVisibility(View.VISIBLE);

			changeFragment(new ContactFragment());
		} else if (view == itemMessage) {
			// Log.e("Id2 : ", ""+itemMessage.getId());
			viewopen = false;
			mTextViewTitle.setText("Messages");
			itemMessage.Bold_Size(MenuActivity.this, Typeface.BOLD, 2);
			mImageButton_add.setVisibility(View.GONE);
			img_textedit.setVisibility(View.VISIBLE);
			mButton_messgae_imgr.setVisibility(View.VISIBLE);

			changeFragment(new MessageFragment(getApplicationContext()));
		} else if (view == itemSettings) {
			// Log.e("Id3 : ", ""+itemSettings.getId());
			viewopen = false;
			mImageButton_add.setVisibility(View.GONE);
			mButton_messgae_imgr.setVisibility(View.GONE);
			img_textedit.setVisibility(View.GONE);
			mTextViewTitle.setText("Settings");
			itemSettings.Bold_Size(MenuActivity.this, Typeface.BOLD, 3);
			changeFragment(new SettingsFragment());
		}

		resideMenu.closeMenu();
	}

	private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
		@Override
		public void openMenu() {
			// Toast.makeText(mContext, "Menu is open!",
			// Toast.LENGTH_SHORT).show();
			// itemHome.Bold_Size(MenuActivity.this, Typeface.BOLD);
			// itemSettings.Bold_Size(MenuActivity.this, Typeface.NORMAL);
			// itemMessage.Bold_Size(MenuActivity.this, Typeface.NORMAL);
		}

		@Override
		public void closeMenu() {
			// Toast.makeText(mContext, "Menu is closed!",
			// Toast.LENGTH_SHORT).show();
		}
	};

	private void changeFragment(Fragment targetFragment) {
		resideMenu.clearIgnoredViewList();
		// resideMenu.cl
		// my_fragment_manager=getSupportFragmentManager();
		// t = my_fragment_manager.beginTransaction();
		// t.replace(R.id.menu_framepage, targetFragment, "fragment")
		// .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		// t.addToBackStack(null);
		// t.commit();
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.menu_framepage, targetFragment, "fragment")
				.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
				.commit();
	}

	// What good method is to access resideMenu？
	public ResideMenu getResideMenu() {
		return resideMenu;
	}

}
