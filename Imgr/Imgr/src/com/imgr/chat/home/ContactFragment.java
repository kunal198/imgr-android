package com.imgr.chat.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.imgr.chat.R;

/**
 * author: amit agnihotri
 */

public class ContactFragment extends Fragment {
	// ListView mListView;
	FragmentTransaction t;
	FragmentManager my_fragment_manager;

	View V;
	Button mButton_invite;

	Contactimgr mContactimgr;
	AllContact mAllContact;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		V = inflater.inflate(R.layout.contacts, container, false);
		mContactimgr = new Contactimgr();
		mAllContact = new AllContact();

		changeFragment(mContactimgr);

		return V;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	public void changeFragment(Fragment targetFragment) {

		my_fragment_manager = getActivity().getSupportFragmentManager();
		t = my_fragment_manager.beginTransaction();
		t.replace(R.id.menu_frame_page, targetFragment);
		// t.addToBackStack(null);
		t.commit();

	}

}
