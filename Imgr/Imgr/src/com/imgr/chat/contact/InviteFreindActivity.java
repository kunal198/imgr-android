package com.imgr.chat.contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.AppBaseFragment;
import com.imgr.chat.R;
import com.imgr.chat.adapter.InviteAdapter;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.FastListView;
import com.imgr.chat.model.Product;

public class InviteFreindActivity extends AppBaseActivity {
	// ListView mListView;
	FastListView mListView;
	InviteAdapter mAdapter;
	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	Button mButton_selectall;
	TextView txtBack;
	private EditText searchBox;

	ArrayList<String> contactname = new ArrayList<String>();
	ArrayList<String> contactlast = new ArrayList<String>();
	ArrayList<String> contactnumber = new ArrayList<String>();
	ArrayList<Product> products = new ArrayList<Product>();
	String result;
	ArrayList<String> mArrayListPHONE = new ArrayList<String>();
	Button mButtonInvite;

	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.activity_invite_listview);
		mListView = (FastListView) findViewById(R.id.list_View_IMGR);
		mButton_selectall = (Button) findViewById(R.id.selectall);
		mButtonInvite = (Button) findViewById(R.id.btn_invite);
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
//		fetchinfo();
//		fetchNonImgrContacts();
		mButtonInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mArrayListPHONE.clear();
				for (Product p : mAdapter.getBox()) {
					if (p.box) {
						result = "\n" + p.getNumber();
						mArrayListPHONE.add(p.getNumber());

					}
				}

				Log.e("mArrayListPHONE: ", "" + mArrayListPHONE);
				String mString = mArrayListPHONE.toString().replace("[", "")
						.replace("]", "").replace(",", "");
				Log.e("", "" + mString);
				StringBuilder uri = new StringBuilder("sms:");
				for (int i = 0; i < mArrayListPHONE.size(); i++) {
					uri.append(mArrayListPHONE.get(i));
					uri.append(", ");
				}

				Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
				smsVIntent.setType("vnd.android-dir/mms-sms");
				// smsVIntent.putExtra("address", mString);
				smsVIntent.setData(Uri.parse(uri.toString()));
				smsVIntent
						.putExtra(
								"sms_body",
								"Check out imgr instant messaging app.Download it today from http://imgr.im/app");
				try {
					startActivity(smsVIntent);
				} catch (Exception ex) {
					Toast.makeText(getApplicationContext(),
							"Your sms has failed...", Toast.LENGTH_LONG).show();

					ex.printStackTrace();

				}

			}
		});

		mButton_selectall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mButton_selectall.getText().toString().equals("Select All")) {
					products.clear();
					for (int i = 0; i < contactnumber.size(); i++) {
						products.add(new Product(contactnumber.get(i),
								contactname.get(i), contactlast.get(i),
								contactname.get(i) + " " + contactlast.get(i),
								true));
						// products.add(new Product(contactnumber.get(i),
						// contactname
						// .get(i), true));
					}
					mAdapter.notifyDataSetChanged();
					mButton_selectall.setText("Deselect All");
				} else {
					products.clear();
					for (int i = 0; i < contactnumber.size(); i++) {
						products.add(new Product(contactnumber.get(i),
								contactname.get(i), contactlast.get(i),
								contactname.get(i) + " " + contactlast.get(i),
								false));
						// products.add(new Product(contactnumber.get(i),
						// contactname
						// .get(i), true));
					}
					mAdapter.notifyDataSetChanged();
					mButton_selectall.setText("Select All");
				}

			}
		});
		txtBack = (TextView) findViewById(R.id.btn_back_loginpage);
		txtBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});
		searchBox = (EditText) findViewById(R.id.input_search_query);
		searchBox.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					mAdapter.filter(s.toString());

				} catch (Exception e) {

				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@SuppressLint("DefaultLocale")
			@Override
			public void afterTextChanged(Editable s) {
				// searchString = searchBox.getText().toString().trim()
				// .toUpperCase();
				// searchBox.setText("");

			}
		});

	}

	public void fetchinfo() {

		try {
			mCursor = mDatasourceHandler.AllContacts();
			// Log.e("value in cursor at starting", mCursor.getCount() + "");
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			contactname.clear();
			contactnumber.clear();
			contactlast.clear();
			do {

				contactname.add(mCursor.getString(1).trim());
				contactnumber.add(mCursor.getString(2).trim());
				contactlast.add(mCursor.getString(3).trim());

			} while (mCursor.moveToNext());

			mCursor.close();
		}
		Log.e("contactlast: ", "" + contactlast);
		for (int i = 0; i < contactnumber.size(); i++) {
			products.add(new Product(contactnumber.get(i), contactname.get(i),
					contactlast.get(i), contactname.get(i) + " "
							+ contactlast.get(i), false));
			
			Log.e("Product Class",""+contactnumber.get(i)+"----"+ contactname.get(i)+"----"+ 
					contactlast.get(i)+"----"+  contactname.get(i) +"----"+ 
							 contactlast.get(i)+"----"+  false);
		}
		
		Log.e("Product",""+products);
		Log.e("contactname",""+contactname);
		Log.e("contactnumber",""+contactnumber);
//		Log.e("contactnumber",""+products.);
//		mAdapter = new InviteAdapter(InviteFreindActivity.this, contactname,
//				contactnumber, products);
//
//		mListView.setAdapter(mAdapter);
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		fetchNonImgrContacts();
	}
	
	public void fetchNonImgrContacts(){
	
		Cursor cursor = mDatasourceHandler.AllContacts();
		Cursor cursor1 = mDatasourceHandler.NoImgrContacts();
		DatabaseHelper dh = new DatabaseHelper(getApplicationContext());
		ArrayList<HashMap<String, String>> cursor2 = dh.NewAllContacts();
		
		Log.e("My Contacts",""+cursor2.size());
		
		ArrayList<HashMap<String, String>> allContact = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> imgrContact = new ArrayList<HashMap<String,String>>();
		
		if (cursor.getCount() != 0) {
//			allContact.clear();
			do {
				HashMap<String, String> map = new HashMap();
				
				map.put("contact_name", cursor.getString(1).trim());
				map.put("contact_number", cursor.getString(2).trim());
				map.put("contact_last", cursor.getString(3).trim());
				
//				contactname.add(mCursor.getString(1).trim());
//				contactnumber.add(mCursor.getString(2).trim());
//				contactlast.add(mCursor.getString(3).trim());
				
				allContact.add(map);

			} while (cursor.moveToNext());

			cursor.close();
		}
		
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
		for(HashMap<String, String> mapp : cursor2){
			
			if(mapp.get("_isImgrUser").equals("0")){
//			String _promoonoff = mapp.get("_promoonoff");
//			String _firstnamecontact = mapp.get("_firstnamecontact");
//			String _isImgrUser = mapp.get("_isImgrUser");
//			String _lastpromoid = mapp.get("_lastpromoid");
//			String _memberidcontact = mapp.get("_memberidcontact");
//			String _lastnamecontact = mapp.get("_lastnamecontact");
//			String _blockcontact = mapp.get("_blockcontact");
//			String autorotatepromoonoff = mapp.get("autorotatepromoonoff");
//			String _base64contact = mapp.get("_base64contact");
//			String _personalpromoonoff = mapp.get("_personalpromoonoff");
//			String _phonenocontact = mapp.get("_phonenocontact");
//			String _sponseredpromoonoff = mapp.get("_sponseredpromoonoff");
//			String _phoneidcontact = mapp.get("_phoneidcontact");
//			String TEXT = mapp.get("TEXT");
//			String _emailcontacts = mapp.get("_emailcontacts");
//			String _jidcontacts = mapp.get("_jidcontacts");
			
			arrayList.add(mapp);
			}
			
			
		}
		Log.e("Uninvited",""+arrayList);
		
		Collections.sort(arrayList,new Comparator<HashMap<String,String>>(){
		    public int compare(HashMap<String,String> mapping1,HashMap<String,String> mapping2){
		        return mapping1.get("_firstnamecontact").compareToIgnoreCase(mapping2.get("_firstnamecontact"));
		}
		});
		Log.e("invited",""+arrayList);
		for (int i = 0; i < arrayList.size(); i++) {
			HashMap<String, String> hs = arrayList.get(i);
			String fullName = hs.get("_firstnamecontact")+" "+hs.get("_lastnamecontact");
			products.add(new Product(hs.get("_phonenocontact"), hs.get("_firstnamecontact"),
					hs.get("_lastnamecontact"), fullName + " "
							+ hs.get("_lastnamecontact"), false));
//			public Product(String _phone, String name, String last, String fullname,
//					boolean _box)
//			9467693261----abcd----efgh----abcd----efgh----false
			contactname.add(hs.get("_firstnamecontact"));
			
			contactnumber.add(hs.get("_phonenocontact"));
		}
		Collections.sort(contactname);
		mAdapter = new InviteAdapter(InviteFreindActivity.this, contactname,
				contactnumber, products);

		mListView.setAdapter(mAdapter);
		
		Log.e("New Array",""+arrayList);
		
		if (cursor1.getCount() != 0) {
//			imgrContact.clear();
			do {
				HashMap<String, String> map = new HashMap();
				
				map.put("contact_name", cursor1.getString(1).trim());
				map.put("contact_number", cursor1.getString(2).trim());
				map.put("contact_last", cursor1.getString(3).trim());
				
//				contactname.add(mCursor.getString(1).trim());
//				contactnumber.add(mCursor.getString(2).trim());
//				contactlast.add(mCursor.getString(3).trim());
				imgrContact.add(map);
				
			} while (cursor1.moveToNext());

			cursor1.close();
			
		}
		Log.e("All Contacts",""+allContact.size());
		Log.e("Imgr Contacts",""+imgrContact.size());
		ArrayList<HashMap<String, String>> inviteContact = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> hashmap;
		HashMap<String, String> hashMap;
//		for(int i=0; i<allContact.size();i++){
//			hashmap = allContact.get(i);
//			String contact = hashmap.get("contact_number");
//			String name = hashmap.get("contact_name");
//			String last = hashmap.get("contact_last");
//			
//			for(int j=0; j<imgrContact.size(); j++){
//				HashMap<String, String> map1 = new HashMap<String, String>();
//				map1 = imgrContact.get(j);
//				
//				String imgrCont = map1.get("contact_number");
//				
//				if(!contact.equals(imgrCont)){
//					
//					
//					hashMap = new HashMap<String, String>();
//					hashMap.put("contact_number", contact);
//					hashMap.put("contact_name", name);
//					hashMap.put("contact_last", last);
//					
////					if(inviteContact.contains(hashMap)){
//					inviteContact.add(hashMap);//}
//				}
//				
//			}
//		}
		
//		for(HashMap<String, String> map : allContact){
//			
//			String contact = map.get("contact_number");
//			String name = map.get("contact_name");
//			String last = map.get("contact_last");
//			
//			for(HashMap<String, String> map1 : imgrContact){
//				String imgrCont = map1.get("contact_number");
//				
//				if(!contact.equals(imgrCont)){
//					HashMap<String, String> hashMap111= new HashMap<String, String>();
//					hashMap111.put("contact_number", contact);
//					hashMap111.put("contact_name", name);
//					hashMap111.put("contact_last", last);
//					
////					if(inviteContact.contains(hashMap)){
//					
//					if(inviteContact.size()>0){
//						for(int i=0; i<inviteContact.size(); i++){
//							HashMap<String, String> Map111= inviteContact.get(i);
//							
//							if(Map111.equals(hashMap111)){
//								
//							}else{
//								inviteContact.add(hashMap111);
//							}
//							
//						}
////					inviteContact.add(hashMap111);
//					}else{
//						inviteContact.add(hashMap111);
//					}//}
//				}
//				
//				
//			}
//		    }
		
		for(int i=0; i<inviteContact.size(); i++){
			HashMap<String, String> hs = inviteContact.get(i);
			
			
		}
		

				
		Log.e("Invite Friends ",""+inviteContact.size());
		
//		Log.e("Invite Friends ",""+it);
		
	}
}
