package com.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.imgr.chat.R;

public class FakeService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		stopForeground(true);
		super.onDestroy();
	}

	@Override
	public void onCreate() {
		super.onCreate();
//		NotificationCompat.Builder builder = new NotificationCompat.Builder(
//				this);
//		builder.setContentText("Imgr App").setContentTitle("Imgr App")
//				.setSmallIcon(R.drawable.ic_launcher);
		if (!ImgrAppService.isMyServiceRunning(this)) {
//			startForeground(100200, builder.build());
			startService(new Intent(FakeService.this, ImgrAppService.class));
		}
	}
}
