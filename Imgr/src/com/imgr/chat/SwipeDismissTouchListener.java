package com.imgr.chat;

import android.app.Activity;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class SwipeDismissTouchListener implements View.OnTouchListener {

	static final String logTag = "ActivitySwipeDetector";
	private Activity activity;
	static final int MIN_DISTANCE = 100;
	private float downX, upX;

	public SwipeDismissTouchListener(Activity mActivity) {
		activity = mActivity;
	}

	public void onRightToLeftSwipe() {
		Log.i(logTag, "RightToLeftSwipe!");
		Toast.makeText(activity, "RightToLeftSwipe", Toast.LENGTH_SHORT).show();
		// activity.doSomething();
	}

	public void onLeftToRightSwipe() {
		Log.i(logTag, "LeftToRightSwipe!");
		Toast.makeText(activity, "LeftToRightSwipe", Toast.LENGTH_SHORT).show();
		// activity.doSomething();
	}

	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: {
			downX = event.getX();
			return true;
		}
		case MotionEvent.ACTION_UP: {
			upX = event.getX();

			float deltaX = downX - upX;

			if (Math.abs(deltaX) > MIN_DISTANCE) {
				if (deltaX < 0) {
					this.onLeftToRightSwipe();
					return true;
				}
				if (deltaX > 0) {
					this.onRightToLeftSwipe();
					return true;
				}
			} else {
				Log.i(logTag, "Swipe was only " + Math.abs(deltaX)
						+ " long horizontally, need at least " + MIN_DISTANCE);
				// return false; // We don't consume the event
			}

			return false; // no swipe horizontally and no swipe vertically
		}// case MotionEvent.ACTION_UP:
		}
		return false;
	}
}
