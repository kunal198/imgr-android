package com.imgr.chat.register;

/**
 * author: amit agnihotri
 */
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.BaseConvert;
import com.imgr.chat.R;
import com.imgr.chat.adapter.SponsoredChatAdapter;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.MenuActivity;
import com.imgr.chat.model.SponsoredModel;
import com.imgr.chat.setting.ViewSponsored;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;

public class CompleteProfileActivity extends AppBaseActivity {
	String s_code, s_phone, versionName, deviceid;
	EditText edtphone, mEditTextFirstname, mEditTextLastName, mEditTextEmail;
	ImageView imgOk;
	String final_path = null;
	String mStringPassword, deviceId;

	SharedPreferences sharedPreferences;
	SharedPreferences mSharedPreferences_gcm;
	Editor editor;
	AlertDialog pinDialog;
	String mStringBase64;
	String regid;

	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "Imgr";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;
	ImageView mImageViewProfile;

	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;

	Bitmap targetBitmap;

	ArrayList<String> mArrayListpromoname;
	ArrayList<String> mArrayListpromoid;
	ArrayList<String> mArrayListImageUrl;
	ArrayList<String> mArrayListisdelete;
	ArrayList<String> mArrayListisactive;
	ArrayList<String> mArrayListisenabled;
	ArrayList<String> mArrayListpersonal_or;

	ArrayList<String> mArrayListheader;
	ArrayList<String> mArrayListlink_footer;
	ArrayList<String> mArrayListlink_text_footer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_complete_your_profile);
		registerBaseActivityReceiver();
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mImageViewProfile = (ImageView) findViewById(R.id.profile_image);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		s_phone = sharedPreferences.getString(Constant.PHONENO, "");
		s_code = sharedPreferences.getString(Constant.COUNTRYCODE, "");
		// s_phone = getIntent().getStringExtra("phonenumber");
		edtphone = (EditText) findViewById(R.id.edt_phone);
		mEditTextFirstname = (EditText) findViewById(R.id.edt_first_name);
		mEditTextLastName = (EditText) findViewById(R.id.edt_last_name);
		mEditTextEmail = (EditText) findViewById(R.id.edt_email);

		imgOk = (ImageView) findViewById(R.id.img_ok);
		edtphone.setText(s_code + " " + s_phone);

		ALL_shared_Prefernces_Personal();
		try {
			versionName = this.getPackageManager().getPackageInfo(
					this.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("versionName:", "" + versionName);
		deviceid = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		Log.e("deviceid:", "" + deviceid);
		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");
		imgOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean receive = validation(edtphone.getText().toString());
				if (receive) {
					new CompleteProfile().execute();

				}

			}
		});
		mImageViewProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectImage();
			}
		});

		mSharedPreferences_gcm = this.getSharedPreferences(
				Constant.IMGRCHATGCM, Context.MODE_PRIVATE);
		editor = mSharedPreferences_gcm.edit();
		regid = mSharedPreferences_gcm.getString(Constant.GCM_ID, null);

		/**
		 * GCM
		 */

		if (regid == null) {
			initGcm();
		} else {

		}

		mArrayListpromoname = new ArrayList<String>();
		mArrayListpromoid = new ArrayList<String>();
		mArrayListisdelete = new ArrayList<String>();
		mArrayListisactive = new ArrayList<String>();
		mArrayListisenabled = new ArrayList<String>();
		mArrayListImageUrl = new ArrayList<String>();
		mArrayListpersonal_or = new ArrayList<String>();

		mArrayListheader = new ArrayList<String>();
		mArrayListlink_footer = new ArrayList<String>();
		mArrayListlink_text_footer = new ArrayList<String>();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	/**
	 * 
	 * GCM WORK
	 * 
	 */

	private void initGcm() {

		GCMRegistrar.checkDevice(this);

		GCMRegistrar.checkManifest(this);

		regid = GCMRegistrar.getRegistrationId(this);

		if (regid.equals("")) {
			GCMRegistrar.register(this, Constant.GCM_PROJECT_NO);

			if (Constant.DEBUG)
				Log.i(Constant.DEBUG_TAG,
						"UiMain - No Regid Found. Starting registration");
		} else {

			if (GCMRegistrar.isRegisteredOnServer(CompleteProfileActivity.this)) {
				if (Constant.DEBUG)
					Log.v(Constant.DEBUG_TAG,
							"UiMain - Regid registered on server");
			} else {
				try {
					// sendHttpPushRegister();
				} catch (Exception e) {
					e.printStackTrace();

				}
			}

		}

	}

	public void sharedPrefernces() {
		editor = sharedPreferences.edit();
		editor.putString(Constant.classdefine, "CompleteProfileActivity");
		editor.putString(Constant.PHONENO, s_phone);
		editor.putString(Constant.COUNTRYCODE, s_code);

		editor.commit();

	}

	protected boolean validation_dialog(String firstname, String lastname,
			String email) {
		// TODO Auto-generated method stub
		if (firstname.matches("")) {

			return false;
		}
		if (lastname.matches("")) {

			return false;
		}
		if (email.matches("")) {

			return false;
		}

		return true;
	}

	protected void showInputDialog() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.completeprofile_dialog, null);
		// EditText pinCode = (EditText) v.findViewById(R.id.pin_dialog);
		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final TextView mTextViewHead = (TextView) v
				.findViewById(R.id.heading_text);
		final TextView mTextViewinside = (TextView) v
				.findViewById(R.id.inside_text);
		// pinCode.setHint("Enter Pin");
		// pinCode.setGravity(C);
		boolean flag_ = validation_dialog(mEditTextFirstname.getText()
				.toString(), mEditTextLastName.getText().toString(),
				mEditTextEmail.getText().toString());
		if (flag_) {
			mTextViewHead.setText("Personal Info. received");
			mTextViewinside.setText("Complete Profile");
			mButtonPop.setText("OK");
		} else {
			mTextViewHead.setText("Personal Info.");
			mTextViewinside.setText("Profile info not completed");
			// mButtonPop.setText("OK");

		}

		// pinCode.setBackgroundColor(Color.parseColor("#ffffff"));
		pinDialog = new AlertDialog.Builder(CompleteProfileActivity.this)
				.setView(v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						Log.e("mStringBase64: ", "" + mStringBase64);
						if (mStringBase64 != null) {

						} else {
							Log.e("mStringBase64: ", "INDIDE");
							mStringBase64 = "";
						}

						mDatasourceHandler.insertProfile(s_phone,
								mEditTextFirstname.getText().toString(),
								mEditTextLastName.getText().toString(),
								mEditTextEmail.getText().toString(),
								mStringBase64, s_code);

						pinDialog.cancel();
						sharedPrefernces();
						mStringBase64 = null;
						Intent intent = new Intent(
								CompleteProfileActivity.this,
								MenuActivity.class);
						intent.putExtra("fragment", "Contacts");
						startActivity(intent);
						overridePendingTransition(R.anim.fade_in,
								R.anim.fade_out);

						finish();

					}
				});

			}
		});

		pinDialog.show();

	}

	protected boolean validation(String strPhoneNo) {
		// TODO Auto-generated method stub
		if (strPhoneNo.matches("")) {
			LogMessage.showDialog(this, null, "Please enter your phone number",
					null, "Ok");
			return false;
		}
		if (strPhoneNo.length() < 10) {
			LogMessage.showDialog(this, null,
					"Please enter valid phone number", null, "Ok");
			return false;
		}

		return true;
	}

	public class CompleteProfile extends AsyncTask<String, Void, String> {
		public CompleteProfile() {
		}

		@Override
		protected String doInBackground(String... params) {
			s_phone = s_phone.replace(" ", "").replace("(", "")
					.replace(")", "").replace("-", "");
			String result = JsonParserConnector.PostPersonalInfo("imgr", "1.0",
					s_code, "2", s_phone, "android", deviceid, mEditTextEmail
							.getText().toString(), mEditTextFirstname.getText()
							.toString(),
					mEditTextLastName.getText().toString(), mStringBase64, "");
			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {

				new ExecuteDevice().execute();
			} else {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(CompleteProfileActivity.this);
		}

	}

	/**
	 * Execute Device Asynctask
	 */
	class ExecuteDevice extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			regid = mSharedPreferences_gcm.getString(Constant.GCM_ID, null);
			s_phone = s_phone.replace(" ", "").replace("(", "")
					.replace(")", "").replace("-", "");
			Log.e("regid: ", "" + regid);
			Log.e("s_phone: ", "" + s_phone);

			String url = JsonParserConnector.getDevice("imgr", s_phone, regid,
					deviceid);

			Log.e("url----", "" + url);
			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(CompleteProfileActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				new ExecuteSponsored().execute();
			}
		}

	}

	/**
	 * Execute Sponsored Asynctask
	 */
	class ExecuteSponsored extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			String url = JsonParserConnector.getSponsored("0", mStringPassword,
					deviceid);

			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(CompleteProfileActivity.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.e("result----", "" + result);

			UI.hideProgressDialog();
			SponsornedList(result);

		}

	}

	private void SponsornedList(String response) {
		try {
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);
			if (jsonOBject.getBoolean("success")) {
				mArrayListpromoid.clear();
				mArrayListpromoname.clear();
				mArrayListisdelete.clear();
				mArrayListisactive.clear();
				mArrayListisenabled.clear();
				mArrayListImageUrl.clear();
				mArrayListlink_text_footer.clear();
				mArrayListheader.clear();
				mArrayListlink_footer.clear();

				JSONArray jArray = jsonOBject.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					JSONObject jsonOBjectContact = jArray.getJSONObject(i);
					// SponsoredModel mContact = new SponsoredModel();
					String mStringspromoid = jsonOBjectContact
							.getString("promo_id");
					String mStringspromo_type = jsonOBjectContact
							.getString("promo_type");

					String mStringspromo_name = jsonOBjectContact
							.getString("promo_name");
					String mStringspromo_image = jsonOBjectContact
							.getString("promo_image");

					String mStringspromo_link = jsonOBjectContact
							.getString("promo_link");

					String mStringspromo_link_text = jsonOBjectContact
							.getString("promo_link_text");
					String mStringspromo_message_header = jsonOBjectContact
							.getString("promo_message_header");
					String mStringsis_deleted = jsonOBjectContact
							.getString("is_deleted");
					String mStringsmodified_date = jsonOBjectContact
							.getString("modified_date");
					String mStringsisActive = jsonOBjectContact
							.getString("isActive");
					String mStringsis_enabled = jsonOBjectContact
							.getString("is_enabled");

					mArrayListpromoid.add(mStringspromoid);
					mArrayListpromoname.add(mStringspromo_name);
					mArrayListisdelete.add(mStringsis_deleted);
					mArrayListisactive.add(mStringsisActive);
					mArrayListisenabled.add(mStringsis_enabled);
					mArrayListImageUrl.add(mStringspromo_image);
					mArrayListlink_text_footer.add(mStringspromo_link_text);
					mArrayListheader.add(mStringspromo_message_header);
					mArrayListlink_footer.add(mStringspromo_link);

				}

			} else {

			}

			for (int i = 0; i < mArrayListpromoid.size(); i++) {
				Log.e("Insert IDS: ", "" + mArrayListpromoid.get(i)
						+ "--------" + "https://imgrapp.com/release/"
						+ mArrayListImageUrl.get(i));
				mDatasourceHandler.insertSponsored(
						mArrayListpromoid.get(i),
						"no_personal",
						mArrayListpromoname.get(i),
						"https://imgrapp.com/release/"
								+ mArrayListImageUrl.get(i),
						mArrayListlink_footer.get(i),
						mArrayListlink_text_footer.get(i),
						mArrayListheader.get(i), mArrayListisdelete.get(i), "",
						mArrayListisactive.get(i), mArrayListisenabled.get(i));
				mArrayListpersonal_or.add("no_personal");
			}

			showInputDialog();

		} catch (JSONException e) {

			Log.e("", e.getMessage());
		}

	}

	protected void selectImage() {

		final CharSequence[] options = { "Choose from Gallery", "Take Photo",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				CompleteProfileActivity.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(CompleteProfileActivity.this,
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(CompleteProfileActivity.this,
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// video successfully recorded
				// preview the recorded video

				previewGalleryImage(data);

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(CompleteProfileActivity.this,
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(CompleteProfileActivity.this,
						"Sorry! Process Failed", Toast.LENGTH_SHORT).show();
			}
			break;
		}

	}

	private int getExifOrientation() {
		ExifInterface exif;
		int orientation = 0;
		try {
			exif = new ExifInterface(final_path);
			orientation = exif
					.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.d("", "got orientation " + orientation);
		return orientation;
	}

	private Bitmap getBitmapRotation(Bitmap bitmap) {
		Matrix matrix = new Matrix();

		int rotation = 0;
		switch (getExifOrientation()) {
		case ExifInterface.ORIENTATION_ROTATE_180:
			rotation = 180;
			matrix.postRotate(rotation);

			break;
		case ExifInterface.ORIENTATION_ROTATE_90:
			rotation = 90;
			matrix.postRotate(rotation);

			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			rotation = 270;
			matrix.postRotate(rotation);

			break;
		}

		return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);

	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Log.e("STREAM", "" + stream);
		// Log.e("BITMAP: ", "" + bitmap);
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to
		// // which
		// // format
		// // you want.

		byte[] byte_arr = stream.toByteArray();
		// long lengthbmp = byte_arr.length;
		// Log.e("lengthbmp---------", ""+lengthbmp);

		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	private void previewCapturedImage() {
		// TODO Auto-generated method stub
		// bimatp factory
		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inJustDecodeBounds = true;

		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		BitmapFactory.decodeFile(fileUri.getPath(), options);
		final_path = fileUri.getPath();
		// options.inSampleSize = 8;

		int imageHeight = options.outHeight;
		int imageWidth = options.outWidth;
		String imageType = options.outMimeType;

		options.inSampleSize = calculateInSampleSize(options, 100, 100);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		Bitmap bitMap = BitmapFactory.decodeFile(fileUri.getPath(), options);
		Bitmap b = getBitmapRotation(bitMap);
		// int imageHeight_a = options.outHeight;
		// int imageWidth_1 = options.outWidth;
		targetBitmap = getRoundedCornerBitmap(b);
		mStringBase64 = convert_image(targetBitmap);

		mImageViewProfile.setImageBitmap(targetBitmap);
		deleteRecursive(mediaStorageDir);

	}

	public Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		int targetWidth = 100;
		int targetHeight = 100;
		targetBitmap = Bitmap.createBitmap(targetWidth,

		targetHeight, Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);

		return targetBitmap;
	}

	private void previewGalleryImage(Intent data) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// downsizing image as it throws OutOfMemory Exception for larger
		// images
		// options.inSampleSize = 8;
		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		if (CompleteProfileActivity.this.getContentResolver() != null) {
			Cursor cursor = CompleteProfileActivity.this.getContentResolver()
					.query(selectedImage, filePathColumn, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();

				BitmapFactory.decodeFile(filePath, options);
				// bitmap = MediaStore.Images.Media.getBitmap(
				// this.getContentResolver(), data.getData());
				final_path = filePath;
				options.inSampleSize = calculateInSampleSize(options, 250, 250);

				// Decode bitmap with inSampleSize set
				options.inJustDecodeBounds = false;

				Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

				int imageHeight_a = options.outHeight;
				int imageWidth_1 = options.outWidth;

				// bitmap = getResizedBitmap(bitmap, frameHeight, frameWidth);
				// // bitmap = getResizedBitmap(bitmap, 500, 500);
				//
				Bitmap b = getBitmapRotation(bitmap);
				targetBitmap = getRoundedCornerBitmap(b);
				mStringBase64 = convert_image(targetBitmap);
				mImageViewProfile.setImageBitmap(targetBitmap);
				deleteRecursive(mediaStorageDir);
			} else {
				Toast.makeText(CompleteProfileActivity.this,
						"Unable to locate image .. Please try again",
						Toast.LENGTH_LONG).show();
			}

		} else {
			Toast.makeText(CompleteProfileActivity.this,
					"Unable to locate image .. Please try again",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 6;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		Log.d("Insample size", inSampleSize + "");
		return inSampleSize;
	}

	public void ALL_shared_Prefernces_Personal() {
		editor = sharedPreferences.edit();

		editor.putString(Constant.PERSONAL_ADS, "true");
		editor.putString(Constant.SPONSORED_ADS, "true");
		editor.putString(Constant.SETTING_MAIN_PROMO_ON_OFF, "true");
		editor.putString(Constant.SETTING_PROMO_AUTO_ROTATE, "true");
		editor.putBoolean(Constant.on_off_ads, true);
		editor.putString(Constant.SETTING_NOTIFICATION_ON_OFF, "true");
		editor.commit();

	}

}
