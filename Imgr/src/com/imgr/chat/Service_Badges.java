package com.imgr.chat;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class Service_Badges extends Service {
	SharedPreferences mSharedPreferences;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("DEMO", "Started");
		Timer tmr = new Timer();
		tmr.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {

				/*
				 * int value; mSharedPreferences = getApplicationContext()
				 * .getSharedPreferences( Constant.IMGRNOTIFICATION_RECEIVE,
				 * Context.MODE_PRIVATE); value =
				 * mSharedPreferences.getInt(Constant.COUNT_NOTIFICATION, 0);
				 * 
				 * Log.e("DEMO", "Changing : " + value);
				 * 
				 * PackageManager pm =
				 * getApplicationContext().getPackageManager();
				 * 
				 * String lastEnabled = getLastEnabled(); // Getting last
				 * enabled // from shared // preference
				 * 
				 * if (TextUtils.isEmpty(lastEnabled)) { lastEnabled =
				 * "com.imgr.chat.SplashActivity"; }
				 * 
				 * ComponentName componentName = new ComponentName(
				 * "com.imgr.chat", lastEnabled);
				 * pm.setComponentEnabledSetting(componentName,
				 * PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				 * PackageManager.GET_ACTIVITIES);
				 * 
				 * Log.i("DEMO", "Removing : " + lastEnabled);
				 * 
				 * if (value <= 0) { lastEnabled =
				 * "com.imgr.chat.SplashActivity"; } else if (value == 1) {
				 * lastEnabled = "com.imgr.chat.a" + value; } else if (value ==
				 * 2) { lastEnabled = "com.imgr.chat.a" + value; } else if
				 * (value == 3) { lastEnabled = "com.imgr.chat.a" + value; }
				 * else if (value == 4) { lastEnabled = "com.imgr.chat.a" +
				 * value; } else if (value == 5) { lastEnabled =
				 * "com.imgr.chat.a" + value; } else if (value == 6) {
				 * lastEnabled = "com.imgr.chat.a" + value; } else if (value ==
				 * 7) { lastEnabled = "com.imgr.chat.a" + value; } else if
				 * (value == 8) { lastEnabled = "com.imgr.chat.a" + value; }
				 * else if (value == 9) { lastEnabled = "com.imgr.chat.a" +
				 * value; } else if (value == 10) { lastEnabled =
				 * "com.imgr.chat.a" + value; } else if (value > 10) {
				 * lastEnabled = "com.imgr.chat.a10p"; } else { lastEnabled =
				 * "com.imgr.chat.SplashActivity"; }
				 * 
				 * componentName = new ComponentName("com.imgr.chat",
				 * lastEnabled); pm.setComponentEnabledSetting(componentName,
				 * PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				 * PackageManager.GET_ACTIVITIES); Log.e("DEMO", "Adding : " +
				 * lastEnabled); setLastEnabled(lastEnabled); // Saving last
				 * enabled to shared
				 */
				// preference
			}
		}, 1000, 1000);
		return START_STICKY;
	}

	private String getLastEnabled() {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		return pref.getString("LastEnabled", "");
	}

	private void setLastEnabled(String value) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("LastEnabled", value);
		editor.commit();
	}

}
