package com.imgr.chat.database;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.imgr.chat.GCMIntentService;
import com.imgr.chat.UILApplication;
import com.imgr.chat.constant.Constant;

import android.app.ActivityManager;
import android.app.ActivityManager.RecentTaskInfo;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatasourceHandler {

	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	String allcolumn[] = { DatabaseHelper.KEY_isImgrUser,
			DatabaseHelper.KEY_FIRST_NAME_CONTACTS,
			DatabaseHelper.KEY_PHONENO_CONTACTS,
			DatabaseHelper.KEY_LASTNAME_CONTACTS,
			DatabaseHelper.KEY_JID_CONTACTS,
			DatabaseHelper.KEY_MEMBERID_CONTACTS,
			DatabaseHelper.KEY_EMAIL_CONTACTS,
			DatabaseHelper.KEY_IMAGE_BASE_CONTACT,
			DatabaseHelper.KEY_PHONE_CONTACTS_ID,
			DatabaseHelper.KEY_BLOCKED_CONTACT,
			DatabaseHelper.PERSONAL_PROMO_ON_OFF,
			DatabaseHelper.SPONSERED_PROMO_ON_OFF, DatabaseHelper.PROMO_ON_OFF,
			DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF,
			DatabaseHelper.LAST_PROMO_ID };

	String allcolumn_User_set[] = { DatabaseHelper.KEY_USER_SET_PROMO,
			DatabaseHelper.KEY_USER_SET_AUTOROATE,
			DatabaseHelper.KEY_USER_SET_SPONSORED_ADS,
			DatabaseHelper.KEY_USER_SET_PERSONAL_ADS,
			DatabaseHelper.KEY_USER_SET_UNQIUE_ID,
			DatabaseHelper.KEY_LAST_PROMO_ID };

	String allcolumn_Profile[] = { DatabaseHelper.KEY_FIRST_NAME_PROFILE,
			DatabaseHelper.KEY_LAST_NAME_PROFILE,
			DatabaseHelper.KEY_NAME_PHONE, DatabaseHelper.KEY_EMAIL_PHONE,
			DatabaseHelper.KEY_IMAGE_BASE, DatabaseHelper.KEY_NAME_PHONE_CODE };

	String allcolumn_Chat_history[] = { DatabaseHelper.TKEY_UNQUIE_CHAT,
			DatabaseHelper.KEY_CHAT_USERNAME, DatabaseHelper.KEY_TOJID_CHAT,
			DatabaseHelper.KEY_FROMJID_CHAT, DatabaseHelper.KEY_MESSAGE_CHAT,
			DatabaseHelper.KEY_TIME_CHAT, DatabaseHelper.KEY_RECEIVE_TICK_CHAT,
			DatabaseHelper.KEY_BUBBLE_OR_NOT_CHAT,
			DatabaseHelper.KEY_AUTO_INC_CHAT, DatabaseHelper.KEY_DATE_CHAT };

	String allcolumn_Chat_history_Recent[] = { DatabaseHelper.KEY_RECENT_DATE,
			DatabaseHelper.KEY_RECENT_FIRSTNAME,
			DatabaseHelper.KEY_RECENT_JID_FROM,
			DatabaseHelper.KEY_RECENT_JID_TO,
			DatabaseHelper.KEY_RECENT_LASTNAME,
			DatabaseHelper.KEY_RECENT_MESSAGE, DatabaseHelper.KEY_RECENT_TIME,
			DatabaseHelper.KEY_RECENT_TIMESTAMP,
			DatabaseHelper.KEY_RECENT_COUNT,
			DatabaseHelper.RECENT_PERSONAL_PROMO_ON_OFF,
			DatabaseHelper.RECENT_SPONSERED_PROMO_ON_OFF,
			DatabaseHelper.RECENT_PROMO_ON_OFF,
			DatabaseHelper.RECENT_AUTO_ROTATE_PROMO_ON_OFF,
			DatabaseHelper.RECENT_LAST_PROMO_ID };

	String allcolumn_Sponsored[] = { DatabaseHelper.KEY_promo_id,
			DatabaseHelper.KEY_isActive, DatabaseHelper.KEY_is_deleted,
			DatabaseHelper.KEY_is_enabled, DatabaseHelper.KEY_promo_name,
			DatabaseHelper.KEY_promo_type, DatabaseHelper.KEY_promo_image,
			DatabaseHelper.KEY_promo_message_header,
			DatabaseHelper.KEY_promo_link_text, DatabaseHelper.KEY_promo_type };

	String allcolumn_fetch_Sponsored[] = { DatabaseHelper.KEY_promo_id,
			DatabaseHelper.KEY_promo_message_header,
			DatabaseHelper.KEY_promo_link, DatabaseHelper.KEY_promo_link_text,
			DatabaseHelper.KEY_promo_image, DatabaseHelper.KEY_promo_name,
			DatabaseHelper.KEY_isActive, DatabaseHelper.KEY_is_deleted,
			DatabaseHelper.KEY_is_enabled, DatabaseHelper.KEY_promo_type };

	String allcolumn_Personal[] = { DatabaseHelper.KEY_personal_promo_id,
			DatabaseHelper.KEY_personal_isActive,
			DatabaseHelper.KEY_personal_is_deleted,
			DatabaseHelper.KEY_personal_is_enabled,
			DatabaseHelper.KEY_personal_promo_name,
			DatabaseHelper.KEY_personal_promo_final_type };

	String allcolumn_Notifications[] = { DatabaseHelper.KEY_NOTIFICATION_ID,
			DatabaseHelper.KEY_PHONENO_NOTIFICATION,
			DatabaseHelper.KEY_AUTO_INC_NOTIFICATION };

	public DatasourceHandler(Context context) {
		try {
			dbHelper = new DatabaseHelper(context);
			database = dbHelper.getWritableDatabase();
			// Log.e("ddddddddddddddddd", "Data Base and table created");
		} catch (Exception e) {
			// TODO: handle exception
			// Log.e("eeeeeeeeeeeeeeeee", e.getMessage());
		}

	}

	public void close() {
		dbHelper.close();
	}

	public boolean insertNotifcations(String notification_id, String phone_no) {

		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();

			cv.put(DatabaseHelper.KEY_NOTIFICATION_ID, notification_id);
			cv.put(DatabaseHelper.KEY_PHONENO_NOTIFICATION, phone_no);

			long row = database.insert(DatabaseHelper.TABLE_NOTIFICATIONS,
					null, cv);
			// Log.e("row----", "" + row);
			if (row > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	public Cursor Notifications_Fetch(String phoneNo) throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_NOTIFICATIONS,
				allcolumn_Notifications,
				DatabaseHelper.KEY_PHONENO_NOTIFICATION + " LIKE ?",
				new String[] { "%" + phoneNo + "%" }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	public int Notifications_Count_Fetch() throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_NOTIFICATIONS,
				allcolumn_Notifications,
				DatabaseHelper.KEY_PHONENO_NOTIFICATION + " != ?",
				new String[] { "" }, null, null, null, null);
		if (mCursor != null) {
			int count = mCursor.getCount();
			mCursor.close();
			return count;
		} else {
			return 0;
		}
	}

	public int Notifications_Delete(String phoneNo) throws SQLException {

		int rows = database.delete(DatabaseHelper.TABLE_NOTIFICATIONS,
				DatabaseHelper.KEY_PHONENO_NOTIFICATION + " LIKE ?",
				new String[] { "%" + phoneNo + "%" });

		return rows;

		/*
		 * if (rows > 0) { return true; } else { return false; }
		 */
	}

	public boolean insertPersonal(String promo_id, String promo_type,
			String promo_name, String promo_image, String promo_link,
			String promo_link_text, String promo_message_header,
			String is_deleted, String modified_date, String isActive,
			String is_enabled, String finaltype) {

		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();

			cv.put(DatabaseHelper.KEY_personal_promo_type, promo_type);
			cv.put(DatabaseHelper.KEY_personal_promo_name, promo_name);
			cv.put(DatabaseHelper.KEY_personal_promo_image, promo_image);
			cv.put(DatabaseHelper.KEY_personal_promo_link, promo_link);

			cv.put(DatabaseHelper.KEY_personal_promo_link_text, promo_link_text);
			cv.put(DatabaseHelper.KEY_personal_promo_message_header,
					promo_message_header);
			cv.put(DatabaseHelper.KEY_personal_is_deleted, is_deleted);
			cv.put(DatabaseHelper.KEY_personal_modified_date, modified_date);
			cv.put(DatabaseHelper.KEY_personal_isActive, isActive);
			cv.put(DatabaseHelper.KEY_personal_promo_final_type, finaltype);
			cv.put(DatabaseHelper.KEY_personal_is_enabled, is_enabled);
			cv.put(DatabaseHelper.KEY_personal_promo_id, promo_id);
			long row = database.insert(DatabaseHelper.TABLE_ALL_PERSONAL, null,
					cv);
			// Log.e("row----", "" + row);
			if (row > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	public boolean insertSponsored(String promo_id, String promo_type,
			String promo_name, String promo_image, String promo_link,
			String promo_link_text, String promo_message_header,
			String is_deleted, String modified_date, String isActive,
			String is_enabled) {

		/*
		 * Log.e("promo_data","promo_id: "+promo_id+", promo_type: "+promo_type+
		 * ", promo_name: "+promo_name+", promo_image: "+promo_image+
		 * ", promo_link: "+promo_link+", promo_link_text: "+promo_link_text+
		 * ", promo_message_header: "+promo_message_header+
		 * ", is_deleted: "+is_deleted
		 * +", modified_date: "+modified_date+", isActive: "
		 * +isActive+", is_enabled: "+is_enabled );
		 */

		boolean flag = false;
		try {

			ContentValues cv = new ContentValues();

			cv.put(DatabaseHelper.KEY_promo_type, promo_type);
			cv.put(DatabaseHelper.KEY_promo_name, promo_name);
			cv.put(DatabaseHelper.KEY_promo_image, promo_image);
			cv.put(DatabaseHelper.KEY_promo_link, promo_link);

			cv.put(DatabaseHelper.KEY_promo_link_text, promo_link_text);
			cv.put(DatabaseHelper.KEY_promo_message_header,
					promo_message_header);
			cv.put(DatabaseHelper.KEY_is_deleted, is_deleted);
			cv.put(DatabaseHelper.KEY_modified_date, modified_date);
			cv.put(DatabaseHelper.KEY_isActive, isActive);

			cv.put(DatabaseHelper.KEY_is_enabled, is_enabled);
			cv.put(DatabaseHelper.KEY_promo_id, promo_id);

			Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
					allcolumn_fetch_Sponsored, DatabaseHelper.KEY_promo_id
							+ "=" + promo_id, null, null, null, null);
			if (mCursor.getCount() != 0) {

				long row = database.update(DatabaseHelper.TABLE_ALL_SPONSORED,
						cv, "_promo_id" + "='" + promo_id + "'", null);
				if (row > 0) {
					flag = true;
				}

			} else {

				long row = database.insert(DatabaseHelper.TABLE_ALL_SPONSORED,
						null, cv);
				// Log.e("row----", "" + row);
				if (row > 0) {
					flag = true;
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	public boolean update_Sponsored(String promo_id, String promo_type,
			String promo_name, String promo_image, String promo_link,
			String promo_link_text, String promo_message_header,
			String is_deleted, String modified_date, String isActive,
			String is_enabled) {

		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();

			cv.put(DatabaseHelper.KEY_promo_type, promo_type);
			cv.put(DatabaseHelper.KEY_promo_name, promo_name);
			cv.put(DatabaseHelper.KEY_promo_image, promo_image);
			cv.put(DatabaseHelper.KEY_promo_link, promo_link);

			cv.put(DatabaseHelper.KEY_promo_link_text, promo_link_text);
			cv.put(DatabaseHelper.KEY_promo_message_header,
					promo_message_header);
			cv.put(DatabaseHelper.KEY_is_deleted, is_deleted);
			cv.put(DatabaseHelper.KEY_modified_date, modified_date);
			cv.put(DatabaseHelper.KEY_isActive, isActive);

			cv.put(DatabaseHelper.KEY_is_enabled, is_enabled);
			// cv.put(DatabaseHelper.KEY_promo_id, promo_id);
			long row = database.update(DatabaseHelper.TABLE_ALL_SPONSORED, cv,
					"_promo_id" + "='" + promo_id + "'", null);
			// long row = database.insert(DatabaseHelper.TABLE_ALL_SPONSORED,
			// null, cv);
			// Log.e("row----", "" + row);
			if (row > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	public boolean updateSponsored(String promo_id, String promo_type,
			String promo_name, String promo_image, String promo_link,
			String promo_link_text, String promo_message_header,
			String is_deleted, String modified_date, String isActive,
			String is_enabled) {

		boolean flag = false;
		try {

			Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
					allcolumn_fetch_Sponsored, "_promo_id= ?",
					new String[] { promo_id }, null, null, null, null);

			if (mCursor.getCount() != 0) {

				do {
					ContentValues cv = new ContentValues();

					cv.put(DatabaseHelper.KEY_promo_type, promo_type);
					cv.put(DatabaseHelper.KEY_promo_name, promo_name);
					cv.put(DatabaseHelper.KEY_promo_image, promo_image);
					cv.put(DatabaseHelper.KEY_promo_link, promo_link);

					cv.put(DatabaseHelper.KEY_promo_link_text, promo_link_text);
					cv.put(DatabaseHelper.KEY_promo_message_header,
							promo_message_header);
					cv.put(DatabaseHelper.KEY_is_deleted, is_deleted);
					cv.put(DatabaseHelper.KEY_modified_date, modified_date);
					cv.put(DatabaseHelper.KEY_isActive, isActive);

					cv.put(DatabaseHelper.KEY_is_enabled, is_enabled);
					// cv.put(DatabaseHelper.KEY_promo_id, promo_id);

					long row = database.update(
							DatabaseHelper.TABLE_ALL_SPONSORED, cv, "_promo_id"
									+ "='" + promo_id + "'", null);
					if (row > 0) {
						flag = true;
					}

				} while (mCursor.moveToNext());

				mCursor.close();

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	public Cursor PullSponsoredUnquie(String promo_id, String promo_type,
			String promo_name, String promo_image, String promo_link,
			String promo_link_text, String promo_message_header,
			String is_deleted, String modified_date, String isActive,
			String is_enabled) throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored, "_promo_id= ?",
				new String[] { promo_id }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				// Log.e("----------pull to fr5esh-------------_promo_id: ", ""
				// + promo_id+"-------is_enabled------"+is_enabled);
				ContentValues cv = new ContentValues();

				cv.put(DatabaseHelper.KEY_promo_type, promo_type);
				cv.put(DatabaseHelper.KEY_promo_name, promo_name);
				cv.put(DatabaseHelper.KEY_promo_image, promo_image);
				cv.put(DatabaseHelper.KEY_promo_link, promo_link);

				cv.put(DatabaseHelper.KEY_promo_link_text, promo_link_text);
				cv.put(DatabaseHelper.KEY_promo_message_header,
						promo_message_header);
				cv.put(DatabaseHelper.KEY_is_deleted, is_deleted);
				cv.put(DatabaseHelper.KEY_modified_date, modified_date);
				cv.put(DatabaseHelper.KEY_isActive, isActive);

				cv.put(DatabaseHelper.KEY_is_enabled, is_enabled);
				// cv.put(DatabaseHelper.KEY_promo_id, promo_id);

				long row = database.update(DatabaseHelper.TABLE_ALL_SPONSORED,
						cv, "_promo_id" + "='" + promo_id + "'", null);
				// Log.e("older update contact unique update row----", "" +
				// row);

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {
			ContentValues cv = new ContentValues();

			cv.put(DatabaseHelper.KEY_promo_type, promo_type);
			cv.put(DatabaseHelper.KEY_promo_name, promo_name);
			cv.put(DatabaseHelper.KEY_promo_image, promo_image);
			cv.put(DatabaseHelper.KEY_promo_link, promo_link);

			cv.put(DatabaseHelper.KEY_promo_link_text, promo_link_text);
			cv.put(DatabaseHelper.KEY_promo_message_header,
					promo_message_header);
			cv.put(DatabaseHelper.KEY_is_deleted, is_deleted);
			cv.put(DatabaseHelper.KEY_modified_date, modified_date);
			cv.put(DatabaseHelper.KEY_isActive, isActive);

			cv.put(DatabaseHelper.KEY_is_enabled, is_enabled);
			cv.put(DatabaseHelper.KEY_promo_id, promo_id);

			long row = database.insert(DatabaseHelper.TABLE_ALL_SPONSORED,
					null, cv);
			// Log.e("new add simple row----", "" + row);
		}

		return mCursor;
	}

	// insert================profile section ============

	public boolean Unique_message_Id(String messageid) {
		String all[] = { DatabaseHelper.TKEY_UNQUIE_CHAT };
		boolean flag = true;
		try {
			Cursor mCursor = database.query(DatabaseHelper.TABLE_CHATTING, all,
					"_iduniquechat= ?", new String[] { messageid }, null, null,
					null, null);

			if (mCursor.getCount() != 0) {

				do {
					flag = false;

				} while (mCursor.moveToNext());

				mCursor.close();

			} else {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		// Log.e("uniquenessssssssssssssss",""+flag);

		return flag;
	}

	public boolean insertChathistory(String message_id, String username,
			String tojid, String fromjid, String message, String time,
			String mString_promo_id, String mString_receive_or_not,
			String date, Context context, String number, String isSender) {

		Log.e("message111111111----", message);

		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.TKEY_UNQUIE_CHAT, message_id);
			cv.put(DatabaseHelper.KEY_CHAT_USERNAME, username);
			cv.put(DatabaseHelper.KEY_TOJID_CHAT, tojid);
			cv.put(DatabaseHelper.KEY_FROMJID_CHAT, fromjid);
			cv.put(DatabaseHelper.KEY_MESSAGE_CHAT, message);
			cv.put(DatabaseHelper.KEY_TIME_CHAT, time);
			cv.put(DatabaseHelper.KEY_RECEIVE_TICK_CHAT, mString_receive_or_not);
			cv.put(DatabaseHelper.KEY_BUBBLE_OR_NOT_CHAT, mString_promo_id);
			cv.put(DatabaseHelper.KEY_DATE_CHAT, date);

			SharedPreferences mPasswordSharedPreferences = context
					.getSharedPreferences(Constant.IMGRCHAT,
							Context.MODE_PRIVATE);
			String mStringPassword = mPasswordSharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");

			if ((fromjid.split("_")[0].equalsIgnoreCase(mStringPassword) || tojid
					.split("_")[0].equalsIgnoreCase(mStringPassword))
					&& ((isSender.equalsIgnoreCase("yes") && fromjid.split("_")[0]
							.equalsIgnoreCase(mStringPassword)) || (isSender
							.equalsIgnoreCase("no") && tojid.split("_")[0]
							.equalsIgnoreCase(mStringPassword)))) {

				String isBlocked = "false";

				Cursor mBlockCursor = FETCH_(number);

				if (mBlockCursor.getCount() != 0) {

					if (mBlockCursor.moveToFirst()) {
						isBlocked = mBlockCursor.getString(9);
					}
				}

				if (!isBlocked.equalsIgnoreCase("true")) {
					long row = database.insert(DatabaseHelper.TABLE_CHATTING,
							null, cv);
					Log.e("rowinserted----", "" + row);
					// Log.e("row----", "sfewfgewtgewtw4tw4");

					SharedPreferences mSharedPreferences = context
							.getSharedPreferences(
									Constant.IMGRNOTIFICATION_RECEIVE,
									Context.MODE_PRIVATE);
					String appStatus = mSharedPreferences.getString(
							"isTerminated", "no");
					String currentPhoneNo = mSharedPreferences.getString(
							"currentPhoneNo", "");
					Log.e("hiiiiiiiiiiiiiiiiiiiiiiiiii", "appStatus:"
							+ appStatus + ", currentPhoneNo:" + currentPhoneNo
							+ ", number:" + number + ", isSender: " + isSender
							+ ", ");

					ActivityManager activityManager = (ActivityManager) context
							.getSystemService(context.ACTIVITY_SERVICE);
					ArrayList<RecentTaskInfo> apps = (ArrayList<RecentTaskInfo>) activityManager
							.getRecentTasks(100,
									ActivityManager.RECENT_WITH_EXCLUDED);

					boolean isAppTerminated = true;

					for (int i = 0; i < apps.size(); i++) {
						if (apps.get(i).baseIntent.getComponent() != null) {
							String packageName = apps.get(i).baseIntent
									.getComponent().getPackageName();
							if (packageName.equals("com.imgr.chat")) {
								isAppTerminated = false;
								Log.e("app is runinggggggggggggggggggggggg",
										packageName);
							}
							// Toast.makeText(context, "Browser is running",
							// Toast.LENGTH_LONG).show();

						}
					}

					Log.e("row----", "sfewfgewtgewtw4tw4");
					if (row > 0) {
						flag = true;

						boolean showNotif = false;

						if (//!isAppTerminated &&
								!currentPhoneNo.toLowerCase().contains(
										number.toLowerCase())
								&& isSender.equalsIgnoreCase("no")) {
							showNotif = true;
						}
						/*else {
							if (isAppTerminated) {
								showNotif = true;
							}
						}*/

						if (showNotif) {
							Log.e("heyyyyyyyyyyyyyyyy", message);

							JSONObject jsonObj = new JSONObject(message);

							JSONArray mJsonArray = null;

							String textmessage = "";

							JSONArray bodyArray = jsonObj.getJSONArray("body");

							JSONObject bodyJsonObj = new JSONObject(
									bodyArray.getString(0));

							if (bodyJsonObj.has("_message_body_tag")) {

								textmessage = bodyJsonObj
										.getString("_message_body_tag");

							} else {
								textmessage = "image";
							}

							Log.e("actual message", textmessage);

							String chatName = "";

							DatasourceHandler mDatasourceHandler = new DatasourceHandler(
									context);
							Cursor mCursor = mDatasourceHandler.FETCH_(number);

							if (mCursor.getCount() != 0) {
								// Log.e("user_info_splaseeeeeee",
								// "shjkwdhjkwdhjkqwhdjkhdjkqwhdjkqhdjk");
								if (mCursor.moveToFirst()) {
									chatName = mCursor.getString(1).trim();
								}
								mCursor.close();

							} else {
								chatName = "";
							}
							mCursor.close();

							Log.e("nameeeeeeeeeeeeeeee", chatName);
							GCMIntentService gcmis = new GCMIntentService();

							int notifcationId = (int) System
									.currentTimeMillis();
							boolean isInserted = mDatasourceHandler
									.insertNotifcations("" + notifcationId,
											number);
							int value = mDatasourceHandler
									.Notifications_Count_Fetch();

							SharedPreferences sharedPreferences = context
									.getSharedPreferences(Constant.IMGRCHAT,
											Context.MODE_PRIVATE);
							String notifStatus = sharedPreferences.getString(
									Constant.SETTING_NOTIFICATION_ON_OFF,
									"true");

							if (!notifStatus.equalsIgnoreCase("false")) {
								notifStatus = "true";
							}

							gcmis.generateNotification_chat_message(context,
									textmessage, username, number, chatName,
									notifcationId, value, notifStatus);
						}
					}
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	// insert================all Contacts============

	public boolean insertDataContacts(String phoneno, String firstname,
			String lastname, String email, String jid, String memberid,
			String isImgrUser, String mStringContactId) {
		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
			cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
			cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, email);
			cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
			cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
			cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
			cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);

			cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

			long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null,
					cv);
			// Log.e("row----", "" + row);
			if (row > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	// insert================profile section ============

	public boolean insertProfile(String phoneno, String firstname,
			String lastname, String email, String base64, String code) {
		boolean flag = false;
		try {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_NAME_PHONE, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_PROFILE, firstname);
			cv.put(DatabaseHelper.KEY_LAST_NAME_PROFILE, lastname);
			cv.put(DatabaseHelper.KEY_EMAIL_PHONE, email);
			cv.put(DatabaseHelper.KEY_IMAGE_BASE, base64);
			cv.put(DatabaseHelper.KEY_NAME_PHONE_CODE, code);

			long row = database.insert(DatabaseHelper.TABLE_PROFILE, null, cv);
			// Log.e("row----", "" + row);
			if (row > 0) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return flag;
	}

	// insert unquie entry all contact table

	public Cursor AllContactsUnquie(String phoneno, String firstname,
			String lastname, String jid, String memberid, String isImgrUser,
			String mString_Email, String base64, String mStringContactId,
			String mStringBlockedContact) throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
				cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
				// cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
				cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
				cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
				// cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				// cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
				// cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID,
				// mStringContactId);
				// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
				// mStringBlockedContact);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + mStringContactId, null);
				// Log.e("older update contact unique update row----", "" +
				// row);
				
				Log.e("AllContactsUnquie_Update", "AllContactsUnquie_Update");

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {
			// Log.e("base64----", "" + base64);
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
			cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
			cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
			cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
			cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
			cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
			cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
			cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
			cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT, mStringBlockedContact);

			cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

			long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null,
					cv);
			// Log.e("new add simple row----", "" + row);
			
			Log.e("AllContactsUnquie_Insert", "AllContactsUnquie_Insert");
			
		}

		return mCursor;
	}

	public boolean FIRST_AllContactsUnquie(String phoneno, String firstname,
			String lastname, String jid, String memberid, String isImgrUser,
			String mString_Email, String base64, String mStringContactId,
			String mStringBlockedContact) throws SQLException {

		boolean flag = false;

		ContentValues cv = new ContentValues();
		cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
		cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
		cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
		cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
		cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
		cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
		cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
		cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
		cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
		cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT, mStringBlockedContact);

		cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
		cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
		cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
		cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
		cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

		Log.e("FIRST_AllContactsUnquie_Insert", "FIRST_AllContactsUnquie_Insert");
		
		long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null, cv);
		// Log.e("new add simple row----", "" + row);
		if (row > 0) {
			flag = true;
		}

		return flag;
	}

	public Cursor AllContactsNoPullUnquie(String phoneno, String firstname,
			String lastname, String jid, String memberid, String isImgrUser,
			String mString_Email, String base64, String mStringContactId)
			throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				// Log.e("base64----", "" + base64);
				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
				cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
				cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
				cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);

				cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
				cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
				cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
				// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
				// mStringBlockedContact);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + mStringContactId, null);
				// Log.e("simple row----", "" + row);
				
				Log.e("AllContactsNoPullUnquie_Update", "AllContactsNoPullUnquie_Update");

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {
			// Log.e("base64----", "" + base64);
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
			cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
			cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
			cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
			cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
			cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
			cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
			cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);

			cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

			// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
			// mStringBlockedContact);

			long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null,
					cv);
			// Log.e("simple row----", "" + row);
			
			Log.e("AllContactsNoPullUnquie_Insert", "AllContactsNoPullUnquie_Insert");
			
		}

		return mCursor;
	}

	public Cursor NewAllContactsNoPullUnquie(String phoneno, String firstname,
			String lastname, String jid, String memberid, String isImgrUser,
			String mString_Email, String base64, String mStringContactId)
			throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
				cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
				// cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				// cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
				// cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);

				// cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
				// cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
				cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
				// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
				// mStringBlockedContact);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + mStringContactId, null);
				
				Log.e("NewAllContactsNoPullUnquie_Update", "NewAllContactsNoPullUnquie_Update");

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
			cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
			cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
			cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
			cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
			cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
			cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
			cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
			// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
			// mStringBlockedContact);

			cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

			long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null,
					cv);

			Log.e("NewAllContactsNoPullUnquie_Insert", "NewAllContactsNoPullUnquie_Insert");
			
		}

		return mCursor;
	}

	public Cursor NewAllContactsPull(String phoneno, String firstname,
			String lastname, String mStringContactId) throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
				cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
				// cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				// cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
				// cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);

				// cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
				// cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
				cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
				// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
				// mStringBlockedContact);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + mStringContactId, null);
				
				Log.e("NewAllContactsPull_Update", "NewAllContactsPull_Update");

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

		}

		return mCursor;
	}

	public Cursor NewAllContactsPull_AllContactsUnquie(String phoneno,
			String firstname, String lastname, String jid, String memberid,
			String isImgrUser, String mString_Email, String base64,
			String mStringContactId, String mStringBlockedContact)
			throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
				cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
				// cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
				cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
				// cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
				//cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
				// cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID,
				// mStringContactId);
				// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
				// mStringBlockedContact);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + mStringContactId, null);
				// Log.e("older update contact unique update row----", "" +
				// row);
				
				Log.e("NewAllContactsPull_AllContactsUnquie_Update", "NewAllContactsPull_AllContactsUnquie_Update");

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
			cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
			cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
			cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
			cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);
			cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);
			cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
			cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
			cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT, mStringBlockedContact);

			cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

			long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null,
					cv);
			
			Log.e("NewAllContactsPull_AllContactsUnquie_Insert", "NewAllContactsPull_AllContactsUnquie_Insert");
			
			// Log.e("new add simple row----", "" + row);
		}

		return mCursor;
	}

	public Cursor AllContactsLocalUnquie(String phoneno, String firstname,
			String lastname, String mStringContactId) throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				// cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
				// cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);

				// cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mString_Email);

				cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
				// cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT,
				// mStringBlockedContact);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + mStringContactId, null);
				// Log.e("simple row----", "" + row);
				
				Log.e("AllContactsLocalUnquie_Update", "AllContactsLocalUnquie_Update");

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

		}

		return mCursor;
	}

	public boolean SingleUnquie(String phoneno, String firstname,
			String lastname, String jid, String memberid, String isImgrUser,
			String mStringEmail, String mStringContactId, String mStringBlocked)
			throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);
		boolean flag = false;
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mStringContactId }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				flag = false;
			} while (mCursor.moveToNext());

			mCursor.close();

		} else {
			flag = true;
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
			cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
			cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
			// cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, email);
			cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
			cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
			cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);
			cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, mStringEmail);
			cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mStringContactId);
			cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT, mStringBlocked);

			cv.put(DatabaseHelper.PERSONAL_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.SPONSERED_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.AUTO_ROTATE_PROMO_ON_OFF, 1);
			cv.put(DatabaseHelper.LAST_PROMO_ID, -1);

			long row = database.insert(DatabaseHelper.TABLE_ALL_CONTACTS, null,
					cv);
			Log.e("SingleUnquie_Update", "SingleUnquie_Update");
			
			// Log.e("row----", "" + row);
		}

		return flag;
	}

	// insert

	public boolean SingleUser_set(String promo, String autorotate,
			String sponsored_ads, String personal_ads, String uniqueid)
			throws SQLException {

		boolean flag = false;
		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER,
				allcolumn_User_set, "_user_set_uniqueId= ?",
				new String[] { uniqueid }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				flag = false;
			} while (mCursor.moveToNext());

			mCursor.close();

		} else {
			flag = true;
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_USER_SET_PROMO, promo);
			cv.put(DatabaseHelper.KEY_USER_SET_AUTOROATE, autorotate);
			cv.put(DatabaseHelper.KEY_USER_SET_SPONSORED_ADS, sponsored_ads);
			// cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, email);
			cv.put(DatabaseHelper.KEY_LAST_PROMO_ID, "");

			cv.put(DatabaseHelper.KEY_USER_SET_PERSONAL_ADS, personal_ads);
			cv.put(DatabaseHelper.KEY_USER_SET_UNQIUE_ID, uniqueid);

			long row = database.insert(
					DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, null, cv);

		}

		return flag;
	}

	public void resetLastPromoId() {
		ContentValues cv = new ContentValues();
		cv.put(DatabaseHelper.KEY_LAST_PROMO_ID, "");
		database.update(DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, cv,
				" 1", null);
	}

	public void updateLastPromoId(String promo_id) {
		ContentValues cv = new ContentValues();
		cv.put(DatabaseHelper.KEY_LAST_PROMO_ID, promo_id);
		database.update(DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, cv,
				" 1", null);
	}

	public Cursor User_Set_Fetch(String id) throws SQLException {

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER,
				allcolumn_User_set, "_user_set_uniqueId = ?",
				new String[] { id }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	public boolean UpdatePROMO(String id, String promo_value) {

		boolean flag = false;

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER,
				allcolumn_User_set, "_user_set_uniqueId= ?",
				new String[] { id }, null, null, null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_USER_SET_PROMO, promo_value);

			long row = database.update(
					DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, cv,
					"_user_set_uniqueId" + "='" + id + "'", null);

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	public boolean UpdateAUTO_PROMO(String id, String auto_value) {

		boolean flag = false;

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER,
				allcolumn_User_set, "_user_set_uniqueId= ?",
				new String[] { id }, null, null, null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_USER_SET_AUTOROATE, auto_value);

			long row = database.update(
					DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, cv,
					"_user_set_uniqueId" + "='" + id + "'", null);

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	public boolean UpdateAUTO_SPONSOREDADS(String id, String sponsoredads) {

		boolean flag = false;

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER,
				allcolumn_User_set, "_user_set_uniqueId= ?",
				new String[] { id }, null, null, null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_USER_SET_SPONSORED_ADS, sponsoredads);

			long row = database.update(
					DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, cv,
					"_user_set_uniqueId" + "='" + id + "'", null);

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	public boolean UpdateAUTO_PERSONALADS(String id, String personalads) {

		boolean flag = false;

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER,
				allcolumn_User_set, "_user_set_uniqueId= ?",
				new String[] { id }, null, null, null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_USER_SET_PERSONAL_ADS, personalads);

			long row = database.update(
					DatabaseHelper.TABLE_PERSONAL_SPONSORED_SET_USER, cv,
					"_user_set_uniqueId" + "='" + id + "'", null);

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	// fetch only Imgr contacts---------
	public Cursor ImgrContacts() throws SQLException {

		String mString_query = DatabaseHelper.KEY_FIRST_NAME_CONTACTS
				+ "  COLLATE NOCASE";
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_isImgrUser = ?", new String[] { "1" }, null, null,
				mString_query, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}
	
	// fetch only Imgr contacts---------
		public Cursor NoImgrContacts() throws SQLException {

			String mString_query = DatabaseHelper.KEY_FIRST_NAME_CONTACTS
					+ "  COLLATE NOCASE";
			Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
					allcolumn, "_isImgrUser = ?", new String[] { "1" }, null, null,
					mString_query, null);
			if (mCursor != null) {
				mCursor.moveToFirst();
			}

			return mCursor;
		}

	// fetch only All contacts---------
	public Cursor AllContacts() throws SQLException {
		// ArrayList<String> a = new ArrayList<String>();
		String mString_query = DatabaseHelper.KEY_FIRST_NAME_CONTACTS
				+ "  COLLATE NOCASE";

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, null, null, null, null, mString_query, null);
		Log.e("mcursor", "" + mCursor);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}
	
	
//	public ArrayList<HashMap<String, String>> NewAllContacts() throws SQLException {
//		 ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();
//		    
//		// ArrayList<String> a = new ArrayList<String>();
//		String mString_query = DatabaseHelper.KEY_isImgrUser
//				+ "  COLLATE NOCASE";
////		SQLiteDatabase database = this.getWritableDatabase();
//		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
//				allcolumn, null, null, null, null, mString_query, null);
//		Log.e("mcursor", "" + mCursor);
////		if (mCursor != null) {
////			mCursor.moveToFirst();
////		}
//		
//		Cursor c = db.rawQuery("SELECT * FROM table WHERE 0", null);
//		if (mCursor.moveToFirst()) {
//	        do {
//	            HashMap<String, String> map = new HashMap<String, String>();
//	            for(int i=0; i<mCursor.getColumnCount();i++)
//	            {
//	                map.put(mCursor.getColumnName(i), mCursor.getString(i));
//	            }
//
//	            maplist.add(map);
//	        } while (mCursor.moveToNext());
//	    }
//
////		db.close();
//	    // return contact list
//	    return maplist;
//	}
	

	// fetch only Profile---------
	public Cursor ProfileInfo() throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_PROFILE,
				allcolumn_Profile, null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	// Update unquie entry all Profile table

	public Cursor UpdateProfileInfo(String phoneno, String firstname,
			String lastname, String email, String base64) throws SQLException {
		// Log.e("update only phoneno: ", "" + phoneno);

		// Log.e("STARTING update only isImgrUser: ", "" + isImgrUser);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_PROFILE,
				allcolumn_Profile, "_phonenoprofile= ?",
				new String[] { phoneno }, null, null, null, null);
		if (mCursor.getCount() != 0) {

			do {
				// Log.e("update only phoneno: ", "" + phoneno);

				// Log.e("update only isImgrUser: ", "" + isImgrUser);
				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_NAME_PHONE, phoneno);

				cv.put(DatabaseHelper.KEY_FIRST_NAME_PROFILE, firstname);
				cv.put(DatabaseHelper.KEY_LAST_NAME_PROFILE, lastname);
				cv.put(DatabaseHelper.KEY_EMAIL_PHONE, email);
				cv.put(DatabaseHelper.KEY_IMAGE_BASE, base64);

				long row = database.update(DatabaseHelper.TABLE_PROFILE, cv,
						"_phonenoprofile" + "=" + phoneno, null);

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			// Log.e("update : ", "Nothing" );

		}

		return mCursor;
	}

	// Update unquie entry all contact table

	public Cursor UpdateImgrContact(String phoneno, String jid,
			String memberid, String isImgrUser, String base64,
			String mString_Contactid, String mStringfirst, String last)
			throws SQLException {

		// Log.e("first: ", "" + mStringfirst+"---"+last);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mString_Contactid }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, mStringfirst);
				cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, last);
				cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
				cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID, mString_Contactid);
				cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);

				cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + '=' + mString_Contactid, null);
				// Log.e("how many calling update row----", "" + row);
			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			// Log.e("update : ", "Nothing" );

		}

		return mCursor;
	}

	public Cursor UpdateImgrSingleContact(String phoneno, String jid,
			String memberid, String isImgrUser, String base64)
			throws SQLException {

		// Log.e("STARTING update only isImgrUser: ", "" + isImgrUser);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phonenocontact= ?", new String[] { phoneno },
				null, null, null, null);
		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);

				cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				cv.put(DatabaseHelper.KEY_MEMBERID_CONTACTS, memberid);
				cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);

				cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phonenocontact" + '=' + phoneno, null);

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			// Log.e("update : ", "Nothing" );

		}

		return mCursor;
	}

	public Cursor UpdateNoImgrContact(String isImgrUser,
			String mString_Contactid) throws SQLException {

		// Log.e("first: ", "" + mStringfirst+"---"+last);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mString_Contactid }, null, null, null, null);
		// Log.e("update COUNT: ", "" + mCursor.getCount());
		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();
				// cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);
				// cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, mStringfirst);
				// cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, last);

				// cv.put(DatabaseHelper.KEY_JID_CONTACTS, jid);
				// cv.put(DatabaseHelper.KEY_PHONE_CONTACTS_ID,
				// mString_Contactid);
				cv.put(DatabaseHelper.KEY_isImgrUser, isImgrUser);

				// cv.put(DatabaseHelper.KEY_IMAGE_BASE_CONTACT, base64);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + '=' + mString_Contactid, null);
				// Log.e("how many calling update row----", "" + row);
			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			// Log.e("update : ", "Nothing" );

		}

		return mCursor;
	}

	public Cursor FetchContact(String phoneno) throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?", new String[] { phoneno },
				null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	public Cursor FETCH_PROMO_DATA(String promoid) throws SQLException {
		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE promoid : ", "" + promoid);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored, "_promo_id= ?",
				new String[] { promoid }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor FETCH_PROMO_DATA_Update(String promoid) throws SQLException {
		//
		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE promoid : ", "" + promoid);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored, "_promo_id= ? AND _promo_type=?",
				new String[] { promoid, "NO_Define" }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor FETCHPROMODATA(String promoid) throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored, "_promo_id= ?",
				new String[] { promoid }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE : ", "" + mCursor.getCount());

		return mCursor;
	}

	public Cursor promo_type(String promoid) throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored, "_promo_id= ?",
				new String[] { promoid }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE : ", "" + mCursor.getCount());

		return mCursor;
	}

	public Cursor All_FETCH_PROMO_DATA() throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored, null, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	public Cursor fetchAllActivePromos() throws SQLException {

		Cursor mCursor = database
				.query(DatabaseHelper.TABLE_ALL_SPONSORED,
						allcolumn_fetch_Sponsored,
						" (_promo_type= ? OR _promo_type = ?) AND (_is_deleted= ? AND _is_enabled = ?) ",
						new String[] { "personal", "no_personal", "0", "1" },
						null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE : ", "" + mCursor.getCount());

		return mCursor;
	}

	public Cursor fetchAllActivePersonalPromos() throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored,
				" _promo_type = ? AND (_is_deleted= ? AND _is_enabled = ?) ",
				new String[] { "personal", "0", "1" }, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE : ", "" + mCursor.getCount());

		return mCursor;
	}

	public Cursor fetchAllActiveSponseredPromos() throws SQLException {

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_fetch_Sponsored,
				" _promo_type = ? AND (_is_deleted= ? AND _is_enabled = ?) ",
				new String[] { "no_personal", "0", "1" }, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		// Log.e("AMIt SEEEEEEEEEEEEEEEEEE : ", "" + mCursor.getCount());

		return mCursor;
	}

	// Update contact entry all contact table

	public void UpdateContact(String mStringolderno, String phoneno,
			String firstname, String lastname, String email,
			String mStringContactId) throws SQLException {

		ContentValues cv = new ContentValues();
		cv.put(DatabaseHelper.KEY_PHONENO_CONTACTS, phoneno);

		cv.put(DatabaseHelper.KEY_FIRST_NAME_CONTACTS, firstname);
		cv.put(DatabaseHelper.KEY_LASTNAME_CONTACTS, lastname);
		cv.put(DatabaseHelper.KEY_EMAIL_CONTACTS, email);

		long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS, cv,
				"_phoneidcontact" + "=" + mStringContactId, null);

	}

	// Update unquie entry all contact table

	public Cursor Update_Invite_Contact(String phoneno) throws SQLException {
		// Log.e("update only phoneno: ", "" + phoneno);

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?", new String[] { phoneno },
				null, null, null, null);
		if (mCursor.getCount() != 0) {

			do {

				// Log.e("update only isImgrUser: ", "" + isImgrUser);
				ContentValues cv = new ContentValues();
				cv.put(DatabaseHelper.KEY_isImgrUser, "2");

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + "=" + phoneno, null);

			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			// Log.e("update : ", "Nothing" );

		}

		return mCursor;
	}

	//

	// Update contact entry all contact table

	public boolean UpdateReceive(String messageid) {

		boolean flag = false;

		Cursor mCursor = database.query(DatabaseHelper.TABLE_CHATTING,
				allcolumn_Chat_history, "_iduniquechat= ?",
				new String[] { messageid }, null, null, null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_RECEIVE_TICK_CHAT, "1");

			long row = database.update(DatabaseHelper.TABLE_CHATTING, cv,
					"_iduniquechat" + "='" + messageid + "'", null);

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	// insert unquie entry all contact table

	public Cursor FetchChatHistory(String tojid, String fromjid)
			throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);

		Cursor mCursor = database
				.query(DatabaseHelper.TABLE_CHATTING,
						allcolumn_Chat_history,
						"(_tojidchat= ? AND _fromjidchat = ?) OR (_fromjidchat=? AND _tojidchat=?)",
						new String[] { tojid, fromjid, tojid, fromjid }, null,
						null, null, null);
		// Log.e("AllContactsUnquie: mCursor.getCount()----: ", "" +
		// mCursor.getCount());
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	public void DeleteAll() throws SQLException {

		database.execSQL("delete from " + DatabaseHelper.TABLE_ALL_CONTACTS);
		database.execSQL("delete from " + DatabaseHelper.TABLE_CHATTING);
		database.execSQL("delete from " + DatabaseHelper.TABLE_PROFILE);
		// UILApplication.context.deleteDatabase(DatabaseHelper.DATABASE_NAME);

	}

	public boolean deleteRow(String id) {
		return database.delete(DatabaseHelper.TABLE_ALL_CONTACTS,
				"_phoneidcontact" + "=" + id, null) > 0;
	}

	public boolean deletePromoRow(String id) {
		return database.delete(DatabaseHelper.TABLE_ALL_SPONSORED, "_promo_id"
				+ "=" + id, null) > 0;
	}

	public Cursor UpdateBlockContact(String mString_Contactid,
			String mStringBlocked) throws SQLException {

		// Log.e("STARTING update only isImgrUser: ", "" + isImgrUser);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phoneidcontact= ?",
				new String[] { mString_Contactid }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {

				ContentValues cv = new ContentValues();

				cv.put(DatabaseHelper.KEY_BLOCKED_CONTACT, mStringBlocked);

				long row = database.update(DatabaseHelper.TABLE_ALL_CONTACTS,
						cv, "_phoneidcontact" + '=' + mString_Contactid, null);
				Log.e("how many calling update row----", "" + row);
				Log.e("Blockeddddddddddddd----", "" + mStringBlocked);
			} while (mCursor.moveToNext());

			mCursor.close();

		} else {

			// Log.e("update : ", "Nothing" );

		}

		return mCursor;
	}

	public boolean deleteMesage(String to, String from) {

		return database.delete(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
				"(_recent_jid_to" + "='" + to + "'" + " AND "
						+ "_recent_jid_from" + "='" + from + "')" + " OR "
						+ "(_recent_jid_from" + "='" + to + "'" + " AND "
						+ "_recent_jid_to" + "='" + from + "')", null) > 0;
	}

	/*
	 * Cursor mCursor = database.query(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
	 * allcolumn_Chat_history_Recent,
	 * "_recent_jid_to= ? AND _recent_jid_from=?", new String[] { "1", "%" + no
	 * + "%" }, null, null, null, null);
	 */
	public boolean deleteHistoryMesage(String to, String from) {

		return database.delete(DatabaseHelper.TABLE_CHATTING, "(_tojidchat"
				+ "='" + to + "'" + " AND " + "_fromjidchat" + "='" + from
				+ "')" + " OR " + "(_fromjidchat" + "='" + to + "'" + " AND "
				+ "_tojidchat" + "='" + from + "')", null) > 0;
	}

	// fetch only Imgr contacts---------
	public Cursor SponsoredData() throws SQLException {
		String mString_query = DatabaseHelper.KEY_promo_name
				+ "  COLLATE NOCASE";
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_Sponsored, "(_is_deleted= ? AND _isActive = ?)",
				new String[] { "0", "1" }, null, null, mString_query, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;

	}

	// fetch only Personal Sponsored
	public Cursor PullPersonalSponsoredData() throws SQLException {
		String mString_query = DatabaseHelper.KEY_promo_name
				+ "  COLLATE NOCASE";
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_Sponsored, "_promo_type=?",
				new String[] { "personal" }, null, null, mString_query, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;

	}

	// fetch only Non Personal Sponsored
	public Cursor PullNoPersonalSponsoredData() throws SQLException {
		String mString_query = DatabaseHelper.KEY_promo_name
				+ "  COLLATE NOCASE";
		Cursor mCursor = database
				.query(DatabaseHelper.TABLE_ALL_SPONSORED, allcolumn_Sponsored,
						"_promo_type=?", new String[] { "no_personal" }, null,
						null, mString_query, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;

	}

	public boolean UpdateAds(String promoid, String enabled) {

		boolean flag = false;

		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_SPONSORED,
				allcolumn_Sponsored, "_promo_id= ?", new String[] { promoid },
				null, null, null, null);
		// Log.e("COUNT: ", "" + mCursor.getCount());
		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_is_enabled, enabled);

			long row = database.update(DatabaseHelper.TABLE_ALL_SPONSORED, cv,
					"_promo_id" + "='" + promoid + "'", null);

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	public Cursor getRecentMessage(String tojid, String fromjid) {

		// TODO Auto-generated method stub
		String query = "Select max("
				+ DatabaseHelper.KEY_AUTO_INC_CHAT
				+ ") from "
				+ DatabaseHelper.TABLE_CHATTING
				+ " Where "
				+ "(_tojidchat = ? AND _fromjidchat = ?) OR (_fromjidchat = ? AND _tojidchat = ?)";
		// Log.e("query", "" + query);
		Cursor cursor = database.rawQuery(query, new String[] { tojid, fromjid,
				tojid, fromjid });

		return cursor;
	}

	public Cursor Test(int id) throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);

		Cursor mCursor = null;
		String query_ = "Select * from " + DatabaseHelper.TABLE_CHATTING
				+ " Where " + DatabaseHelper.KEY_AUTO_INC_CHAT + " =?";

		mCursor = database
				.rawQuery(query_, new String[] { String.valueOf(id) });

		return mCursor;
	}

	public Cursor FetchAllChatHistory() throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);

		Cursor mCursor = database.query(DatabaseHelper.TABLE_CHATTING,
				allcolumn_Chat_history, null, null, null, null, null, null);
		// Log.e("AllContactsUnquie: mCursor.getCount()----: ", "" +
		// mCursor.getCount());
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	public Cursor _FetchAllChatHistory() throws SQLException {
		// Log.e("-----------------------phoneno: ", "" + phoneno);
		// /query(String table, String[] columns, String selection, String[]
		// selectionArgs, String groupBy, String having, String orderBy, String
		// limit)

		// String query= SELECT DatabaseHelper.KEY_TOJID_CHAT

		Cursor mCursor = database.query(DatabaseHelper.TABLE_CHATTING,
				allcolumn_Chat_history, null, null,
				DatabaseHelper.KEY_TOJID_CHAT, null,
				DatabaseHelper.KEY_TOJID_CHAT + " DESC", null);

		// Cursor mCursor = database.query(DatabaseHelper.TABLE_CHATTING,
		// allcolumn_Chat_history, null, null, null, null, null, null);
		// Log.e("AllContactsUnquie: mCursor.getCount()----: ", "" +
		// mCursor.getCount());
		if (mCursor != null) {
			mCursor.moveToFirst();
		}

		return mCursor;
	}

	// / recent database

	public boolean checkRecentEntry(String fronjid, String tojid) {
		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
				allcolumn_Chat_history_Recent, "(_recent_jid_to" + " LIKE '%" + tojid.split("_")[0]
						+ "%'" + " AND " + "_recent_jid_from" + " LIKE '%" + fronjid.split("_")[0]
						+ "%')" + " OR " + "(_recent_jid_from" + " LIKE '%" + tojid.split("_")[0]
						+ "%'" + " AND " + "_recent_jid_to" + " LIKE '%" + fronjid.split("_")[0]
						+ "%')", null, null, null, null);
		if (mCursor.getCount() != 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean insertrecentMessage(String recentmessage, String fronjid,
			String tojid, String lastname, String firstname, String date,
			String time, String timestamp, String count, String isSender,
			String phoneNo) {

		boolean flag = false;
		try {

			Log.e("preupdaterecentttttttttttttttttttttttt", recentmessage);

			if ((fronjid.split("_")[0].equalsIgnoreCase(phoneNo) || tojid
					.split("_")[0].equalsIgnoreCase(phoneNo))
					&& ((isSender.equalsIgnoreCase("yes") && fronjid.split("_")[0]
							.equalsIgnoreCase(phoneNo)) || (isSender
							.equalsIgnoreCase("no") && tojid.split("_")[0]
							.equalsIgnoreCase(phoneNo)))) {

				Log.e("updatedetailssssssssssssssss", fronjid + ", " + tojid
						+ ", " + phoneNo + ", " + isSender);

				String incomingNumber = "";

				if (tojid.split("_")[0].equalsIgnoreCase(phoneNo)) {
					incomingNumber = fronjid.split("_")[0];
				} else {
					incomingNumber = tojid.split("_")[0];
				}

				String isBlocked = "false";

				Cursor mCursor = FETCH_(incomingNumber);

				if (mCursor.getCount() != 0) {

					if (mCursor.moveToFirst()) {
						isBlocked = mCursor.getString(9);
					}
				}

				if (!isBlocked.equalsIgnoreCase("true")) {
					if (checkRecentEntry(fronjid, tojid)) {
						UpdateRecentReceive(tojid, fronjid, recentmessage,
								time, date, timestamp, count, isSender, phoneNo);
					} else {
						ContentValues cv = new ContentValues();

						cv.put(DatabaseHelper.KEY_RECENT_MESSAGE, recentmessage);
						cv.put(DatabaseHelper.KEY_RECENT_JID_FROM, fronjid);
						cv.put(DatabaseHelper.KEY_RECENT_JID_TO, tojid);
						cv.put(DatabaseHelper.KEY_RECENT_LASTNAME, lastname);

						cv.put(DatabaseHelper.KEY_RECENT_FIRSTNAME, firstname);
						cv.put(DatabaseHelper.KEY_RECENT_DATE, date);
						cv.put(DatabaseHelper.KEY_RECENT_TIME, time);
						cv.put(DatabaseHelper.KEY_RECENT_TIMESTAMP, timestamp);

						cv.put(DatabaseHelper.KEY_RECENT_COUNT, count);

						cv.put(DatabaseHelper.RECENT_PERSONAL_PROMO_ON_OFF, 1);
						cv.put(DatabaseHelper.RECENT_SPONSERED_PROMO_ON_OFF, 1);
						cv.put(DatabaseHelper.RECENT_PROMO_ON_OFF, 1);
						cv.put(DatabaseHelper.RECENT_AUTO_ROTATE_PROMO_ON_OFF,
								1);
						cv.put(DatabaseHelper.RECENT_LAST_PROMO_ID, -1);

						//Log.e("rowupdatesssssssssss----", "swdetretr");

						long row = database.insert(
								DatabaseHelper.TABLE_RECENT_CHAT_HISTORY, null,
								cv);
						// Log.e("row----", "" + row);
						if (row > 0) {
							flag = true;
						}
					}
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("rowupdateaaaaaaaaaaaaaaaa----", "swdetretr");
		}
		return flag;
	}

	public boolean UpdateRecentReceive(String tojid, String fromjid,
			String message, String time, String date, String timestamp,
			String count, String isSender, String phoneNo) {

		boolean flag = false;
		try {


			Log.e("updatedetailssssssssssssssss", fromjid + ", " + tojid + ", "
					+ phoneNo + ", " + isSender);


			if ((fromjid.split("_")[0].equalsIgnoreCase(phoneNo) || tojid
					.split("_")[0].equalsIgnoreCase(phoneNo))
					&& ((isSender.equalsIgnoreCase("yes") && fromjid.split("_")[0]
							.equalsIgnoreCase(phoneNo)) || (isSender
							.equalsIgnoreCase("no") && tojid.split("_")[0]
							.equalsIgnoreCase(phoneNo)))) {

				String incomingNumber = "";

				if (tojid.split("_")[0].equalsIgnoreCase(phoneNo)) {
					incomingNumber = fromjid.split("_")[0];
				} else {
					incomingNumber = tojid.split("_")[0];
				}

				String isBlocked = "false";

				Cursor mBlockCursor = FETCH_(incomingNumber);

				if (mBlockCursor.getCount() != 0) {

					if (mBlockCursor.moveToFirst()) {
						isBlocked = mBlockCursor.getString(9);
					}
				}

				Log.e("blockedddddddddddddd", isBlocked);

				if (!isBlocked.equalsIgnoreCase("true")) {

					Cursor mCursor = database
							.query(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
									allcolumn_Chat_history_Recent,
									"(_recent_jid_to LIKE ? AND _recent_jid_from LIKE ?)"
											+ " OR (_recent_jid_from LIKE ? AND _recent_jid_to LIKE ?)",
									new String[] { "%"+tojid.split("_")[0]+"%", "%"+fromjid.split("_")[0]+"%",
									"%"+tojid.split("_")[0]+"%",
									"%"+fromjid.split("_")[0]+"%" }, null, null, null, null);

					Log.e("updatecounttttttttttttttttttttttt",
							"" + mCursor.getCount());

					if (mCursor.getCount() != 0) {
						ContentValues cv = new ContentValues();
						cv.put(DatabaseHelper.KEY_RECENT_MESSAGE, message);
						cv.put(DatabaseHelper.KEY_RECENT_TIME, time);
						cv.put(DatabaseHelper.KEY_RECENT_DATE, date);
						cv.put(DatabaseHelper.KEY_RECENT_TIMESTAMP, timestamp);
						cv.put(DatabaseHelper.KEY_RECENT_COUNT, count);


						long row = database
								.update(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
										cv,
										"(_recent_jid_to LIKE ? AND _recent_jid_from LIKE ?)"
												+ " OR (_recent_jid_from LIKE ? AND _recent_jid_to LIKE ?)",
										new String[] { "%"+tojid.split("_")[0]+"%", "%"+fromjid.split("_")[0]+"%",
										"%"+tojid.split("_")[0]+"%",
										"%"+fromjid.split("_")[0]+"%" });

						if (row > 0) {
							flag = true;
						}

					}
				}

			}
		} catch (Exception ex) {

		}
		return flag;
	}

	public boolean UpdaterecentChat_History(String tojid, String fromjid,
			String name) {

		boolean flag = false;
		// (_tojidchat= ? AND _fromjidchat = ?)
		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
				allcolumn_Chat_history_Recent,
				"(_recent_jid_to = ? AND _recent_jid_from = ?)"
						+ " OR (_recent_jid_from = ? AND _recent_jid_to = ?)",
				new String[] { tojid, fromjid, tojid, fromjid }, null, null,
				null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_RECENT_FIRSTNAME, name);

			long row = database
					.update(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
							cv,
							"(_recent_jid_to = ? AND _recent_jid_from = ?)"
									+ " OR (_recent_jid_from = ? AND _recent_jid_to = ?)",
							new String[] { tojid, fromjid, tojid, fromjid });

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}
	
	public boolean RecentRecentReceiveCount(String tojid, String fromjid) {
		boolean flag = false;
		try 
		{

			ContentValues cv = new ContentValues();
			cv.put(DatabaseHelper.KEY_RECENT_COUNT, "0");

			long row = database
					.update(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
							cv,
							"(_recent_jid_to LIKE ? AND _recent_jid_from LIKE ?)"
									+ " OR (_recent_jid_from LIKE ? AND _recent_jid_to LIKE ?)",
							new String[] { "%"+tojid.split("_")[0]+"%", "%"+fromjid.split("_")[0]+"%",
							"%"+tojid.split("_")[0]+"%",
							"%"+fromjid.split("_")[0]+"%" });
			if (row > 0) {
				flag = true;
			}

		}
		catch (Exception ex) {

		}
		return flag;
	}

	public int UpdateRecentReceiveCount(String tojid, String fromjid,
			String message, String time, String date, String timestamp) {
		int count_ = 0;
		try {
			// (_tojidchat= ? AND _fromjidchat = ?)
			Cursor mCursor = database
					.query(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
							allcolumn_Chat_history_Recent,
							"(_recent_jid_to LIKE ? AND _recent_jid_from LIKE ?)"
									+ " OR (_recent_jid_from LIKE ? AND _recent_jid_to LIKE ?)",
							new String[] { "%"+tojid+"%", "%"+fromjid+"%", "%"+tojid+"%", "%"+fromjid+"%" },
							null, null, null, null);

			if (mCursor.getCount() != 0) {
				if (mCursor.moveToFirst())
					count_ = Integer.parseInt(mCursor.getString(8).trim());
			}
		} catch (Exception e) {

		}
		return count_;
	}

	public int HISTORYUpdateRecentReceiveCount(String messageId) {
		int count_;
		// (_tojidchat= ? AND _fromjidchat = ?)
		try {
			Cursor mCursor = database.query(DatabaseHelper.TABLE_CHATTING,
					allcolumn_Chat_history, "_iduniquechat= ?",
					new String[] { messageId }, null, null, null, null);
			// Log.e("COUNT: ", "" + mCursor.getCount());
			count_ = mCursor.getCount();
		} catch (Exception ex) {
			count_ = 0;
		}

		return count_;
	}

	public Cursor FETCH_RECENT(String mStringPassword) throws SQLException {
		// Log.e(" SEEEEEEEEEEEEEEEEEE promoid : ", "" + promoid);

		// database.delete(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY, "1", null);
		// database.delete(DatabaseHelper.TABLE_CHATTING, "1", null);

		// 9654421291_4260fca6b2d66b23, 7696458945_6c449d596292c05b, 9654421291,
		// yes

		// database.query(table, columns, selection, selectionArgs, groupBy,
		// having, orderBy)
		// database.query(table, columns, selection, selectionArgs, groupBy,
		// having, orderBy, limit)

		// (CASE WHEN _recent_jid_to LIKE '%"+mStringPassword+"%' THEN
		// _recent_jid_to ELSE _recent_jid_from END) AS recentJid

		// String jidColumn =
		// "(CASE WHEN _recent_jid_to LIKE '%"+mStringPassword+"%' THEN _recent_jid_from ELSE _recent_jid_to END) AS recentJid";
		// Log.e("jidColumnnnnnnnn",jidColumn);
		// allcolumn_Chat_history_Recent[allcolumn_Chat_history_Recent.length-1]
		// = jidColumn;

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
				allcolumn_Chat_history_Recent,
				"(_recent_jid_to LIKE ? OR _recent_jid_from LIKE ?)",
				new String[] { "%" + mStringPassword + "%",
						"%" + mStringPassword + "%" }, null, null,
				DatabaseHelper.KEY_RECENT_TIMESTAMP + " DESC", null);

		// Cursor mCursor = database.query(
		// DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
		// allcolumn_Chat_history_Recent,
		// "(_recent_jid_to LIKE ? OR _recent_jid_from LIKE ?)",
		// new String[] { "%" + mStringPassword + "%",
		// "%" + mStringPassword + "%" }, null, null, null, null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
//	
//	public ArrayList<HashMap<String, String>> MYFETCH_RECENT(String mStringPassword) throws SQLException {
//		// Log.e(" SEEEEEEEEEEEEEEEEEE promoid : ", "" + promoid);
//
//		// database.delete(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY, "1", null);
//		// database.delete(DatabaseHelper.TABLE_CHATTING, "1", null);
//
//		// 9654421291_4260fca6b2d66b23, 7696458945_6c449d596292c05b, 9654421291,
//		// yes
//
//		// database.query(table, columns, selection, selectionArgs, groupBy,
//		// having, orderBy)
//		// database.query(table, columns, selection, selectionArgs, groupBy,
//		// having, orderBy, limit)
//
//		// (CASE WHEN _recent_jid_to LIKE '%"+mStringPassword+"%' THEN
//		// _recent_jid_to ELSE _recent_jid_from END) AS recentJid
//
//		// String jidColumn =
//		// "(CASE WHEN _recent_jid_to LIKE '%"+mStringPassword+"%' THEN _recent_jid_from ELSE _recent_jid_to END) AS recentJid";
//		// Log.e("jidColumnnnnnnnn",jidColumn);
//		// allcolumn_Chat_history_Recent[allcolumn_Chat_history_Recent.length-1]
//		// = jidColumn;
//
////		ArrayList<HashMap<String, String>> mCursor = database.query(
////				DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
////				allcolumn_Chat_history_Recent,
////				"(_recent_jid_to LIKE ? OR _recent_jid_from LIKE ?)",
////				new String[] { "%" + mStringPassword + "%",
////						"%" + mStringPassword + "%" }, null, null,
////				DatabaseHelper.KEY_RECENT_TIMESTAMP + " DESC", null);
//
//		// Cursor mCursor = database.query(
//		// DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
//		// allcolumn_Chat_history_Recent,
//		// "(_recent_jid_to LIKE ? OR _recent_jid_from LIKE ?)",
//		// new String[] { "%" + mStringPassword + "%",
//		// "%" + mStringPassword + "%" }, null, null, null, null);
//
//		if (mCursor != null) {
//			mCursor.moveToFirst();
//		}
//		return mCursor;
//	}

	public Cursor FETCH_RECENT_FOR_UNKNOWN(String number) throws SQLException {

		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
				allcolumn_Chat_history_Recent,
				"(_recent_jid_to LIKE ? OR _recent_jid_from LIKE ?)",
				new String[] { "%" + number + "%", "%" + number + "%" }, null,
				null, DatabaseHelper.KEY_RECENT_TIMESTAMP + " DESC", null);

		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public Cursor FETCH_(String no) throws SQLException {
		// Log.e("DATABASE no: ", "" + no);
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_isImgrUser = ? AND _phonenocontact LIKE ? ",
				new String[] { "1", "%" + no + "%" }, null, null, null, null);

		return mCursor;
	}

	public boolean UpdateRecentReceiveCount(String tojid, String fromjid) {

		boolean flag = false;
		// (_tojidchat= ? AND _fromjidchat = ?)
		Cursor mCursor = database.query(
				DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
				allcolumn_Chat_history_Recent,
				"(_recent_jid_to = ? AND _recent_jid_from = ?)"
						+ " OR (_recent_jid_from = ? AND _recent_jid_to = ?)",
				new String[] { tojid, fromjid, tojid, fromjid }, null, null,
				null, null);

		if (mCursor.getCount() != 0) {
			ContentValues cv = new ContentValues();

			cv.put(DatabaseHelper.KEY_RECENT_COUNT, "0");

			long row = database
					.update(DatabaseHelper.TABLE_RECENT_CHAT_HISTORY,
							cv,
							"(_recent_jid_to = ? AND _recent_jid_from = ?)"
									+ " OR (_recent_jid_from = ? AND _recent_jid_to = ?)",
							new String[] { tojid, fromjid, tojid, fromjid });

			if (row > 0) {
				flag = true;
			}

		}

		return flag;
	}

	public boolean Number_Already_Exit(String phoneno) throws SQLException {

		boolean flag = false;
		Cursor mCursor = database.query(DatabaseHelper.TABLE_ALL_CONTACTS,
				allcolumn, "_phonenocontact LIKE ?", new String[] { "%"
						+ phoneno + "%" }, null, null, null, null);

		if (mCursor.getCount() != 0) {

			do {
				flag = true;
			} while (mCursor.moveToNext());

			mCursor.close();

		} else {
			flag = false;

		}

		return flag;
	}

}