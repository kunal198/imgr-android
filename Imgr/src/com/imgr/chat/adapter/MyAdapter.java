package com.imgr.chat.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.contact.BlockAndMeaageActivity;
import com.imgr.chat.contact.InviteImgrPeople;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.AllContact;
import com.imgr.chat.model.ContactPhoneModel;

public class MyAdapter extends BaseAdapter implements SectionIndexer {

	@SuppressWarnings("unused")
	private ArrayList<String> contactnumber1;
	@SuppressWarnings("unused")
	private ArrayList<String> ISIMGR;
	LinearLayout header;

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;

	private static LayoutInflater inflater = null;
	String select;
	Intent intent;
	private List<String> originalData = null;
	private List<String> filteredData = null;
	private Context context;

	private List<ContactPhoneModel> mPhoneModels = null;
	private ArrayList<ContactPhoneModel> arraylist;

	private static String sections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	// private static String sections = "abcdefghijklmnoprrstuvwxyz";
	private ItemFilter mFilter = new ItemFilter();

	public MyAdapter(FragmentActivity fragmentActivity,
			ArrayList<String> contactname, ArrayList<String> contactnumber,
			ArrayList<String> isIMGR, List<ContactPhoneModel> mPhoneModels) {
		// TODO Auto-generated constructor stub

		this.mPhoneModels = mPhoneModels;
		contactnumber1 = contactnumber;
		originalData = contactname;
		filteredData = contactname;
		ISIMGR = isIMGR;
		context = fragmentActivity;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mDatabaseHelper = new DatabaseHelper(context);
		mDatasourceHandler = new DatasourceHandler(context);
		this.arraylist = new ArrayList<ContactPhoneModel>();
		this.arraylist.addAll(mPhoneModels);

	}

	public int getCount() {
		return mPhoneModels.size();
	}

	// public String getItem(int position) {
	// return filteredData.get(position);
	// }

	public ContactPhoneModel getItem(int position) {
		return mPhoneModels.get(position);
	}

	public long getItemId(int position) {
		return mPhoneModels.get(position).hashCode();
	}

	@SuppressLint("DefaultLocale")
	private void setSection(LinearLayout header, String label) {

		select = label.substring(0, 1).toUpperCase();

		TextView text = new TextView(context);

		text.setTextColor(Color.parseColor("#007AFF"));
		text.setText(select);
		text.setTextSize(20);
		text.setPadding(20, 0, 0, 0);

		text.setGravity(Gravity.CENTER_VERTICAL);
		header.addView(text);

	}

	@SuppressLint("DefaultLocale")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View rowView;
		rowView = inflater.inflate(R.layout.listview_row, null);

		header = (LinearLayout) rowView.findViewById(R.id.section);
		String name = mPhoneModels.get(position).getName();
		// String number = contactnumber1.get(position);
		char firstChar = name.toUpperCase().charAt(0);
		if (position == 0) {
			setSection(header, name);
		} else {
			String preLabel = mPhoneModels.get(position - 1).getName();
			char preFirstChar = preLabel.toUpperCase().charAt(0);
			if (firstChar != preFirstChar) {

				setSection(header, name);
			} else {

				header.setVisibility(View.GONE);
			}
		}
		final TextView textView = (TextView) rowView
				.findViewById(R.id.textView);
		final ImageView mView = (ImageView) rowView
				.findViewById(R.id.imageview);

		// final TextView textView1 = (TextView)
		// rowView.findViewById(R.id.textView1);
		textView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mPhoneModels.get(position).getIsimgr().equals("0")) {
					Intent mIntent_info = new Intent(context,
							InviteImgrPeople.class);
					mIntent_info.putExtra("name_select_invite", mPhoneModels
							.get(position).getName());
					mIntent_info.putExtra("phone_id", mPhoneModels
							.get(position).getPhone());
					mIntent_info.putExtra("phoneno_select_invite", mPhoneModels
							.get(position).getNumber());
					mIntent_info.putExtra("invite", 2);

					context.startActivity(mIntent_info);
					((Activity) context).overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
				} else if (mPhoneModels.get(position).getIsimgr().equals("2")) {
					Intent mIntent_info = new Intent(context,
							InviteImgrPeople.class);
					mIntent_info.putExtra("name_select_invite", mPhoneModels
							.get(position).getName());
					mIntent_info.putExtra("phone_id", mPhoneModels
							.get(position).getPhone());
					mIntent_info.putExtra("phoneno_select_invite", mPhoneModels
							.get(position).getNumber());

					mIntent_info.putExtra("invite", 1);
					context.startActivity(mIntent_info);
					((Activity) context).overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
				} else {
					Constant.HANDLE_BACK_CONATCT_ADD = 1;
					Intent mIntent_info = new Intent(context,
							BlockAndMeaageActivity.class);
					mIntent_info.putExtra("name_select",
							mPhoneModels.get(position).getName());
					mIntent_info.putExtra("phone_id", mPhoneModels
							.get(position).getPhone());
					mIntent_info.putExtra("phoneno_select",
							mPhoneModels.get(position).getNumber());

					context.startActivity(mIntent_info);
					((Activity) context).overridePendingTransition(
							R.anim.slide_in_left, R.anim.slide_out_right);
				}

			}
		});
		AllContact.click_invite_all = 0;
		TextView msg = (TextView) rowView.findViewById(R.id.message);
		if (mPhoneModels.get(position).getIsimgr().equals("0")) {
			mView.setVisibility(View.GONE);
		} else if (mPhoneModels.get(position).getIsimgr().equals("2")) {
			mView.setVisibility(View.GONE);
			msg.setText("Invited");
			msg.setTypeface(null, Typeface.ITALIC);
			msg.setTextColor(Color.GRAY);
		} else {
			msg.setVisibility(View.GONE);
			mView.setVisibility(View.VISIBLE);

			mView.setBackgroundResource(R.drawable.ic_logo_imgr_contact);
		}

		mView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AllContact.click_invite_all = 0;
				Constant.HANDLE_BACK_CONATCT_ADD = 1;
				Intent mIntent_info = new Intent(context,
						BlockAndMeaageActivity.class);
				mIntent_info.putExtra("name_select", mPhoneModels.get(position)
						.getName());
				mIntent_info.putExtra("phone_id", mPhoneModels.get(position)
						.getPhone());
				mIntent_info.putExtra("phoneno_select",
						mPhoneModels.get(position).getNumber());

				context.startActivity(mIntent_info);
				((Activity) context).overridePendingTransition(
						R.anim.slide_in_left, R.anim.slide_out_right);

			}
		});
		msg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (mPhoneModels.get(position).getIsimgr().equals("0")) {
					AllContact.click_invite_all = 1;
					mDatasourceHandler.Update_Invite_Contact(mPhoneModels.get(
							position).getPhone());
					Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
					smsVIntent.setType("vnd.android-dir/mms-sms");
					smsVIntent.putExtra("address", mPhoneModels.get(position)
							.getNumber());
					smsVIntent
							.putExtra(
									"sms_body",
									"Check out imgr instant messaging app.Download it today from http://imgr.im/app");
					try {
						context.startActivity(smsVIntent);
					} catch (Exception ex) {
						Toast.makeText(context, "Your sms has failed...",
								Toast.LENGTH_LONG).show();

						ex.printStackTrace();

					}
				} else {
					AllContact.click_invite_all = 0;
				}

			}

		});
		textView.setText(mPhoneModels.get(position).getName() + " "
				+ mPhoneModels.get(position).getLast());

		return rowView;
	}

	@Override
	public int getPositionForSection(int section) {

		for (int i = 0; i < this.getCount(); i++) {

			String item = this.getItem(i).getName();

			if (item.charAt(0) == sections.charAt(section))
				return i;
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int arg0) {

		return 0;
	}

	@Override
	public Object[] getSections() {

		String[] sectionsArr = new String[sections.length()];

		Log.e("sectionsArr: ", "" + sectionsArr);
		for (int i = 0; i < sections.length(); i++) {
			sectionsArr[i] = "" + sections.charAt(i);
			Log.e("sectionsArr: ", "" + sections.charAt(i));
		}

		return sectionsArr;
	}

	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}

	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {

			String filterString = constraint.toString().toLowerCase();

			FilterResults results = new FilterResults();

			final List<ContactPhoneModel> list = mPhoneModels;

			int count = list.size();
			final ArrayList<String> nlist = new ArrayList<String>(count);

			String filterableString;

			for (int i = 0; i < count; i++) {
				filterableString = mPhoneModels.get(i).getName();
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(filterableString);
				}
			}

			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredData = (ArrayList<String>) results.values;

			notifyDataSetChanged();
		}

	}

	// Filter Class
	@SuppressLint("DefaultLocale")
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		mPhoneModels.clear();
		if (charText.length() == 0) {
			mPhoneModels.addAll(arraylist);
		} else {
			for (ContactPhoneModel wp : arraylist) {
				if (wp.getFullName().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					mPhoneModels.add(wp);
				}
			}
		}
		// char firstChar = charText.toUpperCase(Locale.getDefault()).charAt(0);

		// setSection(header, "" + firstChar);
		notifyDataSetChanged();
	}

}