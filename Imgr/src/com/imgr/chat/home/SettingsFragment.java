package com.imgr.chat.home;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.UILApplication;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.setting.BlockedContacts;
import com.imgr.chat.setting.BubbleColorActivity;
import com.imgr.chat.setting.FontActivity;
import com.imgr.chat.setting.SettingProfileActivity;
import com.imgr.chat.setting.SettingPromoActivity;
import com.imgr.chat.views.BetterPopupWindow;
import com.imgr.chat.xmpp.Connection_Pool;

/**
 * author: amit agnihotri
 */
public class SettingsFragment extends Fragment implements OnClickListener {
	View V;
	TextView mTextView, mTextView_senond, mTextView_openurl;
	ImageView mImageView_arrow_first, mImageView_second;
	LinearLayout mLinearLayout_first, mLinearLayout_second, mLinearLayout_five,
			mLinearLayout_third, mLinearLayout_fourth;
	Intent mIntent;
	Button mButtonShare;
	DemoPopupWindow dw;
	Switch mSwitchPromo, switch_notification;
	String mStringPromoOnOFF, mStringNotification;
	SharedPreferences sharedPreferences;
	Editor editor;

	XMPPConnection connection;
	Message message;
	private Timer myTimer;
	boolean isCallForChating = true;
	String mStringPassword, deviceId;
	DatasourceHandler mDatasourceHandler;
	String promoID;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		V = inflater.inflate(R.layout.settings, container, false);
		sharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);
		mStringPromoOnOFF = sharedPreferences.getString(
				Constant.SETTING_MAIN_PROMO_ON_OFF, "true");
		mStringNotification = sharedPreferences.getString(
				Constant.SETTING_NOTIFICATION_ON_OFF, "true");
		mButtonShare = (Button) V.findViewById(R.id.share_setting);
		switch_notification = (Switch) V.findViewById(R.id.switch_notification);
		mSwitchPromo = (Switch) V.findViewById(R.id.switch_promo);
		mTextView = (TextView) V.findViewById(R.id.txtTitle1);
		mImageView_arrow_first = (ImageView) V.findViewById(R.id.imageView1);
		mLinearLayout_first = (LinearLayout) V.findViewById(R.id.first_layout);
		mTextView_senond = (TextView) V.findViewById(R.id.txtTitle2);
		mTextView_openurl = (TextView) V.findViewById(R.id.openurl);
		mImageView_second = (ImageView) V.findViewById(R.id.imageView2);
		mLinearLayout_second = (LinearLayout) V
				.findViewById(R.id.second_layout);
		mLinearLayout_third = (LinearLayout) V.findViewById(R.id.third_layout);
		mLinearLayout_fourth = (LinearLayout) V.findViewById(R.id.four_layout);
		mLinearLayout_five = (LinearLayout) V.findViewById(R.id.five_layout);

		mTextView.setOnClickListener(SettingsFragment.this);
		mImageView_arrow_first.setOnClickListener(SettingsFragment.this);
		mLinearLayout_first.setOnClickListener(SettingsFragment.this);
		mTextView_openurl.setOnClickListener(SettingsFragment.this);
		mTextView_senond.setOnClickListener(SettingsFragment.this);
		mImageView_second.setOnClickListener(SettingsFragment.this);
		mLinearLayout_second.setOnClickListener(SettingsFragment.this);
		mLinearLayout_five.setOnClickListener(SettingsFragment.this);
		mLinearLayout_third.setOnClickListener(SettingsFragment.this);
		mLinearLayout_fourth.setOnClickListener(SettingsFragment.this);
		mButtonShare.setOnClickListener(SettingsFragment.this);
		if (mStringNotification != null) {
			if (mStringNotification.equals("true")) {
				switch_notification.setChecked(true);
			}
		}

		switch_notification
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							shared_Prefernces("true");
						} else {
							shared_Prefernces("false");
						}

					}
				});

		mSwitchPromo.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					sharedPrefernces("true");
				} else {
					sharedPrefernces("false");
				}

			}
		});
		if (mStringPromoOnOFF.equals("true")) {
			mSwitchPromo.setChecked(true);

		} else {
			mSwitchPromo.setChecked(false);

		}
		final Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection(getActivity());

		setConnection(connection);

		/*myTimer = new Timer();
		myTimer.schedule(new TimerTask() {
			@Override
			public void run() {

				connection = connection_Pool.getConnection(getActivity());
				if (connection.isConnected()) {

					if (isCallForChating == true) {

						setConnection(connection);
					}

				}
			}

		}, 0, 30000);*/
		
		

		return V;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		isCallForChating = false;
		//myTimer.cancel();

	}

	public void setConnection(final XMPPConnection connection) {

		this.connection = connection;
		try {
			Log.e("tag", "connectionconnection :" + connection.isConnected());
			if (connection != null) {

				// Add a packet listener to get messages sent to us
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {

					public void processPacket(Packet packet) {

						/*Message message1 = (Message) packet;
						Log.e("tag",
								"message packet offline 111:"
										+ message1.getBody());

						SharedPreferences newSharedPreferences = getActivity()
								.getSharedPreferences(
										Constant.IMGRNOTIFICATION_RECEIVE,
										Context.MODE_PRIVATE);

						String currentPhoneNo = newSharedPreferences.getString(
								"currentPhoneNo", "");
						String currentScreen = newSharedPreferences.getString(
								"currentScreen", "list");

						if (currentScreen.equalsIgnoreCase("list")) {
							if (currentPhoneNo.equalsIgnoreCase("")) {

								message = (Message) packet;

								String message_send_local_notify = null;
								// String promoID = null;
								String mStringnewtojid = null;
								String mStringnewfromjid = null;
								JSONObject objMainList = null;
								String check = null;
								String check_update = null;
								String time = currenttime();
								String date_sent = date();
								String number = "";

								String toJID = "";
								String fromJID = "";

								String message_id = "";

								// Log.e("message.getPacketID(): ", "" +
								// message.getPacketID());

								if (message.getBody() != null) {

									Log.e("message.getBody()1111: ", ""
											+ message.getBody());

									String fromName = StringUtils
											.parseBareAddress(message.getFrom()
													.split("\\@")[0]);

									// mStringnewtojid = mStringPassword + "_"+
									// deviceId;

									mStringnewtojid = mStringPassword + "_"
											+ deviceId;
									mStringnewfromjid = fromName;

									number = mStringnewfromjid.split("_")[0];

									String incomingNumber = "";

									if (mStringnewtojid.split("_")[0]
											.equalsIgnoreCase(mStringPassword)) {
										incomingNumber = mStringnewfromjid
												.split("_")[0];
									} else {
										incomingNumber = mStringnewtojid
												.split("_")[0];
									}

									String isBlocked = "false";

									Cursor mBlockCursor = mDatasourceHandler
											.FETCH_(incomingNumber);

									if (mBlockCursor.getCount() != 0) {

										if (mBlockCursor.moveToFirst()) {
											isBlocked = mBlockCursor
													.getString(9);
										}
									}

									Log.e("blockedddddddddddddd", isBlocked);

									if (!isBlocked.equalsIgnoreCase("true")) {

										try {
											JSONObject bodyJsonObj = new JSONObject(
													message.getBody());

											if (bodyJsonObj.has("message_id")) {
												message_id = bodyJsonObj
														.getString("message_id");
											} else {
												message_id = message
														.getPacketID();
											}

											String messageDataToBeSaved = "";

											String promo_ID = "0";

											if (bodyJsonObj
													.has("_message_body_tag")) {

												// String promo_ID;
												if (bodyJsonObj
														.has("_promo_id_tag")) {
													promo_ID = bodyJsonObj
															.getString("_promo_id_tag");

												} else {
													promo_ID = "0";
												}

												String message = bodyJsonObj
														.getString("_message_body_tag");

												messageDataToBeSaved = "{\"body\":[{\"preview_Icon\":\"null\",\"_promo_id_tag\":\""
														+ promo_ID
														+ "\",\"pushkey\":\""
														+ message
														+ "\","
														+ "\"_message_body_tag\":\""
														+ message + "\"}]}";
											} else {
												JSONObject json = new JSONObject();
												String mStringBase64 = bodyJsonObj
														.getString("preview_Icon");

												try {
													json.put("server_URL",
															"Not Uploaded");
													json.put("pushkey", "image");
													json.put("preview_Icon",
															mStringBase64);

													String messageData = json
															.toString();
													messageDataToBeSaved = "{\"body\":["
															+ messageData
															+ "]}";

												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}

											mDatasourceHandler
													.insertChathistory(
															message_id,
															"username",
															mStringnewtojid,
															mStringnewfromjid,
															messageDataToBeSaved,
															time,
															"0",
															"1",
															date_sent,
															UILApplication.context,
															incomingNumber,
															"no");
											String timestamp = timestamp();

											int countincrease = 0;

											Log.e("messageeeeeeeeeeeeeeee",
													"countttttttttttttttttttttttt");
											countincrease = mDatasourceHandler
													.UpdateRecentReceiveCount(
															mStringnewfromjid,
															mStringnewtojid,
															messageDataToBeSaved,
															time, date_sent,
															timestamp);

											countincrease = countincrease + 1;

											String mString = "" + countincrease;

											mDatasourceHandler
													.insertrecentMessage(
															messageDataToBeSaved,
															mStringnewfromjid,
															mStringnewtojid,
															"", "", date_sent,
															time, timestamp,
															mString, "no",
															mStringPassword);

											if (!promo_ID.equalsIgnoreCase("0")) {
												Log.e("received promo idddddddddddddddd",
														"" + promo_ID);
												promoID = promo_ID;
												// new
												// Execute_New_Promo().execute();
												new ExecuteChatNotification1()
														.execute();
											}

											// fetchChatHistory();

										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}
								}
							}
						}*/
					}

				}, filter);
			}

		} catch (Exception e) {

		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.txtTitle1:
			mIntent = new Intent(getActivity(), SettingProfileActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.imageView1:
			mIntent = new Intent(getActivity(), SettingProfileActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.first_layout:
			mIntent = new Intent(getActivity(), SettingProfileActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.second_layout:
			mIntent = new Intent(getActivity(), SettingPromoActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.third_layout:
			mIntent = new Intent(getActivity(), BubbleColorActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.four_layout:
			mIntent = new Intent(getActivity(), BlockedContacts.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.five_layout:
			mIntent = new Intent(getActivity(), FontActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.imageView2:
			mIntent = new Intent(getActivity(), SettingPromoActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;
		case R.id.txtTitle2:
			mIntent = new Intent(getActivity(), SettingPromoActivity.class);
			startActivity(mIntent);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			break;

		case R.id.openurl:

			break;

		case R.id.share_setting:
			dw = new DemoPopupWindow(v, getActivity());
			dw.showLikeQuickAction(0, 0);
			break;

		default:
			break;
		}

	}

	// ************** Class for pop-up window **********************
	/**
	 * The Class DemoPopupWindow.
	 */
	private class DemoPopupWindow extends BetterPopupWindow {

		/**
		 * Instantiates a new demo popup window.
		 * 
		 * @param anchor
		 *            the anchor
		 * @param cnt
		 *            the cnt
		 */
		public DemoPopupWindow(View anchor, Context cnt) {
			super(anchor);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cellalert24.Views.BetterPopupWindow#onCreate()
		 */
		@Override
		protected void onCreate() {
			// inflate layout
			LayoutInflater inflater = (LayoutInflater) this.anchor.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			ViewGroup root = (ViewGroup) inflater.inflate(
					R.layout.share_choose_popup, null);

			TextView txtmail = (TextView) root.findViewById(R.id.txtMail);
			TextView txtmessage = (TextView) root.findViewById(R.id.txtMessage);
			Button mButton = (Button) root.findViewById(R.id.cancelBtn);

			txtmail.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();
					sendEmail();

				}
			});
			txtmessage.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();
					Intent smsVIntent = new Intent(Intent.ACTION_VIEW);
					smsVIntent.setType("vnd.android-dir/mms-sms");
					// smsVIntent.putExtra("address", mString_phone_no);
					smsVIntent
							.putExtra(
									"sms_body",
									"Check out imgr instant messaging app.Download it today from http://imgr.im/app");

					startActivity(smsVIntent);

				}
			});
			mButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// mFrameLayout.setVisibility(View.GONE);
					// layout.setBackgroundColor(Color.WHITE);
					dismiss();

				}
			});

			this.setContentView(root);
		}

	}

	protected void sendEmail() {
		// Log.e("Send email", "" + mStringEmail);
		String[] TO = {};
		String[] CC = { "" };
		Intent emailIntent = new Intent(Intent.ACTION_SEND);

		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.setType("text/plain");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
		emailIntent.putExtra(Intent.EXTRA_CC, CC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "IMGR Android Application");
		emailIntent
				.putExtra(
						Intent.EXTRA_TEXT,
						"Check out imgr instant messaging app. Download it today from http://imgr.im/app");

		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			// finish();
			Log.i("Finished sending email...", "");
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(getActivity(),
					"There is no email client installed.", Toast.LENGTH_SHORT)
					.show();
		}
	}

	public void sharedPrefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.SETTING_MAIN_PROMO_ON_OFF, mString);

		editor.commit();

	}

	public void shared_Prefernces(String mString) {
		editor = sharedPreferences.edit();
		editor.putString(Constant.SETTING_NOTIFICATION_ON_OFF, mString);

		editor.commit();

	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			// s.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			// /Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public String date_refresh() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	class ExecuteChatNotification1 extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {

			String url;

			String id = promoID;
			Log.e("received promo idddddddddddddddd", "" + id);
			url = JsonParserConnector.getPERSONALSponsored(id);
			Log.e("received promo iddddddddddddd111111", "" + url);
			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Personal_List(result);
		}

		private void Personal_List(String response) {
			try {
				String message_send_local_notify = null;
				String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
				// Log.e("strJson: ", "" + strJson);
				JSONObject jsonOBject = new JSONObject(response);

				if (jsonOBject.getBoolean("success")) {

					Log.e("new promoooooooo", response);

					JSONArray bodyJsonObj = jsonOBject.getJSONArray("data");

					JSONObject sys = new JSONObject(bodyJsonObj.getString(0));

					mStringspromoid = sys.getString("promo_id");
					// mStringsusername = sys.getString("username");

					mStringspromo_name = sys.getString("promo_name");
					mStringspromo_image = sys.getString("promo_image");
					// mStringsmodified_date = sys.getString("modified_date");
					mString_link_text = sys.getString("promo_link_text");
					mString_link_promo = sys.getString("promo_link");
					mString_header_promo_message = sys
							.getString("promo_message_header");

					Log.e("promo_data", "promo_id: " + mStringspromoid
							+ ", promo_type: NO_Define, promo_name: "
							+ mStringspromo_name + ", promo_image: "
							+ "https://imgrapp.com/release/"
							+ mStringspromo_image + ", promo_link: "
							+ mString_link_promo + ", promo_link_text: "
							+ mString_link_promo + ", promo_message_header: "
							+ mString_header_promo_message
							+ ", is_deleted: 0, modified_date: "
							+ mStringsmodified_date
							+ ", isActive: 1, is_enabled: 1");

					Log.e("PROMO NAME",""+mStringspromo_name);
					
					boolean flag = mDatasourceHandler.insertSponsored(
							mStringspromoid, "NO_Define", mStringspromo_name,
							"https://imgrapp.com/release/"
									+ mStringspromo_image, mString_link_promo,
							mString_link_text, mString_header_promo_message,
							"0", mStringsmodified_date, "1", "1");

				} else {

				}

			} catch (JSONException e) {

				// Log.e("", e.getMessage());
			}

		}

	}
}
