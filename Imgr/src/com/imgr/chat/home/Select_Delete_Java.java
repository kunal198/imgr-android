package com.imgr.chat.home;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imgr.chat.R;
import com.imgr.chat.adapter.Delete;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.home.MessageFragment.MessageScreenAdapter;
import com.imgr.chat.message.ChatScreen;
import com.imgr.chat.model.Delete_Model;
import com.imgr.chat.util.LogMessage;

public class Select_Delete_Java extends Activity {
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor, mCursor2;
	String mStringPassword, deviceId;
	SharedPreferences sharedPreferences, mSharedPreferences;
	ConnectionDetector mConnectionDetector;
	ArrayList<Delete_Model> products = new ArrayList<Delete_Model>();
	ArrayList<String> base64_array;
	ArrayList<String> ChatCount = new ArrayList<String>();
	ArrayList<String> mArrayListFirstName = new ArrayList<String>();
	ArrayList<String> mArrayListFirstName_new = new ArrayList<String>();
	ArrayList<String> mArrayListMessage = new ArrayList<String>();
	ArrayList<String> mArrayListTime = new ArrayList<String>();
	ArrayList<String> mArrayListDate = new ArrayList<String>();
	ArrayList<String> mArrayListJid = new ArrayList<String>();
	ArrayList<String> NewmArrayListTestMessage = new ArrayList<String>();
	ArrayList<String> mArrayListuniqueId = new ArrayList<String>();
	ArrayList<String> AllNEWmArrayListTestToJID = new ArrayList<String>();
	ArrayList<String> Imgrcontactnumber = new ArrayList<String>();
	ArrayList<String> NewImgrcontactnumber = new ArrayList<String>();
	ArrayList<String> MemberId = new ArrayList<String>();
	ArrayList<String> NewMemberId = new ArrayList<String>();
	ArrayList<String> NEWmArrayListTime = new ArrayList<String>();

	// ArrayList<String> mArrayListTOTEST = new ArrayList<String>();
	ArrayList<String> mArrayListPhoneno = new ArrayList<String>();
	ArrayList<String> mArrayListPHONE = new ArrayList<String>();
	ArrayList<String> mArrayListJID_ = new ArrayList<String>();
	String msgCount;
	Delete mAdapter;
	// ListView mListView;
	TextView btn_back_loginpage;
	static TextView header_title;
	Button btn_invite;
	AlertDialog pinDialog;
	// SwipeRefreshLayout mSwipeLayout_message;
	private PullToRefreshListView mPullRefreshListView;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
		setContentView(R.layout.activity_delect_java);
		// mSwipeLayout_message = (SwipeRefreshLayout)
		// findViewById(R.id.swipe_imgr);
		sharedPreferences = this.getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		mSharedPreferences = this.getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);
		mConnectionDetector = new ConnectionDetector(this);
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.list_View_IMGR);
		refresh_screen();
		btn_back_loginpage = (TextView) findViewById(R.id.btn_back_loginpage);
		btn_invite = (Button) findViewById(R.id.btn_invite);
		header_title = (TextView) findViewById(R.id.header_title);
		btn_back_loginpage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);
			}
		});

		deviceId = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);

		Log.e("ArraySize",""+mArrayListJid.size());
		
//		if(mArrayListJid.size() > 0){
		
		
		btn_invite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * if (mArrayListJid.size() > 0) {
				 * 
				 * }
				 */
				
				String[] s = header_title.getText().toString().split(" ");
				
				msgCount = s[0];
				
				if(msgCount.equals("0")){
					Toast.makeText(getApplicationContext(), "Please Select Item to delete ...", Toast.LENGTH_SHORT).show();
//					
				}else{

//				if (mArrayListJid.size() == 0) {
//
//					/*
//					 * LogMessage.showDialog(getApplicationContext(), null,
//					 * "No message to delete", null, "Ok");
//					 */
//					
//					Toast.makeText(getApplicationContext(), "Please Select Item to delete ...", Toast.LENGTH_SHORT).show();
//					
//
//				} else {

					mArrayListPHONE.clear();
					mArrayListJID_.clear();
					for (Delete_Model p : mAdapter.getBox()) {
						if (p.box) {
							// result = "\n" + p.getuniqueid();
							mArrayListPHONE.add(p.getuniqueid());
							mArrayListJID_.add(p.getJID());

						}
					}
					Log.e("mArrayListPHONE: ", "" + mArrayListPHONE);

					delete_popup();
//				}
				}

			}
		});
		
//		}else{
//			Toast.makeText(getApplicationContext(), "Please Select Item to delete ...", Toast.LENGTH_SHORT).show();
//		}

		// mSwipeLayout_message.setColorSchemeResources(
		// android.R.color.holo_blue_bright,
		// android.R.color.holo_green_light,
		// android.R.color.holo_orange_light,
		// android.R.color.holo_red_light);
		// mSwipeLayout_message.setOnRefreshListener(new OnRefreshListener() {
		//
		// @Override
		// public void onRefresh() {
		// // TODO Auto-generated method stub
		//
		// new Handler().postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// mSwipeLayout_message.setRefreshing(false);
		// }
		// }, 2000);
		//
		// }
		// });
		mPullRefreshListView
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
						String label = date_refresh() + " " + currenttime();
						// DateUtils.formatDateTime(
						// getApplicationContext(),
						// System.currentTimeMillis(),
						// DateUtils.FORMAT_SHOW_TIME
						// | DateUtils.FORMAT_SHOW_DATE
						// | DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setReleaseLabel(
								"Pull down to update promos " + "Last Updated:"
										+ label);

						// refreshView.getLoadingLayoutProxy().
						// refreshView.getLoadingLayoutProxy().setPullLabel("Pull down to update promos");
						refreshView.getLoadingLayoutProxy().setRefreshingLabel(
								"Pull down to update promos " + "Last Updated:"
										+ label);
						// refreshView.getLoadingLayoutProxy().setLastUpdatedLabel("Pull down to update promos");
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								mPullRefreshListView.onRefreshComplete();
							}
						}, 2000);
						refresh_screen();
						// Do work to refresh the list here.
						// new GetDataTask().execute();
					}
				});
	}

	public static void changetextview(int size) {
		header_title.setText("" + size + " " + "selected");
	}

	/*
	 * protected void refresh_screen() {
	 * 
	 * mDatasourceHandler = new DatasourceHandler(this); mDatabaseHelper = new
	 * DatabaseHelper(this); base64_array = new ArrayList<String>();
	 * ArrayList<String> Imgrcontactunquieid = new ArrayList<String>(); deviceId
	 * = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
	 * 
	 * Log.e("deviceid:", "" + deviceId); mStringPassword =
	 * sharedPreferences.getString(Constant.PASSWORD_XMPP, "");
	 * 
	 * Log.e("", "here"); try { mCursor = mDatasourceHandler.FETCH_RECENT(); if
	 * (mCursor.getCount() != 0) { mArrayListFirstName.clear();
	 * mArrayListMessage.clear(); mArrayListTime.clear(); mArrayListJid.clear();
	 * NewmArrayListTestMessage.clear(); mArrayListFirstName_new.clear();
	 * mArrayListDate.clear();
	 * 
	 * do { mArrayListDate.add(mCursor.getString(0).trim());
	 * mArrayListFirstName.add(mCursor.getString(1).trim());
	 * mArrayListJid.add(mCursor.getString(2).trim());
	 * //mArrayListTOTEST.add(mCursor.getString(3).trim());
	 * mArrayListMessage.add(mCursor.getString(5).trim());
	 * mArrayListTime.add(mCursor.getString(6).trim());
	 * ChatCount.add(mCursor.getString(8).trim());
	 * 
	 * } while (mCursor.moveToNext());
	 * 
	 * mCursor.close(); Log.e("mArrayListTime: ", "" + mArrayListTime);
	 * Log.e("mArrayListMessage: ", "" + mArrayListMessage); // Message decode
	 * section open=======
	 * 
	 * for (int j = 0; j < mArrayListMessage.size(); j++) { String mStringBody =
	 * mArrayListMessage.get(j); // Log.e("mStringBody: ", "" + mStringBody);
	 * JSONObject json_OBject = null; try { json_OBject = new
	 * JSONObject(mStringBody); } catch (JSONException e2) { // TODO
	 * Auto-generated catch block e2.printStackTrace(); } //
	 * Log.e("json_OBject: ", "" + json_OBject);
	 * 
	 * JSONArray mArray = null; try { mArray = json_OBject.getJSONArray("body");
	 * } catch (JSONException e2) { // TODO Auto-generated catch block
	 * e2.printStackTrace(); }
	 * 
	 * for (int z = 0; z < mArray.length(); z++) { JSONObject mJsonObject =
	 * null; try { mJsonObject = mArray.getJSONObject(z); } catch (JSONException
	 * e1) { // TODO Auto-generated catch block e1.printStackTrace(); }
	 * 
	 * try { if (mJsonObject.getString("pushkey") .equals("image")) {
	 * NewmArrayListTestMessage.add("Image"); } else {
	 * 
	 * try { String promo_ID = mJsonObject .getString("_promo_id_tag"); } catch
	 * (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } String message_ = mJsonObject
	 * .getString("pushkey"); NewmArrayListTestMessage.add(mJsonObject
	 * .getString("pushkey"));
	 * 
	 * } } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } }
	 * 
	 * } mArrayListPhoneno = new ArrayList<String>(); mArrayListPhoneno.clear();
	 * AllNEWmArrayListTestToJID.clear(); for (int i = 0; i <
	 * mArrayListJid.size(); i++) {
	 * 
	 * mArrayListPhoneno.add(mArrayListJid.get(i).split("_")[0]);
	 * AllNEWmArrayListTestToJID.add(mArrayListJid.get(i).split( "_")[0]); }
	 * 
	 * Log.e("mArrayListFirstName: ", "" + mArrayListFirstName); try { mCursor =
	 * mDatasourceHandler.ImgrContacts(); Log.e("Local Fragment: ",
	 * mCursor.getCount() + ""); } catch (Exception e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * if (mCursor.getCount() != 0) {
	 * 
	 * NewImgrcontactnumber.clear();
	 * 
	 * // mArrayListFirstName.clear(); mArrayListuniqueId.clear();
	 * MemberId.clear(); mArrayListFirstName_new.clear();
	 * 
	 * Imgrcontactnumber.clear(); mArrayListFirstName.clear();
	 * Imgrcontactunquieid.clear();
	 * 
	 * do {
	 * 
	 * // mArrayListFirstName.add(mCursor.getString(1).trim());
	 * Imgrcontactnumber.add(mCursor.getString(2).trim());
	 * 
	 * MemberId.add(mCursor.getString(5));
	 * 
	 * Imgrcontactunquieid.add(mCursor.getString(8).trim());
	 * 
	 * } while (mCursor.moveToNext());
	 * 
	 * mCursor.close(); Log.e("", "" + Imgrcontactnumber); for (int i = 0; i <
	 * Imgrcontactnumber.size(); i++) { NewImgrcontactnumber
	 * .add(Imgrcontactnumber.get(i).substring(
	 * Imgrcontactnumber.get(i).length() - 10)); } }
	 * 
	 * NewMemberId.clear(); mArrayListuniqueId.clear();
	 * mArrayListFirstName_new.clear(); base64_array.clear(); for (int i = 0; i
	 * < AllNEWmArrayListTestToJID.size(); i++) { Cursor mCursor =
	 * mDatasourceHandler .FETCH_(AllNEWmArrayListTestToJID.get(i)); if
	 * (mCursor.getCount() != 0) { Log.e("", "MATCHED");
	 * 
	 * if (mCursor.moveToFirst()) {
	 * mArrayListFirstName_new.add(mCursor.getString(1) .trim());
	 * NewMemberId.add(mCursor.getString(5));
	 * base64_array.add(mCursor.getString(7));
	 * mArrayListuniqueId.add(mCursor.getString(8).trim());
	 * 
	 * }
	 * 
	 * mCursor.close();
	 * 
	 * } else { NewMemberId.add("0"); mArrayListuniqueId.add("0");
	 * mArrayListFirstName_new.add(""); base64_array.add("Null"); Log.e("",
	 * "NO MATCHED"); mCursor.close(); }
	 * 
	 * } Log.e("NewMemberId: before: ", "" + NewMemberId);
	 * Log.e("mArrayListPhoneno: before: ", "" + mArrayListPhoneno);
	 * Log.e("NewmArrayListTestMessage: before: ", "" +
	 * NewmArrayListTestMessage); Log.e("mArrayListTime: before: ", "" +
	 * mArrayListTime); Log.e("mArrayListuniqueId: before: ", "" +
	 * mArrayListuniqueId); Log.e("ChatCount: before: ", "" + ChatCount);
	 * 
	 * } } catch (Exception e) { Log.e("MESSAGE CRASH:", "" + e.getMessage()); }
	 * products.clear(); for (int i = 0; i < mArrayListJid.size(); i++) {
	 * 
	 * products.add(new Delete_Model(mArrayListPhoneno.get(i),
	 * mArrayListFirstName_new.get(i), mArrayListuniqueId.get(i),
	 * mArrayListJid.get(i), NewmArrayListTestMessage.get(i),
	 * mArrayListTime.get(i), mArrayListDate.get(i), base64_array .get(i),
	 * false)); } mAdapter = new Delete(Select_Delete_Java.this,
	 * mArrayListFirstName_new, mArrayListPhoneno, products);
	 * 
	 * mPullRefreshListView.setAdapter(mAdapter);
	 * 
	 * }
	 */

	protected void refresh_screen() {

		ChatCount = new ArrayList<String>();
		base64_array = new ArrayList<String>();
		mArrayListFirstName = new ArrayList<String>();
		mArrayListFirstName_new = new ArrayList<String>();
		mArrayListMessage = new ArrayList<String>();
		mArrayListTime = new ArrayList<String>();
		mArrayListDate = new ArrayList<String>();
		mArrayListJid = new ArrayList<String>();
		NewmArrayListTestMessage = new ArrayList<String>();
		mDatasourceHandler = new DatasourceHandler(this);
		mDatabaseHelper = new DatabaseHelper(this);
		mArrayListuniqueId = new ArrayList<String>();
		AllNEWmArrayListTestToJID = new ArrayList<String>();
		mArrayListMessage = new ArrayList<String>();
		mArrayListTime = new ArrayList<String>();
		NEWmArrayListTime = new ArrayList<String>();
		mArrayListJid = new ArrayList<String>();
		// mArrayListTOTEST = new ArrayList<String>();
		mArrayListuniqueId = new ArrayList<String>();

		mArrayListPhoneno = new ArrayList<String>();

		Imgrcontactnumber = new ArrayList<String>();
		NewImgrcontactnumber = new ArrayList<String>();
		MemberId = new ArrayList<String>();
		NewMemberId = new ArrayList<String>();

		// Imgrcontactunquieid = new ArrayList<String>();
		// deviceId = Secure.getString(getActivity().getContentResolver(),
		// Secure.ANDROID_ID);

		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");

		try {
			mCursor = mDatasourceHandler.FETCH_RECENT(mStringPassword);
			if (mCursor.getCount() != 0) {
				mArrayListFirstName.clear();
				mArrayListMessage.clear();
				mArrayListTime.clear();
				mArrayListJid.clear();
				NewmArrayListTestMessage.clear();
				mArrayListFirstName_new.clear();
				mArrayListDate.clear();

				mArrayListPhoneno.clear();
				ChatCount.clear();
				NewMemberId.clear();
				mArrayListuniqueId.clear();
				base64_array.clear();

				// Log.e("sfsfadsffffffffffffffffff11111","fefefefeffefefefe");
				do {

					if (mCursor.getString(2).trim().length() > 5
							&& mCursor.getString(3).trim().length() > 5) {
						mArrayListDate.add(mCursor.getString(0).trim());

						mArrayListFirstName.add(mCursor.getString(1).trim());

						Log.e("jiddddddddd", mCursor.getString(2).trim());

						if (!mCursor.getString(2).trim().split("_")[0]
								.equalsIgnoreCase(mStringPassword)) {
							mArrayListJid.add(mCursor.getString(2).trim());
						} else {
							mArrayListJid.add(mCursor.getString(3).trim());
						}

						if (mCursor.getString(5).trim().length() > 0) {
							mArrayListMessage.add(mCursor.getString(5).trim());
							// mArrayListMessage.add("feger");
						} else {
							mArrayListMessage.add("");
						}

						mArrayListTime.add(mCursor.getString(6).trim());
						ChatCount.add(mCursor.getString(8).trim());
					}
				} while (mCursor.moveToNext());

				mCursor.close();

				// Message decode section open=======

				// Log.e("sfsfadsffffffffffffffffff22222",mArrayListJid.get(0).split("_")[0]);

				Log.e("size of message to be deletedddddddddddddd", ""
						+ mArrayListJid.size());

				for (int j = 0; j < mArrayListJid.size(); j++) {

					mArrayListPhoneno.add(mArrayListJid.get(j).split("_")[0]);

					String textmessage = "";

					if (mArrayListMessage.get(j).length() > 0) {
						JSONObject jsonObj = new JSONObject(
								mArrayListMessage.get(j));

						// JSONArray mJsonArray = null;

						JSONArray bodyArray = jsonObj.getJSONArray("body");

						JSONObject bodyJsonObj = new JSONObject(
								bodyArray.getString(0));

						if (bodyJsonObj.has("_message_body_tag")) {

							textmessage = bodyJsonObj
									.getString("_message_body_tag");

						} else {
							textmessage = "image";
						}

						NewmArrayListTestMessage.add(textmessage);
					}

					Cursor mCursor = mDatasourceHandler.FETCH_(mArrayListJid
							.get(j).split("_")[0]);
					if (mCursor.getCount() != 0) {

						if (mCursor.moveToFirst()) {
							mArrayListFirstName_new.add(mCursor.getString(1)
									.trim());
							NewMemberId.add(mCursor.getString(5));
							base64_array.add(mCursor.getString(7).trim());
							mArrayListuniqueId.add(mCursor.getString(8).trim());

						}

						mCursor.close();

					} else {
						NewMemberId.add("0");
						mArrayListuniqueId.add("0");
						base64_array.add("Null");
						mArrayListFirstName_new.add(mArrayListJid.get(j).split(
								"_")[0]);

						mCursor.close();
					}

				}

				products.clear();
				for (int i = 0; i < mArrayListJid.size(); i++) {

					products.add(new Delete_Model(mArrayListPhoneno.get(i),
							mArrayListFirstName_new.get(i), mArrayListuniqueId
									.get(i), mArrayListJid.get(i),
							NewmArrayListTestMessage.get(i), mArrayListTime
									.get(i), mArrayListDate.get(i),
							base64_array.get(i), false));
				}

				mAdapter = new Delete(Select_Delete_Java.this,
						mArrayListFirstName_new, mArrayListPhoneno, products);

				mPullRefreshListView.setAdapter(mAdapter);

			}
		} catch (Exception e) {
			// Log.e("MESSAGE CRASH:", "" + e.getMessage());
			// Log.e("crashdescription", "" + e.getLocalizedMessage());
			// Log.e("crashstack", "" + e.getStackTrace().toString());

		}

	}

	protected void delete_popup() {

		final View v = LayoutInflater.from(Select_Delete_Java.this).inflate(
				R.layout.delete_pop_up_message, null);

		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final Button mButtonyes = (Button) v.findViewById(R.id.yes_dialog);

		pinDialog = new AlertDialog.Builder(Select_Delete_Java.this).setView(v)
				.create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();
						finish();
						overridePendingTransition(R.anim.slide_out_left,
								R.anim.slide_in_right);

					}
				});

				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						// mArrayListJid

						mArrayListPHONE.clear();
						mArrayListJID_.clear();
						for (Delete_Model p : mAdapter.getBox()) {
							if (p.box) {
								// result = "\n" + p.getuniqueid();
								mArrayListPHONE.add(p.getuniqueid());
								mArrayListJID_.add(p.getJID());

								Log.e("arraylisttttttttttttttt",
										"ttttttttttttttt");

							}
						}
						// Log.e("mArrayListJID_: ", "" + mArrayListJID_);
						if (mArrayListPHONE.size() == 0) {
							finish();
							overridePendingTransition(R.anim.slide_out_left,
									R.anim.slide_in_right);
						} else {
							for (int i = 0; i < mArrayListPHONE.size(); i++) {

								Log.e("mArrayListJID_: ",
										"" + mArrayListJID_.get(i));
								Log.e("myJID_: ", "" + mStringPassword + "_"
										+ deviceId);

								boolean flag1 = mDatasourceHandler
										.deleteMesage(mStringPassword + "_"
												+ deviceId,
												mArrayListJID_.get(i));
								boolean flag = mDatasourceHandler
										.deleteHistoryMesage(mStringPassword
												+ "_" + deviceId,
												mArrayListJID_.get(i));

								Log.e("isDeletedOrNot: ", "" + flag1 + "_"
										+ flag);

							}

							finish();
							overridePendingTransition(R.anim.slide_out_left,
									R.anim.slide_in_right);
						}
						pinDialog.cancel();

					}
				});
			}
		});

		pinDialog.show();

	}

	public String date_refresh() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			// s.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			// /Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}
}
