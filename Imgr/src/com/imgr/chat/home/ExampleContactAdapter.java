package com.imgr.chat.home;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imgr.chat.R;
import com.imgr.chat.model.ExampleContactItem;
import com.ngohung.widget.ContactItemInterface;
import com.ngohung.widget.ContactListAdapter;

public class ExampleContactAdapter extends ContactListAdapter {
	Context mContext;

	public ExampleContactAdapter(Context _context, int _resource,
			List<ContactItemInterface> _items) {
		super(_context, _resource, _items);
		mContext = _context;
	}

	// override this for custom drawing
	public void populateDataForRow(View parentView, ContactItemInterface item,
			int position) {
		// default just draw the item only
		View infoView = parentView.findViewById(R.id.infoRowContainer);
		TextView fullNameView = (TextView) infoView
				.findViewById(R.id.fullNameView);
		TextView nicknameView = (TextView) infoView
				.findViewById(R.id.nickNameView);

		ImageView mImageView_info = (ImageView) infoView
				.findViewById(R.id.personal_info);
		mImageView_info.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(mContext, "CLICKED", Toast.LENGTH_LONG).show();

			}
		});

		nicknameView.setText(item.getItemForIndex());

		if (item instanceof ExampleContactItem) {
			ExampleContactItem contactItem = (ExampleContactItem) item;
			fullNameView.setText("Full name: " + contactItem.getFullName());
		}

	}

}
