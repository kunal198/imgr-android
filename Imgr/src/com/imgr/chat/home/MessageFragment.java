package com.imgr.chat.home;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.PrivacyProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLPlainMechanism;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.GroupChatInvitation;
import org.jivesoftware.smackx.PrivateDataManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.packet.ChatStateExtension;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.provider.DataFormProvider;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.jivesoftware.smackx.provider.MessageEventProvider;
import org.jivesoftware.smackx.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.provider.RosterExchangeProvider;
import org.jivesoftware.smackx.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.provider.VCardProvider;
import org.jivesoftware.smackx.provider.XHTMLExtensionProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imgr.chat.R;
import com.imgr.chat.UILApplication;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.message.ChatScreen;
import com.imgr.chat.setting.ViewSponsored.ExecutePullSponsored;
import com.imgr.chat.util.UI;
import com.imgr.chat.xmpp.Connection_Pool;
import com.imgr.service.ScreenReceiver;
import com.joooonho.SelectableRoundedImageView;

public class MessageFragment extends Fragment {
	
	public static boolean pulledFromMessage = false;
	
	
	public String TAG = "MessageFragment";
	View V;
	// SwipeMenuListView mListView;
	PullToRefreshListView mPullRefreshListView;
	ArrayList<String[]> messages = new ArrayList<String[]>();
	Message message;
	JSONArray mArrayPersonal = null;
	JSONObject personaljsonObj;
	String promoID;
	String personal_api_time, personal_api_pushkey, personal_api_idtag,
			personal_jidto, personal_jidfrom;
	boolean flag = false;
	MessageFragment messgaefragment;
	private Handler mHandler = new Handler();

	SharedPreferences sharedPreferences, mSharedPreferences;
	Editor editor;
	// SwipeRefreshLayout mSwipeLayout_message;
	String mStringusername;
	ArrayList<String> mArrayListFirstName;
	ArrayList<String> mArrayListFirstName_new;
	ArrayList<String> mArrayListLastName;
	ArrayList<String> mArrayListProficePic;
	ArrayList<String> mArrayListMessage;
	ArrayList<String> mArrayListTime;
	ArrayList<String> mArrayListDate;
	ArrayList<String> NEWmArrayListTime;
	ArrayList<String> mArrayListJid;
	ArrayList<String> base64_array;
	ArrayList<String> mArrayListPhoneno;
	ArrayList<String> mArrayListuniqueId;
	ConnectionDetector mConnectionDetector;

	ArrayList<String> mArrayListTestTo;
	ArrayList<String> mArrayListTestFrom;
	ArrayList<String> Imgrcontactnumber;
	ArrayList<String> Imgrcontactname;
	ArrayList<String> Imgrcontactunquieid;
	ArrayList<String> NewImgrcontactnumber;
	ArrayList<String> mArrayListTestMessage;
	ArrayList<String> NewmArrayListTestMessage;
	ArrayList<String> mArrayListTestDate;
	ArrayList<String> mArrayListTestTime;
	ArrayList<String> mArrayListTestToJID;
	ArrayList<String> mArrayListTestFromJID;
	ArrayList<String> NEWmArrayListTestToJID;
	ArrayList<String> AllNEWmArrayListTestToJID;

	ArrayList<String> FINALNewImgrcontactnumber;
	ArrayList<String> FINALNewImgrcontactname;
	ArrayList<String> FINALNewImgrcontactuniqueid;
	ArrayList<String> MemberId;
	ArrayList<String> NewMemberId;
	ArrayList<String> ChatCount;

	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor, mCursor2;
	String mStringPassword, deviceId;
	FrameLayout mFrameLayout_no_message;
	AlertDialog pinDialog;
	int pos;
	MessageScreenAdapter message_Adapter;

	boolean isListUpdating = false;

	// ArrayList<String> mArrayListTOTEST;
	boolean list_set = false;

	VCard card;

	// xmpp objects
	ConnectivityManager connectivity_Manager;

	Activity activity;

	ConnectionConfiguration config;
	XMPPConnection connection;
	String mString_notification;
	String visibility;

	Context context;
	DatasourceHandler mDatasourceHandlerNew;
	// PacketListener packetListener;
	private Timer myTimer;
	boolean isCallForChating = true;

	MessageFragment(Context context) {
		this.context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		V = inflater.inflate(R.layout.message, container, false);
		mPullRefreshListView = (PullToRefreshListView) V
				.findViewById(R.id.listView1);

		// mDatasourceHandler = new DatasourceHandler(getActivity());
		// editor.putString("isOnResume","yes");

		final Connection_Pool connection_Pool = Connection_Pool.getInstance();
		connection = connection_Pool.getConnection(context);

		setConnection(connection);
		card = new VCard();
		// added by Saveen
		// holdConnectionHandler.sendEmptyMessage(0);

		mFrameLayout_no_message = (FrameLayout) V
				.findViewById(R.id.frame_no_message);
		sharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRCHAT, Context.MODE_PRIVATE);

		mStringPassword = sharedPreferences.getString(Constant.PASSWORD_XMPP,
				"");

		mSharedPreferences = getActivity().getSharedPreferences(
				Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE);

		editor = mSharedPreferences.edit();
		editor.putString("isOnResume", "yes");
		editor.commit();

		mConnectionDetector = new ConnectionDetector(getActivity());
		messgaefragment = new MessageFragment(getActivity());
		deviceId = Secure.getString(getActivity().getContentResolver(),
				Secure.ANDROID_ID);
		mPullRefreshListView
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
						String label = date_refresh() + " " + currenttime();

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setReleaseLabel(
								"Pull down to update promos " + "Last Updated:"
										+ label);

						refreshView.getLoadingLayoutProxy().setRefreshingLabel(
								"Pull down to update promos " + "Last Updated:"
										+ label);

						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								pulledFromMessage = true;
								try{
								new ExecutePullSponsored().execute();
								}catch(Exception e){}
								mPullRefreshListView.onRefreshComplete();
							}
						}, 4000);
						// refresh_screen();

					}
				});

		/*myTimer = new Timer();
		myTimer.schedule(new TimerTask() {
			@Override
			public void run() {

				connection = connection_Pool.getConnection(context);
				if (connection.isConnected()) {

					if (isCallForChating == true) {

						setConnection(connection);
					}

				}
			}

		}, 0, 30000);*/

		return V;

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		try{
		refresh_screen();
		}catch(Exception e){e.printStackTrace();
		Log.e("Refresh Exception",""+e);}
		
//		if(!connection.isConnected()){
//			setConnection(connection);
//		}else{
//			Log.e(TAG, "inside_onStart- Connection set -"+connection.isConnected());
//		}
	}
	
//    public static void exitApplication(Context context)
//    {
//        Intent intent = new Intent(context, MessageFragment.class);
//
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//
//        context.startActivity(intent);
//    }
	
	public class login_Existing_User extends AsyncTask<String, Integer, String> {

		protected void onPreExecute() {
			UI.showProgressDialog(getActivity());
		}

		@Override
		protected String doInBackground(String... params) {
			Log.e("doInBackground", "doInBackground");
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			connection = new XMPPConnection(config);
			// config.setSASLAuthenticationEnabled(true);
			// config.setCompressionEnabled(true);
			// config.setSecurityMode(SecurityMode.enabled);
			configure(ProviderManager.getInstance());
			Connection_Pool connection_Pool = Connection_Pool.getInstance();
			connection_Pool.setConnection(connection);

			String result = "pass";
			try {
				if (!connection.isConnected())
					connection.connect();
				Log.e("connecteddddddddddddd", "server connect");

				// String temp_Pass=Encrypt_Uttils.encryptPassword(password);
				Log.e("" + mStringusername, "" + mStringPassword);
				connection.login(mStringusername, mStringPassword);
				Presence presence = new Presence(Presence.Type.available);
				connection.sendPacket(presence);
			} catch (XMPPException e) {
				Log.e("Cannot connect to XMPP server with default admin username and password.",
						"0");
				Log.e("XMPPException", "" + e);

			}

			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			UI.hideProgressDialog();
			Log.e("result", "" + result);
			if (result != null) {
				Log.e("ConnectionId", "" + connection.getConnectionID());
				Log.e("Servicename", "" + connection.getServiceName());
				Log.e("User", "" + connection.getUser());
				Log.e("===mConnectionDetector.isConnectingToInternet()", ""
						+ mConnectionDetector.isConnectingToInternet());

			} else {
				connection.disconnect();
				Connection_Pool.getInstance().setConnection(null);
				Toast.makeText(
						getActivity(),
						"There is already an account registered with this name",
						Toast.LENGTH_LONG).show();
			}

		}

		/*** end of onPostExecute ***/

		public void configure(ProviderManager pm) {
			// Private Data Storage
			pm.addIQProvider("query", "jabber:iq:private",
					new PrivateDataManager.PrivateDataIQProvider());
			// Time
			try {
				pm.addIQProvider("query", "jabber:iq:time",
						Class.forName("org.jivesoftware.smackx.packet.Time"));
			} catch (ClassNotFoundException e) {
				Log.w("TestClient",
						"Can't load class for org.jivesoftware.smackx.packet.Time");
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Roster Exchange
			pm.addExtensionProvider("x", "jabber:x:roster",
					new RosterExchangeProvider());

			// Message Events
			pm.addExtensionProvider("x", "jabber:x:event",
					new MessageEventProvider());

			// Chat State
			pm.addExtensionProvider("active",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("composing",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("paused",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("inactive",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			pm.addExtensionProvider("gone",
					"http://jabber.org/protocol/chatstates",
					new ChatStateExtension.Provider());

			// XHTML
			pm.addExtensionProvider("html",
					"http://jabber.org/protocol/xhtml-im",
					new XHTMLExtensionProvider());

			// Group Chat Invitations
			pm.addExtensionProvider("x", "jabber:x:conference",
					new GroupChatInvitation.Provider());

			// Service Discovery # Items
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());

			// Service Discovery # Info
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Data Forms
			pm.addExtensionProvider("x", "jabber:x:data",
					new DataFormProvider());

			// MUC User
			pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
					new MUCUserProvider());

			// MUC Admin
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
					new MUCAdminProvider());

			// MUC Owner
			pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
					new MUCOwnerProvider());

			// SharedGroupsInfo
			pm.addIQProvider("sharedgroup",
					"http://www.jivesoftware.org/protocol/sharedgroup",
					new SharedGroupsInfo.Provider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
					new DiscoverItemsProvider());
			pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
					new DiscoverInfoProvider());

			// Delayed Delivery
			pm.addExtensionProvider("x", "jabber:x:delay",
					new DelayInformationProvider());

			// Version
			try {
				pm.addIQProvider("query", "jabber:iq:version",
						Class.forName("org.jivesoftware.smackx.packet.Version"));
			} catch (ClassNotFoundException e) {
				// Not sure what's happening here.
			}

			// VCard
			pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());

			// Offline Message Requests
			pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
					new OfflineMessageRequest.Provider());

			// Offline Message Indicator
			pm.addExtensionProvider("offline",
					"http://jabber.org/protocol/offline",
					new OfflineMessageInfo.Provider());

			// Last Activity
			pm.addIQProvider("query", "jabber:iq:last",
					new LastActivity.Provider());

			// User Search
			pm.addIQProvider("query", "jabber:iq:search",
					new UserSearch.Provider());

			// JEP-33: Extended Stanza Addressing
			pm.addExtensionProvider("addresses",
					"http://jabber.org/protocol/address",
					new MultipleAddressesProvider());

			// FileTransfer
			pm.addIQProvider("si", "http://jabber.org/protocol/si",
					new StreamInitiationProvider());

			pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
					new BytestreamsProvider());

			// pm.addIQProvider("open","http://jabber.org/protocol/ibb", new
			// IBBProviders.Open());

			// pm.addIQProvider("close","http://jabber.org/protocol/ibb", new
			// IBBProviders.Close());

			// pm.addExtensionProvider("data","http://jabber.org/protocol/ibb",
			// new
			// IBBProviders.Data());

			// Privacy
			pm.addIQProvider("query", "jabber:iq:privacy",
					new PrivacyProvider());

			pm.addIQProvider("command", "http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider());
			pm.addExtensionProvider("malformed-action",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.MalformedActionError());
			pm.addExtensionProvider("bad-locale",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadLocaleError());
			pm.addExtensionProvider("bad-payload",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadPayloadError());
			pm.addExtensionProvider("bad-sessionid",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.BadSessionIDError());
			pm.addExtensionProvider("session-expired",
					"http://jabber.org/protocol/commands",
					new AdHocCommandDataProvider.SessionExpiredError());

			pm.addExtensionProvider(DeliveryReceipt.ELEMENT,
					DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
			pm.addExtensionProvider(DeliveryReceiptRequest.ELEMENT,
					new DeliveryReceiptRequest().getNamespace(),
					new DeliveryReceiptRequest.Provider());
			/*
			 * pm.addExtensionProvider(ReadReceipt.ELEMENT,
			 * ReadReceipt.NAMESPACE, new ReadReceipt.Provider());
			 */
		}

	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	private String getLastEnabled() {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		return pref.getString("LastEnabled", "");
	}

	private void setLastEnabled(String value) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("LastEnabled", value);
		editor.commit();
	}

	public void normal_Icon_Implement() {
		editor = mSharedPreferences.edit();
		editor.putInt(Constant.COUNT_NOTIFICATION, 0);
		editor.commit();

		/*
		 * int value; mSharedPreferences = getActivity().getSharedPreferences(
		 * Constant.IMGRNOTIFICATION_RECEIVE, Context.MODE_PRIVATE); value =
		 * mSharedPreferences.getInt(Constant.COUNT_NOTIFICATION, 0);
		 * 
		 * Log.e("DEMO", "Changing : " + value);
		 * 
		 * PackageManager pm = getActivity().getPackageManager();
		 * 
		 * String lastEnabled = getLastEnabled(); // Getting last enabled //
		 * from shared // preference
		 * 
		 * if (TextUtils.isEmpty(lastEnabled)) { lastEnabled =
		 * "com.imgr.chat.SplashActivity"; }
		 * 
		 * ComponentName componentName = new ComponentName("com.imgr.chat",
		 * lastEnabled); pm.setComponentEnabledSetting(componentName,
		 * PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
		 * PackageManager.GET_ACTIVITIES);
		 * 
		 * Log.i("DEMO", "Removing : " + lastEnabled);
		 * 
		 * if (value <= 0) { lastEnabled = "com.imgr.chat.SplashActivity"; }
		 * else if (value == 1) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 2) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 3) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 4) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 5) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 6) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 7) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 8) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 9) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value == 10) { lastEnabled = "com.imgr.chat.a" + value; }
		 * else if (value > 10) { lastEnabled = "com.imgr.chat.a10p"; } else {
		 * lastEnabled = "com.imgr.chat.SplashActivity"; }
		 * 
		 * componentName = new ComponentName("com.imgr.chat", lastEnabled);
		 * pm.setComponentEnabledSetting(componentName,
		 * PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
		 * PackageManager.GET_ACTIVITIES); Log.e("DEMO", "Adding : " +
		 * lastEnabled); setLastEnabled(lastEnabled);
		 */// Saving last enabled to shared
			// preference
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		Log.e("===mConnectionDetector.isConnectingToInternet()", ""
				+ mConnectionDetector.isConnectingToInternet());
		visibility = "visible";
		UILApplication.activityResumed();

		// mDatasourceHandler = new DatasourceHandler(getActivity());

		SharedPreferences newSharedPreferences = getActivity()
				.getSharedPreferences(Constant.IMGRNOTIFICATION_RECEIVE,
						Context.MODE_PRIVATE);
		editor = newSharedPreferences.edit();
		editor.putString("isTerminated", "no");
		editor.putString("isOnResume", "yes");
		editor.putString("currentPhoneNo", "");
		editor.putString("currentScreen", "list");

		editor.putString("Notification_Click_Message", "false");

		editor.commit();

		Handler handler = new Handler();

		final Runnable r = new Runnable() {
			public void run() {
				// tv.append("Hello World");
				refresh_screen();

				Handler handler = new Handler();

				handler.postDelayed(this, 3000);
			}
		};

		handler.postDelayed(r, 10);

		/*
		 * new Handler().postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { refresh_screen(); } }, 1000);
		 */

		NotificationManager notificationManager = (NotificationManager) getActivity()
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// notificationManager.cancelAll();
		// useHandler();
		if (Constant.CONNECTION_LOST == 1) {
			Constant.CONNECTION_LOST = 0;
			// connectivity_Manager =
			// (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
			activity = getActivity();
			config = new ConnectionConfiguration("64.235.48.26", 5222);
			config.setSASLAuthenticationEnabled(true);
			config.setCompressionEnabled(true);
			config.setSecurityMode(SecurityMode.enabled);
			SASLAuthentication.registerSASLMechanism("PLAIN",
					SASLPlainMechanism.class);
			connection.DEBUG_ENABLED = true;
			config.setSASLAuthenticationEnabled(true);
			connection = new XMPPConnection(config);
			mStringusername = sharedPreferences.getString(
					Constant.USERNAME_XMPP, "");
			mStringPassword = sharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");

			/*
			 * Connection_Pool connection_Pool = Connection_Pool.getInstance();
			 * connection = connection_Pool.getConnection();
			 * 
			 * setConnection(connection);
			 */

			// if (mConnectionDetector.isConnectingToInternet()) {
			// new login_Existing_User().execute();

			try {
				Connection_Pool connection_Pool = Connection_Pool.getInstance();
				connection = connection_Pool.getConnection(context);
				PingManager.getInstanceFor(connection);

				DeliveryReceiptManager delievery_Manager = DeliveryReceiptManager
						.getInstanceFor(connection);
				delievery_Manager.setAutoReceiptsEnabled(true);
				delievery_Manager.enableAutoReceipts();

				delievery_Manager
						.addReceiptReceivedListener(new ReceiptReceivedListener() {
							@Override
							public void onReceiptReceived(String fromJid,
									String toJid, String receiptId) {
								// Log.e("Chat_Screen message sent confirmation:",
								// fromJid
								// + ", " + toJid + ", " + receiptId);
								loop: for (String[] msg_Data : messages) {
									if (msg_Data[3].equals(receiptId)) {
										msg_Data[2] = "1";
										// Log.e("Pinging", "on Resumeflag:  "
										// + receiptId);
										boolean flag = mDatasourceHandler
												.UpdateReceive(receiptId);
										Log.e("Pinging", "on Resumeflag:  "
												+ flag);
										// update query implement--------------
										// Log.v("Delivery", "" + msg_Data[2]);
										mHandler.post(new Runnable() {
											public void run() {

											}
										});
										break loop;
									}

								}

							}

						});
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				Log.e("exceptionn", "exceptionn");
				e1.printStackTrace();
			}

			/*
			 * } else { Log.e("elseeee", "elseeee");
			 * LogMessage.showDialog(getActivity(), null,
			 * "No Internet Connection", null, "Ok");
			 * 
			 * }
			 */
		}

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		visibility = "invisible";
		UILApplication.activityPaused();

		SharedPreferences newSharedPreferences = getActivity()
				.getSharedPreferences(Constant.IMGRNOTIFICATION_RECEIVE,
						Context.MODE_PRIVATE);
		editor = newSharedPreferences.edit();
		editor.putString("isOnResume", "no");
		editor.putString("isTerminated", "no");
		editor.commit();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		isCallForChating = false;
		//myTimer.cancel();
		// connection.removePacketListener(packetListener);
		// SharedPreferences newSharedPreferences =
		// getActivity().getSharedPreferences(Constant.IMGRNOTIFICATION_RECEIVE,
		// Context.MODE_PRIVATE);
		// editor = newSharedPreferences.edit();
		// editor.putString("isTerminated","yes");
		// editor.commit();
	}

	public String date() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MMM dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	// custom class working===========
	public class MessageScreenAdapter extends BaseAdapter {
		Context ctx;
		LayoutInflater inflater;
		ArrayList<String> mArrayList_first = new ArrayList<String>();
		ArrayList<String> mArrayList_last = new ArrayList<String>();
		ArrayList<String> mArrayList_profice = new ArrayList<String>();
		ArrayList<String> mArrayList_phoneno = new ArrayList<String>();
		ArrayList<String> mArrayList_jid = new ArrayList<String>();
		ArrayList<String> mArrayList_message = new ArrayList<String>();
		ArrayList<String> mArrayList_time = new ArrayList<String>();
		ArrayList<String> mArrayList_uniqID = new ArrayList<String>();
		ArrayList<String> mArrayList_count = new ArrayList<String>();
		ArrayList<String> mArrayList_date = new ArrayList<String>();

		public MessageScreenAdapter(Context mContext,
				ArrayList<String> mArrayfirstname,
				ArrayList<String> mArraylastname,
				ArrayList<String> profilecimage, ArrayList<String> phoneno,
				ArrayList<String> Jid, ArrayList<String> message,
				ArrayList<String> time, ArrayList<String> uniqueid,
				ArrayList<String> count, ArrayList<String> date) {
			ctx = mContext;
			// mImageLoader = new ImageLoader(getActivity());

			this.mArrayList_first = mArrayfirstname;
			this.mArrayList_last = mArraylastname;
			this.mArrayList_profice = profilecimage;
			this.mArrayList_phoneno = phoneno;
			this.mArrayList_jid = Jid;
			this.mArrayList_message = message;
			this.mArrayList_time = time;
			this.mArrayList_uniqID = uniqueid;
			this.mArrayList_count = count;
			this.mArrayList_date = date;
			// Log.e("mArrayList_phoneno:", "" + mArrayList_phoneno);
			// Log.e("mArrayList_phoneno SIZE :", "" +
			// mArrayList_phoneno.size());

		}

		public int getCount() {
			int size;
			if (mArrayList_phoneno != null) {
				size = mArrayList_phoneno.size();
			} else {
				size = 0;
			}
			return size;
		}

		public Object getItem(int position) {

			return mArrayList_phoneno.get(position);
		}

		public long getItemId(int position) {

			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {

			isListUpdating = true;

			ViewHolder holder;
			View vi = convertView;
			if (inflater == null)
				inflater = (LayoutInflater) ctx
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				vi = inflater.inflate(R.layout.customlist, null);
				holder = new ViewHolder();
				holder.mFrameLayout = (FrameLayout) vi
						.findViewById(R.id.frameclick);
				holder.unread_layout = (FrameLayout) vi
						.findViewById(R.id.unread_layout);
				holder.unread_message = (TextView) vi
						.findViewById(R.id.unread_message);
				holder.text = (TextView) vi.findViewById(R.id.custo);
				holder.t1 = (TextView) vi.findViewById(R.id.desc);

				holder.imag = (SelectableRoundedImageView) vi
						.findViewById(R.id.video);
				holder.time = (TextView) vi.findViewById(R.id.time);
				vi.setTag(holder);
			} else {
				holder = (ViewHolder) vi.getTag();
			}

			holder.text.setId(position);
			holder.t1.setId(position);
			holder.imag.setId(position);
			holder.unread_message.setId(position);

			if (mArrayList_profice.get(position).equals("Null")) {
				holder.imag.setScaleType(ScaleType.CENTER_CROP);
				holder.imag.setOval(true);
				holder.imag.setBackgroundResource(R.drawable.ic_contact_place);
			} else {
				Bitmap mBitmap = ConvertToImage(mArrayList_profice
						.get(position));
				holder.imag.setScaleType(ScaleType.CENTER_CROP);
				holder.imag.setOval(true);
				holder.imag.setImageBitmap(mBitmap);
			}

			if (mArrayList_count.get(position).equals("0")) {

				holder.unread_layout.setVisibility(View.GONE);
			} else {

				holder.unread_layout.setVisibility(View.VISIBLE);
				holder.unread_message.setText(mArrayList_count.get(position));

			}

			if (mArrayList_first.size() == 0) {
				holder.text.setText(mArrayList_phoneno.get(position));
			} else {

				if (mArrayList_first.get(position).equals("")) {
					holder.text.setText(mArrayList_phoneno.get(position));
				} else if (mArrayList_first.get(position).matches("No Name")) {
					holder.text.setText(mArrayList_phoneno.get(position));
				} else if (mArrayList_first.get(position).matches("No")) {
					holder.text.setText(mArrayList_phoneno.get(position));
				}

				else {
					holder.text.setText(mArrayList_first.get(position));
				}
			}

			try {
				holder.t1.setText(mArrayList_message.get(position));
				String date_matched = date();
				if (date_matched.matches(mArrayList_date.get(position))) {
					holder.time.setText(mArrayList_time.get(position));
				} else {
					holder.time.setText(mArrayList_date.get(position)
							.split("/")[0]);
				}

				holder.mFrameLayout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						goToChatScreen(position);
					}
				});

				holder.imag.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						goToChatScreen(position);
					}
				});

				holder.t1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						goToChatScreen(position);
					}
				});

				holder.time.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						goToChatScreen(position);
					}
				});

				isListUpdating = false;

			} catch (Exception e) {

			}

			isListUpdating = false;

			return vi;

		}

		public void goToChatScreen(int position) {
			Log.e("New Member",""+NewMemberId);
			Friend_Prefernces(NewMemberId.get(position));

			Intent mIntent_info = new Intent(getActivity(), ChatScreen.class);

			if (mArrayList_first.get(position).equals("")
					|| mArrayList_first.get(position).matches("No Name")
					|| mArrayList_first.get(position).matches("No")) {

				mIntent_info.putExtra("Chat_name_select",
						mArrayList_phoneno.get(position));

			} else {
				mIntent_info.putExtra("Chat_name_select",
						mArrayList_first.get(position));
			}

			mIntent_info.putExtra("Chat_no_select",
					mArrayList_phoneno.get(position));
			
			Log.e("Profile Array","Pic"+mArrayListProficePic);
			Log.e("Profile Array","profile"+mArrayList_profice);
			Log.e("Profile Array","Count"+mArrayList_count);

			mIntent_info.putExtra("phone_id", mArrayList_uniqID.get(position));
			mIntent_info.putExtra("origin", "Messages");
			getActivity().startActivity(mIntent_info);
			getActivity().overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);

			/*
			 * if (mArrayList_first.get(position).matches("")) {
			 * 
			 * Intent mIntent_info = new Intent(getActivity(),
			 * NumberChatScreen.class);
			 * 
			 * mIntent_info.putExtra("Chat_name_select",
			 * mArrayList_phoneno.get(position));
			 * 
			 * mIntent_info .putExtra("Chat_no_select",
			 * mArrayList_phoneno.get(position) .split("_")[0]);
			 * mIntent_info.putExtra("phone_id",
			 * mArrayList_uniqID.get(position));
			 * 
			 * getActivity().startActivity(mIntent_info);
			 * getActivity().overridePendingTransition( R.anim.slide_in_left,
			 * R.anim.slide_out_right); //
			 * mIntent_info.putExtra("Chat_name_select", //
			 * mArrayList_phoneno.get(position)); } else if
			 * (mArrayList_first.get(position).matches( "No Name")) { Intent
			 * mIntent_info = new Intent(getActivity(), ChatScreen.class);
			 * 
			 * mIntent_info .putExtra("Chat_name_select",
			 * mArrayList_phoneno.get(position) .split("_")[0]);
			 * 
			 * mIntent_info .putExtra("Chat_no_select",
			 * mArrayList_phoneno.get(position) .split("_")[0]);
			 * mIntent_info.putExtra("phone_id",
			 * mArrayList_uniqID.get(position));
			 * 
			 * getActivity().startActivity(mIntent_info);
			 * getActivity().overridePendingTransition( R.anim.slide_in_left,
			 * R.anim.slide_out_right); } else if
			 * (mArrayList_first.get(position).matches("No")) { Intent
			 * mIntent_info = new Intent(getActivity(), ChatScreen.class);
			 * 
			 * mIntent_info .putExtra("Chat_name_select",
			 * mArrayList_phoneno.get(position) .split("_")[0]);
			 * 
			 * mIntent_info .putExtra("Chat_no_select",
			 * mArrayList_phoneno.get(position) .split("_")[0]);
			 * mIntent_info.putExtra("phone_id",
			 * mArrayList_uniqID.get(position));
			 * 
			 * getActivity().startActivity(mIntent_info);
			 * getActivity().overridePendingTransition( R.anim.slide_in_left,
			 * R.anim.slide_out_right); } else { Intent mIntent_info = new
			 * Intent(getActivity(), ChatScreen.class);
			 * 
			 * mIntent_info.putExtra("Chat_name_select",
			 * mArrayList_first.get(position));
			 * 
			 * mIntent_info .putExtra("Chat_no_select",
			 * mArrayList_phoneno.get(position) .split("_")[0]);
			 * mIntent_info.putExtra("phone_id",
			 * mArrayList_uniqID.get(position));
			 * 
			 * getActivity().startActivity(mIntent_info);
			 * getActivity().overridePendingTransition( R.anim.slide_in_left,
			 * R.anim.slide_out_right); //
			 * mIntent_info.putExtra("Chat_name_select", //
			 * mArrayList_phoneno.get(position)); }
			 */

		}

	}

	public class ViewHolder {
		public TextView text, t1, time, unread_message;
		public SelectableRoundedImageView imag;

		public FrameLayout mFrameLayout, unread_layout;

	}
	


	protected void refresh_screen() {

		if (!isListUpdating) {
			ChatCount = new ArrayList<String>();
			base64_array = new ArrayList<String>();
			mArrayListFirstName = new ArrayList<String>();
			mArrayListFirstName_new = new ArrayList<String>();
			mArrayListMessage = new ArrayList<String>();
			mArrayListTime = new ArrayList<String>();
			mArrayListDate = new ArrayList<String>();
			mArrayListJid = new ArrayList<String>();
			NewmArrayListTestMessage = new ArrayList<String>();
			mDatasourceHandler = new DatasourceHandler(getActivity());
			mDatabaseHelper = new DatabaseHelper(getActivity());
			mArrayListuniqueId = new ArrayList<String>();
			AllNEWmArrayListTestToJID = new ArrayList<String>();
			mArrayListMessage = new ArrayList<String>();
			mArrayListTime = new ArrayList<String>();
			NEWmArrayListTime = new ArrayList<String>();
			mArrayListJid = new ArrayList<String>();
			// mArrayListTOTEST = new ArrayList<String>();
			mArrayListuniqueId = new ArrayList<String>();

			mArrayListPhoneno = new ArrayList<String>();

			Imgrcontactnumber = new ArrayList<String>();
			NewImgrcontactnumber = new ArrayList<String>();
			MemberId = new ArrayList<String>();
			NewMemberId = new ArrayList<String>();

			Imgrcontactunquieid = new ArrayList<String>();
			// deviceId = Secure.getString(getActivity().getContentResolver(),
			// Secure.ANDROID_ID);

			mStringPassword = sharedPreferences.getString(
					Constant.PASSWORD_XMPP, "");

			try {
				mCursor = mDatasourceHandler.FETCH_RECENT(mStringPassword);

				Log.e(TAG, "Cursor Data - "+mCursor);
				if (mCursor.getCount() != 0) {
					mArrayListFirstName.clear();
					mArrayListMessage.clear();
					mArrayListTime.clear();
					mArrayListJid.clear();
					NewmArrayListTestMessage.clear();
					mArrayListFirstName_new.clear();
					mArrayListDate.clear();

					mArrayListPhoneno.clear();
					ChatCount.clear();
					NewMemberId.clear();
					mArrayListuniqueId.clear();
					base64_array.clear();

					do {
Log.e("Columnssss",""+mCursor.getColumnNames());
						if (mCursor.getString(2).trim().length() > 5
								&& mCursor.getString(3).trim().length() > 5) {
							mArrayListDate.add(mCursor.getString(0).trim());

							mArrayListFirstName
									.add(mCursor.getString(1).trim());

							// Log.e("jiddddddddd",mCursor.getString(9).trim());

							if (!mCursor.getString(2).trim().split("_")[0]
									.equalsIgnoreCase(mStringPassword)) {
								mArrayListJid.add(mCursor.getString(2).trim());
							} else {
								mArrayListJid.add(mCursor.getString(3).trim());
							}

							// mArrayListTOTEST.add(mCursor.getString(3).trim());

							if (mCursor.getString(5).trim().length() > 0) {
								mArrayListMessage.add(mCursor.getString(5)
										.trim());
								// mArrayListMessage.add("feger");
							} else {
								mArrayListMessage.add("");
							}

							mArrayListTime.add(mCursor.getString(6).trim());
							
							Log.e(TAG, "Chat Count - "+mCursor.getString(8).trim());
							ChatCount.add(mCursor.getString(8).trim());
						}
						Log.e("MyChatCount",""+ChatCount);
					} while (mCursor.moveToNext());

					mCursor.close();

					// Message decode section open=======

					// Log.e("sfsfadsffffffffffffffffff22222",mArrayListJid.get(0).split("_")[0]);

					Log.e("sfsfadsffffffffffffffffff11111",
							"" + mArrayListJid.size());

					for (int j = 0; j < mArrayListJid.size(); j++) {

						mArrayListPhoneno
								.add(mArrayListJid.get(j).split("_")[0]);

						String textmessage = "";

						if (mArrayListMessage.get(j).length() > 0) {
							JSONObject jsonObj = new JSONObject(
									mArrayListMessage.get(j));

							// JSONArray mJsonArray = null;

							JSONArray bodyArray = jsonObj.getJSONArray("body");

							JSONObject bodyJsonObj = new JSONObject(
									bodyArray.getString(0));

							if (bodyJsonObj.has("_message_body_tag")) {

								textmessage = bodyJsonObj
										.getString("_message_body_tag");

							} else {
								textmessage = "image";
							}

							NewmArrayListTestMessage.add(textmessage);
						}

						Cursor mCursor = mDatasourceHandler
								.FETCH_(mArrayListJid.get(j).split("_")[0]);
						if (mCursor.getCount() != 0) {

							if (mCursor.moveToFirst()) {
								mArrayListFirstName_new.add(mCursor
										.getString(1).trim());
								NewMemberId.add(mCursor.getString(5));
								base64_array.add(mCursor.getString(7).trim());
								mArrayListuniqueId.add(mCursor.getString(8)
										.trim());

							}

							mCursor.close();

						} else {
							NewMemberId.add("0");
							mArrayListuniqueId.add("0");
							base64_array.add("Null");
							mArrayListFirstName_new.add(mArrayListJid.get(j)
									.split("_")[0]);

							mCursor.close();
						}

					}
				}

				if (mArrayListJid.size() == 0) {

					list_set = false;
					mPullRefreshListView.setVisibility(View.GONE);
					// mSwipeLayout_message.setVisibility(View.GONE);
					mFrameLayout_no_message.setVisibility(View.VISIBLE);

				} else {
					list_set = true;
					mFrameLayout_no_message.setVisibility(View.GONE);
					// mSwipeLayout_message.setVisibility(View.VISIBLE);
					mPullRefreshListView.setVisibility(View.VISIBLE);

					Log.e("ChatCount",""+ChatCount);
					
					// Message decode section closed
					message_Adapter = new MessageScreenAdapter(getActivity(),
							mArrayListFirstName_new, mArrayListFirstName_new,
							base64_array, mArrayListPhoneno, mArrayListJid,
							NewmArrayListTestMessage, mArrayListTime,
							mArrayListuniqueId, ChatCount, mArrayListDate);
					mPullRefreshListView.setAdapter(message_Adapter);
					
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
					Log.e("mArrayListFirstName_new",""+mArrayListFirstName_new.size());
				}

			} catch (Exception e) {
				// Log.e("MESSAGE CRASH:", "" + e.getMessage());
				// Log.e("crashdescription", "" + e.getLocalizedMessage());
				// Log.e("crashstack", "" + e.getStackTrace().toString());
			}
		}
	}

	private int dp2px(int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				getResources().getDisplayMetrics());
	}

	protected void delete_popup() {

		final View v = LayoutInflater.from(getActivity()).inflate(
				R.layout.delete_pop_up_message, null);

		final Button mButtonPop = (Button) v.findViewById(R.id.change_dialog);
		final Button mButtonyes = (Button) v.findViewById(R.id.yes_dialog);

		pinDialog = new AlertDialog.Builder(getActivity()).setView(v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						boolean flag1 = mDatasourceHandler.deleteMesage(
								mStringPassword + "_" + deviceId,
								mArrayListJid.get(pos));
						boolean flag = mDatasourceHandler.deleteHistoryMesage(
								mStringPassword + "_" + deviceId,
								mArrayListJid.get(pos));

						mArrayListFirstName_new.remove(pos);
						mArrayListJid.remove(pos);
						NewmArrayListTestMessage.remove(pos);
						mArrayListTime.remove(pos);
						mArrayListuniqueId.remove(pos);

						message_Adapter.notifyDataSetChanged();
						if (mArrayListJid.size() == 0) {
							list_set = false;
							mPullRefreshListView.setVisibility(View.GONE);
							// mSwipeLayout_message.setVisibility(View.GONE);
							mFrameLayout_no_message.setVisibility(View.VISIBLE);
						} else {
							list_set = true;
						}

						pinDialog.cancel();

					}
				});
			}
		});

		pinDialog.show();

	}

	public void useHandler() {
		mHandler = new Handler();
		//mHandler.postDelayed(mRunnable, 30000);
	}

	private Runnable mRunnable = new Runnable() {

		@Override
		public void run() {

			/** Do something **/

			//mHandler.postDelayed(mRunnable, 30000);
		}
	};

	public void Friend_Prefernces(String mString) {
		editor = sharedPreferences.edit();

		editor.putString(Constant.FRIEND_ID_PROMO, mString);

		editor.commit();

	}

	public boolean visibleorinvisible() {

		if (visibility.equals("visible")) {
			flag = true;
		} else if (!UILApplication.activityVisible) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;

	}

	public void makeConnection() {

		/*
		 * config = new ConnectionConfiguration("64.235.48.26", 5222);
		 * connection = new XMPPConnection(config);
		 * config.setSASLAuthenticationEnabled(true);
		 * config.setCompressionEnabled(true);
		 * config.setReconnectionAllowed(true);
		 * SASLAuthentication.registerSASLMechanism("PLAIN",
		 * SASLPlainMechanism.class);
		 * config.setSecurityMode(SecurityMode.enabled);
		 * configure(ProviderManager.getInstance());
		 * config.setDebuggerEnabled(true); connection.DEBUG_ENABLED = true;
		 * config.setReconnectionAllowed(true);
		 * 
		 * Connection_Pool connection_Pool = Connection_Pool.getInstance();
		 * connection = connection_Pool.getConnection();
		 * setConnection(connection); card = new VCard();
		 * Connection.DEBUG_ENABLED = true;
		 */
		// new login_Existing_User().execute();

	}

	public static int retrieveState_mode(Mode userMode, boolean isOnline) {
		int userState = 0;
		/** 0 for offline, 1 for online, 2 for away,3 for busy */
		if (userMode == Mode.dnd) {
			userState = 3;
		} else if (userMode == Mode.away || userMode == Mode.xa) {
			userState = 2;
		} else if (isOnline) {
			userState = 1;
		}
		return userState;
	}

	public void setConnection(final XMPPConnection connection) {

		this.connection = connection;
		try {
			Log.e("tag", "connectionconnection :" + connection.isConnected());
			if (connection != null) {

				

				// Add a packet listener to get messages sent to us
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {

					public void processPacket(Packet packet) {

						/*Message message1 = (Message) packet;
						Log.e("tag",
								"message packet offline 111:"
										+ message1.getBody());

						SharedPreferences newSharedPreferences = getActivity()
								.getSharedPreferences(
										Constant.IMGRNOTIFICATION_RECEIVE,
										Context.MODE_PRIVATE);

						String currentPhoneNo = newSharedPreferences.getString(
								"currentPhoneNo", "");
						String currentScreen = newSharedPreferences.getString(
								"currentScreen", "list");

						if (currentScreen.equalsIgnoreCase("list")) {
							if (currentPhoneNo.equalsIgnoreCase("")) {

								message = (Message) packet;

								String message_send_local_notify = null;
								// String promoID = null;
								String mStringnewtojid = null;
								String mStringnewfromjid = null;
								JSONObject objMainList = null;
								String check = null;
								String check_update = null;
								String time = currenttime();
								String date_sent = date();
								String number = "";

								String toJID = "";
								String fromJID = "";

								String message_id = "";

								// Log.e("message.getPacketID(): ", "" +
								// message.getPacketID());

								if (message.getBody() != null) {

									Log.e("message.getBody()1111: ", ""
											+ message.getBody());

									String fromName = StringUtils
											.parseBareAddress(message.getFrom()
													.split("\\@")[0]);

									// mStringnewtojid = mStringPassword + "_"+
									// deviceId;

									mStringnewtojid = mStringPassword + "_"
											+ deviceId;
									mStringnewfromjid = fromName;

									number = mStringnewfromjid.split("_")[0];

									String incomingNumber = "";

									if (mStringnewtojid.split("_")[0]
											.equalsIgnoreCase(mStringPassword)) {
										incomingNumber = mStringnewfromjid
												.split("_")[0];
									} else {
										incomingNumber = mStringnewtojid
												.split("_")[0];
									}

									String isBlocked = "false";

									Cursor mBlockCursor = mDatasourceHandler
											.FETCH_(incomingNumber);

									if (mBlockCursor.getCount() != 0) {

										if (mBlockCursor.moveToFirst()) {
											isBlocked = mBlockCursor
													.getString(9);
										}
									}

									Log.e("blockedddddddddddddd", isBlocked);

									if (!isBlocked.equalsIgnoreCase("true")) {

										try {
											JSONObject bodyJsonObj = new JSONObject(
													message.getBody());

											if (bodyJsonObj.has("message_id")) {
												message_id = bodyJsonObj
														.getString("message_id");
											} else {
												message_id = message
														.getPacketID();
											}

											String messageDataToBeSaved = "";

											String promo_ID = "0";

											if (bodyJsonObj
													.has("_message_body_tag")) {

												// String promo_ID;
												if (bodyJsonObj
														.has("_promo_id_tag")) {
													promo_ID = bodyJsonObj
															.getString("_promo_id_tag");

												} else {
													promo_ID = "0";
												}

												String message = bodyJsonObj
														.getString("_message_body_tag");

												messageDataToBeSaved = "{\"body\":[{\"preview_Icon\":\"null\",\"_promo_id_tag\":\""
														+ promo_ID
														+ "\",\"pushkey\":\""
														+ message
														+ "\","
														+ "\"_message_body_tag\":\""
														+ message + "\"}]}";
											} else {
												JSONObject json = new JSONObject();
												String mStringBase64 = bodyJsonObj
														.getString("preview_Icon");

												try {
													json.put("server_URL",
															"Not Uploaded");
													json.put("pushkey", "image");
													json.put("preview_Icon",
															mStringBase64);

													String messageData = json
															.toString();
													messageDataToBeSaved = "{\"body\":["
															+ messageData
															+ "]}";

												} catch (JSONException e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
											}

											mDatasourceHandler
													.insertChathistory(
															message_id,
															"username",
															mStringnewtojid,
															mStringnewfromjid,
															messageDataToBeSaved,
															time,
															"0",
															"1",
															date_sent,
															UILApplication.context,
															incomingNumber,
															"no");
											String timestamp = timestamp();

											int countincrease = 0;

											Log.e("messageeeeeeeeeeeeeeee",
													"countttttttttttttttttttttttt");
											countincrease = mDatasourceHandler
													.UpdateRecentReceiveCount(
															mStringnewfromjid,
															mStringnewtojid,
															messageDataToBeSaved,
															time, date_sent,
															timestamp);

											countincrease = countincrease + 1;

											String mString = "" + countincrease;

											mDatasourceHandler
													.insertrecentMessage(
															messageDataToBeSaved,
															mStringnewfromjid,
															mStringnewtojid,
															"", "", date_sent,
															time, timestamp,
															mString, "no",
															mStringPassword);

											if (!promo_ID.equalsIgnoreCase("0")) {
												Log.e("received promo idddddddddddddddd",
														"" + promo_ID);
												promoID = promo_ID;
												// new
												// Execute_New_Promo().execute();
												new ExecuteChatNotification1()
														.execute();
											}


											// fetchChatHistory();

										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}
								}
							}
						}*/
					}

				}, filter);
			}

		} catch (Exception e) {

		}
	}

	/**
	 * Execute NewPromo Asynctask
	 */

	class ExecuteChatNotification1 extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {

			String url;

			String id = promoID;
			Log.e("received promo idddddddddddddddd", "" + id);
			url = JsonParserConnector.getPERSONALSponsored(id);
			Log.e("received promo iddddddddddddd111111", "" + url);
			return url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// UI.showProgressDialog(ChatScreen.this);
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Personal_List(result);
		}

	}

	private void Personal_List(String response) {
		try {
			String message_send_local_notify = null;
			String mStringspromoid = null, mStringsusername = null, mStringspromo_name = null, mStringspromo_image = null, mStringsmodified_date = null, mString_link_text = null, mString_link_promo = null, mString_header_promo_message = null;
			// Log.e("strJson: ", "" + strJson);
			JSONObject jsonOBject = new JSONObject(response);

			if (jsonOBject.getBoolean("success")) {

				Log.e("new promoooooooo", response);

				JSONArray bodyJsonObj = jsonOBject.getJSONArray("data");

				JSONObject sys = new JSONObject(bodyJsonObj.getString(0));

				mStringspromoid = sys.getString("promo_id");
				// mStringsusername = sys.getString("username");

				mStringspromo_name = sys.getString("promo_name");
				mStringspromo_image = sys.getString("promo_image");
				// mStringsmodified_date = sys.getString("modified_date");
				mString_link_text = sys.getString("promo_link_text");
				mString_link_promo = sys.getString("promo_link");
				mString_header_promo_message = sys
						.getString("promo_message_header");

				Log.e("promo_data", "promo_id: " + mStringspromoid
						+ ", promo_type: NO_Define, promo_name: "
						+ mStringspromo_name + ", promo_image: "
						+ "https://imgrapp.com/release/" + mStringspromo_image
						+ ", promo_link: " + mString_link_promo
						+ ", promo_link_text: " + mString_link_promo
						+ ", promo_message_header: "
						+ mString_header_promo_message
						+ ", is_deleted: 0, modified_date: "
						+ mStringsmodified_date
						+ ", isActive: 1, is_enabled: 1");

				boolean flag = mDatasourceHandler.insertSponsored(
						mStringspromoid, "NO_Define", mStringspromo_name,
						"https://imgrapp.com/release/" + mStringspromo_image,
						mString_link_promo, mString_link_text,
						mString_header_promo_message, "0",
						mStringsmodified_date, "1", "1");

			} else {

			}

		} catch (JSONException e) {

			// Log.e("", e.getMessage());
		}

	}

	public String countcheck(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

	private String currenttime() {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		try {

			SimpleDateFormat s = new SimpleDateFormat("hh:mm a");
			String format = s.format(new Date());

			// s.setTimeZone(TimeZone.getTimeZone("GMT-4")); // give a timezone
			// reference for
			// formating (see
			// comment at the
			// bottom

			// /Log.e("format: ", "" + format);
			return format;
		} catch (Exception ex) {
			return null;
		}
	}

	public String date_refresh() {
		Calendar c = Calendar.getInstance();
		System.out.println("Current time => " + c.getTime());

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	public String timestamp() {
		Long tsLong = System.currentTimeMillis() / 1000;
		String ts = tsLong.toString();
		return ts;
	}

	public String count_check_update(String id) {
		String promoimage = null;
		try {

			mCursor = mDatasourceHandler.FETCH_PROMO_DATA_Update(id);

		} catch (Exception e) {

			e.printStackTrace();
		}

		if (mCursor.getCount() != 0) {
			promoimage = "" + mCursor.getCount();
			// promo_name = mCursor.getString(5).trim();
		} else {
			promoimage = "" + mCursor.getCount();
			// Log.e("", "Nothing data show");
		}
		return promoimage;
	}

}
