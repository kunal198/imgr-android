package com.imgr.chat.util;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.provider.Contacts.People;
import android.provider.ContactsContract.CommonDataKinds.Phone;

public class LogMessage {
	static AlertDialog dialog;

	public static void showDialog(Context context, String title,
			String Message, String text, String text_) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog
				.setTitle(title)
				.setMessage(Message)
				.setCancelable(false)
				.setPositiveButton(text, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				})
				.setNegativeButton(text_,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();

							}
						});

		dialog = alertDialog.create();
		dialog.show();

	}

	public static void dismiss() {

		dialog.dismiss();

	}
	
	
	

}
