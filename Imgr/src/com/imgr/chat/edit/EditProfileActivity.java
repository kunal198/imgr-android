package com.imgr.chat.edit;

/**
 * author: amit agnihotri
 */
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.imgr.chat.AppBaseActivity;
import com.imgr.chat.BaseConvert;
import com.imgr.chat.R;
import com.imgr.chat.connector.ConnectionDetector;
import com.imgr.chat.connector.JsonParserConnector;
import com.imgr.chat.constant.Constant;
import com.imgr.chat.database.DatabaseHelper;
import com.imgr.chat.database.DatasourceHandler;
import com.imgr.chat.register.CompleteProfileActivity;
import com.imgr.chat.register.RegisterActivity;
import com.imgr.chat.ui.TermsConditionActivity;
import com.imgr.chat.util.LogMessage;
import com.imgr.chat.util.UI;
import com.joooonho.SelectableRoundedImageView;

public class EditProfileActivity extends AppBaseActivity implements
		OnClickListener {
	Button mButton_cancel, mButton_done, mButtonDelete;

	// database work
	DatabaseHelper mDatabaseHelper;
	DatasourceHandler mDatasourceHandler;
	Cursor mCursor;
	String mStringFirstname, mStringLastname, mStringPhone, mStringEmail,
			mStringCode, mStringBase64, s_phone, deviceid, s_code;

	EditText mEditTextFirstname, mEditTextLastname, mEditTextemail,
			mEditTextPhone;
	String final_path = null;

	SelectableRoundedImageView mImageViewProfile;

	AlertDialog pinDialog;

	SharedPreferences sharedPreferences;
	Editor editor;
	boolean test = false;

	private static final int GALLERY_IMAGE_REQUEST_CODE = 99;
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_GALLERY = 2;

	private static final String IMAGE_DIRECTORY_NAME = "Imgr";
	private Uri fileUri;
	private File mediaStorageDir;
	private File mediaFile;

	Bitmap targetBitmap;

	ConnectionDetector mConnectionDetector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actvity_editprofile);
		registerBaseActivityReceiver();
		mDatabaseHelper = new DatabaseHelper(this);
		mDatasourceHandler = new DatasourceHandler(this);
		mEditTextFirstname = (EditText) findViewById(R.id.text_first_name);
		mEditTextLastname = (EditText) findViewById(R.id.text_last_name);
		mEditTextemail = (EditText) findViewById(R.id.text_Email);
		mEditTextPhone = (EditText) findViewById(R.id.text_phone_no);
		mButton_cancel = (Button) findViewById(R.id.btn_cancel);
		mButton_done = (Button) findViewById(R.id.btn_done);
		mButtonDelete = (Button) findViewById(R.id.delete_account);
		mImageViewProfile = (SelectableRoundedImageView) findViewById(R.id.image_profile);
		mButton_cancel.setOnClickListener(this);
		mImageViewProfile.setOnClickListener(this);
		mButton_done.setOnClickListener(this);
		mButtonDelete.setOnClickListener(this);
		sharedPreferences = getSharedPreferences(Constant.IMGRCHAT,
				Context.MODE_PRIVATE);
		deviceid = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);
		// Log.e("deviceid:", "" + deviceid);
		s_code = sharedPreferences.getString(Constant.COUNTRYCODE, "");
		mConnectionDetector = new ConnectionDetector(this);

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterBaseActivityReceiver();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!test) {
			try {
				mCursor = mDatasourceHandler.ProfileInfo();
				// Log.e("value in cursor at starting", mCursor.getCount() +
				// "");
			} catch (Exception e) {

				e.printStackTrace();
			}

			if (mCursor.getCount() != 0) {

				do {
					mStringFirstname = mCursor.getString(0).trim();
					mStringLastname = mCursor.getString(1).trim();
					mStringPhone = mCursor.getString(2).trim();
					mStringEmail = mCursor.getString(3).trim();
					mStringBase64 = mCursor.getString(4).trim();
					mStringCode = mCursor.getString(5).trim();

				} while (mCursor.moveToNext());

				mCursor.close();
			}
			mEditTextFirstname.setText(mStringFirstname);
			mEditTextLastname.setText(mStringLastname);
			mEditTextemail.setText(mStringEmail);
			mEditTextPhone.setText(mStringPhone);
			Bitmap mBitmap;
			if (mStringBase64.equals("")) {
				mImageViewProfile
						.setBackgroundResource(R.drawable.ic_contact_place);
			} else if (mStringBase64 != null) {
				mBitmap = ConvertToImage(mStringBase64);
				mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
				mImageViewProfile.setOval(true);
				mImageViewProfile.setImageBitmap(mBitmap);
			} else {
				mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
				mImageViewProfile.setOval(true);
				mImageViewProfile
						.setBackgroundResource(R.drawable.ic_contact_place);
			}

			test = true;
		} else {
			// Log.e("", "NOTHING");
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_cancel:
			finish();
			overridePendingTransition(R.anim.slide_out_left,
					R.anim.slide_in_right);
			break;
		case R.id.btn_done:
			if (mConnectionDetector.isConnectingToInternet()) {
				new CompleteProfile().execute();
			} else {
				LogMessage.showDialog(EditProfileActivity.this, null,
						"No Internet Connection", null, "Ok");
			}

			break;
		case R.id.delete_account:

			showInputDialog();

			break;

		case R.id.image_profile:

			selectImage();

			break;

		default:
			break;
		}
	}

	protected void selectImage() {

		final CharSequence[] options = { "Take Photo", "Choose from Gallery",
				"Cancel" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				EditProfileActivity.this);
		builder.setTitle("Add Photo!");
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals("Take Photo")) {
					captureImage();

				} else if (options[item].equals("Choose from Gallery")) {
					chooseFromGallery();

				} else if (options[item].equals("Cancel")) {
					dialog.dismiss();
				}
			}
		});
		builder.show();
	}

	protected void chooseFromGallery() {

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_GALLERY);
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.name());
		startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(EditProfileActivity.this,
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(EditProfileActivity.this,
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
			break;
		case GALLERY_IMAGE_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// video successfully recorded
				// preview the recorded video

				previewGalleryImage(data);

			} else if (resultCode == Activity.RESULT_CANCELED) {
				// user cancelled recording
				Toast.makeText(EditProfileActivity.this,
						"User cancelled Gallery selection", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to record video
				Toast.makeText(EditProfileActivity.this,
						"Sorry! Process Failed", Toast.LENGTH_SHORT).show();
			}
			break;
		}

	}

	public String convert_image(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		// Log.e("STREAM", "" + stream);
		// Log.e("BITMAP: ", "" + bitmap);
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // compress to
		// // which
		// // format
		// // you want.

		byte[] byte_arr = stream.toByteArray();
		// long lengthbmp = byte_arr.length;
		// Log.e("lengthbmp---------", ""+lengthbmp);

		String ba1 = BaseConvert.encodeBytes(byte_arr);
		return ba1;
	}

	private void previewCapturedImage() {

		BitmapFactory.Options options = new BitmapFactory.Options();

		options.inJustDecodeBounds = true;

		// BitmapFactory.decodeFile(fileUri.getPath(), options);

		options.inSampleSize = calculateInSampleSize(options, 100, 100);

		options.inJustDecodeBounds = false;
		final_path = fileUri.getPath();

		Bitmap bitMap = BitmapFactory.decodeFile(fileUri.getPath(), options);

		// targetBitmap = getRoundedCornerBitmap(bitMap);
		if (mStringBase64 != null) {
			mStringBase64 = "";
		}
		Bitmap b = getBitmapRotation(bitMap);
		mStringBase64 = convert_image(b);

		mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
		mImageViewProfile.setOval(true);
		mImageViewProfile.setImageBitmap(b);
		deleteRecursive(mediaStorageDir);

	}

	private int getExifOrientation() {
		ExifInterface exif;
		int orientation = 0;
		try {
			exif = new ExifInterface(final_path);
			orientation = exif
					.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.d("", "got orientation " + orientation);
		return orientation;
	}

	private Bitmap getBitmapRotation(Bitmap bitmap) {
		Matrix matrix = new Matrix();

		int rotation = 0;
		switch (getExifOrientation()) {
		case ExifInterface.ORIENTATION_ROTATE_180:
			rotation = 180;
			matrix.postRotate(rotation);

			break;
		case ExifInterface.ORIENTATION_ROTATE_90:
			rotation = 90;
			matrix.postRotate(rotation);

			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			rotation = 270;
			matrix.postRotate(rotation);

			break;
		}

		return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);

	}

	public Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		int targetWidth = 100;
		int targetHeight = 100;
		targetBitmap = Bitmap.createBitmap(targetWidth,

		targetHeight, Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = bitmap;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
				sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
				targetHeight), null);

		return targetBitmap;
	}

	private void previewGalleryImage(Intent data) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		if (EditProfileActivity.this.getContentResolver() != null) {
			Cursor cursor = EditProfileActivity.this.getContentResolver()
					.query(selectedImage, filePathColumn, null, null, null);

			if (cursor != null) {
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();

				BitmapFactory.decodeFile(filePath, options);

				options.inSampleSize = calculateInSampleSize(options, 250, 250);
				final_path = filePath;

				// Decode bitmap with inSampleSize set
				options.inJustDecodeBounds = false;

				Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

				// targetBitmap = getRoundedCornerBitmap(bitmap);
				if (mStringBase64 != null) {
					// Log.e("", "inside call");
					mStringBase64 = "";
				}
				Bitmap b = getBitmapRotation(bitmap);
				mStringBase64 = convert_image(b);
				mImageViewProfile.setScaleType(ScaleType.CENTER_CROP);
				mImageViewProfile.setOval(true);

				mImageViewProfile.setImageBitmap(b);

				// deleteRecursive(mediaStorageDir);
			} else {
				Toast.makeText(EditProfileActivity.this,
						"Unable to locate image .. Please try again",
						Toast.LENGTH_LONG).show();
			}

		} else {
			Toast.makeText(EditProfileActivity.this,
					"Unable to locate image .. Please try again",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				deleteRecursive(child);
			}
		}

		boolean status = fileOrDirectory.delete();
	}

	protected void captureImage() {

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

	}

	/**
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * returning image / video
	 */
	private File getOutputMediaFile(int type) {

		// External sdcard location
		mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
				// + IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_" + timeStamp + ".jpg");
		} else if (type == MEDIA_TYPE_GALLERY) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "CHAT_IMG_GAL_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 6;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		// Log.d("Insample size", inSampleSize + "");
		return inSampleSize;
	}

	protected void showInputDialog() {
		// final String currentClass = getClass().getSimpleName();
		final View v = LayoutInflater.from(this).inflate(
				R.layout.delete_pop_up, null);

		final Button mButtonPop = (Button) v
				.findViewById(R.id.no_dialog_delete);
		final Button mButtonyes = (Button) v
				.findViewById(R.id.yes_dialog_delete);

		pinDialog = new AlertDialog.Builder(EditProfileActivity.this)
				.setView(v).create();
		pinDialog.setCancelable(false);

		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				// Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				mButtonPop.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {

						pinDialog.cancel();

					}
				});

				mButtonyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						mDatasourceHandler.DeleteAll();
						clearPrefernces();
						pinDialog.cancel();
						closeAllActivities();
						// finish();
						Intent mIntent = new Intent(EditProfileActivity.this,
								TermsConditionActivity.class);
						mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP
								| Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(mIntent);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_right);

					}
				});
			}
		});

		pinDialog.show();

	}

	public void clearPrefernces() {
		// TODO Auto-generated method stub
		editor = sharedPreferences.edit();

		editor.remove(Constant.classdefine);
		editor.remove(Constant.PHONENO);
		editor.remove(Constant.USERNAME_XMPP);
		editor.remove(Constant.PASSWORD_XMPP);

		// editor.remove(Constant.USERNAME_XMPP);
		// editor.remove(Constant.USERNAME_XMPP);
		// editor.remove(Constant.USERNAME_XMPP);
		// editor.remove(Constant.USERNAME_XMPP);
		// editor.remove(Constant.USERNAME_XMPP);
		editor.clear();
		editor.commit();
	}

	public Bitmap ConvertToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	public Bitmap ConvertTESTToImage(String image) {
		try {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);

			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);

			// Log.e("", "CALL");
			return bitmap;
		} catch (Exception e) {
			return null;
		}
	}

	public class CompleteProfile extends AsyncTask<String, Void, String> {
		public CompleteProfile() {
		}

		@Override
		protected String doInBackground(String... params) {
			s_phone = mEditTextPhone.getText().toString();
			s_phone = s_phone.replace(" ", "").replace("(", "")
					.replace(")", "").replace("-", "");
			String result = JsonParserConnector.PostPersonalInfo("imgr", "1.0",
					s_code, "2", s_phone, "android", deviceid, mEditTextemail
							.getText().toString(), mEditTextFirstname.getText()
							.toString(),
					mEditTextLastname.getText().toString(), mStringBase64, "");
			return result;
		}

		@Override
		protected void onCancelled() {

			super.onCancelled();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			UI.hideProgressDialog();
			if (result.equals("true")) {
				mDatasourceHandler.UpdateProfileInfo(s_phone,
						mEditTextFirstname.getText().toString(),
						mEditTextLastname.getText().toString(), mEditTextemail
								.getText().toString(), mStringBase64);
				finish();
				overridePendingTransition(R.anim.slide_out_left,
						R.anim.slide_in_right);

			} else {

			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			UI.showProgressDialog(EditProfileActivity.this);
		}

	}
}
