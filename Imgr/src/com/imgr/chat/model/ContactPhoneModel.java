package com.imgr.chat.model;

public class ContactPhoneModel {

	private String name;
	private String number;
	private String isimgr;
	private String last;
	private String phoneid;
	private String fullname;

	public ContactPhoneModel(String name, String number, String isimgr,
			String last, String phoneid, String fullname) {
		this.name = name;
		this.number = number;
		this.isimgr = isimgr;
		this.last = last;
		this.phoneid = phoneid;
		this.fullname = fullname;

	}

	public String getLast() {
		return this.last;
	}

	public String getPhone() {
		return this.phoneid;
	}

	public String getIsimgr() {
		return this.isimgr;
	}

	public String getName() {
		return this.name;
	}

	public String getNumber() {
		return this.number;
	}

	public String toLowerCase() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFullName() {
		// TODO Auto-generated method stub
		return this.fullname;
	}

}
