package com.imgr.chat.model;

public class IMGR_Model {
	private String name;
	private String last;
	private String number;
	private String base64;
	private String phoneid;
	private String fullname;
	private String memberId;

	public String getBase64() {
		return base64;
	}

	public IMGR_Model(String name, String last, String number, String base64,
			String phoneid, String fullname, String memberId) {
		this.name = name;
		this.number = number;
		this.last = last;
		this.base64 = base64;
		this.phoneid = phoneid;
		this.fullname = fullname;
		this.memberId = memberId;

	}

	public String getLast() {
		return this.last;
	}

	public String getMemberId() {
		return this.memberId;
	}

	public String getPhone() {
		return this.phoneid;
	}

	public String getName() {
		return this.name;
	}

	public String getNumber() {
		return this.number;
	}

	public String toLowerCase() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getFullName() {
		// TODO Auto-generated method stub
		return this.fullname;
	}

}
