package com.imgr.chat.model;

public class SingleModelAdd {
	private String IsImgrUser;
	private String phonenumber;
	private String memberId;
	private String jid;
	private String base64;

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public SingleModelAdd() {
		super();

	}

	public String getIsImgrUser() {
		return IsImgrUser;
	}

	public void setIsImgrUser(String isImgrUser) {
		IsImgrUser = isImgrUser;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getJid() {
		return jid;
	}

	public void setJid(String jid) {
		this.jid = jid;
	}
}
