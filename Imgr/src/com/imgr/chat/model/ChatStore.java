package com.imgr.chat.model;

public class ChatStore {
	private String To_message;
	private String From_Message;
	private String id;

	public ChatStore() {
		super();

	}

	public String getTo_message() {
		return To_message;
	}

	public void setTo_message(String to_message) {
		To_message = to_message;
	}

	public String getFrom_Message() {
		return From_Message;
	}

	public void setFrom_Message(String from_Message) {
		From_Message = from_Message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
