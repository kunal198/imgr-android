package com.imgr.chat.model;

public class ModelPersonalSponsoredSelect {
	private String promoid;
	private String promo_name;
	private String isdeleted;
	private String isActive;
	private String isenabled;

	public boolean box;

	public ModelPersonalSponsoredSelect(String promoid, String promo_name,
			String isenabled, String isdeleted, String isActive, boolean _box) {
		this.promo_name = promo_name;
		this.promoid = promoid;
		this.isenabled = isenabled;
		this.isdeleted = isdeleted;
		this.isActive = isActive;

		box = _box;
	}

	public String toLowerCase() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPromoName() {
		return this.promo_name;
	}

	public String getisdeleted() {
		return this.isdeleted;
	}

	public String getisActive() {
		return this.isActive;
	}

	public String getisenabled() {
		return this.isenabled;
	}

	public String getPromoId() {
		return this.promoid;
	}
}
