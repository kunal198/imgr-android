package com.service;

import org.jivesoftware.smack.packet.Packet;

public abstract interface ImgrPacketListener {
	public abstract void OnPacketReceived(Packet paramPacket);
}
