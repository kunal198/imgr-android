package com.service;

import java.io.IOException;

import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.imgr.chat.xmpp.Connection_Pool;

@SuppressLint("HandlerLeak")
public class ImgrAppService extends Service {
	public static final String ACTION_LOGGED_IN = "Imgrapp.loggedin";
	private final LocalBinder binder = new LocalBinder();
	// DatabaseHelper db;
	Connection_Pool xmppConnection;

	public static boolean isMyServiceRunning(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (ImgrAppService.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public Connection_Pool XMPP() {
		return this.xmppConnection;
	}

	public IBinder onBind(Intent paramIntent) {
		return this.binder;
	}

	Handler holdConnectionHandler = new Handler() {

		public void handleMessage(android.os.Message msg) {

			Presence.Type type = null;
			if (Connection_Pool.getInstance().getConnection(
					getApplicationContext()) != null) {
				try {
					final Roster roster = Connection_Pool.getInstance()
							.getConnection(getApplicationContext()).getRoster();

					Presence entryPresence = roster.getPresence(Connection_Pool
							.getInstance()
							.getConnection(getApplicationContext()).getUser());
					type = entryPresence.getType();
					Log.e("tag", "connection mode :" + roster + "," + type
							+ " , " + entryPresence);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (Connection_Pool.getInstance().getConnection(
					getApplicationContext()) == null
					|| !Connection_Pool.getInstance()
							.getConnection(getApplicationContext())
							.isConnected() || type == null) {
				final String user = AppSettings.getUser(ImgrAppService.this);
				final String pass = AppSettings
						.getPassword(ImgrAppService.this);
				final String username = AppSettings
						.getUserName(ImgrAppService.this);

				Log.e("tag", "login by hanlder service :" + user + " ,,,,,,, "
						+ pass + " ,,,,,, " + username);
				try {
					Connection_Pool.getInstance().login(user, pass, username);
				} catch (SaslException e) {
					e.printStackTrace();
				} catch (XMPPException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			holdConnectionHandler.sendEmptyMessageDelayed(0, 5000);
		};
	};

	public static int retrieveState_mode(Mode userMode, boolean isOnline) {
		int userState = 0;
		/** 0 for offline, 1 for online, 2 for away,3 for busy */
		if (userMode == Mode.dnd) {
			userState = 3;
		} else if (userMode == Mode.away || userMode == Mode.xa) {
			userState = 2;
		} else if (isOnline) {
			userState = 1;
		}
		return userState;
	}

	public void onCreate() {
		super.onCreate();
		this.xmppConnection = Connection_Pool.getInstance();
		holdConnectionHandler.sendEmptyMessage(0);
		// NotificationCompat.Builder builder = new NotificationCompat.Builder(
		// this);
		// builder.setContentText("imgr App").setContentTitle("imgr App")
		// .setSmallIcon(R.drawable.ic_launcher);
		// startForeground(100200, builder.build());

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				stopService(new Intent(ImgrAppService.this, FakeService.class));
			}
		}, 500);

		// startService(new Intent(FakeService.this, imgrAppService.class));

		if ((AppSettings.getUser(this) != null)
				&& (AppSettings.getPassword(this) != null)) {
			final String user = AppSettings.getUser(this);
			final String pass = AppSettings.getPassword(this);
			final String username = AppSettings
					.getUserName(ImgrAppService.this);

			new Thread(new Runnable() {
				public void run() {
					try {

						ImgrAppService.this.xmppConnection.login(user, pass,
								username);
					} catch (XMPPException e) {
						e.printStackTrace();
					} catch (SaslException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					ImgrAppService.this.sendBroadcast(new Intent(
							"Imgrapp.loggedin"));
					return;
				}
			}).start();
		}

	}

	public class LocalBinder extends Binder {
		public LocalBinder() {
		}

		public ImgrAppService getService() {
			return ImgrAppService.this;
		}
	}

}
