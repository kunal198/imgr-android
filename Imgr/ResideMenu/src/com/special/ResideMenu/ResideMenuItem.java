package com.special.ResideMenu;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * User: special Date: 13-12-10 Time: 下午11:05 Mail: specialcyci@gmail.com
 */
public class ResideMenuItem extends LinearLayout {

	/** menu item icon */
	private ImageView iv_icon;
	/** menu item title */
	public static TextView tv_title, tv_title1, tv_title2;
	public static int postion = 0;
	Context mContext;

	public ResideMenuItem(Context context) {
		super(context);
		initViews(context);
	}

	// public ResideMenuItem(Context context, int icon, int title, int pos) {
	// super(context);
	// initViews(context);
	// postion = pos;
	// iv_icon.setImageResource(icon);
	// tv_title.setText(title);
	// }

	public ResideMenuItem(Context context, int icon, String title, int pos) {
		super(context);

		postion = pos;

		if (postion == 1) {
			initViews(context);
			mContext = context;
			iv_icon.setImageResource(icon);
			Log.e("VERY FIRST TIME: ", "" + postion);
			postion = 1;

			tv_title.setText(title);
			// tv_title.setTypeface(null, Typeface.BOLD);

		} else if (postion == 2) {
			initViews1(context);
			mContext = context;
			iv_icon.setImageResource(icon);
			Log.e("VERY SECOND TIME: ", "" + postion);
			postion = 2;

			tv_title1.setText(title);

		} else if (postion == 3) {
			initViews2(context);
			mContext = context;
			iv_icon.setImageResource(icon);
			Log.e("VERY THIRD TIME: ", "" + postion);
			postion = 3;

			tv_title2.setText(title);

		}

	}

	public void Bold_Size(Context context, int style, int pos) {
		postion = pos;
		Log.e("start", "Outside: " + pos + "----------psition-------" + postion);
		if (postion == 1) {

			postion = 1;

			tv_title.setTypeface(null, Typeface.BOLD);
			tv_title1.setTypeface(null, Typeface.NORMAL);

			tv_title2.setTypeface(null, Typeface.NORMAL);

			Log.e("", "Inside: " + pos);
		} else if (postion == 2) {
			postion = 2;

			tv_title1.setTypeface(null, Typeface.BOLD);
			tv_title.setTypeface(null, Typeface.NORMAL);
			tv_title2.setTypeface(null, Typeface.NORMAL);

			Log.e("", "CALLING: " + pos);
		} else if (postion == 3) {
			postion = 3;

			tv_title2.setTypeface(null, Typeface.BOLD);
			tv_title.setTypeface(null, Typeface.NORMAL);

			tv_title1.setTypeface(null, Typeface.NORMAL);

			Log.e("", "CALLING: " + pos);
		}

	}

	private void initViews(Context context) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.residemenu_item, this);
		iv_icon = (ImageView) findViewById(R.id.iv_icon);
		tv_title = (TextView) findViewById(R.id.tv_title);

	}

	private void initViews1(Context context) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.residemenu_item1, this);
		iv_icon = (ImageView) findViewById(R.id.iv_icon);
		tv_title1 = (TextView) findViewById(R.id.tv_title);

	}

	private void initViews2(Context context) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.residemenu_item2, this);
		iv_icon = (ImageView) findViewById(R.id.iv_icon);
		tv_title2 = (TextView) findViewById(R.id.tv_title);

	}

	/**
	 * set the icon color;
	 * 
	 * @param icon
	 */
	public void setIcon(int icon) {
		iv_icon.setImageResource(icon);
	}

	/**
	 * set the title with resource ;
	 * 
	 * @param title
	 */
	public void setTitle(int title) {
		tv_title.setText(title);
		// tv_title1.setText(title);
		// tv_title2.setText(title);
	}

	/**
	 * set the title with string;
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		tv_title.setText(title);
		// tv_title1.setText(title);
		// tv_title2.setText(title);
	}

	/**
	 * set the title with string;
	 * 
	 * @param title
	 */
	public void setType(int title) {
		tv_title.setTypeface(null, title);

		// tv_title1.setText(title);
		// tv_title2.setText(title);
	}

}
